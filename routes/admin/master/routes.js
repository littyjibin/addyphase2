const express = require('express');

const { addPackageSize, getPackageSize } = require('../../../controllers/admin/master/packageSize');
const { addPickupTime, getPickupTime } = require('../../../controllers/admin/master/pickupTime');
const { addColor, getActiveColors } = require('../../../controllers/admin/master/color');

const { addCourierCompany, getCourierCompany,addCourierCompanyBoxRate } = require('../../../controllers/admin/courierCompany');
const { checkImageValidation } = require('../../../validations/admin/imageValidation');

const router = express.Router();

// Add package size details
router.post('/add-package-size', addPackageSize);
router.get('/get-package-sizes', getPackageSize);

// Add pick up time details
router.post('/add-pickup-time', addPickupTime);
router.get('/get-pickup-time', getPickupTime);


// Add courier company details
router.post('/add-courier-company',addCourierCompany);
router.get('/get-active-courier-companies', getCourierCompany);

// Add courier company box rate
router.post('/add-courier-company-box-rate',addCourierCompanyBoxRate);

// Add color details
router.post('/add-color', addColor);
router.get('/get-active-colors', getActiveColors);

module.exports = router;
