const express = require('express');


//const auth  = require('../../controllers/user/auth/auth');
const masterRoute = require('./master/routes');

const router = express.Router();

//User Protect
//router.use(auth);

router.use('/master', masterRoute);

module.exports = router;
