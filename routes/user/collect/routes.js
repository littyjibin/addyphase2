const express = require('express');
const auth = require('../../../controllers/user/auth/auth');
const { AddCollectDetails,getCollectUserByMobile,
    getCollectUserByUsername,getCollectOrderDetail } = require('../../../controllers/user/collect/collect');


const router = express.Router();



//Auth middleware
router.use(auth);
router.post('/get-collect-user-by-mobile', getCollectUserByMobile);
router.post('/get-collect-user-by-username', getCollectUserByUsername);
router.post('/add-collect-details', AddCollectDetails);
router.post('/get-collect-order-details', getCollectOrderDetail);
module.exports = router;
