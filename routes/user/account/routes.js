const express = require('express');
const { login, verifyOtp, resendOtp, updateUserInfo, addNewAddress, 
  getUserAddress, updateUserAddress,deleteUserAddress,verifyUserName,
  updateUserName,addNewAddressWeb,getUserAddressWeb,listAddressWeb,
  getRecentReciepient,updateUserProfile,updateUserSettings } = require('../../../controllers/user/account');
const auth = require('../../../controllers/user/auth/auth');

const router = express.Router();

router.post('/customer_login', login);
router.post('/verify_otp', verifyOtp);
router.post('/resend_otp', resendOtp);
router.get('/get-user-address-web/:id', getUserAddressWeb);
router.post("/list-address-web", listAddressWeb);
router.get('/get-recent-reciepient', getRecentReciepient);
router.post('/add-user-address-web', addNewAddressWeb);

//Auth middleware
router.use(auth);

router.post('/update_user_info', updateUserInfo);
router.post('/add-user-address', addNewAddress);
router.get('/get-user-address', getUserAddress);
router.post('/update-user-address', updateUserAddress);
router.delete('/delete-user-address', deleteUserAddress);
router.post('/verify_username', verifyUserName);
router.post('/update_username', updateUserName);
router.post('/update_user-profile', updateUserProfile);
router.post('/update_user-settings', updateUserSettings);



router.get('/hello', (req, res) => {
  res.send({ message: 'hello' });
});

module.exports = router;
