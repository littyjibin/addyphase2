const express = require('express');


const auth  = require('../../controllers/user/auth/auth');
const productRoute = require('./product/routes');
const accountRoute = require('./account/routes');
const collectRoute = require('./collect/routes');
const sendRoute = require('./send/routes');
const router = express.Router();



router.use('/account', accountRoute);
router.use('/send', sendRoute);
router.use('/collect', collectRoute);
//User Protect
router.use(auth);

router.use('/product', productRoute);

module.exports = router;
