const express = require('express');
const auth = require('../../../controllers/user/auth/auth');
const { getUserByMobile,addSendDetails, 
    getUserByUsername,checkLink,
    acceptPackageWeb,getCustomerOrders,getOrderDetail,addComplaint,acceptPackage,
    getUserDetail,searchCourierCompaniesByUser,listCourierCompaniesByUser,registerUserFromWeb } = require('../../../controllers/user/send/send');


const router = express.Router();


router.post("/check-link", checkLink);
router.put("/accept_package_web", acceptPackageWeb);
router.post('/get-user-details', getUserDetail);
router.post('/register-user-from-web', registerUserFromWeb);


//Auth middleware
router.use(auth);

router.post('/get-user-by-mobile', getUserByMobile);
router.post('/add-send-details', addSendDetails);
router.post('/get-user-by-username', getUserByUsername);
router.post('/check-link', checkLink);
router.put('/accept_package_web', acceptPackageWeb);
router.put('/accept_package', acceptPackage);
router.post('/get-customer-orders', getCustomerOrders);
router.post('/get-order-details', getOrderDetail);

router.post('/add-complaint', addComplaint);

// list courier company by name or phone number
router.post('/search-courier-companies-byUser', searchCourierCompaniesByUser);

// list courier company based on country
router.get('/list-courier-companies-byUser', listCourierCompaniesByUser);

module.exports = router;
