const express = require('express');
//const auth = require('../../controllers/user/auth/auth');

const {
  userGetAllActiveProductsBySellerId,
  userGetActiveProductById
} = require('../../../controllers/user/product/productDetails');


const {
  addProductToCart,
  getCartProducts,
  deleteCartItem,
  updateCartItem,
  updateCartInstruction
} = require('../../../controllers/user/product/cart');

const {  
  getOrderReview,
  placeOrder,
  getOrderDetailsByOrderId,
  rescheduleDeliveryDateByUser,
  cancelOrderByUser,
  raiseComplaintsAboutOrderByUser,
  returnOrderByUser
} = require('../../../controllers/user/product/order');

const router = express.Router();

//Auth middleware
//router.use(auth);


// -------------------  Product listing by seller  after sign in web-----------------------//

router.post('/user-get-all-active-products-bySellerId', userGetAllActiveProductsBySellerId);
router.post('/user-get-active-product-byId', userGetActiveProductById);

// -------------------  Cart ----------------------------------//
router.post('/add-product-to-cart', addProductToCart);
router.delete('/delete-cart-item/:id', deleteCartItem);
router.put('/update-cart-item', updateCartItem);
router.get('/get-cart-products/:sellerId', getCartProducts);
router.put('/update-cart-instruction', updateCartInstruction);

// -------------------  Order Placing--------------------------//
router.post('/get-order-review', getOrderReview);
router.post('/place-order', placeOrder);


// -------------------  Order Listing--------------------------//
router.get('/get-order-details-byOrderId/:id', getOrderDetailsByOrderId);
router.put('/reschedule-deliveryDate-byUser', rescheduleDeliveryDateByUser);
router.put('/cancel-order-byUser/:id', cancelOrderByUser);
router.post('/raise-complaints-about-order-byUser', raiseComplaintsAboutOrderByUser);
router.post('/return-order-byUser', returnOrderByUser);

module.exports = router;
