const express = require('express');
const {
  getOrdersbySellerId,
  getOrderbyId,
  changeOrderStatusById,
  sellerChangeOrderPaymentStatusById,
  confirmOrderbyId,
  reschedulepickupDateBySeller,
  getReturnOrdersBySeller,
  getReturnOrderbyId,
  changeReturnOrderStatusById
} = require('../../../controllers/seller/order');

const router = express.Router();

// Apis for seller based on order

// get all orders by seller id based on status
router.get('/get-orders-bySellerId/:status', getOrdersbySellerId);

// order status change
router.put('/change-order-status-byId', changeOrderStatusById);

//change customer order payment status by seller
router.put('/seller-change-order-payment-status-byId', sellerChangeOrderPaymentStatusById);

// get the order details by order id
router.get('/get-order-byId/:id', getOrderbyId);

// confirm the order id
router.post('/confirm-order-byId', confirmOrderbyId);

router.put('/reschedule-pickupDate-bySeller', reschedulepickupDateBySeller);


// get the return orders
router.get('/get-return-orders-bySeller', getReturnOrdersBySeller);

// get the return order details by  id
router.get('/get-return-order-byId/:id', getReturnOrderbyId);

// return order status change
router.put('/change-return-order-status-byId', changeReturnOrderStatusById);

module.exports = router;
