const express = require('express');
const { addSellerContact,getRecentAddyReciepient,getContactNotInAddy,getContactInAddy } = require('../../../controllers/seller/contact')

const router = express.Router();




router.post('/add-seller-contact', addSellerContact);
router.get('/get-recent-addy-user', getRecentAddyReciepient);
router.get('/get-contact-notin-addy/:page', getContactNotInAddy);
router.get('/get-contact-in-addy/:page', getContactInAddy);

module.exports = router;
