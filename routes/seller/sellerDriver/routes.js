const express = require('express');
const {
    sellerDriverLogin, 
    checkSellerDriverLink,
    getSellerDriverStatuses,
    getSellerDriverOrderDetails,
    sellerDriverStatusUpdate
} = require('../../../controllers/seller/sellerDriver');

const router = express.Router();

router.post('/seller-driver-login', sellerDriverLogin);
router.post('/seller-driver-check-link', checkSellerDriverLink);
router.get('/seller-driver-status-list/:orderId', getSellerDriverStatuses);
router.post('/seller-driver-order-details', getSellerDriverOrderDetails);
router.put('/seller-driver-status-update', sellerDriverStatusUpdate);

module.exports = router;
