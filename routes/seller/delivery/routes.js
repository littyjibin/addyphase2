const express = require('express');
const {
    sellerAddCourierCompany,
    sellerAddCourierPartner,
    searchCourierCompanies,
    getDeliveryMethodsBySeller,
    deleteDeliveryMethodBySeller,
    sellerInviteCourierCompany,
    sellerAddDriver,
    sellerEditDriver,
    setDefaultDeliveryMethodBySeller,
  } = require('../../../controllers/seller/registration');

const router = express.Router();


// Seller registration - add courier comapny - one api
//router.post('/seller-add-courier-partner', sellerAddCourierPartner);

//  add courier company
router.post('/seller-add-courier-company', sellerAddCourierCompany);

//  seller add driver
router.post('/seller-add-driver', sellerAddDriver);

//  seller edit driver
router.put('/seller-edit-driver', sellerEditDriver);

// invite courier company
router.post('/seller-invite-courier-company', sellerInviteCourierCompany);

// list courier company by name or phone number
router.post('/search-courier-companies', searchCourierCompanies);

// get delivery methods by seller
router.post('/get-delivery-methods-by-seller', getDeliveryMethodsBySeller);
router.delete('/delete-delivery-method-by-seller/:id', deleteDeliveryMethodBySeller);
router.put('/set-default-delivery-method-by-seller', setDefaultDeliveryMethodBySeller);


module.exports = router;
