const express = require('express');

const settingsRoute = require('./settings/routes');
const contactRoute = require('./contact/routes');
const orderRoute = require('./order/routes');
const sellerDriverRoute = require('./sellerDriver/routes');
const productRoute = require('./product/routes');
const deliveryRoute = require('./delivery/routes');
const customerRoute = require('./customer/routes');

const { addSellerCategory, getSellerCategories } = require('../../controllers/seller/category');
const {
  addSellerSellingType,
  addSellerBusinessDetails,
  addSellerPackageDetails,
  imageUpload,
  sellerSignup,
  updateSellerProfile,
  getSellerDetailsBySellerId,  
} = require('../../controllers/seller/registration');
const {
  addColorBySeller,
  getColors,
  getColorsBySeller,
  getSellerColorById,
  deleteSellerColorById,
  updateSellerColorById,
  getActiveColorsByAdmin,
  getActivePickupTimeByAdmin
} = require('../../controllers/seller/color');
const {
  addSellerProductCategory,
  getSellerProductCategories,
  deleteSellerProductCategory,
  getSellerProductCategoriesBySellerId,
  getActiveSellerProductCategoriesBySeller,
  changeSellerProductCategoryStatus
} = require('../../controllers/seller/productCategory');
const { addSellerPackage, getAllSellerPackages,getSellerPackages,getSellerPackageById } = require('../../controllers/seller/package');
const {
  addDriver,
  getDriverDetails,
  GetDriverByMobile,
  DriverLogin,
  getOrderDetails,
  driverStatusUpdate,
  getDriverstatuses,
  checkDriverLink,
  estimatedDeliveryFee,
} = require('../../controllers/seller/driver');
const {
  getAllActiveProductsBySellerId,
  getAllActiveProductsBySellerCategoryId,
  getActiveProductById,
  getSellerDetailsWebBySellerId,
  getSellerDetailsWebByStoreUrl,
  getFilterMethodsBySellerId,
  filterActiveProductBySellerId,
  getActiveSellerProductCategoriesWebBySellerId
} = require('../../controllers/seller/productWeb');
const { addUom, getActiveUom, addUomValue, getActiveUomValueByUomId } = require('../../controllers/seller/uom');
// const { checkProductImageValidation } = require('../../validations/seller/product/imageValidation');
const { checkImageValidation,checkImage } = require('../../validations/seller/imageValidation');

const auth = require('../../controllers/user/auth/auth');
const router = express.Router();

router.use('/sellerDriver', sellerDriverRoute);

// Add seller categories
router.post('/add-seller-category', addSellerCategory);

// package added by admin
router.post('/add-seller-package', addSellerPackage);
router.get('/get-all-seller-packages', getAllSellerPackages);
router.get('/get-seller-packages', getSellerPackages);
router.get('/get-seller-package_byId/:id', getSellerPackageById);

// Add uom
router.post('/add-uom', addUom);
router.get('/get-active-uom', getActiveUom);

// Add uom
router.post('/add-uom-value', addUomValue);
router.get('/get-active-uom-value-byUomId/:uomId', getActiveUomValueByUomId);

//driver
router.post('/add-driver', addDriver);
router.get('/get-driver', getDriverDetails);
router.post('/driver-order-details', getOrderDetails);
router.post('/get-driver-by-mobile', GetDriverByMobile);
router.post('/driver-login', DriverLogin);
router.post('/driver-status-update', driverStatusUpdate);
router.get('/driver-status-list/:orderId', getDriverstatuses);
router.post('/driver-check-link', checkDriverLink);
router.post('/driver-estimated-deliveryfee', estimatedDeliveryFee);

router.post('/driver-check-link', checkDriverLink);

//seller products ------------------------ web Apis
router.post('/get-all-active-products-bySellerId', getAllActiveProductsBySellerId);
router.post('/get-all-active-products-bySellerCategoryId', getAllActiveProductsBySellerCategoryId);
router.post('/get-active-product-byId', getActiveProductById);
router.post('/filter-active-product-bySellerId', filterActiveProductBySellerId);
router.post('/get-filter-methods-bySellerId', getFilterMethodsBySellerId);
router.post('/get-seller-details-web-bySellerId', getSellerDetailsWebBySellerId);
router.post('/get-seller-details-web-byStoreUrl', getSellerDetailsWebByStoreUrl);
router.get('/get-active-seller-product-category-web/:sellerId', getActiveSellerProductCategoriesWebBySellerId);


// Get the colors added by admin
router.get('/get-active-colors', getActiveColorsByAdmin);

// Get the pick up time added by admin
router.get('/get-active-pickupTime', getActivePickupTimeByAdmin);



//Auth middleware
router.use(auth);

// Add color -seller
router.post('/add-color-bySeller', addColorBySeller);
router.get('/get-all-colors', getColors);
router.get('/get-colors-bySeller', getColorsBySeller);
router.get('/get-seller-color-byId/:id', getSellerColorById);
router.delete('/delete-seller-color-byId/:id', deleteSellerColorById);
router.put('/update-seller-color-byId', updateSellerColorById);


router.get('/get-seller-categories', getSellerCategories);

// Seller registration
// router.post('/add-seller-selling-type', addSellerSellingType);
// router.post('/add-seller-business-details', addSellerBusinessDetails);
// router.post('/add-seller-package-details', addSellerPackageDetails);

// seller product categories
router.post('/add-seller-product-category', addSellerProductCategory);
router.get('/get-seller-product-category', getSellerProductCategories);
router.delete('/delete-seller-product-category', deleteSellerProductCategory);
router.get('/get-seller-product-category-bySellerId', getSellerProductCategoriesBySellerId);
router.put('/change-seller-product-category-status/:id', changeSellerProductCategoryStatus);
router.get('/get-active-seller-product-category-bySeller', getActiveSellerProductCategoriesBySeller);

// --------------------Seller registration -Apis
router.post('/seller-signup', checkImage, sellerSignup);
router.post('/seller-profile-update', updateSellerProfile);
router.post('/get-seller-details-bySellerId', getSellerDetailsBySellerId);

// common image upload
router.post('/image-upload', checkImageValidation, imageUpload);

// Route settings-----------
router.use('/settings', settingsRoute);
router.use('/contact', contactRoute);
router.use('/order', orderRoute);
router.use('/product', productRoute);
router.use('/delivery', deliveryRoute);
router.use('/customer', customerRoute);

module.exports = router;
