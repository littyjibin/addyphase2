const express = require('express');
const {
  getCustomersBySeller,
  getCustomerOrderById,
  getCustomerOrderbyUserId
} = require('../../../controllers/seller/customer');

const router = express.Router();

//Apis for seller - orde based on customer

// get customers by seller with order details
router.get('/get-customers-bySeller', getCustomersBySeller);

// get order by orderid in customer
router.get('/get-customer-order-byId/:id', getCustomerOrderById);


// get orders by customer with status
router.get('/get-customer-order-byUserId/:userId', getCustomerOrderbyUserId);


module.exports = router;
