const express = require('express');
const { updateNotification, getSettings
    , addOrUpdatePrivacy,
    addOrUpdateBusinessDetails,
    adSellerTaxNumber,
    addSellerRegNumber,
    addSellerReturnDays,
    addSellerBankDetails,
    updateSellerBankDetails,
    addOrUpdatePayment,
    addSellerVat,
    getSettingsStoreDetails,
    getSettingsNotification,getSettingsBusinessDetails,getSettingsPayment,getSettingsPrivacy } = require('../../../controllers/seller/settings')

const router = express.Router();




router.post('/add-seller-notification-settings', updateNotification);
router.get('/get-seller-settings', getSettings);
router.get('/get-seller-settings-notification', getSettingsNotification);
router.get('/get-seller-settings-business-details', getSettingsBusinessDetails);
router.get('/get-seller-settings-store-details', getSettingsStoreDetails);
router.get('/get-seller-settings-privacy', getSettingsPrivacy);
router.get('/get-seller-settings-payment-details', getSettingsPayment);
router.post('/add-seller-privacy-settings', addOrUpdatePrivacy);
router.post('/add-seller-business-details-settings', addOrUpdateBusinessDetails);
router.post('/add-seller-tax-number', adSellerTaxNumber);
router.post('/add-seller-reg-number', addSellerRegNumber);
router.post('/add-seller-return-days', addSellerReturnDays);
router.post('/add-seller-bank-details', addSellerBankDetails);
router.post('/add-seller-vat', addSellerVat);
router.post('/update-seller-bank-details', updateSellerBankDetails);
router.post('/update-seller-payment-details', addOrUpdatePayment);



module.exports = router;
