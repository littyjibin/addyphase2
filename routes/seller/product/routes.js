const express = require('express');

const { checkProductImageValidation } = require('../../../validations/seller/product/imageValidation');

const {
    addSellerProduct,
    addSellerMultipleProducts,
    getSellerProductById,
    updateSellerProduct,
    addSellerProductImage,
    getAllProductsBySeller,
    changeSellerProductStatus,
    deleteSellerProductById,
    addSellerInstagramImageDetails
  } = require('../../../controllers/seller/product');

const router = express.Router();


router.post('/add-seller-product', addSellerProduct);
router.post('/add-seller-multiple-products', addSellerMultipleProducts);
router.get('/get-seller-product-byId/:id', getSellerProductById);
router.put('/update-seller-product', updateSellerProduct);
router.post('/add-seller-product-image', checkProductImageValidation, addSellerProductImage);
router.put('/change-seller-product-status/:id', changeSellerProductStatus);
router.get('/get-all-products-bySeller', getAllProductsBySeller);
router.delete('/delete-seller-product-byId/:id', deleteSellerProductById);


// APis for images from instagram
router.post('/add-seller-instagram-image-details', addSellerInstagramImageDetails);



module.exports = router;
