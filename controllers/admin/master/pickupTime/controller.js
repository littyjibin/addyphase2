const pickupTime = require('../../../../model/admin/master/pickupTime');
const { validatePickupTime } = require('../../../../validations/admin/master/validation');

const doAddPickupTime = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {

      //validate incoming data
      const dataValidation = await validatePickupTime(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      //check if time already exist
      console.log("time", data.fromTime)
      let isTimeExist = await pickupTime.findOne({ fromTime:data.fromTime,toTime:data.toTime  });
      if (isTimeExist) {
        return resolve({ statusCode: 200, status: false, message: 'Time already exist.' });
      }
   
      //save to db
      let schemaObj = new pickupTime(data);
      schemaObj
        .save()
        .then(() => {
          return resolve({ statusCode: 200, status: true, message: 'Pickup time added successfully.' });
        })
        .catch(async (error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};

// get active colours
const doGetPickupTime = () => {
  return new Promise(async (resolve, reject) => {
    try {
      //get from db
      let pickupTimes = await pickupTime.find({ isActive: true }, {}).sort({ _id: -1 });
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { pickupTimes } });
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  doAddPickupTime,
  doGetPickupTime,
};
