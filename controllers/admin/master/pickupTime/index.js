const { doAddPickupTime, doGetPickupTime } = require('./controller');

const addPickupTime = (req, res, next) => {
  doAddPickupTime(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getPickupTime = (req, res, next) => {
  doGetPickupTime()
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

module.exports = {
  addPickupTime,
  getPickupTime,
};
