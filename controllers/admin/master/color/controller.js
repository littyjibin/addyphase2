const color = require('../../../../model/admin/master/colors');


const doAddColor = (data) => {
  return new Promise(async (resolve, reject) => {
    try {

      const { colorCode, colorName } = data;
    
      //check if color code already exist
      let isColorCodeExist = await color.findOne({ colorCode});
      if (isColorCodeExist) {
        return resolve({ statusCode: 200, status: false, message: 'Color code already exist.' });
      }

      //check if color name already exist
      let isColorNameExist = await color.findOne({ colorName });
      if (isColorNameExist) {
        return resolve({ statusCode: 200, status: false, message: 'Color name already exist.' });
      }

      //save to db
      let schemaObj = new color({ colorCode, colorName });
      schemaObj
        .save()
        .then(() => {
          return resolve({ statusCode: 200, status: true, message: 'Color added successfully.' });
        })
        .catch(async (error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};

// get active colours
const doGetActiveColors = () => {
  return new Promise(async (resolve, reject) => {
    try {
      //get from db
      let colors = await color
        .find({ isActive: true }, { colorCode: 1, colorName: 1 })
        .sort({ _id: -1 })
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { colors } });
    } catch (error) {
      reject(error);
    }
  });
};


module.exports = {
    doAddColor,
    doGetActiveColors,
 
};
