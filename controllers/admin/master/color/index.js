const { doAddColor, doGetActiveColors } = require('./controller');

const addColor = (req, res, next) => {
    doAddColor(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getActiveColors = (req, res, next) => {
    doGetActiveColors()
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

module.exports = {
    addColor,
    getActiveColors,
};
