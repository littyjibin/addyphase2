const packagesize = require('../../../../model/admin/master/packageSize');
const { validatePackageSize } = require('../../../../validations/admin/master/validation');

const doAddPackageSize = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {

      //validate incoming data
      const dataValidation = await validatePackageSize(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      //check if size already exist
      let isSizeExist = await packagesize.findOne({ size:data.size });
      if (isSizeExist) {
        return resolve({ statusCode: 400, status: false, message: 'Size already exist.' });
      }


      //save to db
      let schemaObj = new packagesize(data);
      schemaObj
        .save()
        .then(() => {
          return resolve({ statusCode: 200, status: true, message: 'Package size added successfully.' });
        })
        .catch(async (error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};

// get active colours
const doGetPackageSize = () => {
  return new Promise(async (resolve, reject) => {
    try {
      //get from db
      let packagesizes = await packagesize.find({ isActive: true }, { size: 1,height:1,width:1,length:1,weight:1}).sort({ _id: -1 });
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { packagesizes } });
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  doAddPackageSize,
  doGetPackageSize,
};
