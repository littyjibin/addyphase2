const { doAddCourierCompany, doGetCourierCompany,doAddCourierCompanyBoxRate } = require('./controller');

const addCourierCompany = (req, res, next) => {
    doAddCourierCompany(req.body,req?.files?.image)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getCourierCompany = (req, res, next) => {
    doGetCourierCompany()
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};



const addCourierCompanyBoxRate = (req, res, next) => {
  doAddCourierCompanyBoxRate(req.body)
  .then(({ statusCode = 200, message, status }) => {
    res.status(statusCode).json({
      message,
      status,
    });
  })
  .catch((error) => {
    next(error);
  });
};


module.exports = {
    addCourierCompany,
    getCourierCompany,
    addCourierCompanyBoxRate
};
