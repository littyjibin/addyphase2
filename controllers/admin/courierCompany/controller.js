const courierCompany = require('../../../model/admin/courierCompany');
const packagesize = require('../../../model/admin/master/packageSize');
const courierCoampnyBoxRate = require('../../../model/admin/courierCompanyBoxRate');

const {
  validateCourierCompany,
  validateCourierCompanyBoxRate,
} = require('../../../validations/admin/courierComapany/validation');
const { uploadImage } = require('../../../utils/imageUpload');

const doAddCourierCompany = (data, logo) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateCourierCompany(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      //check if image is available
      // if (!logo) {
      //   return resolve({ statusCode: 400, status: false, message: 'Logo not found.' });
      // }

      //check if company already exist
      let isCompanysExist = await courierCompany.findOne({ companyName: data.companyName });
      if (isCompanysExist) {
        return resolve({ statusCode: 200, status: false, message: 'Company name already exist.' });
      }

      //check if company mobile exist
      let isMobileExist = await courierCompany.findOne({ mobile: data.mobile });
      if (isMobileExist) {
        return resolve({ statusCode: 200, status: false, message: 'Mobile number already exist.' });
      }

      //Upload logo image
      // let logoImageFileName = `courier/logo/${new Date().getTime()}_${logo.name}`;
      // await uploadImage(logo.data, logoImageFileName).then(({ url }) => {
      //   data.logo = url;
      // });

      let existingCourierCompany = await courierCompany.findOne({}).sort({ _id: -1 });
      if (!existingCourierCompany) {
        data.companyId = 1;
      } else {
        let lastInsertedCompanyId = existingCourierCompany.companyId;
        data.companyId = parseInt(lastInsertedCompanyId) + 1;
      }

      for (let items of data.boxRate){
        items.total = items.rate+ items.driverCommission
      }
      //save to db
      let schemaObj = new courierCompany(data);
      schemaObj
        .save()
        .then(() => {
          return resolve({ statusCode: 200, status: true, message: 'Courier Company added successfully.' });
        })
        .catch(async (error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};

// get active courier coampanies
const doGetCourierCompany = () => {
  return new Promise(async (resolve, reject) => {
    try {
      //get from db
      let courierCompanies = await courierCompany
        .find({ isActive: true }, { __v: 0, createdAt: 0, updatedAt: 0 })
        .sort({ _id: -1 });
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { courierCompanies } });
    } catch (error) {
      reject(error);
    }
  });
};

const doAddCourierCompanyBoxRate = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateCourierCompanyBoxRate(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      const { companyId } = data;
      let rate = parseInt(data.rate);
      let driverCommission = parseInt(data.driverCommission);

      //check if company already exist
      let isCompanyExist = await courierCompany.findOne({ _id: companyId });
      if (!isCompanyExist) {
        return resolve({ statusCode: 200, status: false, message: 'Company does not exist' });
      }

      //  checking box size exist or not
      let isBoxSizeExist = await packagesize.findOne({
        _id: data.boxSize,
        isActive: true,
      });
      if (!isBoxSizeExist) return resolve({ statusCode: 200, status: false, message: 'Invalid box size' });
     
     
      //  checking box size already added 
      let isBoxRateExist = await courierCoampnyBoxRate.findOne({ from: data.from ,upto:data.upto,companyId:data.companyId});
      if (isBoxRateExist) {
        return resolve({ statusCode: 200, status: false, message: 'Box rate already added' });
      }

      data.total = parseInt(rate + driverCommission);
      //save to db
      let schemaObj = new courierCoampnyBoxRate(data);
      schemaObj
        .save()
        .then(() => {
          return resolve({ statusCode: 200, status: true, message: 'Courier Company box rate added successfully.' });
        })
        .catch(async (error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  doAddCourierCompany,
  doGetCourierCompany,
  doAddCourierCompanyBoxRate,
};
