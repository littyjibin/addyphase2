const { doAddOrUpdateNotification, doGetSettings, doAddOrUpdatePrivacy, doAddOrUpdateBusinessDetails,
  doAddSellerTaxNumber, doAddSellerRegNumber, doAddSellerReturnDays,
  doAddSellerBankDetails, doAddSellerVat, doUpdateSellerBankDetails, doAddOrUpdatePayment, doGetSettingsNotification,doGetSettingsPrivacy,doGetSettingsPayment,doGetSettingsBusinessDetails,
  doGetSettingsStoreDetails
 } = require('./controller');


const updateNotification = (req, res, next) => {
  doAddOrUpdateNotification(req.body, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        data
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getSettings = (req, res, next) => {
  doGetSettings(req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getSettingsStoreDetails = (req, res, next) => {
  doGetSettingsStoreDetails(req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
const getSettingsNotification = (req, res, next) => {
  doGetSettingsNotification(req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
const getSettingsPrivacy = (req, res, next) => {
  doGetSettingsPrivacy(req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
const getSettingsPayment = (req, res, next) => {
  doGetSettingsPayment(req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
const getSettingsBusinessDetails = (req, res, next) => {
  doGetSettingsBusinessDetails(req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};



const addOrUpdatePrivacy = (req, res, next) => {
  doAddOrUpdatePrivacy(req.body, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        data
      });
    })
    .catch((error) => {
      next(error);
    });
};

const addOrUpdateBusinessDetails = (req, res, next) => {
  doAddOrUpdateBusinessDetails(req.body, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        data
      });
    })
    .catch((error) => {
      next(error);
    });
};

const adSellerTaxNumber = (req, res, next) => {
  doAddSellerTaxNumber(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const addSellerRegNumber = (req, res, next) => {
  doAddSellerRegNumber(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const addSellerReturnDays = (req, res, next) => {
  doAddSellerReturnDays(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const addSellerBankDetails = (req, res, next) => {
  doAddSellerBankDetails(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const addSellerVat = (req, res, next) => {
  doAddSellerVat(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const updateSellerBankDetails = (req, res, next) => {
  doUpdateSellerBankDetails(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const addOrUpdatePayment = (req, res, next) => {
  doAddOrUpdatePayment(req.body, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        data
      });
    })
    .catch((error) => {
      next(error);
    });
};

module.exports = {
  updateNotification, getSettings, addOrUpdatePrivacy,
  addOrUpdateBusinessDetails, adSellerTaxNumber, addSellerRegNumber,
  addSellerReturnDays, addSellerBankDetails, addSellerVat, updateSellerBankDetails, addOrUpdatePayment, getSettingsNotification,getSettingsBusinessDetails,getSettingsPayment,getSettingsPrivacy,getSettingsStoreDetails
};