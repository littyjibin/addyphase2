const settings = require('../../../model/seller/settings/settings');
const sellerDetails = require('../../../model/seller/registration/sellerDetails');
const { validateSellerNotificationSettings, validateSettings,
    validateSellerPrivacySettings, validateSellerBusinessDetailsSettings,
    validateSellerTaxNumber, validateSellerRegNumber, validateSellerReturnDays,
    validateSellerBankDetails, validateSellerVat, validateEditBankDetails,
    validateSellerPaymentSettings } = require('../../../validations/seller/settings/settings');
const mongoose = require('mongoose');

const doGetSettings = (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            let notification = await settings.find({ sellerId: data._id }, {})
            return resolve({ statusCode: 200, status: true, message: 'Success', data: notification });
        } catch (error) {
            reject(error);
        }
    });
};

const doGetSettingsNotification = (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            let notification = await settings.find({ sellerId: data._id }, {notifications:1,_id:1})
            let sendData = {"_id":notification[0]._id,"notifications":notification[0].notifications[0]}
            return resolve({ statusCode: 200, status: true, message: 'Success', data: sendData });
        } catch (error) {
            reject(error);
        }
    });
};

const doGetSettingsPrivacy = (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            let privacyDetails = await settings.find({ sellerId: data._id }, {privacy:1,_id:1})
            let sendData = {"_id":privacyDetails[0]._id,"privacy":privacyDetails[0].privacy[0]}
            return resolve({ statusCode: 200, status: true, message: 'Success', data: sendData });
        } catch (error) {
            reject(error);
        }
    });
};
const doGetSettingsPayment = (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            let paymentDetails = await settings.find({ sellerId: data._id }, {payment:1,_id:1})
            let bankDetails = await sellerDetails.find({ sellerId: data._id }, {bankDetails:1,vat:1,_id:0})
            console.log("ttttttt", bankDetails)
            let sendData = {"_id":paymentDetails[0]._id,vat:bankDetails[0].vat,bankDetails:bankDetails[0].bankDetails,"payment":paymentDetails[0].payment[0]}
            return resolve({ statusCode: 200, status: true, message: 'Success', data: sendData });
        } catch (error) {
            reject(error);
        }
    });
};
const doGetSettingsBusinessDetails = (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            let businessDetail = await settings.find({ sellerId: data._id }, {businessDetails:1,_id:1})
            let businessregDetail = await sellerDetails.find({ sellerId: data._id }, {commercialRegNum:1,taxNumber:1,_id:0})
            let sendData = {"_id":businessDetail[0]._id,"commercialRegNum":businessregDetail[0].commercialRegNum,"taxNumber":businessregDetail[0].taxNumber,"businessDetails":businessDetail[0].businessDetails[0]}
            return resolve({ statusCode: 200, status: true, message: 'Success', data: sendData });
        } catch (error) {
            reject(error);
        }
    });
};


const doGetSettingsStoreDetails = (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            let storeDetail = await sellerDetails.find({ sellerId: data._id }, {returnPolicyDays:1,_id:1})
            return resolve({ statusCode: 200, status: true, message: 'Success', data: storeDetail });
        } catch (error) {
            reject(error);
        }
    });
};
const doAddOrUpdateNotification = (data, user) => {
    return new Promise(async (resolve, reject) => {
        try {
            const dataValidation = await validateSellerNotificationSettings(data);
            if (dataValidation.error) {
                const message = dataValidation.error.details[0].message.replace(/"/g, '');
                return resolve({
                    status: false,
                    message: message,
                    statusCode: 400,
                });
            }
            let insertData = {};
            insertData.sellerId = user._id;
            var settingsObj = [{
                callForeOutForDelivery: data.callForeOutForDelivery,
                callForMarkForDelivery: data.callForMarkForDelivery,
                pushNotifyForeOutForDelivery: data.pushNotifyForeOutForDelivery,
                pushNotifyForMarkForDelivery: data.pushNotifyForMarkForDelivery,
                whatsappNotifyForeOutForDelivery: data.whatsappNotifyForeOutForDelivery,
                whatsappNotifyForMarkForDelivery: data.whatsappNotifyForMarkForDelivery
            }]
            insertData.notifications = settingsObj;
            let notificationDetails = await settings.findOne({ sellerId: user._id });
            if (!notificationDetails) {
                let schemaObj = new settings(insertData);
                schemaObj
                    .save()
                    .then(() => {
                        return resolve({ statusCode: 200, status: true, message: 'Notification settings added successfully.' });
                    })
                    .catch((error) => {
                        return resolve({ statusCode: 400, status: false, message: error });
                    });
            } else if (!notificationDetails.notifications.length) {
                settings.updateOne(
                    { sellerId: mongoose.Types.ObjectId(user._id) },
                    {
                        "$push": {
                            "notifications": {
                                "$each": settingsObj
                            }
                        }
                    }
                ).then((response) => {
                    return resolve({
                        status: true,
                        message: 'Notification settings added successfully'
                    });
                });

            } else {
               
                settings.updateOne(
                    { "notifications._id": mongoose.Types.ObjectId(notificationDetails.notifications[0]._id) },
                    {
                        $set: {
                            "notifications.$.callForeOutForDelivery": data.callForeOutForDelivery,
                            "notifications.$.callForMarkForDelivery": data.callForMarkForDelivery,
                            "notifications.$.pushNotifyForeOutForDelivery": data.pushNotifyForeOutForDelivery,
                            "notifications.$.pushNotifyForMarkForDelivery": data.pushNotifyForMarkForDelivery,
                            "notifications.$.whatsappNotifyForeOutForDelivery": data.whatsappNotifyForeOutForDelivery,
                            "notifications.$.whatsappNotifyForMarkForDelivery": data.whatsappNotifyForMarkForDelivery
                        },
                    }
                ).then((response) => {
                    return resolve({
                        status: true,
                        message: 'Notification settings updated',
                    });
                });
            }
        } catch (error) {
            reject(error);
        }
    });
}

const doAddOrUpdatePrivacy = (data, user) => {
    return new Promise(async (resolve, reject) => {
        try {
            const dataValidation = await validateSellerPrivacySettings(data);
            if (dataValidation.error) {
                const message = dataValidation.error.details[0].message.replace(/"/g, '');
                return resolve({
                    status: false,
                    message: message,
                    statusCode: 400,
                });
            }
            let insertData = {};
            insertData.sellerId = user._id;
            var settingsObj = [{
                displayMobileNumber: data.displayMobileNumber,
                displayWhatsappNumber: data.displayWhatsappNumber
            }]
            insertData.privacy = settingsObj;
            let privacyDetails = await settings.findOne({ sellerId: user._id });

            if (!privacyDetails) {
                let schemaObj = new settings(insertData);
                schemaObj
                    .save()
                    .then(() => {
                        return resolve({ statusCode: 200, status: true, message: 'Privacy settings added successfully.' });
                    })
                    .catch((error) => {
                        return resolve({ statusCode: 400, status: false, message: error });
                    });
            } else if (!privacyDetails.privacy.length) {
                settings.updateOne(
                    { sellerId: mongoose.Types.ObjectId(user._id) },
                    {
                        "$push": {
                            "privacy": {
                                "$each": settingsObj
                            }
                        }
                    }
                ).then((response) => {
                    return resolve({
                        status: true,
                        message: 'privacy settings added successfully'
                    });
                });

            } else {
               
                settings.updateOne(
                    { "privacy._id": mongoose.Types.ObjectId(privacyDetails.privacy[0]._id) },
                    {
                        $set: {
                            "privacy.$.displayMobileNumber": data.displayMobileNumber,
                            "privacy.$.displayWhatsappNumber": data.displayWhatsappNumber
                        },
                    }
                ).then((response) => {
                    return resolve({
                        status: true,
                        message: 'privacy settings updated',
                    });
                });
            }
        } catch (error) {
            reject(error);
        }
    });
}

const doAddOrUpdateBusinessDetails = (data, user) => {
    return new Promise(async (resolve, reject) => {
        try {
            const dataValidation = await validateSellerBusinessDetailsSettings(data);
            if (dataValidation.error) {
                const message = dataValidation.error.details[0].message.replace(/"/g, '');
                return resolve({
                    status: false,
                    message: message,
                    statusCode: 400,
                });
            }
            let insertData = {};
            insertData.sellerId = user._id;
            var settingsObj = [{
                displayComRegNumber: data.displayComRegNumber,
                displayTaxNumber: data.displayTaxNumber
            }]
            insertData.businessDetails = settingsObj;
            let businessDetails = await settings.findOne({ sellerId: user._id });

            if (!businessDetails) {
                let schemaObj = new settings(insertData);
                schemaObj
                    .save()
                    .then(() => {
                        return resolve({ statusCode: 200, status: true, message: 'Business details settings added successfully.'});
                    })
                    .catch((error) => {
                        return resolve({ statusCode: 400, status: false, message: error });
                    });
            } else if (!businessDetails.businessDetails.length) {
                settings.updateOne(
                    { sellerId: mongoose.Types.ObjectId(user._id) },
                    {
                        "$push": {
                            "businessDetails": {
                                "$each": settingsObj
                            }
                        }
                    }
                ).then((response) => {
                    return resolve({
                        status: true,
                        message: 'businessDetails settings added successfully'
                    });
                });

            } else {
                settings.updateOne(
                    { "businessDetails._id": mongoose.Types.ObjectId(businessDetails.businessDetails[0]._id) },
                    {
                        $set: {
                            "businessDetails.$.displayComRegNumber": data.displayComRegNumber,
                            "businessDetails.$.displayTaxNumber": data.displayTaxNumber
                        },
                    }
                ).then((response) => {
                    return resolve({
                        status: true,
                        message: 'business details settings updated',
                    });
                });
            }
        } catch (error) {
            reject(error);
        }
    });
}

const doAddSellerTaxNumber = (data, user) => {
    return new Promise(async (resolve, reject) => {
        try {
            const dataValidation = await validateSellerTaxNumber(data);
            if (dataValidation.error) {
                const message = dataValidation.error.details[0].message.replace(/"/g, '');
                return resolve({
                    status: false,
                    message: message,
                    statusCode: 400,
                });
            }
            const isSellerExist = await sellerDetails.findOne({
                sellerId: user._id,
            });

            if (!isSellerExist) {
                return resolve({ statusCode: 400, status: false, message: 'Seller not found' });
            }

            data.sellerId = user._id;
            await sellerDetails
                .updateOne({ sellerId: user._id }, data)
                .then(() => {
                    return resolve({ statusCode: 200, status: true, message: 'Tax number added successfully.' });
                })
                .catch((error) => {
                    return resolve({ statusCode: 400, status: false, message: error });
                });
        } catch (error) {
            reject(error);
        }
    });
};

const doAddSellerRegNumber = (data, user) => {
    return new Promise(async (resolve, reject) => {
        try {
            const dataValidation = await validateSellerRegNumber(data);
            if (dataValidation.error) {
                const message = dataValidation.error.details[0].message.replace(/"/g, '');
                return resolve({
                    status: false,
                    message: message,
                    statusCode: 400,
                });
            }
            const isSellerExist = await sellerDetails.findOne({
                sellerId: user._id,
            });

            if (!isSellerExist) {
                return resolve({ statusCode: 400, status: false, message: 'Seller not found' });
            }

            data.sellerId = user._id;
            await sellerDetails
                .updateOne({ sellerId: data.userId }, data)
                .then(() => {
                    return resolve({ statusCode: 200, status: true, message: 'commercial Registration Number added successfully.' });
                })
                .catch((error) => {
                    return resolve({ statusCode: 400, status: false, message: error });
                });
        } catch (error) {
            reject(error);
        }
    });
};

const doAddSellerReturnDays = (data, user) => {
    return new Promise(async (resolve, reject) => {
        try {
            const dataValidation = await validateSellerReturnDays(data);
            if (dataValidation.error) {
                const message = dataValidation.error.details[0].message.replace(/"/g, '');
                return resolve({
                    status: false,
                    message: message,
                    statusCode: 400,
                });
            }
            const isSellerExist = await sellerDetails.findOne({
                sellerId: user._id,
            });

            if (!isSellerExist) {
                return resolve({ statusCode: 400, status: false, message: 'Seller not found' });
            }

            data.sellerId = user._id;
            await sellerDetails
                .updateOne({ sellerId: user._id }, data)
                .then(() => {
                    return resolve({ statusCode: 200, status: true, message: 'Return days added successfully.' });
                })
                .catch((error) => {
                    return resolve({ statusCode: 400, status: false, message: error });
                });
        } catch (error) {
            reject(error);
        }
    });
};

const doAddSellerBankDetails = (data, user) => {
    return new Promise(async (resolve, reject) => {
        try {
            const dataValidation = await validateSellerBankDetails(data);
            if (dataValidation.error) {
                const message = dataValidation.error.details[0].message.replace(/"/g, '');
                return resolve({
                    status: false,
                    message: message,
                    statusCode: 400,
                });
            }
            var bankDetails = [{
                bankName: data.bankName,
                accountNumber: data.accountNumber,
                IBAN: data.IBAN,
                bankAddress: data.bankAddress
            }]
            let sellerDetail = await sellerDetails.findOne({ sellerId: user._id });
            if (sellerDetail != null) {
                let existingBankDetails = false;
                if (sellerDetail && sellerDetail.bankDetails) {
                    sellerDetail.bankDetails.forEach((e, i) => {
                        if (
                            e.bankName == data.bankName &&
                            e.accountNumber == data.accountNumber &&
                            e.IBAN == data.IBAN &&
                            e.bankAddress == data.bankAddress
                        ) {
                            existingBankDetails = true;
                        }
                    });
                }
                if (existingBankDetails) {
                    return resolve({ statusCode: 400, status: false, message: 'Existing Bank details.' });
                } else {
                    sellerDetails.updateOne(
                        { sellerId: user._id },
                        {
                            "$push": {
                                "bankDetails": {
                                    "$each": bankDetails
                                }
                            }
                        }
                    ).then((response) => {
                        return resolve({
                            status: true,
                            message: 'Bank details added successfully',
                        });
                    });
                }
            } else {
                return resolve({
                    status: false,
                    message: 'No seller found',
                });
            }
        } catch (error) {
            reject(error);
        }
    });
}

const doAddSellerVat = (data, user) => {
    return new Promise(async (resolve, reject) => {
        try {
            const dataValidation = await validateSellerVat(data);
            if (dataValidation.error) {
                const message = dataValidation.error.details[0].message.replace(/"/g, '');
                return resolve({
                    status: false,
                    message: message,
                    statusCode: 400,
                });
            }
            const isSellerExist = await sellerDetails.findOne({
                sellerId: user._id,
            });

            if (!isSellerExist) {
                return resolve({ statusCode: 400, status: false, message: 'Seller not found' });
            }
            data.sellerId = user._id;
            await sellerDetails
                .updateOne({ sellerId: user._id }, data)
                .then(() => {
                    return resolve({ statusCode: 200, status: true, message: 'Vat added successfully.' });
                })
                .catch((error) => {
                    return resolve({ statusCode: 400, status: false, message: error });
                });
        } catch (error) {
            reject(error);
        }
    });
};

const doUpdateSellerBankDetails = (data, user) => {
    return new Promise(async (resolve, reject) => {
        try {
            //validate incoming data
            const dataValidation = await validateEditBankDetails(data);
            if (dataValidation.error) {
                const message = dataValidation.error.details[0].message.replace(/"/g, '');
                return resolve({
                    status: false,
                    message: message,
                    statusCode: 400,
                });
            }
            let sellerDetail = await sellerDetails.findOne({ sellerId: user._id });
            let existingBankDetails = false;
            if (sellerDetail != null) {
                if (sellerDetail && sellerDetail.bankDetails) {
                    sellerDetail.bankDetails.forEach((e, i) => {
                        if (
                            e.bankName == data.bankName &&
                            e.accountNumber == data.accountNumber &&
                            e.IBAN == data.IBAN &&
                            e.bankAddress == data.bankAddress
                        ) {
                            existingBankDetails = true;
                        }
                    });
                }
                if (existingBankDetails) {
                    return resolve({ statusCode: 400, status: false, message: 'Existing Bank details.' });
                } else {
                    sellerDetails.updateOne(
                        { sellerId: user._id, "bankDetails._id": mongoose.Types.ObjectId(data.id) },
                        {
                            $set: {
                                "bankDetails.$.bankName": data.bankName,
                                "bankDetails.$.accountNumber": data.accountNumber,
                                "bankDetails.$.IBAN": data.IBAN,
                                "bankDetails.$.bankAddress": data.bankAddress
                            },
                        }
                    ).then((response) => {
                        return resolve({
                            status: true,
                            message: 'Bank details updated successfully',
                        });
                    });
                }
            } else {
                return resolve({
                    status: false,
                    message: 'No seller found',
                });
            }
        } catch (error) {
            reject(error);
        }
    });
};

const doAddOrUpdatePayment = (data, user) => {
    return new Promise(async (resolve, reject) => {
        try {
            const dataValidation = await validateSellerPaymentSettings(data);
            if (dataValidation.error) {
                const message = dataValidation.error.details[0].message.replace(/"/g, '');
                return resolve({
                    status: false,
                    message: message,
                    statusCode: 400,
                });
            }
            let insertData = {};
            insertData.sellerId = user._id;
            var settingsObj = [{
                cashOnDelivery: data.cashOnDelivery,
                onlinePayment: data.onlinePayment,
                priceIncludeVat: data.priceIncludeVat,
                enableVat: data.enableVat,
                priceIncludeDeliveryFee: data.priceIncludeDeliveryFee
            }]
            insertData.payment = settingsObj;
            let paymentDetailsDetails = await settings.findOne({ sellerId: user._id });
            if (!paymentDetailsDetails) {
                let schemaObj = new settings(insertData);
                schemaObj
                    .save()
                    .then(() => {
                        return resolve({ statusCode: 200, status: true, message: 'Payment settings added successfully.'});
                    })
                    .catch((error) => {
                        return resolve({ statusCode: 400, status: false, message: error });
                    });
            } else if (!paymentDetailsDetails.payment.length) {
                settings.updateOne(
                    { sellerId: mongoose.Types.ObjectId(user._id) },
                    {
                        "$push": {
                            "payment": {
                                "$each": settingsObj
                            }
                        }
                    }
                ).then((response) => {
                    return resolve({
                        status: true,
                        message: 'payment settings added successfully'
                    });
                });

            } else {
                console.log("test", paymentDetailsDetails)
                settings.updateOne(
                    { "payment._id": mongoose.Types.ObjectId(paymentDetailsDetails.payment[0]._id) },
                    {
                        $set: {
                            "payment.$.cashOnDelivery": data.cashOnDelivery,
                            "payment.$.onlinePayment": data.onlinePayment,
                            "payment.$.priceIncludeVat": data.priceIncludeVat,
                            "payment.$.enableVat": data.enableVat,
                            "payment.$.priceIncludeDeliveryFee": data.priceIncludeDeliveryFee
                        },
                    }
                ).then((response) => {
                    return resolve({
                        status: true,
                        message: 'Payment settings updated',
                    });
                });
            }
        } catch (error) {
            reject(error);
        }
    });
}

module.exports = {
    doAddOrUpdateNotification,
    doGetSettings,
    doAddOrUpdatePrivacy,
    doAddSellerTaxNumber,
    doAddOrUpdateBusinessDetails,
    doAddSellerRegNumber,
    doAddSellerReturnDays,
    doAddSellerBankDetails,
    doAddSellerVat,
    doUpdateSellerBankDetails,
    doAddOrUpdatePayment,
    doGetSettingsNotification,
    doGetSettingsPrivacy,
    doGetSettingsPayment,
    doGetSettingsBusinessDetails,
    doGetSettingsStoreDetails

};
