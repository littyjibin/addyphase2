const driver = require('../../../model/seller/driver/driver');
const Notification = require("../../../model/user/notification");
const OrderHistory = require("../../../model/seller/driver/order_history");
const { validateDriver, validateDriverByNumber, validateDriverLogin, validateOrderDetails, validateStatusUpdate, validateStatusList,validateEstimatedDeliveryFee } = require('../../../validations/seller/driver/driver/validation');
const sendSms = require('../../../helpers/sms/sendSms');
const numberValidation = require("../../../helpers/number/numberValidation");
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const send = require('../../../model/user/send');
const _ = require('lodash');

const doAddDriver = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const dataValidation = await validateDriver(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      data.sellerId = user._id;
      const isTitleExist = await driver.findOne({
        mobile: data.mobile,
        name: data.name,
      });

      if (isTitleExist) {
        return resolve({ statusCode: 400, status: false, message: 'Driver  is already exist.' });
      }
      data.sellerId = user._id;

      let schemaObj = new driver(data);
      schemaObj
        .save()
        .then(() => {
          return resolve({ statusCode: 200, status: true, message: 'Driver created successfully.' });
        })
        .catch(async (error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};

const doGetDriverByMobile = (datas, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const dataValidation = await validateDriverByNumber(datas);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      let drivers = await driver.find({ isActive: true, mobile: datas.mobile, code: datas.code, sellerId: user._id }, {}).sort({ _id: -1 });
      if (!drivers)
        return resolve({ statusCode: 400, status: false, message: 'No data found' });
      else
        return resolve({ statusCode: 200, status: true, message: 'Success', data: { drivers } });

    } catch (error) {
      reject(error);
    }
  });
};

// get active drivers
const doGetDriverDetails = () => {
  return new Promise(async (resolve, reject) => {
    try {
      //get from db
      let drivers = await driver.find({ isActive: true }, {}).sort({ _id: -1 });
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { drivers } });
    } catch (error) {
      reject(error);
    }
  });
};

const doDriverLogin = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateDriverLogin(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      let validNumber = numberValidation(data.code, data.mobile);
      let params = data;
      if (!validNumber) {
        return resolve({
          status: true,
          data: {},
          message: 'Destination number' + " " + params.mobile + ' is invalid',
        });
      }

      const driverExist = await driver.findOne({
        mobile: params.mobile,
        code: params.code,
      });

      if (driverExist) {
        var payload = {
          driverId: driverExist._id,
        };
        let token = jwt.sign(
          {
            data: payload,
          },
          "JWT_KEY",
          {
            expiresIn: "30 days",
          }
        );
        if (token) {
          return resolve({ statusCode: 200, status: true, message: 'logged in successfully', data: { token: token } });
        } else {
          return resolve({ statusCode: 400, status: false, message: 'token error' });
        }
      } else {
        return resolve({ statusCode: 200, status: false, message: 'Invalid login credentials' });
      }
    } catch (error) {
      reject(error);
    }
  });
};

const doOrderDetails = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateOrderDetails(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      let order = await send.findOne({
        _id: mongoose.Types.ObjectId(data.orderId),
      })
        .populate({
          path: "sender",
          select: [
            "firstName",
            "lastName",
            "userName",
            "code",
            "mobile",
            "location",
            "address",
          ],
        })
        .populate({
          path: "packageSizeId",
          select: [
            "size"
          ],
        })
        .populate({
          path: "assigneeId",
          select: [
            "firstName",
            "lastName",
            "userName",
            "code",
            "mobile",
            "location",
            "address",
          ],
        })
        .lean();
        console.log("ggggggg", order);
      if (order) {
        if (!order.isApproved) {
          return resolve({ statusCode: 200, status: true, message: 'waiting order', data: { waiting_order: true } });
        }

        if (
          order.status == "delivery_completed" ||
          order.status == "delivery_failed"
        ) {
          return resolve({ statusCode: 200, status: true, message: 'waiting order', data: { expired_order: true } });
        }
        let fromAddress;
        let toAddress;
        //Sender Address

        for (let item of order.sender.address) {
          if (order.assigneeAddress == item._id + "") {
            fromAddress = item;
            fromAddress.firstName = order.sender.firstName;
            fromAddress.lastName = order.sender.lastName;
            fromAddress.userName = order.sender.userName;
            fromAddress.mobile = order.sender.code + order.sender.mobile;
          }
          if (order.selfAddress == item._id + "") {
            fromAddress = item;
            fromAddress.firstName = order.sender.firstName;
            fromAddress.lastName = order.sender.lastName;
            fromAddress.userName = order.sender.userName;
            fromAddress.mobile = order.sender.code + order.sender.mobile;
          }
        }

        //Collector Address
      
        for (let item of order.assigneeId.address) {
          if (order.assigneeAddress == item._id + "") {
            toAddress = item;
            toAddress.firstName = order.assigneeId.firstName;
            toAddress.lastName = order.assigneeId.lastName;
            toAddress.userName = order.assigneeId.userName;
            toAddress.mobile = order.assigneeId.code + order.assigneeId.mobile;
          }
          if (order.selfAddress == item._id + "") {
            toAddress = item;
            toAddress.firstName = order.assigneeId.firstName;
            toAddress.lastName = order.assigneeId.lastName;
            toAddress.userName = order.assigneeId.userName;
            toAddress.mobile = order.assigneeId.code + order.assigneeId.mobile;
          }
        }

        return resolve({
          statusCode: 200, status: true, message: 'order details are', data: {
            fromAddress,
            toAddress,
            status: order.status,
            packagePrice: order.priceOfPackage,
            size:order.packageSizeId.size,
            estimatedDeliveryFee:order.estimatedDeliveryFee

          }
        });
      } else {
        return resolve({ statusCode: 400, status: false, message: 'invalid order_id' });
      }
    } catch (error) {
      reject(error);
    }
  });
};

const doUpdateStatus = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateStatusUpdate(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      let params = data;
      let validOrder = {};
      let validStatuses = [
        "Approved",
        "Rejected",
        "Pickup initiated",
        "Pickup completed",
        "Pickup failed",
        "Delivery initiated",
        "Delivery completed",
        "Delivery failed",
      ];

      validOrder = await send.findOne({
        _id: mongoose.Types.ObjectId(params.orderId),
      });
      if (!validOrder) {
        return resolve({ statusCode: 200, status: false, message: "Invalid order_id" });
      }
      let validStatus = validStatuses.includes(params.status);
      if (!validStatus) {
        return resolve({ statusCode: 200, status: false, message: "Invalid status" });
      }
      let notificationStatus = params.status;

      if (params.status == "Approved") {
        params.status = "driver_approved";
      } else if (params.status == "Rejected") {
        params.status = "driver_rejected";
      } else if (params.status == "Pickup initiated") {
        params.status = "pickup_initiated";
      } else if (params.status == "Pickup completed") {
        params.status = "pickup_completed";
      } else if (params.status == "Pickup failed") {
        params.status = "pickup_failed";
      } else if (params.status == "Delivery initiated") {
        params.status = "delivery_initiated";
      } else if (params.status == "Delivery completed") {
        params.status = "delivery_completed";
      } else if (params.status == "Delivery failed") {
        params.status = "delivery_failed";
      }
      send.updateOne(
        { _id: mongoose.Types.ObjectId(params.orderId) },
        {
          $set: { status: params.status },
        }
      ).then(async (response) => {
        let orderHistory = await OrderHistory.findOne({
          order_id: params.orderId,
        }).lean();

        for (const property in orderHistory) {
          orderHistory[property].value = false;
        }
        orderHistory[params.status] = {
          value: true,
          date: new Date(),
        };

        await OrderHistory.updateOne(
          { order_id: params.orderId },
          orderHistory
        );

        // adding notification to assigner
        let assignerId = validOrder.userId;

        let notfication = new Notification({
          userId: assignerId,
          message:
            "Your package with order id " +
            validOrder.order_id +
            " is " +
            notificationStatus,
        });
        await notfication.save();

        // adding notification to assignee
        let assigneeId = validOrder.assigneeId;

        let notfication2 = new Notification({
          userId: assigneeId,
          message:
            "Your package with order id " +
            validOrder.order_id +
            " is " +
            notificationStatus,
        });
        await notfication2.save();
        return resolve({ statusCode: 200, status: true, message: "Status updated" });
      });
    } catch (error) {
      reject(error);
    }
  });
};

const doGetDriverstatuses = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateStatusList(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      let validOrder = await send.findOne({
        _id: mongoose.Types.ObjectId(data.orderId),
      });
      if (!validOrder) {
        return resolve({ statusCode: 200, status: false, message: "Invalid order_id" });
      }

      let status_list = [];

      if (validOrder.status == "approved") {
        status_list.push("Approved", "Rejected");
      } else if (validOrder.status == "driver_approved") {
        status_list.push("Start Pickup","Attempt Pickup","Pickup Completed","Pickup Failed","Start Delivery", "Attempt Delivery", "Delivered", "Failed Delivery","Cancel");
      } else if (validOrder.status == "Assigned to driver") {
        status_list.push("Approved", "Rejected");
      } else if (validOrder.status == "driver_rejected") {
        status_list.push("Rejected", "Approved");
      } else if (validOrder.status == "pickup_initiated") {
        status_list.push(
          "Attempt Pickup",
          "Pickup Completed",
          "Pickup Failed",
          "In Warehouse", "Start Delivery", "Attempt Delivery", "Delivered", "Failed Delivery","Cancel"
        );
      } else if (validOrder.status == "pickup_failed") {
        status_list.push("Start Pickup","Attempt Pickup","Pickup Completed","Pickup Failed", "In Warehouse", "Start Delivery", "Attempt Delivery", "Delivered", "Failed Delivery","Cancel");
      } else if (validOrder.status == "pickup_completed") {
        status_list.push("Pickup completed", "In Warehouse", "Start Delivery", "Attempt Delivery", "Delivered", "Failed Delivery","Cancel");
      } else if (validOrder.status == "delivery_initiated") {
        status_list.push(
          "In Warehouse", "Start Delivery", "Attempt Delivery", "Delivered", "Failed Delivery","Cancel"
        );
      } else if (validOrder.status == "delivery_failed") {
        status_list.push("Delivery failed");
      } else if (validOrder.status == "delivery_completed") {
        status_list.push("Delivery completed");
      }
      return resolve({
        statusCode: 200, status: false, message: "Driver statuses", data: {
          statuses: status_list,
        },
      })

    } catch (error) {
      reject(error);
    }
  });
};

const doCheckDriverLink = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateStatusList(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      let validOrder = await send.findOne({
        _id: mongoose.Types.ObjectId(data.orderId),
      });
      console.log("tttt", validOrder)
      if (!validOrder) {
        return resolve({ statusCode: 200, status: false, message: "Invalid Link" });
      }
      if (validOrder.isRejected) {
        return resolve({ statusCode: 200, status: false, message: "Link expired" });
      }
      if (!validOrder.isApproved) {
        return resolve({ statusCode: 200, status: false, message: "Link expired" });
      }
      if (validOrder.status == "driver_rejected" || validOrder.status == "pickup_completed") {
        return resolve({ statusCode: 200, status: false, message: "Link expired" });
      }
      return resolve({ statusCode: 200, status: true, message: "Valid link" });
    } catch (error) {
      reject(error);
    }
  });
};

const doEstimatedDeliveryFee = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateEstimatedDeliveryFee(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      validOrder = await send.findOne({
        _id: mongoose.Types.ObjectId(data.orderId),
      });
      if (!validOrder) {
        return resolve({ statusCode: 200, status: false, message: "Invalid order_id" });
      }
      send.updateOne(
        { _id: mongoose.Types.ObjectId(data.orderId) },
        {
          $set: { estimatedDeliveryFee: data.estimatedDeliveryFee, },
        }
      ).then(async (response) => {
        return resolve({ statusCode: 200, status: false, message: "estimatedDeliveryFee added" });
      });
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  doAddDriver,
  doGetDriverDetails,
  doGetDriverByMobile,
  doDriverLogin,
  doOrderDetails,
  doUpdateStatus,
  doGetDriverstatuses,
  doCheckDriverLink,
  doEstimatedDeliveryFee
};
