const {
  doAddDriver,
  doGetDriverDetails,
  doGetDriverByMobile,
  doDriverLogin,
  doOrderDetails,
  doUpdateStatus,
  doGetDriverstatuses,
  doCheckDriverLink,
  doEstimatedDeliveryFee
} = require('./controller');

const addDriver = (req, res, next) => {
  doAddDriver(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};


const GetDriverByMobile = (req, res, next) => {
  doGetDriverByMobile(req.body, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};


const getDriverDetails = (req, res, next) => {
  doGetDriverDetails()
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};


const DriverLogin = (req, res, next) => {
  doDriverLogin(req.body)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getOrderDetails = (req, res, next) => {
  doOrderDetails(req.body, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};


const driverStatusUpdate = (req, res, next) => {
  doUpdateStatus(req.body)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getDriverstatuses = (req, res, next) => {
  doGetDriverstatuses(req.params)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const checkDriverLink = (req, res, next) => {
  doCheckDriverLink(req.body)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};


const estimatedDeliveryFee = (req, res, next) => {
  doEstimatedDeliveryFee(req.body)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
 
module.exports = {
  addDriver,
  getDriverDetails,
  GetDriverByMobile,
  DriverLogin,
  getOrderDetails,
  driverStatusUpdate,
  getDriverstatuses,
  checkDriverLink,
  estimatedDeliveryFee
};
