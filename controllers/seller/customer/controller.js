const mongoose = require('mongoose');
const moment = require('moment-timezone');

const order = require('../../../model/user/product/order');
const { getOrderDetailsById, getOrderHistoryByOrderId } = require('../../../helpers/product/orderHelper');

// get customers by seller
const doGetCustomersBySeller = (user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const userId = user._id;

      //get from db
      let orders = await order.aggregate([
        { $sort: { createdAt: -1 } },
        {
          $match: {
            sellerId: mongoose.Types.ObjectId(userId),
            isActive: true,
          },
        },
        {
          $lookup: {
            from: 'accounts',
            localField: 'userId',
            foreignField: '_id',
            as: 'userAccountDetails',
          },
        },
        {
          $project: {
            _id: 1,
            userId: 1,
            countryCode: 1,
            createdAt: 1,
            updatedAt: 1,
            firstName: { $first: '$userAccountDetails.firstName' },
            lastName: { $first: '$userAccountDetails.lastName' },
            city: '$userDetails.city',
            area: '$userDetails.area',
          },
        },
        {
          $set: {
            name: { $concat: ['$firstName', ' ', '$lastName'] },
            yearMonthDayUTC: { $dateToString: { format: '%m-%d-%Y', date: '$createdAt' } },
            city: { $ifNull: ['$city', '$area'] },
          },
        },
        {
          $group: {
            _id: '$userId',
            name: { $first: '$name' },
            city: { $first: '$city' },
            lastOrder: { $first: '$createdAt' },
            totalOrders: { $sum: 1 },
          },
        },
      ]);

      if (orders.length) {
        orders.forEach((e, i) => {
          e.lastOrder = moment(e.lastOrder).format('MMM DD,YYYY');
        });
      }

      return resolve({ statusCode: 200, status: true, message: 'Success', data: { orders } });
    } catch (error) {
      reject(error);
    }
  });
};

// get customer order by id
const doGetCustomerOrderById = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const userId = user._id;
      const id = data.id;

      // Get order details by id
      let orderDetails = await getOrderDetailsById(id);

      // Get order history by order id
      let orderHistory = await getOrderHistoryByOrderId(id);

      orderDetails.deliveryDate = moment(orderDetails.deliveryDate).format('MMM DD,YYYY hh mm a');
      orderDetails.expectedDeliveryDate = moment(orderDetails.expectedDeliveryDate).format('MMM DD,YYYY') + " " +orderDetails.expectedDeliveryFromTime;

      return resolve({ statusCode: 200, status: true, message: 'Success', data: { orderDetails, orderHistory } });
    } catch (error) {
      reject(error);
    }
  });
};

// get orders by seller and customer
const doGetCustomerOrderbyUserId = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const sellerId = user._id;

      //get from db
      let orders = await order.aggregate([
        { $sort: { createdAt: -1 } },
        {
          $match: {
            sellerId: mongoose.Types.ObjectId(sellerId),
            userId: mongoose.Types.ObjectId(data.userId),
            isActive: true,
          },
        },
        {
          $lookup: {
            from: 'accounts',
            localField: 'userId',
            foreignField: '_id',
            as: 'userAccountDetails',
          },
        },
        {
          $project: {
            _id: 1,
            userId: 1,
            orderId:1,
            countryCode: 1,
            orderStatus: 1,
            noOfItems:1,
            grandTotal:1,
            paymentMode:1,
            expectedDeliveryDate:1,
            name: {
              $concat: [{ $first: '$userAccountDetails.firstName' }, ' ', { $first: '$userAccountDetails.lastName' }],
            }
          },
        },
      ]);

      
      let statuses = ['completePickup', 'startDelivery', 'attemptDelivery', 'failedToDelivery'];
      if (orders.length) {
        orders.forEach((e) => {
          if (statuses.includes(e.orderStatus)){
            e.orderStatus = "shipped"
          }
          e.expectedDeliveryDate = moment(e.expectedDeliveryDate).format('MMM DD,YYYY');
        });
      }

      return resolve({ statusCode: 200, status: true, message: 'Success', data: { orders } });
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  doGetCustomersBySeller,
  doGetCustomerOrderById,
  doGetCustomerOrderbyUserId,
};
