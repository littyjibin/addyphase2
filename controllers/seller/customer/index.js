const { doGetCustomersBySeller, doGetCustomerOrderById,doGetCustomerOrderbyUserId} = require('./controller');


const getCustomersBySeller = (req, res, next) => {
    doGetCustomersBySeller(req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
const getCustomerOrderById = (req, res, next) => {
  doGetCustomerOrderById(req.params,req.user)
  .then(({ statusCode = 200, message, status, data }) => {
    res.status(statusCode).json({
      message,
      status,
      ...(data && { data }),
    });
  })
  .catch((error) => {
    next(error);
  });
};

const getCustomerOrderbyUserId = (req, res, next) => {
  doGetCustomerOrderbyUserId(req.params,req.user)
  .then(({ statusCode = 200, message, status, data }) => {
    res.status(statusCode).json({
      message,
      status,
      ...(data && { data }),
    });
  })
  .catch((error) => {
    next(error);
  });
};
module.exports = {
    getCustomersBySeller,
    getCustomerOrderById,
    getCustomerOrderbyUserId

};