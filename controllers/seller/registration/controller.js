const mongoose = require('mongoose');
const sellerCategory = require('../../../model/seller/category/category');
const sellerDetails = require('../../../model/seller/registration/sellerDetails');
const sellerCourierCompany = require('../../../model/seller/registration/sellerCourierCompanies');
const package = require('../../../model/seller/package/package');
const account = require('../../../model/Account');
const courierCompany = require('../../../model/admin/courierCompany');
const driver = require('../../../model/seller/registration/sellerDriver');
const sendSms = require('../../../helpers/sms/sendSms');
const numberValidation = require('../../../helpers/number/numberValidation');
const { uploadImage } = require('../../../utils/imageUpload');

const {
  validateSellerSignup,
  validateSellerCourierComapny,
  validateSellerSellingType,
  validateSellerBusinessDetails,
  validateSellerDriver,
  validateEditSellerDriver,
  validateSetDefaultDeliveryMethod,
  validateSellerProfileUpdate,
} = require('../../../validations/seller/registation/validation');

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function validateEmail(emailAdress) {
  let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (emailAdress.match(regexEmail)) {
    return true;
  }
}


const doAddSellerSellingType = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      console.log(user);
      //validate incoming data
      const dataValidation = await validateSellerSellingType(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      //check if category exist
      const isCategoryExist = await sellerCategory.findOne({ _id: data.categoryId });
      if (!isCategoryExist) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid category id.' });
      }

      data.userId = user._id;
      //check if seller already exist
      const isSellerExist = await sellerDetails.findOne({ userId: data.userId });
      if (!isSellerExist) {
        //save to db
        let schemaObj = new sellerDetails(data);
        schemaObj
          .save()
          .then(() => {
            return resolve({ statusCode: 200, status: true, message: 'Selling type added successfully.' });
          })
          .catch(async (error) => {
            return resolve({ statusCode: 400, status: false, message: error });
          });
      } else {
        //update in db
        await sellerDetails
          .updateOne({ userId: data.userId }, data)
          .then(() => {
            return resolve({ statusCode: 200, status: true, message: 'Selling type added successfully.' });
          })
          .catch((error) => {
            return resolve({ statusCode: 400, status: false, message: error });
          });
      }
    } catch (error) {
      reject(error);
    }
  });
};

const doAddSellerBusinessDetails = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateSellerBusinessDetails(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      //check user id in sellerdetails
      const isSellerExist = await sellerDetails.findOne({
        userId: user._id,
      });
      if (!isSellerExist) {
        return resolve({ statusCode: 400, status: false, message: 'Seller not found' });
      }

      //check color id exist or not
      // const isColorExist = await color.findOne({
      //   _id: data.colorId,
      // });
      // if (!isColorExist) {
      //   return resolve({ statusCode: 400, status: false, message: 'Invalid color id' });
      // }

      //check store url already exist or not
      const isStoreUrl = await sellerDetails.findOne({
        storeUrl: data.storeUrl,
      });
      if (isStoreUrl) {
        return resolve({ statusCode: 400, status: false, message: 'Store url already exist' });
      }

      data.userId = user._id;

      //update in db
      await sellerDetails
        .updateOne({ userId: data.userId }, data)
        .then(() => {
          return resolve({ statusCode: 200, status: true, message: 'Business details added successfully.' });
        })
        .catch((error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};

const doAddSellerPackageDetails = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      console.log('data', data);
      const dataValidation = await validateSellerPackageDetails(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      //check user id in sellerdetails
      const isSellerExist = await sellerDetails.findOne({
        userId: user._id,
      });

      if (!isSellerExist) {
        return resolve({ statusCode: 400, status: false, message: 'Seller not found' });
      }

      data.userId = user._id;
      var currentDate = new Date();
      if (data.packageDuration == '1 year') {
        var expirePackage = currentDate.setFullYear(currentDate.getFullYear() + 1);
      } else {
        var expirePackage = currentDate.setMonth(currentDate.getMonth() + 6);
      }
      data.expirePackage = expirePackage;
      data.packageCreatedDate = currentDate;
      //update in db
      await sellerDetails
        .updateOne({ userId: data.userId }, data)
        .then(() => {
          return resolve({ statusCode: 200, status: true, message: 'Package details added successfully.' });
        })
        .catch((error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};

const doSellerSignup = (data, user, logo) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateSellerSignup(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      //check if user is an adddy user
      const isUserExist = await account.findOne({ _id: user._id, didRegistered: 1 });
      if (!isUserExist) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid user.' });
      }

      //check if category exist
      const isCategoryExist = await sellerCategory.findOne({ _id: data.categoryId });
      if (!isCategoryExist) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid category id.' });
      }

      //check if package  exist
      const isPackageExist = await package.findOne({ _id: data.packageId });
      if (!isPackageExist) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid package id.' });
      }

      //check if address  exist
      const isAddressExist = await account.findOne({ _id: user._id, 'address._id': data.addressId });
      if (!isAddressExist) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid address id.' });
      }

      //check store url already exist or not
      const isStoreUrl = await sellerDetails.findOne({
        storeUrl: data.storeUrl,
        sellerId: { $ne: user._id },
      });
      if (isStoreUrl) {
        return resolve({ statusCode: 400, status: false, message: 'Store url already exist' });
      }

      let accountLogo = isUserExist.logo;

      if (logo) {
        //Upload logo image
        let logoImageFileName = `seller/logo/${new Date().getTime()}_${logo.name}`;
        await uploadImage(logo.data, logoImageFileName).then(({ url }) => {
          accountLogo = url;
        });
      }

      data.sellerId = user._id;
      //check if seller already exist
      let isSellerExist = await sellerDetails.findOne({ sellerId: data.sellerId });
      if (!isSellerExist) {
        if (accountLogo) {
          // update logo in user account collection
          await account.updateOne({ _id: data.sellerId }, { logo: accountLogo });
        }

        //save to db
        let schemaObj = new sellerDetails(data);
        schemaObj
          .save()
          .then(() => {
            return resolve({ statusCode: 200, status: true, message: 'Seller sign up successfully.' });
          })
          .catch(async (error) => {
            return resolve({ statusCode: 400, status: false, message: error });
          });
      } else {
        if (accountLogo) {
          // update logo in user account collection
          await account.updateOne({ _id: data.sellerId }, { logo: accountLogo });
        }

        //update in db
        await sellerDetails
          .updateOne({ sellerId: data.sellerId }, data)
          .then(() => {
            return resolve({ statusCode: 200, status: true, message: 'Seller sign up successfully.' });
          })
          .catch((error) => {
            return resolve({ statusCode: 400, status: false, message: error });
          });
      }
    } catch (error) {
      reject(error);
    }
  });
};

const doSellerAddCourierCompany = (data, user, logo) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!data.courierCompany) {
        return resolve({ statusCode: 400, status: false, message: 'Please add courier company ' });
      }

      //check user id in sellerdetails
      let isSellerExist = await sellerDetails.findOne({
        userId: user._id,
      });

      if (!isSellerExist) {
        return resolve({ statusCode: 400, status: false, message: 'Seller not found' });
      }

      let countryCode = '';
      //get user details
      const userDetails = await account.findOne({
        _id: user._id,
      });
      if (userDetails) countryCode = userDetails.countryCode;

      const { courierCompany: courierCompanyId } = data;
      const courierDetails = await courierCompany.findOne({
        isActive: true,
        countryCode: countryCode,
        _id: courierCompanyId,
      });
      if (!courierDetails) return resolve({ statusCode: 400, status: false, message: 'Invalid courier company ' });

      const courierExist = await sellerCourierCompany.findOne({
        sellerId: user._id,
        courierCompany: courierCompanyId,
      });

      if (courierExist) return resolve({ statusCode: 400, status: false, message: 'Courier company already exist.' });

      // await sellerDetails.updateOne(
      //   { sellerId: mongoose.Types.ObjectId(user._id) },
      //   {
      //     $push: {
      //       courierCompany: data.courierCompany,
      //     },
      //   }
      // );
      const userId = user._id;

      const isDriverDefaultSet = await driver.findOne({
        sellerId: userId,
        isDefault: true,
      });
      const isCompanyDefaultSet = await sellerCourierCompany.findOne({
        sellerId: userId,
        isDefault: true,
      });

      let isDefault = false;
      if (!isDriverDefaultSet && !isCompanyDefaultSet) {
        isDefault = true;
      }

      //save to db
      let schemaObj = new sellerCourierCompany({
        sellerId: userId,
        courierCompany: courierCompanyId,
        isDefault: isDefault,
      });
      schemaObj
        .save()
        .then(async () => {
          await account.updateOne({ _id: userId }, { isSeller: true });
          return resolve({ statusCode: 200, status: true, message: 'Courier company added successfully' });
        })
        .catch(async (error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};

const doSellerAddCourierPartner = (data, user, logo) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!data.courierCompanyId && !data.driver) {
        return resolve({ statusCode: 400, status: false, message: 'Please add courier partner' });
      }

      //validate incoming data
      const dataValidation = await validateSellerCourierComapny(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      //check user id in sellerdetails
      let isSellerExist = await sellerDetails.findOne({
        userId: user._id,
      });

      if (!isSellerExist) {
        return resolve({ statusCode: 400, status: false, message: 'Seller not found' });
      }

      let countryCode = '';
      //get user details
      const userDetails = await account.findOne({
        _id: user._id,
      });
      if (userDetails) countryCode = userDetails.countryCode;

      // check courier company exist or not
      if (data.courierCompanyId) {
        const courierDetails = await courierCompany.findOne({
          isActive: true,
          countryCode: countryCode,
          _id: { $in: data.courierCompanyId },
        });
        if (!courierDetails) return resolve({ statusCode: 400, status: false, message: 'Invalid courier company id' });

        let couriercompanyIds = data.courierCompanyId;
        await sellerDetails.updateOne(
          { sellerId: mongoose.Types.ObjectId(user._id) },
          {
            $push: {
              courierCompanyId: {
                $each: couriercompanyIds,
              },
            },
          }
        );
      }

      if (data.driver) {
        let obj = {
          sellerId: user._id,
          name: data.driver.name,
          image: data.driver.image,
          code: data.driver.code,
          mobile: data.driver.mobile,
        };
        const driverDetails = await driver.findOne({
          sellerId: user._id,
          mobile: data.driver.mobile,
        });

        if (!driverDetails) {
          let schemaObj = new driver(obj);
          schemaObj.save();
        }
      }

      await account.updateOne({ _id: user._id }, { isSeller: true });

      return resolve({ statusCode: 200, status: true, message: 'Seller register successfully' });
    } catch (error) {
      reject(error);
    }
  });
};

// get active courier company by name or phone number
const doSearchCourierCompanies = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      let { searchBy } = data;

      let searchQuery;
      searchQuery = [
        { mobile: { $regex: `${searchBy}`, $options: 'i' } },
        { companyName: { $regex: `${searchBy}`, $options: 'i' } },
      ];

      //check user id in sellerdetails
      let isSellerExist = await sellerDetails.findOne({
        userId: user._id,
      });

      if (!isSellerExist) {
        return resolve({ statusCode: 400, status: false, message: 'Seller not found' });
      }

      let countryCode = '';
      //get user details
      const userDetails = await account.findOne({
        _id: user._id,
      });
      if (userDetails) countryCode = userDetails.countryCode;

      let courierCompanies = await courierCompany
        .aggregate([
          {
            $match: {
              isActive: true,
              countryCode: countryCode,
              ...(searchBy && { $or: searchQuery }),
            },
          },
          {
            $project: {
              __v: 0,
              createdAt: 0,
              updatedAt: 0,
            },
          },
        ])
        .sort({ _id: -1 });

      return resolve({ statusCode: 200, status: true, message: 'Success', data: { courierCompanies } });
    } catch (error) {
      reject(error);
    }
  });
};

// get delivery method of seller
const doGetDeliveryMethodsBySeller = (user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //check user id in sellerdetails
      let isSellerExist = await sellerDetails.findOne({
        userId: user._id,
        isActive: true,
      });

      if (!isSellerExist) {
        return resolve({ statusCode: 400, status: false, message: 'Seller not found' });
      }

      // get the courier company partner list
      let courierCompanies = await sellerCourierCompany
        .find({ sellerId: user._id }, { _id: 1, isDefault: 1 })
        .populate({
          path: 'courierCompany',
          select: [
            'companyName',
            'logo',
            'code',
            'mobile',
            'description',
            'webUrl',
            'address',
            'street',
            'area',
            'city',
            'pinCode',
            'country',
          ],
        });
      // get the driver  list of seller
      let drivers = await driver.find(
        { sellerId: user._id },
        { name: 1, code: 1, mobile: 1, sellerId: 1, deliveryFee: 1, isDefault: 1 }
      );

      return resolve({ statusCode: 200, status: true, message: 'Success', data: { courierCompanies, drivers } });
    } catch (error) {
      reject(error);
    }
  });
};

// delete delivery method of seller
const doDeleteDeliveryMethodBySeller = (id, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //check user id in sellerdetails
      let isSellerExist = await sellerDetails.findOne({
        userId: user._id,
        isActive: true,
      });

      if (!isSellerExist) {
        return resolve({ statusCode: 400, status: false, message: 'Seller not found' });
      }

      // get the courier company partner list
      //let isCourierCompanyexist = await sellerDetails.findOne({ sellerId: user._id, courierCompany: id }, { _id: 1 });
      let isCourierCompanyexist = await sellerCourierCompany.findById({ _id: id }, { _id: 1, isDefault: 1 });
      // get the driver list
      let isDriverExist = await driver.findById({ _id: id, isDefault: 1 });

      if (!isCourierCompanyexist && !isDriverExist)
        return resolve({ statusCode: 400, status: false, message: 'Invalid id' });

      if (isCourierCompanyexist) {
        if (isCourierCompanyexist.isDefault == true)
          return resolve({ statusCode: 400, status: false, message: 'Please choose default delivery method' });
        const deleteCompany = await sellerCourierCompany.deleteOne({ _id: mongoose.Types.ObjectId(id) });
        // const updatedSeller = await sellerDetails.findOneAndUpdate(
        //   { sellerId: mongoose.Types.ObjectId(user._id) },
        //   { $pull: { courierCompany: mongoose.Types.ObjectId(id) } }
        // );
        return resolve({ statusCode: 200, status: true, message: 'Delivery method deleted successfully' });
      }
      if (isDriverExist) {
        if (isDriverExist.isDefault == true)
          return resolve({ statusCode: 400, status: false, message: 'Please choose default delivery method' });

        const deleteDriver = await driver.deleteOne({ _id: mongoose.Types.ObjectId(id) });
        return resolve({ statusCode: 200, status: true, message: 'Delivery method deleted successfully' });
      }
    } catch (error) {
      reject(error);
    }
  });
};

// invite new seller company
const doSellerInviteCourierCompany = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!data.mobile) {
        return resolve({ statusCode: 400, status: false, message: 'Please add mobile number' });
      }

      let code = data.mobile.replace('+', '');
      let values = code.split(' ');
      code = values[0];
      let mobile = values[1];

      //get from db
      let users = await courierCompany.findOne(
        {
          mobile: mobile,
          code: code,
        },
        { companyName: 1 }
      );
      if (users)
        return resolve({ statusCode: 400, status: false, message: 'Courier comapany already exist', data: {} });

      if (!users) {
        let payload = {
          origin: 'ADDYAE',
          destination: data.mobile,
          message: 'Welcome to Addy',
        };

        await sendSms(payload)
          .then((_) => {
            return resolve({ statusCode: 200, status: true, message: 'Invite successfully', data: {} });
          })
          .catch((error) => {
            return resolve({ statusCode: 400, status: false, message: error, data: {} });
          });
      }
    } catch (error) {
      reject(error);
    }
  });
};

// upload image
const doUploadImage = (image, data) => {
  return new Promise(async (resolve, reject) => {
    try {
      let fileName = '';

      // seller driver profile pic
      if (data.type == 'sellerdriver') {
        fileName = `sellerdriver/${new Date().getTime()}_${image.name}`;
        // packaged product images
      } else if (data.type == 'order') {
        fileName = `order/${new Date().getTime()}_${image.name}`;
      } else if (data.type == 'contact') {
        fileName = `contact/${new Date().getTime()}_${image.name}`;
      } else if (data.type == 'send') {
        fileName = `send/${new Date().getTime()}_${image.name}`;
      } else {
        fileName = `others/${new Date().getTime()}_${image.name}`;
      }
      let imageUrl;
      //user image here is folder name
      await uploadImage(image.data, fileName).then(({ url }) => {
        imageUrl = url;
      });

      return resolve({ statusCode: 200, status: true, message: 'Image saved successfully', data: imageUrl });
    } catch (error) {
      reject(error);
    }
  });
};

// get seller details by user id (seller id)
const doGetSellerDetailsBySellerId = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      let sellerDetail = await account.aggregate([
        {
          $match: {
            _id: mongoose.Types.ObjectId(user._id),
            isActive: true,
            didRegistered: 1
          },
        },
        {
          $lookup: {
            from: 'sellerdetails',
            localField: '_id',
            foreignField: 'sellerId',
            as: 'sellerDetails',
          },
        },
        {
          $unwind: {
            path: '$sellerDetails',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $project: {
            _id: 0,
            sellerId: '$_id',
            firstName: 1,
            lastName: 1,
            countryCode: 1,
            userName: 1,
            code: 1,
            mobile: 1,
            logo: 1,
            email: { $ifNull: ['$email', ''] },
            // userAddress:'$address',
            address: {
              $filter: {
                input: '$address',
                as: 'userAddresss',
                cond: { $eq: ['$$userAddresss._id', '$sellerDetails.addressId'] },
              },
            },
            businessName: '$sellerDetails.businessName',
            storeUrl: '$sellerDetails.storeUrl',
            colorCode: '$sellerDetails.colorCode',
          },
        },
        {
          $project: {
            sellerId: 1,
            firstName: 1,
            lastName: 1,
            code: 1,
            mobile: 1,
            countryCode: 1,
            logo: 1,
            email:1,
            logo: { $ifNull: ['$logo', ""] },
            // userAddress:1,
            address: { $first: '$address.address' },
            houseNumber: { $first: '$address.houseNumber' },
            street: { $first: '$address.street' },
            area: { $first: '$address.area' },
            latitude: { $first: '$address.location.latitude' },
            longitude: { $first: '$address.location.longitude' },
            businessName: 1,
            storeUrl: 1,
            userName: 1,
            colorCode: 1,
          },
        },
      ]);

      // let sellerDetail = await sellerDetails.aggregate([
      //   {
      //     $match: {
      //       sellerId: mongoose.Types.ObjectId(user._id),
      //       isActive: true,
      //     },
      //   },
      //   {
      //     $lookup: {
      //       from: 'accounts',
      //       localField: 'addressId',
      //       foreignField: 'address._id',
      //       as: 'userDetails',
      //     },
      //   },
      //   {
      //     $unwind: {
      //       path: '$userDetails',
      //     },
      //   },
      //   {
      //     $project: {
      //       sellerId: 1,
      //       firstName: '$userDetails.firstName',
      //       lastName: '$userDetails.lastName',
      //       countryCode: '$userDetails.countryCode',
      //       userName: '$userDetails.userName',
      //       code: '$userDetails.code',
      //       mobile: '$userDetails.mobile',
      //       address: {
      //         $filter: {
      //           input: '$userDetails.address',
      //           as: 'userAddress',
      //           cond: { $eq: ['$$userAddress._id', '$addressId'] },
      //         },
      //       },
      //       _id: 1,
      //       businessName: 1,
      //       logo: 1,
      //       storeUrl: 1,
      //       colorName: 1,
      //       colorCode: 1,
      //     },
      //   },
      //   {
      //     $project: {
      //       _id: 1,
      //       sellerId: 1,
      //       businessName: 1,
      //       storeUrl: 1,
      //       userName: 1,
      //       logo: 1,
      //       colorName: 1,
      //       colorCode: 1,
      //       firstName: 1,
      //       lastName: 1,
      //       code: 1,
      //       mobile: 1,
      //       countryCode: 1,
      //       address: { $first: '$address.address' },
      //       houseNumber: { $first: '$address.houseNumber' },
      //       street: { $first: '$address.street' },
      //       area: { $first: '$address.area' },
      //       latitude: { $first: '$address.location.latitude' },
      //       longitude: { $first: '$address.location.longitude' },
      //     },
      //   },
      // ]);

      if (!sellerDetail.length)
        return resolve({ statusCode: 400, status: false, message: 'The seller id you entered was not found.' });

      return resolve({
        statusCode: 200,
        status: true,
        message: 'Success',
        data: {
          sellerDetails: sellerDetail[0],
        },
      });
    } catch (error) {
      reject(error);
    }
  });
};

// seller add driver details
const doSellerAddDriver = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const dataValidation = await validateSellerDriver(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      data.code = data.code.replace('+', '');
      data.sellerId = user._id;

      // check number is valid or not
      let validNumber = numberValidation(data.code, data.mobile);
      if (!validNumber) {
        return resolve({
          status: false,
          data: {},
          message: 'Destination number' + ' ' + data.mobile + ' is invalid',
          statusCode: 400,
        });
      }

      const isMobileExist = await driver.findOne({
        sellerId: data.sellerId,
        mobile: data.mobile,
      });
      if (isMobileExist) {
        return resolve({ statusCode: 400, status: false, message: 'Driver is already exist.' });
      }

      let sellerCode = user.code;
      if (data.code != sellerCode)
        return resolve({ statusCode: 400, status: false, message: 'Please add driver from your country.' });

      // check default delivery method set
      const isDriverDefaultSet = await driver.findOne({
        sellerId: data.sellerId,
        isDefault: true,
      });
      const isCompanyDefaultSet = await sellerCourierCompany.findOne({
        sellerId: data.sellerId,
        isDefault: true,
      });
      if (!isDriverDefaultSet && !isCompanyDefaultSet) {
        data.isDefault = true;
      }

      // let result = await sellerCourierCompany. findOneAndUpdate({
      //   query: {sellerId:user._id , isDefault:true  },
      //   update: { isDefault: false },
      // });
      // console.log(result)
      // let driverStatusChange = await driver. findOneAndUpdate({
      //   query: {sellerId:user._id , isDefault:true  },
      //   update: { isDefault: false },
      // });

      let schemaObj = new driver(data);
      schemaObj
        .save()
        .then(async () => {
          await account.updateOne({ _id: user._id }, { isSeller: true });
          return resolve({ statusCode: 200, status: true, message: 'Driver created successfully.' });
        })
        .catch(async (error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};

// seller edi driver details
const doSellerEditDriver = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const dataValidation = await validateEditSellerDriver(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      data.code = data.code.replace('+', '');

      data.sellerId = user._id;

      const isDriverExist = await driver.findOne({
        _id: data.id,
        sellerId: data.sellerId,
      });
      if (!isDriverExist) {
        return resolve({ statusCode: 400, status: false, message: 'Driver not found.' });
      }
      const isMobileExist = await driver.findOne({
        mobile: data.mobile,
        _id: { $ne: data.id },
        sellerId: data.sellerId,
      });
      if (isMobileExist) {
        return resolve({ statusCode: 400, status: false, message: 'Driver is already exist.' });
      }

      driver
        .updateOne({ _id: mongoose.Types.ObjectId(data.id) }, data)
        .then((response) => {
          return resolve({ statusCode: 400, status: false, message: 'Driver updated successfully.' });
        })
        .catch(async (error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};

// seller edit driver details
const doSetDefaultDeliveryMethodBySeller = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const dataValidation = await validateSetDefaultDeliveryMethod(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      data.sellerId = user._id;
      const { id } = data;

      //check user id in sellerdetails
      let isSellerExist = await sellerDetails.findOne({
        userId: user._id,
        isActive: true,
      });

      if (!isSellerExist) {
        return resolve({ statusCode: 400, status: false, message: 'Seller not found' });
      }

      let isCourierCompanyExist = await sellerCourierCompany.findOne({ sellerId: user._id, _id: id }, { _id: 1 });

      // get the driver list
      let isDriverExist = await driver.findById({ sellerId: user._id, _id: id });
      if (!isCourierCompanyExist && !isDriverExist)
        return resolve({ statusCode: 400, status: false, message: 'Invalid id' });

      // updte the old default method to false
      let isCourierCompany = await sellerCourierCompany.updateMany(
        { sellerId: user._id, isDefault: true },
        { $set: { isDefault: false } }
      );
      let isDriver = await driver.updateMany({ sellerId: user._id, isDefault: true }, { $set: { isDefault: false } });

      if (isCourierCompanyExist) {
        sellerCourierCompany
          .updateOne({ _id: mongoose.Types.ObjectId(id) }, { $set: { isDefault: true } })
          .then((response) => {
            console.log(response);
            return resolve({
              statusCode: 200,
              status: true,
              message: 'Default delivery method updated successfully.',
            });
          })
          .catch(async (error) => {
            return resolve({ statusCode: 400, status: false, message: error });
          });
      }
      if (isDriverExist) {
        driver
          .updateOne({ _id: mongoose.Types.ObjectId(id) }, { $set: { isDefault: true } })
          .then((response) => {
            return resolve({
              statusCode: 200,
              status: true,
              message: 'Default delivery method updated successfully.',
            });
          })
          .catch(async (error) => {
            return resolve({ statusCode: 400, status: false, message: error });
          });
      }
    } catch (error) {
      reject(error);
    }
  });
};

const doUpdateSellerProfile = (data, user) => {
  return new Promise(async (resolve, reject) => {
    let userId = user._id;
    let params = data;
    try {
      if (
        data.firstName ||
        data.lastName ||
        data.logo ||
        data.email ||
        data.mobile ||
        data.storeUrl ||
        data.businessName
      ) {
        const dataValidation = await validateSellerProfileUpdate(data);
        if (dataValidation.error) {
          const message = dataValidation.error.details[0].message.replace(/"/g, '');
          return resolve({
            status: false,
            message: message,
            statusCode: 400,
          });
        }
        if (params.firstName) {
          params.firstName = params.firstName.toLowerCase();
          params.firstName = capitalizeFirstLetter(params.firstName);
        }
        if (params.lastName) {
          params.lastName = params.lastName.toLowerCase();
          params.lastName = capitalizeFirstLetter(params.lastName);
        }
        let sellerAccountDetails = {};
        let sellerDetailsData = {};
        if (params.email) {
          let validMail = validateEmail(params.email);
          if (!validMail) {
            return resolve({
              status: false,
              message: 'Invalid email',
            });
          }
        }
        let isSellerExist = await sellerDetails.findOne({ sellerId: userId });
        if (isSellerExist) {
          if (params?.email) sellerAccountDetails.email = params.email;
          if (params?.logo) sellerAccountDetails.logo = params.logo;
          if (params?.firstName) sellerAccountDetails.firstName = params.firstName;
          if (params?.lastName) sellerAccountDetails.lastName = params.lastName;
          if (params?.mobile) sellerAccountDetails.mobile = params.mobile;
          if (params?.businessName) {
            sellerDetailsData.businessName = params.businessName;
          }
          if (params?.storeUrl) {
            sellerDetailsData.storeUrl = params.storeUrl;
          }

          account
            .updateOne(
              {
                _id: userId,
              },
              sellerAccountDetails
            )
            .then((response) => {});
          await sellerDetails
            .updateOne({ sellerId: userId }, sellerDetailsData)
            .then(() => {})
            .catch((error) => {
              return resolve({ statusCode: 400, status: false, message: error });
            });
          return resolve({
            status: true,
            message: 'Customer data updated',
          });
        } else {
          return resolve({
            status: false,
            message: 'No seller found',
          });
        }
      } else {
        return resolve({
          status: false,
          message: 'Please add update params',
        });
      }
    } catch (error) {
      reject(error);
    }
  });
};


module.exports = {
  doUploadImage,
  doSellerSignup,
  doSellerAddCourierCompany,
  doSellerAddCourierPartner,
  doSearchCourierCompanies,
  doGetDeliveryMethodsBySeller,
  doDeleteDeliveryMethodBySeller,
  doSellerInviteCourierCompany,
  doAddSellerSellingType,
  doAddSellerBusinessDetails,
  doAddSellerPackageDetails,
  doGetSellerDetailsBySellerId,
  doSellerAddDriver,
  doSellerEditDriver,
  doSetDefaultDeliveryMethodBySeller,
  doUpdateSellerProfile,
};
