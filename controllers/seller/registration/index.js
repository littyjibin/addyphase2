const {
  doUploadImage,
  doSellerSignup,
  doSearchCourierCompanies,
  doGetDeliveryMethodsBySeller,
  doDeleteDeliveryMethodBySeller,
  doSellerAddCourierCompany,
  doSellerAddCourierPartner,
  doSellerInviteCourierCompany,
  doGetSellerDetailsBySellerId,
  doAddSellerSellingType,
  doAddSellerBusinessDetails,
  doAddSellerPackageDetails,
  doSellerAddDriver,
  doSellerEditDriver,
  doSetDefaultDeliveryMethodBySeller,
  doUpdateSellerProfile
} = require('./controller');

const addSellerSellingType = (req, res, next) => {
  doAddSellerSellingType(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const addSellerBusinessDetails = (req, res, next) => {
  doAddSellerBusinessDetails(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const addSellerPackageDetails = (req, res, next) => {
  doAddSellerPackageDetails(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const sellerSignup = (req, res, next) => {
  doSellerSignup(req.body, req.user, req?.files?.image)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const sellerAddCourierCompany = (req, res, next) => {
  doSellerAddCourierCompany(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const sellerAddCourierPartner = (req, res, next) => {
  doSellerAddCourierPartner(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const searchCourierCompanies = (req, res, next) => {
  doSearchCourierCompanies(req?.body, req?.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getDeliveryMethodsBySeller = (req, res, next) => {
  doGetDeliveryMethodsBySeller(req?.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const deleteDeliveryMethodBySeller = (req, res, next) => {
  doDeleteDeliveryMethodBySeller(req.params.id, req?.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const sellerInviteCourierCompany = (req, res, next) => {
  doSellerInviteCourierCompany(req?.body, req?.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getSellerDetailsBySellerId = (req, res, next) => {
  doGetSellerDetailsBySellerId(req.body, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const sellerAddDriver = (req, res, next) => {
  doSellerAddDriver(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};
const sellerEditDriver = (req, res, next) => {
  doSellerEditDriver(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};
const setDefaultDeliveryMethodBySeller = (req, res, next) => {
  doSetDefaultDeliveryMethodBySeller(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};
const imageUpload = (req, res, next) => {
  doUploadImage(req?.files?.image, req.body)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};


const updateSellerProfile = (req, res, next) => {
  doUpdateSellerProfile(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};


module.exports = {
  imageUpload,
  sellerSignup,
  sellerAddCourierCompany,
  sellerAddCourierPartner,
  searchCourierCompanies,
  getDeliveryMethodsBySeller,
  deleteDeliveryMethodBySeller,
  sellerInviteCourierCompany,
  addSellerSellingType,
  addSellerBusinessDetails,
  addSellerPackageDetails,
  getSellerDetailsBySellerId,
  sellerAddDriver,
  sellerEditDriver,
  setDefaultDeliveryMethodBySeller,
  updateSellerProfile
};
