const { doAddSellerPackage, doGetAllSellerPackages,doGetSellerPackages,doGetSellerPackageById } = require('./controller');


const addSellerPackage = (req, res, next) => {
  doAddSellerPackage(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getAllSellerPackages = (req, res, next) => {
  doGetAllSellerPackages()
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
const getSellerPackages = (req, res, next) => {
  doGetSellerPackages()
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
const getSellerPackageById = (req, res, next) => {
  doGetSellerPackageById(req.params)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
module.exports = { addSellerPackage,getAllSellerPackages,getSellerPackages,getSellerPackageById};