const package = require('../../../model/seller/package/package');
const { validateSellerPackage } = require('../../../validations/seller/package/validation');

const doAddSellerPackage = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      const dataValidation = await validateSellerPackage(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      const { packageName, description, ordersCount, price, startDate, endDate } = data;
      const isDataExist = await package.findOne({ packageName, description, ordersCount, price, startDate, endDate });
      if (isDataExist) {
        return resolve({ statusCode: 400, status: false, message: 'Already added package with same details' });
      }
      // let schemaObj = new package({ packageName, description, ordersCount, price,startDate,endDate });
      let schemaObj = new package(data);
      schemaObj
        .save()
        .then(() => {
          return resolve({ statusCode: 200, status: true, message: 'Package created successfully.' });
        })
        .catch(async (error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};

const doGetAllSellerPackages = () => {
  return new Promise(async (resolve, reject) => {
    try {
      let packages = await package
        .find({ isActive: true }, { __v: 0, createdAt: 0, updatedAt: 0, isActive: 0 })
        .sort({ _id: -1 });
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { packages } });
    } catch (error) {
      reject(error.message);
    }
  });
};
const doGetSellerPackageById = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      let packages = await package
        .findOne({ isActive: true, _id: data.id }, { __v: 0, createdAt: 0, updatedAt: 0, isActive: 0 })
        .sort({ _id: -1 });

      if (!packages) return resolve({ statusCode: 400, status: false, message: 'Invalid id', data: {} });
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { packages } });
    } catch (error) {
      reject(error.message);
    }
  });
};

const doGetSellerPackages = () => {
  return new Promise(async (resolve, reject) => {
    try {
      let packages = await package
        .find({ isActive: true,packageFor:"Seller" }, { packageName: 1, description: 1, orderCount: 1, price: 1, countryCode: 1,packageDuration:1,features:1 })
        .sort({ _id: -1 });
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { packages } });
    } catch (error) {
      reject(error.message);
    }
  });
};

module.exports = { doAddSellerPackage, doGetAllSellerPackages, doGetSellerPackages, doGetSellerPackageById };
