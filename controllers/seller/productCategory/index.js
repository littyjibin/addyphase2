const {
  doAddSellerProductCategory,
  doGetSellerProductCategories,
  doDeleteSellerProductCategory,
  doGetSellerProductCategoriesBySellerId,
  doGetActiveSellerProductCategoriesBySellerId,
  doChangeSellerProductCategoryStatus,
} = require('./controller');

const addSellerProductCategory = (req, res, next) => {
  doAddSellerProductCategory(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getSellerProductCategories = (req, res, next) => {
  doGetSellerProductCategories()
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const deleteSellerProductCategory = (req, res, next) => {
  doDeleteSellerProductCategory(req.body.id)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getSellerProductCategoriesBySellerId = (req, res, next) => {
  doGetSellerProductCategoriesBySellerId(req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getActiveSellerProductCategoriesBySeller = (req, res, next) => {
  doGetActiveSellerProductCategoriesBySellerId(req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
const changeSellerProductCategoryStatus = (req, res, next) => {
  doChangeSellerProductCategoryStatus(req.params, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

module.exports = {
  addSellerProductCategory,
  getSellerProductCategories,
  deleteSellerProductCategory,
  getSellerProductCategoriesBySellerId,
  getActiveSellerProductCategoriesBySeller,
  changeSellerProductCategoryStatus,
};
