const productcategory = require('../../../model/seller/productCategory/productCategory');
const { validateSellerProductCategory,validateSellerProductCategoryId} = require('../../../validations/seller/productCategory/validation');
const mongoose = require('mongoose');

const doAddSellerProductCategory = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const dataValidation = await validateSellerProductCategory(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      data.sellerId = user._id;
      const isTitleExist = await productcategory.findOne({
        productCategory: data.productCategory,
        sellerId: data.sellerId,
      });

      if (isTitleExist) {
        return resolve({ statusCode: 400, status: false, message: 'Product Category is already exist.' });
      }
      let schemaObj = new productcategory(data);
      schemaObj
        .save()
        .then(() => {
          return resolve({ statusCode: 200, status: true, message: 'Product Category created successfully.' });
        })
        .catch(async (error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};

// get active categories
const doGetSellerProductCategories = () => {
  return new Promise(async (resolve, reject) => {
    try {
      //get from db
      let categories = await productcategory.find({ isActive: true }, { productCategory: 1 }).sort({ _id: -1 });
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { categories } });
    } catch (error) {
      reject(error.message);
    }
  });
};

const doDeleteSellerProductCategory = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      //get from db
      let categories = await productcategory.findOne({ _id: mongoose.Types.ObjectId(data) });
      if (categories) {
        await productcategory
          .deleteOne({ _id: data })
          .then(() => {
            return resolve({ statusCode: 200, status: true, message: 'Category deleted succesfully' });
          })
          .catch((error) => {
            return resolve({ statusCode: 400, status: false, message: error, data: {} });
          });
      } else {
        return resolve({ statusCode: 400, status: false, message: 'Invalid id' });
      }
    } catch (error) {
      reject(error);
    }
  });
};

// get all product categories
const doGetSellerProductCategoriesBySellerId = (user) => {
  return new Promise(async (resolve, reject) => {
    try {
      console.log(user)
      const sellerId = user._id
      //get from db
      let categories = await productcategory
        .find({ sellerId:sellerId }, { productCategory: 1 ,isActive:1})
        .sort({ _id: -1 });
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { categories } });
    } catch (error) {
      reject(error.message);
    }
  });
};

// get active categories by seller
const doGetActiveSellerProductCategoriesBySellerId = (user) => {
  return new Promise(async (resolve, reject) => {
    try {
     
      const sellerId = user._id
      //get from db
      let categories = await productcategory
        .find({ isActive: true, sellerId:sellerId }, { productCategory: 1 })
        .sort({ _id: -1 });
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { categories } });
    } catch (error) {
      reject(error.message);
    }
  });
};

const doChangeSellerProductCategoryStatus = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateSellerProductCategoryId(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      const id = data.id;
      const sellerId = user._id;
      //get from db
      let productcategories = await productcategory.findOne({ _id: id, sellerId: sellerId });
      if (!productcategories) return resolve({ statusCode: 400, status: false, message: 'Invalid id' });

      let isActive = true;
      if (productcategories.isActive) {
        isActive = false;
      }
      //status update in db
      await productcategory
        .updateOne(
          { _id: id },
          {
            $set: {
              isActive: isActive,
            },
          }
        )
        .then(() => {
          return resolve({ statusCode: 200, status: true, message: 'Status changed successfully' });
        })
        .catch((error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  doAddSellerProductCategory,
  doGetSellerProductCategories,
  doDeleteSellerProductCategory,
  doGetSellerProductCategoriesBySellerId,
  doGetActiveSellerProductCategoriesBySellerId,
  doChangeSellerProductCategoryStatus
};
