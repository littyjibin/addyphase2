const {
  doGetOrdersbySellerId,
  doGetOrderbyId,
  doChangeOrderStatusById,
  doSellerChangeOrderPaymentStatusById,
  doConfirmOrderbyId,
  doReschedulepickupDateBySeller,
  doGetReturnOrdersBySeller,
  doGetReturnOrderbyId,
  doChangeReturnOrderStatusById
} = require('./controller');

const getOrdersbySellerId = (req, res, next) => {
  doGetOrdersbySellerId(req.params, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
const getOrderbyId = (req, res, next) => {
  doGetOrderbyId(req.params, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
const changeOrderStatusById = (req, res, next) => {
  doChangeOrderStatusById(req.body, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const sellerChangeOrderPaymentStatusById = (req, res, next) => {
  doSellerChangeOrderPaymentStatusById(req.body, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
const confirmOrderbyId = (req, res, next) => {
  doConfirmOrderbyId(req.body, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
const reschedulepickupDateBySeller = (req, res, next) => {
  doReschedulepickupDateBySeller(req?.body, req?.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getReturnOrdersBySeller = (req, res, next) => {
  doGetReturnOrdersBySeller(req?.body, req?.user)
    .then(({ statusCode = 200, message, status,data}) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getReturnOrderbyId = (req, res, next) => {
  doGetReturnOrderbyId(req?.params, req?.user)
    .then(({ statusCode = 200, message, status,data}) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const changeReturnOrderStatusById = (req, res, next) => {
  doChangeReturnOrderStatusById(req.body, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
module.exports = {
  getOrdersbySellerId,
  getOrderbyId,
  changeOrderStatusById,
  sellerChangeOrderPaymentStatusById,
  confirmOrderbyId,
  reschedulepickupDateBySeller,
  getReturnOrdersBySeller,
  getReturnOrderbyId,
  changeReturnOrderStatusById
};
