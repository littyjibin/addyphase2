const mongoose = require('mongoose');
const moment = require('moment-timezone');
const timeZoneValidation = require('../../../helpers/firebase/timeZoneValidation');

const order = require('../../../model/user/product/order');
const sellerCourierCompany = require('../../../model/seller/registration/sellerCourierCompanies');
const sellerDriver = require('../../../model/seller/registration/sellerDriver');
const pickupTime = require('../../../model/admin/master/pickupTime');
const orderReturn = require('../../../model/user/product/orderReturn');

const {
  addOrderHistoryDetails,
  getOrderHistoryByOrderId,
  isOrderValidBySeller,
} = require('../../../helpers/product/orderHelper');

const sendSms = require('../../../helpers/sms/sendSms');

const {
  validateGetOrder,
  validateGetOrderById,
  validateOrderStatusUpdate,
  validateConfirmOrder,
  validateReschedulePickupDate,
  validateReturnOrderStatusUpdate,
} = require('../../../validations/seller/orderValidation');
const Account = require('../../../model/Account');

// get active orders based on status
const doGetOrdersbySellerId = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateGetOrder(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      const userId = user._id;
      // let { status } = data;

      let status = data.status;
      let matchQuery = [{ isActive: true }, { sellerId: mongoose.Types.ObjectId(userId) }];

      let statuses = [
        'accepted',
        'confirmed',
        'deliveryAssigned',
        'driverAssigned',
        'driverRejected',
        'companyAssigned',
        'companyRejected',
        'startPickup',
        'attemptPickup',
        'failedToPickup',
        'completePickup',
        'startDelivery',
        'attemptDelivery',
        'failedToDelivery',
      ];

      let rejectedStatuses = ['cancelled', 'rejected'];

      if (status == 'accepted') {
        matchQuery.push({ orderStatus: { $in: statuses } });
      } else if (status == 'shipped') {
        matchQuery.push({ orderStatus: 'delivered' });
      } else if (status == 'rejected') {
        matchQuery.push({ orderStatus: { $in: rejectedStatuses } });
      } else {
        matchQuery.push({ orderStatus: status });
      }

      //get from db
      let orders = await order
        .aggregate([
          {
            $match: {
              $and: matchQuery,
            },
          },
          {
            $lookup: {
              from: 'accounts',
              localField: 'userId',
              foreignField: '_id',
              as: 'userDetails',
            },
          },
          {
            $project: {
              _id: 1,
              orderId: 1,
              isSellerChangedPaymentType: 1,
              noOfItems: 1,
              paymentType: 1,
              userId: 1,
              countryCode: { $first: '$products.countryCode' },
              orderStatus: 1,
              paymentStatus: 1,
              expectedDeliveryDate: 1,
              expectedDeliveryTime: '$expectedDeliveryTime.fromTime',
              deliveryDate: 1,
              total: '$cartDetails.grandTotal',
              name: { $concat: [{ $first: '$userDetails.firstName' }, ' ', { $first: '$userDetails.lastName' }] },
              imageList: {
                $map: {
                  input: '$products',
                  as: 'product',
                  in: {
                    image: '$$product.image',
                  },
                },
              },
            },
          },
        ])
        .sort({ expectedDeliveryDate: 1 });
      let orderCount = 0;
      if (orders.length) {
        orderCount = orders.length;
        orders.forEach((e) => {
          e.expectedDeliveryDate = {
            month: moment(e.expectedDeliveryDate).format('MMM'),
            day: moment(e.expectedDeliveryDate).format('DD'),
            year: moment(e.expectedDeliveryDate).format('YY'),
            time: e.expectedDeliveryTime,
          };
          // if order is rejected by seller then it set as false
          e.isSellerRejected = false;
          e.isPaymentPending = false;
          if (e.paymentStatus == 'pending' || e.paymentStatus == 'failed') e.isPaymentPending = true;
          if (status == 'shipped') {
            e.expectedDeliveryDate = {
              month: moment(e.deliveryDate).format('MMM'),
              day: moment(e.deliveryDate).format('DD'),
              year: moment(e.deliveryDate).format('YY'),
              time: moment(e.deliveryDate).format('hh:mm a'),
            };
          }
          if (status == 'rejected') {
            if (e.orderStatus == 'rejected') e.isSellerRejected = true;
          }
          delete e.expectedDeliveryTime;
          delete e.deliveryDate;
        });
      }

      return resolve({ statusCode: 200, status: true, message: 'Success', data: { orderCount, orders } });
    } catch (error) {
      reject(error);
    }
  });
};

// get orders by id
const doGetOrderbyId = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateGetOrderById(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      let today = moment().format('MMM DD,YYYY');
      let tomorrow = moment().add(1, 'days').format('MMM DD,YYYY');

      const sellerId = user._id;
      const { id } = data;

      //get from db
      let orders = await order.aggregate([
        {
          $match: {
            sellerId: mongoose.Types.ObjectId(sellerId),
            isActive: true,
            _id: mongoose.Types.ObjectId(id),
          },
        },
        {
          $lookup: {
            from: 'accounts',
            localField: 'userId',
            foreignField: '_id',
            as: 'userDetails',
          },
        },
        {
          $lookup: {
            from: 'sellersettings',
            localField: 'sellerId',
            foreignField: 'sellerId',
            as: 'settingsDetails',
          },
        },
        {
          $unwind: {
            path: '$settingsDetails',
            preserveNullAndEmptyArrays: true,
          },
        },
        // {
        //   $group: {
        //     _id: null,
        //     orderId: { $first: '$orderId' },
        //     noOfItems: { $first: '$noOfItems' },
        //     paymentType: { $first: '$paymentType' },
        //     userId: { $first: '$userId' },
        //     countryCode: { $first: '$countryCode' },
        //     products: {
        //       $push: {
        //         productName: '$products.productName',
        //         quantity: '$products.quantity',
        //       },
        //     },
        //   },
        // },

        {
          $project: {
            _id: 1,
            orderId: 1,
            noOfItems: 1,
            paymentType: 1,
            userId: 1,
            sellerCountryCode: 1,
            expectedDeliveryDate: 1,
            settingsDetails: {
              cashOnDelivery: { $ifNull: [{ $first: '$settingsDetails.payment.cashOnDelivery' }, true] },
              onlinePayment: { $ifNull: [{ $first: '$settingsDetails.payment.onlinePayment' }, false] },
            },
            expectedPickupDate: '$expectedPickupDate',
            expectedPickupTime: '$expectedPickupTime.fromTime',
            orderStatus: 1,
            paymentStatus: 1,
            isPickupCompleted: 1,
            pickupDate: 1,
            pickupType: { $ifNull: ['$pickupType', ''] },
            delivered: 1,
            deliveryDate: 1,
            expectedDeliveryTime: '$expectedDeliveryTime.fromTime',
            // expectedDeliveryTime: { $concat: ['$expectedDeliveryTime.fromTime', '-', '$expectedDeliveryTime.toTime'] },
            products: {
              $map: {
                input: '$products',
                as: 'product',
                in: {
                  productId: '$$product.productId',
                  productName: '$$product.productName',
                  image: '$$product.image',
                  instruction: '$$product.instruction',
                  uom: '$$product.uom',
                  uomValue: '$$product.uomValue',
                  color: '$$product.color',
                  quantity: '$$product.quantity',
                  price: '$$product.price',
                  total: '$$product.total',
                },
              },
            },
            itemTotal: { $ifNull: ['$cartDetails.itemTotal', 0] },
            deliveryFee: { $ifNull: ['$cartDetails.deliveryFee', 0] },
            vat: { $ifNull: ['$cartDetails.vat', 0] },
            grandTotal: { $ifNull: ['$cartDetails.grandTotal', 0] },
            customerDetails: {
              name: { $concat: [{ $first: '$userDetails.firstName' }, ' ', { $first: '$userDetails.lastName' }] },
              code: { $first: '$userDetails.code' },
              mobile: { $first: '$userDetails.mobile' },
              image: { $ifNull: [{ $first: '$userDetails.logo' }, ''] },
            },
          },
        },
      ]);
      if (!orders.length) return resolve({ statusCode: 400, status: false, message: 'Invalid id', data: {} });

      let orderDetails = orders[0];

      orderDetails.isReschedule = true;
      orderDetails.isCancel = true;
      orderDetails.isPaymentPending = false;
      orderDetails.isChange = true;
      if (orderDetails.paymentStatus == 'pending' || orderDetails.paymentStatus == 'failed')
        orderDetails.isPaymentPending = true;
      if (orderDetails.paymentType == 'online') orderDetails.isChange = false;

      orderDetails.expectedDeliveryDate = {
        month: moment(orderDetails.expectedDeliveryDate).format('MMM'),
        day: moment(orderDetails.expectedDeliveryDate).format('DD'),
        year: moment(orderDetails.expectedDeliveryDate).format('YY'),
        time: orderDetails.expectedDeliveryTime,
      };

      orderDetails.deliveryDate = {
        month: moment(orderDetails.deliveryDate).format('MMM'),
        day: moment(orderDetails.deliveryDate).format('DD'),
        year: moment(orderDetails.deliveryDate).format('YYYY'),
        time: moment(orderDetails.deliveryDate).format('hh:mm a'),
      };

      // check if pick up complated
      if (orderDetails.isPickupCompleted) {
        // orderDetails.pickupDate = moment(orderDetails.pickupDate).format('MMM DD,YYYY hh:mm a');

        orderDetails.pickupDate = {
          month: moment(orderDetails.pickupDate).format('MMM'),
          day: moment(orderDetails.pickupDate).format('DD'),
          year: moment(orderDetails.pickupDate).format('YY'),
          time: moment(orderDetails.pickupDate).format('hh:mm a'),
        };
        orderDetails.expectedPickupDate = {
          month: moment(orderDetails.expectedPickupDate).format('MMM'),
          day: moment(orderDetails.expectedPickupDate).format('DD'),
          year: moment(orderDetails.expectedPickupDate).format('YYYY'),
          time: orderDetails.expectedPickupTime,
        };

        // orderDetails.expectedPickupDate = moment(expectedPickup).format('MMM DD,YYYY');
        // orderDetails.expectedPickupDate = orderDetails.expectedPickupDate + ' ' + expectedPickupTime;
      }

      //
      if (!orderDetails.isPickupCompleted) {
        if (orderDetails.pickupType == '') {
          orderDetails.expectedPickupDate = {
            month: '',
            day: '',
            year: '',
            time: '',
          };
          orderDetails.pickupDate = {
            month: '',
            day: '',
            year: '',
            time: '',
          };
        } else {
          expectedPickup = moment(orderDetails.expectedPickupDate).format('MMM DD,YYYY ');

          orderDetails.expectedPickupDate = {
            month: moment(orderDetails.expectedPickupDate).format('MMM'),
            day: moment(orderDetails.expectedPickupDate).format('DD'),
            year: moment(orderDetails.expectedPickupDate).format('YYYY'),
            time: orderDetails.expectedPickupTime,
          };
          orderDetails.pickupDate = {
            month: '',
            day: '',
            year: '',
            time: '',
          };

          if (expectedPickup == today) {
            // orderDetails.expectedPickupDate = `Today - ${orderDetails.expectedPickupTime}`;
            orderDetails.expectedPickupDate = {
              month: '',
              day: 'Today',
              year: '',
              time: orderDetails.expectedPickupTime,
            };
          } else if (expectedPickup == tomorrow) {
            // orderDetails.expectedPickupDate = `Tomorrow - ${orderDetails.expectedPickupTime}`;
            orderDetails.expectedPickupDate = {
              month: '',
              day: 'Tomorrow',
              year: '',
              time: orderDetails.expectedPickupTime,
            };
          }
        }
        // orderDetails.pickupDate = moment(orderDetails.pickupDate).format('MMM DD,YYYY hh:mm a');
        // expectedPickup = moment(orderDetails.expectedPickupDate).format('MMM DD,YYYY ');
      }

      // orderDetails.deliveryDate = moment(orderDetails.deliveryDate).format('MMM DD,YYYY hh:mm a');

      delete orderDetails.expectedPickupTime;
      delete orderDetails.expectedDeliveryTime;

      orderDetails.isReschedule = true;
      orderDetails.isCancel = true;

      let statusReschedule = ['pending', 'accepted', 'confirmed', 'deliveryRejected'];

      // after assigned delivery seller cannot reschedule the pickup date
      if (!statusReschedule.includes(orderDetails.orderStatus)) {
        orderDetails.isReschedule = false;
        orderDetails.isCancel = false;
      }

      // Get order history by order id
      let orderHistory = await getOrderHistoryByOrderId(id);

      return resolve({ statusCode: 200, status: true, message: 'Success', data: { orderDetails, orderHistory } });
    } catch (error) {
      reject(error);
    }
  });
};

const doChangeOrderStatusById = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateOrderStatusUpdate(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      const { id, status } = data;
      const sellerId = user._id;

      let validOrder = await order.findOne({
        _id: mongoose.Types.ObjectId(id),
        sellerId: mongoose.Types.ObjectId(sellerId),
        isActive: true,
      });
      if (!validOrder) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid id' });
      }

      if (validOrder.orderStatus == status)
        return resolve({ statusCode: 400, status: false, message: 'Status already updated' });

      // let isSellerChangedPaymentType = false;
      // let paymentType = '';
      // let paymentStatus;
      // if (status == 'accepted') {
      //   if (validOrder.paymentType == 'cod') {
      //     if (data.isOnline == 'true') {
      //       isSellerChangedPaymentType = true;
      //       paymentType = 'online';
      //       paymentStatus = 'pending';
      //       let updateOrder = await order.updateOne(
      //         { _id: mongoose.Types.ObjectId(id) },
      //         {
      //           $set: { orderStatus: status, isSellerChangedPaymentType, paymentType, paymentStatus },
      //         }
      //       );
      //     }
      //   }
      // }

      order
        .updateOne(
          { _id: mongoose.Types.ObjectId(id) },
          {
            $set: { orderStatus: status },
          }
        )
        .then(async (response) => {
          let data = {
            id: id,
            orderId: validOrder.orderId,
            userId: validOrder.userId,
            sellerId: sellerId,
            status: status,
            notificationStatus: status,
          };
          // add confirmed order details to order history and notification
          const orderDetails = await addOrderHistoryDetails(data);

          return resolve({ statusCode: 200, status: true, message: 'Status updated successfully' });
        });
    } catch (error) {
      reject(error);
    }
  });
};

const doSellerChangeOrderPaymentStatusById = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateGetOrderById(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      const { id } = data;
      const sellerId = user._id;

      let validOrder = await order.findOne({
        _id: mongoose.Types.ObjectId(id),
        sellerId: mongoose.Types.ObjectId(sellerId),
        isActive: true,
      });
      if (!validOrder) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid id' });
      }

      let userDetails = await Account.findOne({
        _id: mongoose.Types.ObjectId(validOrder.userId),
        isActive: true,
      });
      if (!userDetails) {
        return resolve({ statusCode: 400, status: false, message: 'User does not exist' });
      }

      let isSellerChangedPaymentType = false;
      let paymentType = '';
      let paymentStatus;
      if (validOrder.isSellerChangedPaymentType)
        return resolve({ statusCode: 400, status: false, message: 'Seller already change the payment type.' });

      if (validOrder.paymentType == 'online')
        return resolve({ statusCode: 400, status: false, message: 'Online Payment cannot change' });

      isSellerChangedPaymentType = true;
      paymentType = 'online';
      paymentStatus = 'pending';
      order
        .updateOne(
          { _id: mongoose.Types.ObjectId(id) },
          {
            $set: { isSellerChangedPaymentType, paymentType, paymentStatus },
          }
        )
        .then(async (response) => {
          // if the user is not an addy user then a link will go to the web view
          // send link to driver about this order
          let payload = {
            origin: 'ADDYAE',
            destination: userDetails.code + userDetails.mobile,
            message: `${process.env.FRONTEND_URL}user/${id}/payment`,
          };
          console.log(payload);
          // await sendSms(payload)
          //   .then((_) => {
          //     return resolve({ statusCode: 200, status: true, message: 'Order confirmed successfully' });
          //   })
          //   .catch((error) => {
          //     return resolve({ statusCode: 400, status: false, message: error});
          //   });

          let data = {
            id: id,
            orderId: validOrder.orderId,
            userId: validOrder.userId,
            sellerId: sellerId,
            status: 'paymentModeChangedBySeller',
            notificationStatus: 'payment method changed to online by seller',
          };
          // add confirmed order details to order history and notification
          const orderDetails = await addOrderHistoryDetails(data);

          return resolve({ statusCode: 200, status: true, message: 'Payment status changed successfully' });
        });
    } catch (error) {
      reject(error);
    }
  });
};

const doConfirmOrderbyId = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateConfirmOrder(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      const sellerId = user._id;
      const id = data.id;

      // check valid order or not
      let validOrder = await order.findOne({
        _id: mongoose.Types.ObjectId(id),
        sellerId: mongoose.Types.ObjectId(sellerId),
        isActive: true,
      });
      if (!validOrder) return resolve({ statusCode: 400, status: false, message: 'Invalid id' });

      if (data.pickupType == 'schedulePickup') {
        if (!data.expectedPickupDate || !data.expectedPickupTime)
          return resolve({ status: false, message: 'Please add date and time', statusCode: 400 });

        // check the pick up date
        if (data.expectedPickupDate) {
          let pickupDate = moment(data.expectedPickupDate).format('YYYY-MM-DD');
          let today = moment().format('YYYY-MM-DD');
          if (pickupDate < today) {
            return resolve({ status: false, message: 'Invalid date', statusCode: 400 });
          }
        }
      }

      if (data.expectedPickupTime) {
        //  check the pick up time exist or not
        let isPickupTime = await pickupTime.findOne({
          _id: data.expectedPickupTime.pickupTimeId,
        });
        if (!isPickupTime) return resolve({ status: false, message: 'Invalid time.', statusCode: 400 });
      }

      // if (validOrder.orderStatus == 'pending')
      //   return resolve({ statusCode: 400, status: false, message: 'Please accept order first' });
      if (validOrder.orderStatus == 'confirmed')
        return resolve({ statusCode: 400, status: false, message: 'Order already confirmed' });

      // check delivery method exist or not
      let isCourierCompanyExist = await sellerCourierCompany.findOne(
        { sellerId: user._id, _id: data.deliveryMethodId },
        { _id: 1, courierCompany: 1 }
      );
      let isDriverExist = await sellerDriver.findById({ sellerId: sellerId, _id: data.deliveryMethodId });
      if (!isCourierCompanyExist && !isDriverExist)
        return resolve({ statusCode: 400, status: false, message: 'Invalid delivery method id' });

      let orderHistoryStatus = '';
      let driverCode = '';
      let driverMobile = '';
      if (isCourierCompanyExist) {
        data.selectedCompanyId = isCourierCompanyExist.courierCompany;
        orderHistoryStatus = 'assignedToCompany';
      }
      if (isDriverExist) {
        data.selectedDriverId = data.deliveryMethodId;
        orderHistoryStatus = 'assignedToDriver';
        driverCode = isDriverExist.code;
        driverMobile = isDriverExist.mobile;
      }

      if (isCourierCompanyExist) return resolve({ statusCode: 400, status: false, message: 'Courier company pending' });

      // let deliveryDate = moment(validOrder.expectedDeliveryDate).format('YYYY-MM-DD');
      // if(moment(pickupDate).isAfter(deliveryDate)){
      //   deliveryDate  = pickupDate
      // }

      let isSellerChangedPaymentType = false;
      let paymentType = validOrder.paymentType;
      let paymentStatus = '';
      if (validOrder.paymentType == 'cod') {
        if (data.isSellerChangedPaymentType) {
          isSellerChangedPaymentType = true;
          paymentType = 'online';
          paymentStatus = 'pending';
        }
      }

      let obj = '';
      obj = {
        pickupType: data.pickupType,
        selectedCompanyId: data.selectedCompanyId,
        selectedDriverId: data.selectedDriverId,
        packedProductImages: data.packedProductImages,
        orderStatus: 'confirmed',
        isSellerChangedPaymentType: isSellerChangedPaymentType,
        paymentType: paymentType,
        paymentStatus: paymentStatus,
      };
      if (data.pickupType == 'schedulePickup') {
        obj.expectedPickupDate = data.expectedPickupDate;
        obj.expectedPickupTime = data.expectedPickupTime;
      } else {
        obj.expectedPickupDate = moment();
        obj.expectedPickupTime = validOrder.expectedDeliveryTime;
      }

      order.updateOne({ _id: mongoose.Types.ObjectId(id) }, obj).then(async (response) => {
        // add order id to driver
        if (isDriverExist) await sellerDriver.updateOne({ _id: data.deliveryMethodId }, { $push: { orders: id } });

        let orderDetail = {
          id: id,
          orderId: validOrder.orderId,
          userId: validOrder.userId,
          sellerId: validOrder.sellerId,
          status: orderHistoryStatus,
          notificationStatus: 'confirmed',
        };

        // add confirmed order details to order history and notification
        const orderDetails = await addOrderHistoryDetails(orderDetail);

        // send link to driver about this order
        let payload = {
          origin: 'ADDYAE',
          destination: driverCode + driverMobile,
          message: `${process.env.FRONTEND_URL}user/${id}/seller`,
        };
        console.log(payload);
        // await sendSms(payload)
        //   .then((_) => {
        //     return resolve({ statusCode: 200, status: true, message: 'Order confirmed successfully' });
        //   })
        //   .catch((error) => {
        //     return resolve({ statusCode: 400, status: false, message: error});
        //   });

        let details = {
          orderId: validOrder.orderId,
        };
        return resolve({ statusCode: 200, status: true, message: 'Order confirmed successfully', data: details });
      });
    } catch (error) {
      reject(error);
    }
  });
};

// Reschedule pick up date by seller
const doReschedulepickupDateBySeller = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateReschedulePickupDate(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      const { id } = data;
      const userId = user._id;

      // check if order is valid or not
      let validOrder = await isOrderValidBySeller(id, userId);
      if (!validOrder) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid id' });
      }
      if (!validOrder.pickupType) {
        return resolve({
          statusCode: 400,
          status: false,
          message: 'Please confirm the order first.',
        });
      }

      if (validOrder.isDeliveryAssigned) {
        return resolve({
          statusCode: 400,
          status: false,
          message: 'You cannot change the pick date. Order already assigned.',
        });
      }

      if (data.expectedPickupTime) {
        //  check the pick up time exist or not
        let isPickupTime = await pickupTime.findOne({
          _id: data.expectedPickupTime.pickupTimeId,
        });
        if (!isPickupTime) return resolve({ status: false, message: 'Invalid time.', statusCode: 400 });
      }

      let obj = '';
      if (data.pickupType == 'schedulePickup') {
        let timeZone = timeZoneValidation(user.code);
        let pickupDay = moment(data.expectedPickupDate).tz(timeZone).format('YYYY-MM-DD');
        let today = moment().tz(timeZone).format('YYYY-MM-DD');
        if (pickupDay < today) {
          return resolve({ status: false, message: 'Invalid pickup date', statusCode: 400 });
        }
        obj = {
          pickupType: data.pickupType,
          expectedPickupDate: pickupDay,
          pickupDate: pickupDay,
          expectedPickupTime: data.expectedPickupTime,
        };
      } else {
        obj = {
          pickupType: data.pickupType,
          expectedPickupDate: moment().format('YYYY-MM-DD'),
          expectedPickupTime: data.expectedDeliveryTime,
        };
      }

      order
        .updateOne(
          { _id: mongoose.Types.ObjectId(id) },
          {
            $set: obj,
          }
        )
        .then(async (response) => {
          return resolve({ statusCode: 200, status: true, message: 'Pickup date updated successfully' });
        });
    } catch (error) {
      reject(error);
    }
  });
};

// get return orders based on seller
const doGetReturnOrdersBySeller = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const sellerId = user._id;

      //get from db
      let orders = await orderReturn
        .aggregate([
          {
            $match: {
              sellerId: mongoose.Types.ObjectId(sellerId),
            },
          },
          {
            $lookup: {
              from: 'accounts',
              localField: 'userId',
              foreignField: '_id',
              as: 'userDetails',
            },
          },
          {
            $project: {
              _id: 1,
              orderObjectId: 1,
              orderId: 1,
              returnItems: 1,
              paymentType: 1,
              userId: 1,
              countryCode: { $first: '$products.countryCode' },
              image: { $first: '$products.image' },
              status: 1,
              createdAt: 1,
              refundableAmount: 1,
              total: '$cartDetails.grandTotal',
              name: { $concat: [{ $first: '$userDetails.firstName' }, ' ', { $first: '$userDetails.lastName' }] },
            },
          },
        ])
        .sort({ createdAt: 1 });
      let orderCount = 0;
      if (orders.length) {
        orderCount = orders.length;
        orders.forEach((e) => {
          // e.createdAt = moment(e.createdAt).format('MMM DD,YY hh:mm a');
          e.createdAt = {
            month: moment(e.createdAt).format('MMM'),
            day: moment(e.createdAt).format('DD'),
            year: moment(e.createdAt).format('YY'),
            time: moment(e.createdAt).format('hh:mm a'),
          };
        });
      }

      return resolve({ statusCode: 200, status: true, message: 'Success', data: { orderCount, orders } });
    } catch (error) {
      reject(error);
    }
  });
};

// get orders by id
const doGetReturnOrderbyId = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateGetOrderById(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      const sellerId = user._id;
      const { id } = data;

      //get from db
      let orders = await orderReturn.aggregate([
        {
          $match: {
            sellerId: mongoose.Types.ObjectId(sellerId),
            isActive: true,
            _id: mongoose.Types.ObjectId(id),
          },
        },
        {
          $lookup: {
            from: 'accounts',
            localField: 'userId',
            foreignField: '_id',
            as: 'userDetails',
          },
        },
        {
          $lookup: {
            from: 'orders',
            localField: 'orderObjectId',
            foreignField: '_id',
            as: 'orderDetails',
          },
        },
        {
          $project: {
            _id: 1,
            orderId: 1,
            orderObjectId: 1,
            returnItems: 1,
            paymentType: 1,
            userId: 1,
            sellerId: 1,
            sellerCountryCode: { $first: '$orderDetails.sellerCountryCode' },
            status: 1,
            returnReason: 1,
            products: {
              $map: {
                input: '$products',
                as: 'product',
                in: {
                  productId: '$$product.productId',
                  productName: '$$product.productName',
                  image: '$$product.image',
                  instruction: '$$product.instruction',
                  uom: '$$product.uom',
                  uomValue: '$$product.uomValue',
                  color: '$$product.color',
                  quantity: '$$product.quantity',
                  price: '$$product.price',
                  total: '$$product.total',
                },
              },
            },
            itemTotal: { $ifNull: ['$refundableAmount', 0] },
            grandTotal: { $ifNull: ['$refundableAmount', 0] },
            customerDetails: {
              name: { $concat: [{ $first: '$userDetails.firstName' }, ' ', { $first: '$userDetails.lastName' }] },
              code: { $first: '$userDetails.code' },
              mobile: { $first: '$userDetails.mobile' },
              image: { $ifNull: [{ $first: '$userDetails.logo' }, ''] },
            },
          },
        },
      ]);

      if (!orders.length) return resolve({ statusCode: 400, status: false, message: 'Invalid id', data: {} });

      let orderDetails = orders[0];

      orderDetails.createdAt = moment(orderDetails.createdAt).format('MMM DD,YYYY hh:mm a');

      return resolve({ statusCode: 200, status: true, message: 'Success', data: { orderDetails } });
    } catch (error) {
      reject(error);
    }
  });
};

const doChangeReturnOrderStatusById = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateReturnOrderStatusUpdate(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      const { id, status } = data;
      const sellerId = user._id;

      let validOrder = await orderReturn.findOne({
        _id: mongoose.Types.ObjectId(id),
        sellerId: mongoose.Types.ObjectId(sellerId),
        isActive: true,
      });
      if (!validOrder) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid id' });
      }

      // if (validOrder.status == status)
      //   return resolve({ statusCode: 400, status: false, message: 'Status already updated' });

      orderReturn
        .updateOne({ _id: mongoose.Types.ObjectId(id) }, { $set: { status } })
        .then(async (response) => {
          // update order status in order collection
          await order.updateOne(
            { _id: mongoose.Types.ObjectId(validOrder.orderObjectId) },
            { $set: { orderStatus: status } }
          );
          let data = {
            id: validOrder.orderObjectId,
            orderId: validOrder.orderId,
            userId: validOrder.userId,
            sellerId: sellerId,
            status: status,
            notificationStatus: status,
          };

          // add confirmed order details to order history and notification
          const orderDetails = await addOrderHistoryDetails(data);

          return resolve({ statusCode: 200, status: true, message: 'Status updated successfully' });
        })
        .catch((error) => {
          console.log(error);
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  doGetOrdersbySellerId,
  doGetOrderbyId,
  doChangeOrderStatusById,
  doSellerChangeOrderPaymentStatusById,
  doConfirmOrderbyId,
  doReschedulepickupDateBySeller,
  doGetReturnOrdersBySeller,
  doGetReturnOrderbyId,
  doChangeReturnOrderStatusById,
};
