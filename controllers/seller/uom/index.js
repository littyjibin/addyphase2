const { doAddUom, doGetActiveUom,doAddUomValue, doGetActiveUomValueByUomId } = require('./controller');

const addUom = (req, res, next) => {
  doAddUom(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getActiveUom = (req, res, next) => {
    doGetActiveUom()
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const addUomValue = (req, res, next) => {
    doAddUomValue(req.body, req.user)
      .then(({ statusCode = 200, message, status }) => {
        res.status(statusCode).json({
          message,
          status,
        });
      })
      .catch((error) => {
        next(error);
      });
  };
  
  const getActiveUomValueByUomId = (req, res, next) => {
      doGetActiveUomValueByUomId(req.params.uomId)
      .then(({ statusCode = 200, message, status, data }) => {
        res.status(statusCode).json({
          message,
          status,
          ...(data && { data }),
        });
      })
      .catch((error) => {
        next(error);
      });
  };
  

module.exports = {
    addUom,
    getActiveUom,
    addUomValue,
    getActiveUomValueByUomId,
};
