const uom = require('../../../model/seller/uom/uom');
const uomValue = require('../../../model/seller/uom/uomValue');

const doAddUom = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
       
      const  uomTitle = data.uom;
      if(!uomTitle)
        return resolve({ statusCode: 400, status: false, message: 'Please enter uom' });

      //check if uom already exist
      const isUomExist = await uom.findOne({ uom:uomTitle });
      if (isUomExist) {
        return resolve({ statusCode: 400, status: false, message: 'Uom already exist.' });
      }

      //save to db
      let schemaObj = new uom({ uom:uomTitle });
      schemaObj
        .save()
        .then(() => {
          return resolve({ statusCode: 200, status: true, message: 'Uom added successfully.' });
        })
        .catch(async (error) => {
          return resolve({ statusCode: 400, status: false, message: error .message});
        });
    } catch (error) {
      reject(error);
    }
  });
};

// get active uom
const doGetActiveUom = () => {
  return new Promise(async (resolve, reject) => {
    try {
      //get from db
      let uoms = await uom.find({ isActive: true }, { uom:1 }).sort({ _id: -1 });
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { uoms } });
    } catch (error) {
      reject(error);
    }
  });
};


const doAddUomValue = (data, user) => {
    return new Promise(async (resolve, reject) => {
      try {
         
        const  uomValueTitle = data.uomValue;
        const  uomId = data.uomId;
        if(!uomId )
        return resolve({ statusCode: 400, status: false, message: 'Please enter uomId' });

        if(!uomValueTitle )
          return resolve({ statusCode: 400, status: false, message: 'Please enter uomValue' });

          //check if uom id exist
        const isUomExist = await uom.findOne({ _id:uomId });
        if (!isUomExist) {
          return resolve({ statusCode: 400, status: false, message: 'Invalid uom id.' });
        }  
  
        //check if uom value already exist
        const isUomValueExist = await uomValue.findOne({ uomValue:uomValueTitle });
        if (isUomValueExist) {
          return resolve({ statusCode: 400, status: false, message: 'Uom value already exist.' });
        }
  
        //save to db
        let schemaObj = new uomValue(data);
        schemaObj
          .save()
          .then(() => {
            return resolve({ statusCode: 200, status: true, message: 'Uom value added successfully.' });
          })
          .catch(async (error) => {
            return resolve({ statusCode: 400, status: false, message: error .message});
          });
      } catch (error) {
        reject(error);
      }
    });
  };
  
  // get active uom
  const doGetActiveUomValueByUomId = (uomId) => {
    return new Promise(async (resolve, reject) => {
      try {
        //get from db
        let uomValues = await uomValue.find({ isActive: true,uomId:uomId }, { uomValue:1 }).sort({ _id: -1 });
        return resolve({ statusCode: 200, status: true, message: 'Success', data: { uomValues } });
      } catch (error) {
        reject(error);
      }
    });
  };

module.exports = {
    doAddUom,
    doGetActiveUom,
    doAddUomValue,
    doGetActiveUomValueByUomId
};
