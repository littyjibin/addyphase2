const {
  doAddColorBySeller,
  doGetColors,
  doGetColorsBySeller,
  doGetSellerColorById,
  doDeleteSellerColorById,
  doUpdateSellerColorById,
  doGetActiveColorsByAdmin,
  doGetActivePickupTimeByAdmin
} = require('./controller');

const addColorBySeller = (req, res, next) => {
  doAddColorBySeller(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getColors = (req, res, next) => {
  doGetColors()
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getColorsBySeller = (req, res, next) => {
  doGetColorsBySeller(req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getSellerColorById = (req, res, next) => {
  doGetSellerColorById(req.params, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const deleteSellerColorById = (req, res, next) => {
  doDeleteSellerColorById(req.params, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const updateSellerColorById = (req, res, next) => {
  doUpdateSellerColorById(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getActiveColorsByAdmin = (req, res, next) => {
  doGetActiveColorsByAdmin()
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getActivePickupTimeByAdmin = (req, res, next) => {
  doGetActivePickupTimeByAdmin()
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

module.exports = {
  addColorBySeller,
  getColors,
  getColorsBySeller,
  getSellerColorById,
  deleteSellerColorById,
  updateSellerColorById,
  getActiveColorsByAdmin,
  getActivePickupTimeByAdmin
};
