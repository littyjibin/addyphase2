const sellerColor = require('../../../model/seller/colors/color');
const account = require('../../../model/Account');

const color = require('../../../model/admin/master/colors');
const pickupTime = require('../../../model/admin/master/pickupTime');

const {
  validateSellerColor,
  validateSellerColorById,
  validateUpdateSellerColorById,
} = require('../../../validations/seller/colorValidation');
const { identity } = require('lodash');

const doAddColorBySeller = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const dataValidation = await validateSellerColor(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      const { colorCode, colorName } = data;
      const sellerId = user._id;

      //  checking seller exist or not
      let isSellerExist = await account.findOne({
        _id: sellerId,
        isSeller: true,
        isActive: true,
      });
      if (!isSellerExist) return resolve({ statusCode: 200, status: false, message: 'Seller not found.' });

      //check if color code already exist
      let isColorCodeExist = await sellerColor.findOne({ colorCode, sellerId });
      if (isColorCodeExist) {
        return resolve({ statusCode: 200, status: false, message: 'Color code already exist.' });
      }

      //check if color name already exist
      let isColorNameExist = await sellerColor.findOne({ colorName, sellerId });
      if (isColorNameExist) {
        return resolve({ statusCode: 200, status: false, message: 'Color name already exist.' });
      }

      //save to db
      let schemaObj = new sellerColor({ colorCode, colorName, sellerId });
      schemaObj
        .save()
        .then(() => {
          return resolve({ statusCode: 200, status: true, message: 'Color added successfully.' });
        })
        .catch(async (error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};

// get active colours
const doGetColors = () => {
  return new Promise(async (resolve, reject) => {
    try {
      //get from db
      let colors = await sellerColor
        .find({ isActive: true }, { colorCode: 1, colorName: 1 })
        .sort({ _id: -1 })
        .populate({ path: 'sellerId', select: ['_id', 'firstName'] });
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { colors } });
    } catch (error) {
      reject(error);
    }
  });
};

// get active colours by seller
const doGetColorsBySeller = (user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const sellerId = user._id;

      //  checking seller exist or not
      let isSellerExist = await account.findOne({
        _id: sellerId,
        isSeller: true,
        isActive: true,
      });
      if (!isSellerExist) return resolve({ statusCode: 200, status: false, message: 'Seller not found.' });
      //get from db
      let colors = await sellerColor
        .find({ isActive: true, sellerId: sellerId }, { colorCode: 1, colorName: 1 })
        .sort({ _id: -1 });
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { colors } });
    } catch (error) {
      reject(error);
    }
  });
};

// get active colours by id
const doGetSellerColorById = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const dataValidation = await validateSellerColorById(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      const sellerId = user._id;

      //  checking seller exist or not
      let isSellerExist = await account.findOne({
        _id: sellerId,
        isSeller: true,
        isActive: true,
      });
      if (!isSellerExist) return resolve({ statusCode: 200, status: false, message: 'Seller not found.' });

      //get from db
      let color = await sellerColor
        .findOne({ sellerId: sellerId, _id: data.id }, { colorCode: 1, colorName: 1 })
        .sort({ _id: -1 });
      if (!color) return resolve({ statusCode: 200, status: false, message: 'Invalid id', data: {} });

      return resolve({ statusCode: 200, status: true, message: 'Success', data: { color } });
    } catch (error) {
      reject(error);
    }
  });
};

// delete  colour by id
const doDeleteSellerColorById = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const dataValidation = await validateSellerColorById(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      const sellerId = user._id;
      const id = data.id;

      //  checking seller exist or not
      let isSellerExist = await account.findOne({
        _id: sellerId,
        isSeller: true,
        isActive: true,
      });
      if (!isSellerExist) return resolve({ statusCode: 200, status: false, message: 'Seller not found.' });

      //check if color id is valid and delete
      const deleteColor = await sellerColor.findOneAndDelete({
        _id: id,
        sellerId,
      });

      if (deleteColor) {
        return resolve({ statusCode: 200, status: true, message: 'Color deleted successfully.' });
      } else {
        return resolve({ statusCode: 400, status: false, message: 'Invalid id.' });
      }
    } catch (error) {
      reject(error);
    }
  });
};

// update  colour by id
const doUpdateSellerColorById = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const dataValidation = await validateUpdateSellerColorById(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      const sellerId = user._id;
      const id = data.id;
      const { colorCode, colorName } = data;

      //check if color id is valid and delete
      const findColor = await sellerColor.findOne({
        _id: id,
        sellerId,
      });

      if (!findColor) return resolve({ statusCode: 200, status: false, message: 'Invalid id.' });

      //check if color code already exist
      let isColorCodeExist = await sellerColor.findOne({ colorCode, sellerId, _id: { $ne: id } });
      if (isColorCodeExist) {
        return resolve({ statusCode: 200, status: false, message: 'Color code already exist.' });
      }
      //check if color name already exist
      let isColorNameExist = await sellerColor.findOne({ colorName, sellerId  ,_id: { $ne: id }});
      if (isColorNameExist) {
        return resolve({ statusCode: 200, status: false, message: 'Color name already exist.' });
      }

      //update in db
      await sellerColor
        .updateOne({ _id: id }, data)
        .then(() => {
          return resolve({ statusCode: 200, status: true, message: 'Color updated successfully.' });
        })
        .catch((error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};

// get active colours by admin
const doGetActiveColorsByAdmin = (user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //const sellerId = user._id;

      //  checking seller exist or not
      // let isSellerExist = await account.findOne({
      //   _id: sellerId,
      //   isSeller: true,
      //   isActive: true,
      // });
      // if (!isSellerExist) return resolve({ statusCode: 200, status: false, message: 'Seller not found.' });
      //get from db
      let colors = await color
        .find({ isActive: true }, { colorCode: 1, colorName: 1 })
        .sort({ _id: -1 });
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { colors } });
    } catch (error) {
      reject(error);
    }
  });
};

// get active colours by admin
const doGetActivePickupTimeByAdmin = (user) => {
  return new Promise(async (resolve, reject) => {
    try {
    
      //get from db
      let pickupTimes = await pickupTime.find({ isActive: true }, {fromTime:1,toTime:1}).sort({ _id: -1 });
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { pickupTimes } });
   
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  doAddColorBySeller,
  doGetColors,
  doGetColorsBySeller,
  doGetSellerColorById,
  doDeleteSellerColorById,
  doUpdateSellerColorById,
  doGetActiveColorsByAdmin,
  doGetActivePickupTimeByAdmin
};
