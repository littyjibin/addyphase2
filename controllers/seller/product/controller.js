const mongoose = require('mongoose');
const product = require('../../../model/seller/product/product');
const productCategory = require('../../../model/seller/productCategory/productCategory');
const uom = require('../../../model/seller/uom/uom');
const account = require('../../../model/Account');
const sellerDetail = require('../../../model/seller/registration/sellerDetails');
const packagesize = require('../../../model/admin/master/packageSize');
const storeView = require('../../../model/user/product/storeViews');
const productView = require('../../../model/user/product/productView');
const order = require('../../../model/user/product/order');
const instagramImage = require('../../../model/seller/product/instagramImages');

const {
  validateSellerProduct,
  validateSellerMultipleProduct,
  validateUpdateSellerProduct,
  validateProductId,
  validateSellerInstagramImageDetails,
} = require('../../../validations/seller/product/validation');
const { uploadImage } = require('../../../utils/imageUpload');

// Add product -seller
const doAddSellerProduct = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateSellerProduct(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      let variantCount = 0;
      let variantDefaultTotal = 0;

      // check if product has varients
      if (data.variants) {
        data.isVariant = true;
        //console.log(data)

        for (item of data.variants) {
          if (item?.images) {
            let filterResult = item.images.filter((items) => {
              if (items.isDefault === true) {
                item.isDefaultVariant = true;
              }
              return items.isDefault === true;
            });
            variantCount = filterResult.length;
            variantDefaultTotal = variantDefaultTotal + variantCount;
          } else {
            return resolve({
              statusCode: 400,
              status: false,
              message: 'Please add atleast one image for product variant.',
            });
          }
        }
        if (variantDefaultTotal == 0 || variantDefaultTotal > 1)
          return resolve({ statusCode: 400, status: false, message: 'Please add one image as default.' });
      } else {
        if (data?.images) {
          // check the count of default image
          const count = data.images.filter((item) => item.isDefault === true).length;
          if (count == 0 || count > 1)
            return resolve({ statusCode: 400, status: false, message: 'Please add one as default image.' });
        } else {
          return resolve({ statusCode: 400, status: false, message: 'Please add atleast one image for product.' });
        }
      }

      //check if product already exist
      const isTitleExist = await product.findOne({ productName: data.productName, sellerId: user._id });
      if (isTitleExist) {
        return resolve({ statusCode: 400, status: false, message: 'Product already exist.' });
      }

      //  checking tagged categories exist or not
      let isCategoryExist = await productCategory.findOne({
        _id: { $in: data.taggedCategories },
        sellerId: user._id,
        isActive: true,
      });
      if (!isCategoryExist) return resolve({ statusCode: 400, status: false, message: 'Invalid tagged categories' });

      //  checking uom exist or not
      let isUomExist = await uom.findOne({
        _id: data.uom,
        isActive: true,
      });
      if (!isUomExist) return resolve({ statusCode: 400, status: false, message: 'Invalid uom ' });

      //  checking box size exist or not
      //  let isBoxSizeExist = await packagesize.findOne({
      //   _id: data.boxSize,
      //   isActive: true,
      // });
      // if (!isBoxSizeExist) return resolve({ statusCode: 400, status: false, message: 'Invalid box size' });

      let existingProduct = await product.findOne({}).sort({ _id: -1 });
      if (!existingProduct) {
        data.productId = 1;
      } else {
        let lastInsertedProductId = existingProduct.productId;
        data.productId = lastInsertedProductId + 1;
      }

      data.sellerId = user._id;
      //save to db
      let schemaObj = new product(data);
      schemaObj
        .save()
        .then(() => {
          return resolve({ statusCode: 200, status: true, message: 'Product added successfully.' });
        })
        .catch((error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};

// Add multiple products -seller
const doAddSellerMultipleProducts = (datas, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      // for (let data of datas) {
      //validate incoming data
      const dataValidation = await validateSellerMultipleProduct(datas);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      let multipleProducts = [];
      let productId = 1;
      let existingProduct = await product.findOne({}).sort({ _id: -1 });
      if (!existingProduct) {
        productId = productId;
      } else {
        let lastInsertedProductId = existingProduct.productId;
        productId = lastInsertedProductId + 1;
      }

      // get the product details
      const isTitleExist = await product.find({ sellerId: user._id });

      for (let data of datas) {
        // check cost price is greater or equal to selling price
        if (data.basicCostPrice < data.basicSellingPrice)
          return resolve({
            statusCode: 400,
            status: false,
            message: `Basic cost price is must be greater than or equal to basic selling price for product ${data.productName}.`,
          });

        // let variantCount = 0;
        // let variantDefaultTotal = 0;

        let imageCheck = false;
        // check if product has varients
        if (data.variants.length) {
          data.isVariant = true;
          for (item of data.variants) {
            // check cost price is greater or equal to selling price
            if (item.costPrice < item.sellingPrice)
              return resolve({
                statusCode: 400,
                status: false,
                message: `Cost price is must be greater than or equal to selling price for product ${data.productName} with variant ${item.uomValue}`,
              });

            if (item?.images) {
              // checking image is default or not
              for (let items of item.images) {
                if (items.isDefault === true) {
                  item.isDefaultVariant = true;
                  imageCheck = true;
                }
              }

              // let filterResult = item.images.filter((items) => {
              //   if (items.isDefault === true) {
              //     item.isDefaultVariant = true;
              //   }
              //   return items.isDefault === true;
              // });

              // variantCount = filterResult.length;
              // variantDefaultTotal = variantDefaultTotal + variantCount;
            } else {
              return resolve({
                statusCode: 400,
                status: false,
                message: 'Please add atleast one image for  variant in product ${data.productName}.',
              });
            }
            if (imageCheck) break;
          }
          // if (variantDefaultTotal == 0 || variantDefaultTotal > 1)
          //   return resolve({
          //     statusCode: 400,
          //     status: false,
          //     message: `Please add one default image for product ${data.productName}.`,
          //   });
        } else {
          if (data?.images) {
            // check the count of default image
            const count = data.images.filter((item) => item.isDefault === true).length;
            if (count == 0 || count > 1)
              return resolve({
                statusCode: 400,
                status: false,
                message: `Please add one default image for product ${data.productName}`,
              });
          } else {
            return resolve({
              statusCode: 400,
              status: false,
              message: `Please add atleast one image for product ${data.productName}`,
            });
          }
        }
        //check if product already exist
        // const isTitleExist = await product.findOne({ productName: data.productName, sellerId: user._id });
        const isFound = isTitleExist.some((el) => el.productName === data.productName);
        if (isFound) {
          return resolve({ statusCode: 400, status: false, message: `${data.productName} already exist.` });
        }

        //  checking tagged categories exist or not
        let isCategoryExist = await productCategory.findOne({
          _id: { $in: data.taggedCategories },
          sellerId: user._id,
          isActive: true,
        });
        if (!isCategoryExist)
          return resolve({
            statusCode: 400,
            status: false,
            message: `Please add category for product ${data.productName}`,
            // message: `Invalid tagged categories for product ${data.productName}`,
          });

        //  checking uom exist or not
        let isUomExist = await uom.findOne({
          _id: data.uom,
          isActive: true,
        });
        if (!isUomExist)
          return resolve({ statusCode: 400, status: false, message: `Invalid uom for product ${data.productName}` });

        data.productId = productId;
        data.sellerId = user._id;
        multipleProducts.push(data);
        productId++;
      }

      //save to db
      product
        .insertMany(multipleProducts)
        .then(() => {
          console.log('success');
          return resolve({ statusCode: 200, status: true, message: 'Products added successfully.' });
        })
        .catch((error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};

// update product -seller
const doUpdateSellerProduct = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateUpdateSellerProduct(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      // check if product has varients
      if (data.variants.length) {
        data.isVariant = true;
        for (item of data.variants) {
          let imageCheck = false;
          // check cost price is greater or equal to selling price
          if (item.costPrice < item.sellingPrice)
            return resolve({
              statusCode: 400,
              status: false,
              message: `Cost price is must be greater than or equal to selling price for product ${data.productName} with variant ${item.uomValue}`,
            });

          if (item?.images) {
            // checking image is default or not
            for (let items of item.images) {
              if (items.isDefault === true) {
                imageCheck = true;
                item.isDefaultVariant = true;
              }
            }
          } else {
            return resolve({
              statusCode: 400,
              status: false,
              message: 'Please add atleast one image for  variant in product ${data.productName}.',
            });
          }

          if (imageCheck) break;
        }
      } else {
        if (data?.images) {
          // check the count of default image
          const count = data.images.filter((item) => item.isDefault === true).length;

          if (count == 0 || count > 1)
            return resolve({
              statusCode: 400,
              status: false,
              message: `Please add one default image for product ${data.productName}`,
            });
        } else {
          return resolve({
            statusCode: 400,
            status: false,
            message: `Please add atleast one image for product ${data.productName}`,
          });
        }
      }

      const isProductExist = await product.findOne({ _id: data.id, sellerId: user._id });
      if (!isProductExist) return resolve({ statusCode: 400, status: false, message: 'Product does not exist.' });

      //  checking tagged categories exist or not
      let isCategoryExist = await productCategory.findOne({
        _id: { $in: data.taggedCategories },
        sellerId: user._id,
        isActive: true,
      });
      if (!isCategoryExist) return resolve({ statusCode: 400, status: false, message: 'Invalid tagged categories' });

      //  checking uom exist or not
      let isUomExist = await uom.findOne({
        _id: data.uom,
        isActive: true,
      });
      if (!isUomExist) return resolve({ statusCode: 400, status: false, message: 'Invalid uom ' });

      //check if product with  this title already exist
      const isNameExist = await product.findOne({
        productName: data.productName,
        sellerId: user._id,
        _id: { $ne: data.id },
      });

      if (isNameExist) {
        return resolve({ statusCode: 400, status: false, message: 'Product with this title already exist.' });
      }

      //update in db
      await product
        .updateOne({ _id: data.id }, data)
        .then(() => {
          return resolve({ statusCode: 200, status: true, message: 'Product updated successfully.' });
        })
        .catch((error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error.message);
    }
  });
};

// add product image -seller
const doAddSellerProductImage = (image) => {
  return new Promise(async (resolve, reject) => {
    try {
      //check if image is available
      if (!image) {
        return resolve({ statusCode: 400, status: false, message: 'Image not found.' });
      }

      let imageUrl;
      let fileName = `products/${new Date().getTime()}_${image.name}`;

      //user image here is folder name
      await uploadImage(image.data, fileName).then(({ url }) => {
        imageUrl = url;
      });

      return resolve({ status: true, message: 'image saved to cdn successfully', data: imageUrl });
    } catch (error) {
      reject(error);
    }
  });
};

// get product by id  - seller
const doGetSellerProductsById = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateProductId(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      const id = data.id;
      let products = await product.aggregate([
        {
          $match: {
            _id: mongoose.Types.ObjectId(data.id),
          },
        },
        {
          $lookup: {
            from: 'uoms',
            localField: 'uom',
            foreignField: '_id',
            as: 'uomDetails',
          },
        },
        {
          $lookup: {
            from: 'productcategories',
            localField: 'taggedCategories',
            foreignField: '_id',
            as: 'categories',
          },
        },
        {
          $project: {
            _id: 1,
            productId: 1,
            productName: 1,
            countryCode: 1,
            description: 1,
            defaultImage: 1,
            taggedCategory: {
              id: { $first: '$categories._id' },
              name: { $first: '$categories.productCategory' },
            },
            uom: {
              id: { $first: '$uomDetails._id' },
              name: { $first: '$uomDetails.uom' },
            },
            height: 1,
            width: 1,
            length: 1,
            weight: 1,
            piece: 1,
            basicCostPrice: 1,
            basicSellingPrice: 1,
            basicCostPrice: 1,
            basicSellingPrice: 1,
            images: 1,
            isActiveProduct: 1,
            isVariant: 1,
            isDeliveryVerification: 1,
            variants: 1,
            isActiveProduct: 1,
            isVariant: 1,
            isDeliveryVerification: 1,
            type: 1,
            images: 1,
          },
        },

      ]);
  

      if (!products.length) return resolve({ statusCode: 400, status: false, message: 'Invalid id', data: {} });

      products = products[0];
      let newVariantArray = [];
      for (let variant of products.variants) {
        let index = newVariantArray.findIndex((i) => i.uomValue == variant.uomValue);
        if (index != -1) {
          //  let index = newVariantArray.findIndex((i)=>i.uomValue==variant.uomValue);
          let tempObj = {
            images: variant.images,
            colorName: variant.colorName,
            colorCode: variant.colorCode,
            isDefaultVariant: variant.isDefaultVariant,
            _id: variant._id,
          };
          newVariantArray[index].color.push(tempObj);
        } else {
          let tempObj = {
            uomValue: variant.uomValue,
            costPrice: variant.costPrice,
            sellingPrice: variant.sellingPrice,
            isActiveVariant: variant.isActiveVariant,
            color: [
              {
                images: variant.images,
                colorName: variant.colorName,
                colorCode: variant.colorCode,
                isDefaultVariant: variant.isDefaultVariant,
                _id: variant._id,
              },
            ],
          };

          newVariantArray.push(tempObj);
        }
      }

      products.variants = newVariantArray;
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { products } });
    } catch (error) {
      reject(error);
    }
  });
};

// get all products by seller id  - seller
const doGetAllProductsBySeller = (user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const sellerId = user._id;
      //get from db
      let products = await product
        .aggregate([
          {
            $match: {
              sellerId: mongoose.Types.ObjectId(sellerId),
            },
          },
          {
            $unwind: {
              path: '$variants',
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $lookup: {
              from: 'productcategories',
              localField: 'taggedCategories',
              foreignField: '_id',
              as: 'productCategories',
            },
          },
          {
            $project: {
              productId: 1,
              sellerId: 1,
              productName: 1,
              price: '$basicSellingPrice',
              countryCode: 1,
              isActiveProduct: 1,
              productCategory: { $first: '$productCategories.productCategory' },
              image: '$defaultImage',
            },
          },
        ])
        .sort({ _id: -1 });
      for (item of products) {
        item.url = `${process.env.FRONTEND_URL}product/${item._id}/${item.sellerId}`;
        delete item.sellerId;
      }

      return resolve({ statusCode: 200, status: true, message: 'Success', data: { products } });
    } catch (error) {
      reject(error);
    }
  });
};

const doChangeSellerProductStatus = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateProductId(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      const id = data.id;
      const sellerId = user._id;
      //get from db
      let products = await product.findOne({ _id: id, sellerId: sellerId });
      if (!products) return resolve({ statusCode: 400, status: false, message: 'Invalid id' });

      let isActiveProduct = true;
      if (products.isActiveProduct) {
        isActiveProduct = false;
      }
      //status update in db
      await product
        .updateOne(
          { _id: id },
          {
            $set: {
              isActiveProduct: isActiveProduct,
            },
          }
        )
        .then(() => {
          return resolve({ statusCode: 200, status: true, message: 'Status changed successfully' });
        })
        .catch((error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};

const doDeleteSellerProductById = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateProductId(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      const id = data.id;
      const sellerId = user._id;
      //get from db
      let products = await product.findOne({ _id: id, sellerId: sellerId });
      if (!products) return resolve({ statusCode: 400, status: false, message: 'Invalid id' });

      //  checking product exist in order
      let isOrderExist = await order.findOne({
        'products.productId': id,
      });

      if (isOrderExist) {
        return resolve({
          statusCode: 400,
          status: false,
          message: 'You cannot delete the product. Because order exist for this product.',
        });
      }
      await product
        .deleteOne({ _id: id })
        // await product
        // .updateOne(
        //   { _id: id },
        //   {
        //     $set: {
        //       isActiveProduct: fa,
        //     },
        //   }
        // )
        .then(() => {
          return resolve({ statusCode: 200, status: true, message: 'Product deleted succesfully' });
        })
        .catch((error) => {
          return resolve({ statusCode: 400, status: false, message: error, data: {} });
        });
    } catch (error) {
      reject(error);
    }
  });
};

const doAddSellerInstagramImageDetails = (datas, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const dataValidation = await validateSellerInstagramImageDetails(datas);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      let sellerId = user._id;
      let instagramImages = [];
      // let instaImageDetails = await instagramImage.find({
      //   sellerId,
      // });

      for (let data of datas) {
        data.sellerId = sellerId;
        // console.log(data)
        let imageDetails = data.imageDetails;
        let mediaIds = [];
        for (let image of imageDetails) {
          mediaIds.push(image.mediaId);
        }
        // console.log(mediaIds)
        let instaImageDetails = await instagramImage.findOne({
          sellerId,
          'imageDetails.mediaId': { $in: mediaIds },
        });
        // console.log(instaImageDetails)
        if (!instaImageDetails) {
          instagramImages.push(data);
        }
      }
      // return resolve({ statusCode: 200, status: true, message: 'Instagram images added successfully.' });
      instagramImage
        .insertMany(instagramImages)
        .then(async (response) => {
          // test
          let instaImageDetails = await instagramImage.find({
            sellerId,
          });
          return resolve({
            statusCode: 200,
            status: true,
            message: 'Instagram images synchronized.',
            data: { instaImageDetails },
          });
          // let instagramDetails = [];
          // if (response.length) {
          //   for (let item of response) {
          //     instagramDetails.push({
          //       imageDetails: item.imageDetails,
          //       mediaType: item.mediaType,
          //       mediaCation: item.mediaCaption,
          //     });
          //   }
          //   return resolve({
          //     statusCode: 200,
          //     status: true,
          //     message: 'Instagram images added successfully.',
          //     data: { instagramDetails },
          //   });
          // } else {
          //   return resolve({ statusCode: 200, status: true, message: 'Instagram already synchronized.', data: {} });
          // }
        })
        .catch((error) => {
          return resolve({ statusCode: 400, status: false, message: error, data: {} });
        });
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  doAddSellerProduct,
  doAddSellerMultipleProducts,
  doUpdateSellerProduct,
  doAddSellerProductImage,
  doGetSellerProductsById,
  doGetAllProductsBySeller,
  doChangeSellerProductStatus,
  doDeleteSellerProductById,
  doAddSellerInstagramImageDetails,
};
