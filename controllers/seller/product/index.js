const {
  doAddSellerProduct,
  doAddSellerMultipleProducts,
  doUpdateSellerProduct,
  doGetSellerProductsById,
  doGetAllProductsBySeller,
  doChangeSellerProductStatus,
  doDeleteSellerProductById,
  doAddSellerProductImage,
  doAddSellerInstagramImageDetails
} = require('./controller');

const addSellerProduct = (req, res, next) => {
  doAddSellerProduct(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const addSellerMultipleProducts = (req, res, next) => {
  doAddSellerMultipleProducts(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getSellerProductById = (req, res, next) => {
  doGetSellerProductsById(req.params)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
const getAllProductsBySeller = (req, res, next) => {
  doGetAllProductsBySeller(req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const updateSellerProduct = (req, res, next) => {
  doUpdateSellerProduct(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const changeSellerProductStatus = (req, res, next) => {
  doChangeSellerProductStatus(req.params, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
const deleteSellerProductById = (req, res, next) => {
  doDeleteSellerProductById(req.params, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const addSellerProductImage = (req, res, next) => {
  doAddSellerProductImage(req?.files?.image)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};



const addSellerInstagramImageDetails = (req, res, next) => {
  doAddSellerInstagramImageDetails(req?.body,req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

module.exports = {
  addSellerProduct,
  addSellerMultipleProducts,
  updateSellerProduct,
  getSellerProductById,
  getAllProductsBySeller,
  changeSellerProductStatus,
  deleteSellerProductById,
  addSellerProductImage,
  addSellerInstagramImageDetails
};
