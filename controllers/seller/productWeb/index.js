const {
    doGetAllActiveProductsBySellerId,
    doGetSellerDetailsWebBySellerId,
    doGetSellerDetailsWebByStoreUrl,
    doGetAllActiveProductsBySellerCategoryId,
    doGetActiveProductById,
    doFilterActiveProductBySellerId,
    doGetFilterMethodsBySellerId,
    doGetActiveSellerProductCategoriesWebBySellerId
  } = require('./controller');
  
  const getAllActiveProductsBySellerId = (req, res, next) => {
    doGetAllActiveProductsBySellerId(req.body)
      .then(({ statusCode = 200, message, status, data }) => {
        res.status(statusCode).json({
          message,
          status,
          ...(data && { data }),
        });
      })
      .catch((error) => {
        next(error);
      });
  };
  const getSellerDetailsWebBySellerId = (req, res, next) => {
    doGetSellerDetailsWebBySellerId(req.body,req.user)
      .then(({ statusCode = 200, message, status, data }) => {
        res.status(statusCode).json({
          message,
          status,
          ...(data && { data }),
        });
      })
      .catch((error) => {
        next(error);
      });
  };
  const getSellerDetailsWebByStoreUrl = (req, res, next) => {
    doGetSellerDetailsWebByStoreUrl(req.body,req.user)
      .then(({ statusCode = 200, message, status, data }) => {
        res.status(statusCode).json({
          message,
          status,
          ...(data && { data }),
        });
      })
      .catch((error) => {
        next(error);
      });
  };
  const getAllActiveProductsBySellerCategoryId = (req, res, next) => {
    doGetAllActiveProductsBySellerCategoryId(req.body,req.user)
      .then(({ statusCode = 200, message, status, data }) => {
        res.status(statusCode).json({
          message,
          status,
          ...(data && { data }),
        });
      })
      .catch((error) => {
        next(error);
      });
  };
  const getActiveProductById = (req, res, next) => {
    doGetActiveProductById(req.body,req.user)
      .then(({ statusCode = 200, message, status, data }) => {
        res.status(statusCode).json({
          message,
          status,
          ...(data && { data }),
        });
      })
      .catch((error) => {
        next(error);
      });
  };
  const filterActiveProductBySellerId = (req, res, next) => {
    doFilterActiveProductBySellerId(req.body)
      .then(({ statusCode = 200, message, status, data }) => {
        res.status(statusCode).json({
          message,
          status,
          ...(data && { data }),
        });
      })
      .catch((error) => {
        next(error);
      });
  };
  
  const getFilterMethodsBySellerId = (req, res, next) => {
    doGetFilterMethodsBySellerId(req.body,req.user)
      .then(({ statusCode = 200, message, status, data }) => {
        res.status(statusCode).json({
          message,
          status,
          ...(data && { data }),
        });
      })
      .catch((error) => {
        next(error);
      });
  };
  const getActiveSellerProductCategoriesWebBySellerId = (req, res, next) => {
    doGetActiveSellerProductCategoriesWebBySellerId(req.params,req.user)
      .then(({ statusCode = 200, message, status, data }) => {
        res.status(statusCode).json({
          message,
          status,
          ...(data && { data }),
        });
      })
      .catch((error) => {
        next(error);
      });
  };
  module.exports = {
    getAllActiveProductsBySellerId,
    getSellerDetailsWebBySellerId,
    getSellerDetailsWebByStoreUrl,
    getFilterMethodsBySellerId,
    getAllActiveProductsBySellerCategoryId,
    getActiveProductById,
    filterActiveProductBySellerId ,
    getActiveSellerProductCategoriesWebBySellerId

    
  };
  