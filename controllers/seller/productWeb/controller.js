const mongoose = require('mongoose');
const product = require('../../../model/seller/product/product');
const productCategory = require('../../../model/seller/productCategory/productCategory');
const account = require('../../../model/Account');
const sellerDetail = require('../../../model/seller/registration/sellerDetails');
const storeView = require('../../../model/user/product/storeViews');
const productView = require('../../../model/user/product/productView');


// get all active products by seller id -web
const doGetAllActiveProductsBySellerId = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!data.sellerId) return resolve({ statusCode: 400, status: false, message: 'Please add sellerId', data: {} });

      //  checking seller exist or not
      let isSellerExist = await account.findOne({
        _id: data.sellerId,
        isSeller: true,
        isActive: true,
      });
      if (!isSellerExist)
        return resolve({ statusCode: 400, status: false, message: 'The seller id you entered was not found.' });

      let sellerDetails = await sellerDetail.aggregate([
        {
          $match: {
            sellerId: mongoose.Types.ObjectId(data.sellerId),
            isActive: true,
          },
        },
        {
          $lookup: {
            from: 'accounts',
            localField: 'addressId',
            foreignField: 'address._id',
            as: 'userDetails',
          },
        },
        {
          $unwind: {
            path: '$userDetails',
          },
        },
        {
          $project: {
            sellerId: 1,
            firstName: '$userDetails.firstName',
            lastName: '$userDetails.lastName',
            countryCode: '$userDetails.countryCode',
            code: '$userDetails.code',
            mobile: '$userDetails.mobile',
            address: {
              $filter: {
                input: '$userDetails.address',
                as: 'userAddress',
                cond: { $eq: ['$$userAddress._id', '$addressId'] },
              },
            },
            _id: 1,
            businessName: 1,
            logo: 1,
            colorName: 1,
            colorCode: 1,
          },
        },
        {
          $project: {
            _id: 1,
            sellerId: 1,
            businessName: 1,
            logo: 1,
            colorName: 1,
            colorCode: 1,
            firstName: 1,
            lastName: 1,
            code: 1,
            mobile: 1,
            countryCode: 1,
            address: { $first: '$address.address' },
            houseNumber: { $first: '$address.houseNumber' },
            street: { $first: '$address.street' },
            area: { $first: '$address.area' },
            latitude: { $first: '$address.location.latitude' },
            longitude: { $first: '$address.location.longitude' },
          },
        },
      ]);

      if (!sellerDetails)
        return resolve({ statusCode: 400, status: false, message: 'The seller id you entered was not found.' });

      // Add the view count of store
      let isStoreExist = await storeView.findOne({
        sellerId: data.sellerId,
      });

      if (!isStoreExist) {
        let schemaObj = new storeView({ sellerId: data.sellerId, viewCount: 1 }).save();
      } else {
        await storeView.findOneAndUpdate(
          { sellerId: mongoose.Types.ObjectId(data.sellerId) },
          {
            $inc: { viewCount: 1 },
          }
        );
      }

      let userDetails = {};

      if (data.userId) {
        //  checking user exist
        let isUserExist = await account.findOne({
          _id: data.userId,
        });
        if (isUserExist) userDetails.countryCode = isUserExist.countryCode;
      }

      //get from db
      let products = await product
        .aggregate([
          {
            $match: {
              isActiveProduct: true,
              sellerId: mongoose.Types.ObjectId(data.sellerId),
            },
          },
          {
            $unwind: {
              path: '$variants',
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $project: {
              productName: 1,
              basicSellingPrice: 1,
              isVariant: 1,
              countryCode: 1,
              images: {
                $filter: {
                  input: '$images',
                  as: 'item',
                  cond: { $eq: ['$$item.isDefault', true] },
                },
              },
              sellingPrice: '$variants.sellingPrice',
              variantImages: {
                $filter: {
                  input: '$variants.images',
                  as: 'items',
                  cond: { $eq: ['$$items.isDefault', true] },
                },
              },
            },
          },
          {
            $group: {
              _id: '$_id',
              image: { $first: '$images.imageUrl' },
              isVariant: { $first: '$isVariant' },
              countryCode: { $first: '$countryCode' },
              productName: { $first: '$productName' },
              price: { $first: '$basicSellingPrice' },
              variantImages: { $push: { variantImage: '$variantImages.imageUrl', price: '$sellingPrice' } },
            },
          },
        ])
        .sort({ _id: -1 });
      for (item of products) {
        if (item.isVariant) {
          for (image of item.variantImages) {
            if (image.variantImage.length) {
              item.image = image.variantImage[0];
              item.price = image.price;
              delete item.variantImages;
              break;
            }
          }
        } else {
          delete item.variantImages;
          item.image = item.image[0];
        }
      }

      // pagination
      let limit = 30;
      let page = 0;
      if (data.limit) {
        limit = parseInt(data.limit);
      }

      if (data.page) {
        page = parseInt(data.page) - 1;
      }

      let nextPage = false;
      let start = page * limit;
      let end = page * limit + limit;
      let productDetails = products.slice(start, end);
      if (products.length > end) {
        nextPage = true;
      } else {
        nextPage = false;
      }
      if (products.length == 0) {
        nextPage = false;
      }
      let totalPage = Math.ceil(products.length / limit);
      return resolve({
        statusCode: 200,
        status: true,
        message: 'Success',
        data: {
          sellerDetails: sellerDetails[0],
          userDetails: userDetails,
          products: {
            totalPage,
            nextPage,
            productDetails,
          },
        },
      });

      // return resolve({ statusCode: 200, status: true, message: 'Success', data: { products } });
    } catch (error) {
      reject(error);
    }
  });
};

// get details by seller id - web
const doGetSellerDetailsWebBySellerId = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!data.sellerId) return resolve({ statusCode: 400, status: false, message: 'Please add sellerId', data: {} });

      //  checking seller exist or not
      let isSellerExist = await account.findOne({
        _id: data.sellerId,
        isSeller: true,
        isActive: true,
      });
      if (!isSellerExist)
        return resolve({ statusCode: 400, status: false, message: 'The seller id you entered was not found.' });

      let sellerDetails = await sellerDetail.aggregate([
        {
          $match: {
            sellerId: mongoose.Types.ObjectId(data.sellerId),
            isActive: true,
          },
        },
        {
          $lookup: {
            from: 'accounts',
            localField: 'addressId',
            foreignField: 'address._id',
            as: 'userDetails',
          },
        },
        {
          $unwind: {
            path: '$userDetails',
          },
        },
        {
          $lookup: {
            from: 'sellersettings',
            localField: 'sellerId',
            foreignField: 'sellerId',
            as: 'settingsDetails',
          },
        },
        {
          $unwind: {
            path: '$settingsDetails',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $project: {
            sellerId: 1,
            firstName: '$userDetails.firstName',
            lastName: '$userDetails.lastName',
            countryCode: '$userDetails.countryCode',
            code: '$userDetails.code',
            mobile: '$userDetails.mobile',
            address: {
              $filter: {
                input: '$userDetails.address',
                as: 'userAddress',
                cond: { $eq: ['$$userAddress._id', '$addressId'] },
              },
            },
            _id: 1,
            businessName: 1,
            logo: 1,
            colorName: 1,
            colorCode: 1,
            payment: '$settingsDetails.payment',
            privacy: '$settingsDetails.privacy',
            businessDetails: '$settingsDetails.businessDetails',
            commercialRegNum: 1,
            taxNumber: 1,
            vat: 1,
            returnPolicyDays: 1,
          },
        },
        {
          $project: {
            _id: 1,
            sellerId: 1,
            businessName: 1,
            logo: 1,
            colorName: 1,
            colorCode: 1,
            firstName: 1,
            lastName: 1,
            code: 1,
            mobile: 1,
            countryCode: 1,
            address: { $first: '$address.address' },
            houseNumber: { $first: '$address.houseNumber' },
            street: { $first: '$address.street' },
            area: { $first: '$address.area' },
            latitude: { $first: '$address.location.latitude' },
            longitude: { $first: '$address.location.longitude' },
            payment: { $first: '$payment' },
            privacy: { $first: '$privacy' },
            businessDetails: { $first: '$businessDetails' },
            commercialRegNum: 1,
            taxNumber: 1,
            vat: 1,
            returnPolicyDays: 1,
          },
        },
      ]);

      if (!sellerDetails.length)
        return resolve({ statusCode: 400, status: false, message: 'The seller id you entered was not found.' });

      return resolve({
        statusCode: 200,
        status: true,
        message: 'Success',
        data: {
          sellerDetails: sellerDetails[0],
        },
      });
    } catch (error) {
      reject(error);
    }
  });
};

// get details by seller store url - web
const doGetSellerDetailsWebByStoreUrl = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!data.storeUrl) return resolve({ statusCode: 400, status: false, message: 'Please enter store url', data: {} });
     
     
      let sellerDetails = await sellerDetail.aggregate([
        {
          $match: {
            storeUrl:data.storeUrl,
            isActive: true,
          },
        },
        {
          $lookup: {
            from: 'accounts',
            localField: 'addressId',
            foreignField: 'address._id',
            as: 'userDetails',
          },
        },
        {
          $unwind: {
            path: '$userDetails',
          },
        },
        {
          $lookup: {
            from: 'sellersettings',
            localField: 'sellerId',
            foreignField: 'sellerId',
            as: 'settingsDetails',
          },
        },
        {
          $unwind: {
            path: '$settingsDetails',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $project: {
            sellerId: 1,
            firstName: '$userDetails.firstName',
            lastName: '$userDetails.lastName',
            countryCode: '$userDetails.countryCode',
            code: '$userDetails.code',
            mobile: '$userDetails.mobile',
            email: '$userDetails.email',
            address: {
              $filter: {
                input: '$userDetails.address',
                as: 'userAddress',
                cond: { $eq: ['$$userAddress._id', '$addressId'] },
              },
            },
            _id: 1,
            businessName: 1,
            logo:'$userDetails.logo',
            colorCode: 1,
            payment: '$settingsDetails.payment',
            privacy: '$settingsDetails.privacy',
            businessDetails: '$settingsDetails.businessDetails',
            commercialRegNum: 1,
            taxNumber: 1,
            vat: 1,
            returnPolicyDays: 1,
          },
        },
        {
          $project: {
            _id: 1,
            sellerId: 1,
            businessName: 1,
            logo: 1,
            colorCode: 1,
            firstName: 1,
            lastName: 1,
            code: 1,
            mobile: 1,
            email: { $ifNull: ['$email', ''] },
            logo: { $ifNull: ['$logo', ''] },
            countryCode: 1,
            address: { $first: '$address.address' },
            houseNumber: { $first: '$address.houseNumber' },
            street: { $first: '$address.street' },
            area: { $first: '$address.area' },
            latitude: { $first: '$address.location.latitude' },
            longitude: { $first: '$address.location.longitude' },
            commercialRegNum: { $ifNull: ['$commercialRegNum', ''] },
            taxNumber: { $ifNull: ['$taxNumber', ''] },
            vat: { $ifNull: ['$vat', ''] },
            returnPolicyDays: { $ifNull: ['$returnPolicyDays', ''] },
            payment: { $first: '$payment' },
            privacy: { $first: '$privacy' },
            businessDetails: { $first: '$businessDetails' },          
          },
        },
      ]);

      if (!sellerDetails.length)
        return resolve({ statusCode: 400, status: false, message: 'The store url you entered was not found.' });

      return resolve({
        statusCode: 200,
        status: true,
        message: 'Success',
        data: {
          sellerDetails: sellerDetails[0],
        },
      });
    } catch (error) {
      reject(error);
    }
  });
};

// get all active products by category id and seller id - web
const doGetAllActiveProductsBySellerCategoryId = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!data.categoryId)
        return resolve({ statusCode: 400, status: false, message: 'Please add categoryId', data: {} });
      if (!data.sellerId) return resolve({ statusCode: 400, status: false, message: 'Please add sellerId', data: {} });

      //  checking seller exist or not
      let isSellerExist = await account.findOne({
        _id: data.sellerId,
        isSeller: true,
        isActive: true,
      });
      if (!isSellerExist)
        return resolve({ statusCode: 400, status: false, message: 'The seller id you entered was not found.' });

      //  checking category exist or not
      let isCategoryExist = await productCategory.findOne({
        _id: data.categoryId,
        sellerId: data.sellerId,
        isActive: true,
      });
      if (!isCategoryExist) return resolve({ statusCode: 400, status: false, message: 'Invalid categoryId' });

      let sellerDetails = await sellerDetail.aggregate([
        {
          $match: {
            sellerId: mongoose.Types.ObjectId(data.sellerId),
            isActive: true,
          },
        },
        {
          $lookup: {
            from: 'accounts',
            localField: 'addressId',
            foreignField: 'address._id',
            as: 'userDetails',
          },
        },
        {
          $unwind: {
            path: '$userDetails',
          },
        },
        {
          $project: {
            firstName: '$userDetails.firstName',
            lastName: '$userDetails.lastName',
            countryCode: '$userDetails.countryCode',
            address: {
              $filter: {
                input: '$userDetails.address',
                as: 'userAddress',
                cond: { $eq: ['$$userAddress._id', '$addressId'] },
              },
            },
            _id: 1,
            businessName: 1,
            logo: 1,
            colorName: 1,
            colorCode: 1,
          },
        },
        {
          $project: {
            _id: 1,
            businessName: 1,
            logo: 1,
            colorName: 1,
            colorCode: 1,
            firstName: 1,
            lastName: 1,
            countryCode: 1,
            address: { $first: '$address.address' },
            houseNumber: { $first: '$address.houseNumber' },
            street: { $first: '$address.street' },
            area: { $first: '$address.area' },
            latitude: { $first: '$address.location.latitude' },
            longitude: { $first: '$address.location.longitude' },
          },
        },
      ]);

      if (!sellerDetails)
        return resolve({ statusCode: 400, status: false, message: 'The seller id you entered was not found.' });

      //get from db
      let products = await product
        .aggregate([
          {
            $match: {
              isActiveProduct: true,
              taggedCategories: { $elemMatch: { $eq: mongoose.Types.ObjectId(data.categoryId) } },
            },
          },
          {
            $unwind: {
              path: '$variants',
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $project: {
              productName: 1,
              basicSellingPrice: 1,
              isVariant: 1,
              countryCode: 1,
              images: {
                $filter: {
                  input: '$images',
                  as: 'item',
                  cond: { $eq: ['$$item.isDefault', true] },
                },
              },
              sellingPrice: '$variants.sellingPrice',
              variantImages: {
                $filter: {
                  input: '$variants.images',
                  as: 'items',
                  cond: { $eq: ['$$items.isDefault', true] },
                },
              },
            },
          },
          {
            $group: {
              _id: '$_id',
              image: { $first: '$images.imageUrl' },
              isVariant: { $first: '$isVariant' },
              countryCode: { $first: '$countryCode' },
              productName: { $first: '$productName' },
              price: { $first: '$basicSellingPrice' },
              variantImages: { $push: { variantImage: '$variantImages.imageUrl', price: '$sellingPrice' } },
            },
          },
        ])
        .sort({ _id: -1 });
      for (item of products) {
        if (item.isVariant) {
          for (image of item.variantImages) {
            if (image.variantImage.length) {
              item.image = image.variantImage[0];
              item.price = image.price;
              delete item.variantImages;
              break;
            }
          }
        } else {
          delete item.variantImages;
          item.image = item.image[0];
        }
      }

      // pagiation
      let limit = 30;
      let page = 0;
      if (data.limit) {
        limit = parseInt(data.limit);
      }

      if (data.page) {
        page = parseInt(data.page) - 1;
      }

      let nextPage = false;
      let start = page * limit;
      let end = page * limit + limit;
      let productDetails = products.slice(start, end);
      if (products.length > end) {
        nextPage = true;
      } else {
        nextPage = false;
      }
      if (products.length == 0) {
        nextPage = false;
      }
      let totalPage = Math.ceil(products.length / limit);
      return resolve({
        statusCode: 200,
        status: true,
        message: 'Success',
        data: {
          sellerDetails: sellerDetails[0],
          products: {
            totalPage,
            nextPage,
            productDetails,
          },
        },
      });

      // return resolve({ statusCode: 200, status: true, message: 'Success', data: { products } });
    } catch (error) {
      reject(error);
    }
  });
};

// get active product by id - web
const doGetActiveProductById = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      let products = await product.aggregate([
        // { $match: { sellerId: data.sellerId, isActiveProduct: true,'variants.isActiveVariant': true, _id: mongoose.Types.ObjectId(data.id) } },
        {
          $match: {
            sellerId: mongoose.Types.ObjectId(data.sellerId),
            isActiveProduct: true,
            _id: mongoose.Types.ObjectId(data.id),
          },
        },
        {
          $lookup: {
            from: 'uoms',
            localField: 'uom',
            foreignField: '_id',
            as: 'uomDetails',
          },
        },
        {
          $project: {
            variants: {
              $filter: {
                input: '$variants',
                as: 'variant',
                cond: { $eq: ['$$variant.isActiveVariant', true] },
              },
            },
            _id: 1,
            productName: 1,
            countryCode: 1,
            description: 1,
            taggedCategories: 1,
            uom: { $first: '$uomDetails.uom' },
            piece: 1,
            basicCostPrice: 1,
            basicSellingPrice: 1,
            images: 1,
            isActiveProduct: 1,
            isVariant: 1,
            isDeliveryVerification: 1,
          },
        },
      ]);

      await productCategory.populate(products, { path: 'taggedCategories', select: ['_id', 'productCategory'] });
      if (!products.length)
        return resolve({ statusCode: 400, status: false, message: 'Invalid seller id or id', data: {} });

      // Add the view count of store
      let isProductExist = await productView.findOne({
        sellerId: data.sellerId,
        productId: data.id,
      });

      if (!isProductExist) {
        let schemaObj = new productView({ sellerId: data.sellerId, productId: data.id, viewCount: 1 }).save();
      } else {
        await productView.findOneAndUpdate(
          { sellerId: mongoose.Types.ObjectId(data.sellerId), productId: mongoose.Types.ObjectId(data.id) },
          {
            $inc: { viewCount: 1 },
          }
        );
      }

      return resolve({ statusCode: 200, status: true, message: 'Success', data: { products } });
    } catch (error) {
      reject(error);
    }
  });
};

// filter active product by seller id -web
const doFilterActiveProductBySellerId = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!data.sellerId) return resolve({ statusCode: 400, status: false, message: 'Please add sellerId', data: {} });

      //  checking seller exist or not
      let isSellerExist = await account.findOne({
        _id: data.sellerId,
        isSeller: true,
        isActive: true,
      });
      if (!isSellerExist)
        return resolve({ statusCode: 400, status: false, message: 'The seller id you entered was not found.' });

      let { searchBy } = data;
      let searchQuery;
      searchQuery = [{ productName: { $regex: `${searchBy}`, $options: 'i' } }];

      // checking uom value
      let { uomValue } = data;

      // matchQuery
      let matchQuery = [{ isActiveProduct: true }, { sellerId: mongoose.Types.ObjectId(data.sellerId) }];

      if (data.categoryId) {
        matchQuery.push({ taggedCategories: { $elemMatch: { $eq: mongoose.Types.ObjectId(data.categoryId) } } });
      }
      if (data.searchBy) {
        matchQuery.push({ ...(searchBy && { $or: searchQuery }) });
      }

      if (data.uomValue || data.color) {
        matchQuery.push({ isVariant: true });
      }

      // variant filter
      let colorLessFilter = '';
      if (data.uomValue) {
        colorLessFilter = { $eq: ['$$variant.uomValue', uomValue] };
        matchQuery.push({ 'variants.uomValue': data.uomValue });
      }
      console.log(matchQuery);
      if (data.color) {
        colorLessFilter = { $eq: ['$$variant.colorName', data.color] };

        matchQuery.push({ 'variants.colorName': data.color });
      }

      if (data.color && data.uomValue) {
        colorLessFilter = {
          $and: [{ $eq: ['$$variant.uomValue', uomValue] }, { $eq: ['$$variant.colorName', data.color] }],
        };

        matchQuery.push({ variants: { $elemMatch: { $and: [{ uomValue: uomValue }, { colorName: data.color }] } } });
      }

      const minPrice = data.minPrice ? parseInt(data.minPrice) : 0;
      const maxPrice = data.maxPrice ? parseInt(data.maxPrice) : 10000000000000;

      //get from db
      let products = await product
        .aggregate([
          {
            $match: { $and: matchQuery },
          },
          {
            $project: {
              productName: 1,
              basicSellingPrice: 1,
              isVariant: 1,
              countryCode: 1,
              images: {
                $filter: {
                  input: '$images',
                  as: 'item',
                  cond: { $eq: ['$$item.isDefault', true] },
                },
              },
              variants: {
                $filter: {
                  input: '$variants',
                  as: 'variant',
                  //cond: { $and: [{$eq: ['$$variant.isActiveVariant', true] },{colorLessFilter}] },
                  cond: colorLessFilter,
                },
              },
            },
          },
          {
            $set: {
              price: { $first: '$variants.sellingPrice' },
              image: { $first: '$images.imageUrl' },
              images: { $first: { $first: '$variants.images.imageUrl' } },
            },
          },
          {
            $set: {
              price: { $ifNull: ['$price', '$basicSellingPrice'] },
            },
          },
          {
            $match: {
              $and: [{ price: { $gte: minPrice } }, { price: { $lte: maxPrice } }],
            },
          },
          {
            $project: {
              productName: 1,
              isVariant: 1,
              countryCode: 1,
              image: 1,
              uomValue: { $first: '$variants.uomValue' },
              colorName: { $first: '$variants.colorName' },
              image: { $ifNull: ['$images', '$image'] },
              price: 1,
            },
          },
        ])
        .sort({ _id: -1 });

      if (data.sort == 'lowToHigh') {
        products = products.sort(dynamicSort('price'));
      }
      if (data.sort == 'highToLow') {
        products = products.sort(dynamicSort('price')).reverse();
      }

      // pagiation
      let limit = 30;
      let page = 0;
      if (data.limit) {
        limit = parseInt(data.limit);
      }

      if (data.page) {
        page = parseInt(data.page) - 1;
      }

      let nextPage = false;
      let start = page * limit;
      let end = page * limit + limit;
      let productDetails = products.slice(start, end);
      if (products.length > end) {
        nextPage = true;
      } else {
        nextPage = false;
      }
      if (products.length == 0) {
        nextPage = false;
      }
      let totalPage = Math.ceil(products.length / limit);
      return resolve({
        statusCode: 200,
        status: true,
        message: 'Success',
        data: {
          products: {
            totalPage,
            nextPage,
            productDetails,
          },
        },
      });

      // return resolve({ statusCode: 200, status: true, message: 'Success', data: { products } });
    } catch (error) {
      reject(error);
    }
  });
};

//get uom , uom value and color in products by seller id for filter products -web
const doGetFilterMethodsBySellerId = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      let filter = await product.aggregate([
        // { $match: { sellerId: data.sellerId, isActiveProduct: true,'variants.isActiveVariant': true, _id: mongoose.Types.ObjectId(data.id) } },
        {
          $match: {
            sellerId: mongoose.Types.ObjectId(data.sellerId),
            isActiveProduct: true,
          },
        },
        {
          $lookup: {
            from: 'uoms',
            localField: 'uom',
            foreignField: '_id',
            as: 'uomDetails',
          },
        },
        { $unwind: '$uomDetails' },
        { $unwind: '$variants' },
        {
          $group: {
            _id: '$uom',
            uom: { $first: '$uomDetails.uom' },
            uomValue: {
              $addToSet: '$variants.uomValue',
            },
            color: {
              $addToSet: '$variants.colorName',
            },
          },
        },
      ]);

      return resolve({ statusCode: 200, status: true, message: 'Success', data: { filter } });
    } catch (error) {
      reject(error);
    }
  });
};

// get active categories by seller id
const doGetActiveSellerProductCategoriesWebBySellerId = (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        if (!data.sellerId) return resolve({ statusCode: 400, status: false, message: 'Please add sellerId', data: {} });

        const sellerId = data.sellerId
         //  checking seller exist or not
        let isSellerExist = await account.findOne({
          _id:sellerId,
          isSeller: true,
          isActive: true,
        });
        if (!isSellerExist)
          return resolve({ statusCode: 400, status: false, message: 'The seller id you entered was not found.' });

        //get from db
        let categories = await productCategory
          .find({ isActive: true, sellerId:sellerId }, { productCategory: 1 })
          .sort({ _id: -1 });
        return resolve({ statusCode: 200, status: true, message: 'Success', data: { categories } });
      } catch (error) {
        reject(error);
      }
    });
  };

module.exports = {
  doGetAllActiveProductsBySellerId,
  doGetSellerDetailsWebBySellerId,
  doGetSellerDetailsWebByStoreUrl,
  doGetAllActiveProductsBySellerCategoryId,
  doGetActiveProductById,
  doFilterActiveProductBySellerId,
  doGetFilterMethodsBySellerId,
  doGetActiveSellerProductCategoriesWebBySellerId
};

function dynamicSort(property) {
  let sortOrder = 1;
  if (property[0] === '-') {
    sortOrder = -1;
    property = property.substr(1);
  }
  return function (a, b) {
    /* next line works with strings and numbers,
     * and you may want to customize it to your needs
     */

    let result = a[property] < b[property] ? -1 : a[property] > b[property] ? 1 : 0;
    return result * sortOrder;
  };
}
