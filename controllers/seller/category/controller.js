const category = require('../../../model/seller/category/category');
const { validateSellerCategory } = require('../../../validations/seller/category/validation');

const doAddSellerCategory = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateSellerCategory(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      const { categoryTitle, description } = data;
      //check if category with  title already exist
      const isTitleExist = await category.findOne({ categoryTitle });

      if (isTitleExist) {
        return resolve({ statusCode: 200, status: false, message: 'Category with this title already exist.' });
      }

      //save to db
      let schemaObj = new category({ categoryTitle, description });
      schemaObj
        .save()
        .then(() => {
          return resolve({ statusCode: 200, status: true, message: 'Category created successfully.' });
        })
        .catch(async (error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};


// get active categories
const doGetSellerCategories = () => {
    return new Promise(async (resolve, reject) => {
      try {
        //get from db
        // let firstname = await user.find({ firstName: { $exists: true }}).count()
        let categories = await category.find({isActive:true},{categoryTitle:1,description:1})
        return resolve({ statusCode: 200, status: true, message: 'Success', data: { categories } });
      } catch (error) {
        reject(error);
      }
    });
  };

module.exports = {
  doAddSellerCategory,
  doGetSellerCategories
};
