const { doAddSellerCategory,doGetSellerCategories } = require('./controller');

const addSellerCategory = (req, res, next) => {
  console.log("test", req.body)
  doAddSellerCategory(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getSellerCategories = (req, res, next) => {
    doGetSellerCategories()
      .then(({ statusCode = 200, message, status, data }) => {
        res.status(statusCode).json({
          message,
          status,
          ...(data && { data }),
        });
      })
      .catch((error) => {
        next(error);
      });
  };

module.exports = {
  addSellerCategory,
  getSellerCategories
};
