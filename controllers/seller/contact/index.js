const { doAddSellerContact,doGetRecentAddyReciepient,doGetContactNotInAddy,doGetContactInAddy } = require('./controller');

const addSellerContact = (req, res, next) => {
  doAddSellerContact(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getRecentAddyReciepient = (req, res, next) => {
  console.log("req", req.params.page);
  doGetRecentAddyReciepient(req.params.page,req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getContactInAddy = (req, res, next) => {
  console.log("req", req.params.page);
  doGetContactInAddy(req.params.page,req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};


const getContactNotInAddy = (req, res, next) => {
  doGetContactNotInAddy(req.params.page,req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
module.exports = {
  addSellerContact,
  getRecentAddyReciepient,
  getContactNotInAddy,
  getContactInAddy
};
