const contact = require('../../../model/seller/contact/contact');
const Account = require('../../../model/Account');
const send = require('../../../model/user/send');
const { validateSellerContact } = require('../../../validations/seller/contact/validation');
const mongoose = require("mongoose");


const doAddSellerContact = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const dataValidation = await validateSellerContact(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      let insertData = {};
      insertData.sellerId = user._id;
      insertData.userName = data.userName;
      if(data.logo && data.logo.length){ 
        insertData.logo = data.logo;  
        insertData.logoFlag = true;
      }else
      insertData.logoFlag = false;
      insertData.logo = "";
      var phoneObj = {};
      let saveobj = [];
      let phoneDetails = 0;
      for (phone of data.phone) {
        phoneDetails = (phone).split(" ").join("");
        if (phoneDetails.slice(0, 1) == '+') {
          code = phoneDetails.replace('+', '');
          if (code.slice(0, 2) == 91) {
            code = code.slice(0, 2)
            phoneObj = {};
            phoneObj["code"] = code;
            phoneObj["number"] = (phoneDetails.slice(3, 14));
            let addyUser = await Account.findOne({ mobile: phoneObj.number, code: phoneObj.code, didRegistered: 1 }, { _id: 1 });
            if (addyUser) {
              phoneObj["addyUser"] = true;
            } else
              phoneObj["addyUser"] = false;
            saveobj.push(phoneObj)
          } else {
            code = code.slice(0, 3)
            phoneObj = {};
            phoneObj["number"] = (phoneDetails.slice(4, 14));
            phoneObj["code"] = code;
            let addyUser = await Account.findOne({ mobile: phoneObj.number, code: phoneObj.code, didRegistered: 1 }, { _id: 1 });
            if (addyUser) {
              phoneObj["addyUser"] = true;
            } else
              phoneObj["addyUser"] = false;
            saveobj.push(phoneObj)
          }
        } else if (phoneDetails.length > 10) {
       
          if (phoneDetails.slice(0, 2) == 91) {
            code = phoneDetails.slice(0, 2)
            phoneObj = {};
            phoneObj["code"] = code;
            phoneObj["number"] = (phoneDetails.slice(2, 14));
            let addyUser = await Account.findOne({ mobile: phoneObj.number, code: phoneObj.code, didRegistered: 1 }, { _id: 1 });
            if (addyUser) {
              phoneObj["addyUser"] = true;
            } else
              phoneObj["addyUser"] = false;
            saveobj.push(phoneObj)
          } else {
            code = phoneDetails.slice(0, 3)
            phoneObj = {};
            phoneObj["code"] = code;
            phoneObj["number"] = (phoneDetails.slice(3, 14));
            let addyUser = await Account.findOne({ mobile: phoneObj.number, code: phoneObj.code, didRegistered: 1 }, { _id: 1 });
            if (addyUser) {
              phoneObj["addyUser"] = true;
            } else
              phoneObj["addyUser"] = false;
            saveobj.push(phoneObj)
          }
        } else if (phoneDetails.length == 10 || phoneDetails.length == 9) {

          phoneObj["number"] = phoneDetails;
          phoneObj["code"] = user.code;
          let addyUser = await Account.findOne({ mobile: phoneObj.number, code: phoneObj.code, didRegistered: 1 }, { _id: 1 });
          if (addyUser) {
            phoneObj["addyUser"] = true;
          } else
            phoneObj["addyUser"] = false;
          saveobj.push(phoneObj)
        } else {

          phoneObj = {};
          phoneObj["code"] = user.code;
          phoneObj["number"] = phoneDetails;
          let addyUser = await Account.findOne({ mobile: phoneObj.number, code: phoneObj.code, didRegistered: 1 }, { _id: 1 });
          if (addyUser) {
            phoneObj["addyUser"] = true;
          } else
            phoneObj["addyUser"] = false;
          saveobj.push(phoneObj)
        }
      }
      insertData.phone = saveobj;
      if (data.email) {
        var emailObj = [{
          email: data.email
        }]
        insertData.email = emailObj;
      }
      let contactDetails = await contact.findOne({ sellerId: user._id, userName: data.userName });

      if (!contactDetails) {
        let schemaObj = new contact(insertData);
        schemaObj
          .save()
          .then(() => {
            return resolve({ statusCode: 200, status: true, message: 'contact details added successfully.' });
          })
          .catch((error) => {
            return resolve({ statusCode: 400, status: false, message: error });
          });
      } else {
       // console.log("inside last loop", contactDetails);
        let existingPhone = false;
        let newArray = [];
        for (phone of saveobj) {
          existingPhone = false;
          contactDetails.phone.forEach((e, i) => {
            if (e.code == phone.code && e.number == phone.number) {
              existingPhone = true;
            }
          });
          if (existingPhone == false) {
            if (newArray.length) {
              for (newValue of newArray) {
                if (
                  phone.code !== newValue.code &&
                  phone.number !== newValue.number
                ) {
                  newArray.push(phone)
                }
              }
            } else {
              newArray.push(phone)
            }
          }
        }
        contact.updateOne(
          { sellerId: user._id, userName: data.userName },
          {
            "$push": {
              "phone": {
                "$each": newArray
              }
            }
          }
        ).then((response) => {
          return resolve({
            status: true,
            message: 'Contact details added successfully'
          });
        });
      }
    } catch (error) {
      reject(error);
    }
  });
}

const doGetRecentAddyReciepient = (data,user) => {
  return new Promise(async (resolve, reject) => {
    try {
      let recentReciepient = await send.aggregate([
        { $sort: { createdAt: -1 } },
        {
          $match: {
            userId: mongoose.Types.ObjectId(user._id),
          },
        },
          {
            $lookup: {
              from: 'accounts',
              localField: 'assigneeId',
              foreignField: '_id',
              as: 'userDetails',
            },
        },
         {
          $unwind: {
            path: '$userDetails',
            preserveNullAndEmptyArrays: false,
          },
        },
        {
        $set: {
          name: { $concat: ['$userDetails.firstName', ' ', '$userDetails.lastName'] },
         
        },
      },
        {
          $group: {
            _id: '$userDetails.mobile',
            name: { $first: '$name' },
            mobile: { $first: '$userDetails.mobile' },
            code: { $first: '$userDetails.code' },
            logo: { $first: '$userDetails.logo' },
          },
        },
      ]).catch((error) => {
        console.log(error);
      });
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { recentReciepient } });
    } catch (error) {
      reject(error);
    }
  });
};

const doGetContactInAddy = (data,user) => {
  return new Promise(async (resolve, reject) => {
    try {
      var pageNo = data;
      var size = 16;
      var query;
      query = size * (pageNo - 1)
      let nextPage = false;
      let count =  await contact.count({ sellerId: user._id,"phone.addyUser":true })
      if((pageNo*size)<count){
        nextPage = true
      }
      let contactInAddy = await contact.find({ sellerId: user._id,"phone.addyUser":true }, { userName: 1, phone:1,logo:1,logoFlag:1 }).sort({ createdAt: -1 }).limit(size).skip(query);
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { nextPage,contactInAddy } });
    } catch (error) {
      reject(error);
    }
  });
};

const doGetContactNotInAddy = (data,user) => {
  return new Promise(async (resolve, reject) => {
    try {
      var pageNo = data;
      var size = 16;
      var query;
      query = size * (pageNo - 1)
      let nextPage = false;
      let count =  await contact.count({ sellerId: user._id,"phone.addyUser":false })
      if((pageNo*size)<count){
        nextPage = true
      }
      let contactNotInAddy = await contact.find({ sellerId: user._id,"phone.addyUser":false }, { userName: 1, phone:1 ,logo:1,logoFlag:1}).sort({ createdAt: -1 }).limit(size).skip(query);
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { nextPage,contactNotInAddy } });
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  doAddSellerContact,
  doGetRecentAddyReciepient,
  doGetContactNotInAddy,
  doGetContactInAddy
};
