const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const moment = require('moment-timezone');

const numberValidation = require('../../../helpers/number/numberValidation');
const order = require('../../../model/user/product/order');
const sellerDriver = require('../../../model/seller/registration/sellerDriver');
// const sellerOrderHistory = require('../../../model/seller/registration/sellerOrderHistory');
const sellerOrderHistory = require('../../../model/seller/product/sellerOrderHistory');

const { addOrderHistoryDetails } = require('../../../helpers/product/orderHelper');
const {
  validateSellerDriverLogin,
  validateDriverLink,
  validateStatusList,
  validateOrderDetails,
  validateSellerDriverStatusUpdate,
} = require('../../../validations/seller/sellerDriverValidation');

const doSellerDriverLogin = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateSellerDriverLogin(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      let validNumber = numberValidation(data.code, data.mobile);

      if (!validNumber) {
        return resolve({
          status: false,
          data: {},
          message: 'Destination number' + ' ' + data.mobile + ' is invalid',
          statusCode: 400,
        });
      }

      const sellerDriverExist = await sellerDriver.findOne({
        mobile: data.mobile,
        code: data.code,
        orders: data.orderId,
      });

      if (sellerDriverExist) {
        let payload = {
          driverId: sellerDriverExist._id,
        };
        let token = jwt.sign(
          {
            data: payload,
          },
          'JWT_KEY',
          {
            expiresIn: '30 days',
          }
        );
        if (token) {
          return resolve({ statusCode: 200, status: true, message: 'Logged in successfully', data: { token: token } });
        } else {
          return resolve({ statusCode: 400, status: false, message: 'Token error' });
        }
      } else {
        return resolve({ statusCode: 200, status: false, message: 'Invalid login credentials' });
      }
    } catch (error) {
      reject(error);
    }
  });
};

const doCheckSellerDriverLink = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateDriverLink(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      // check order id is valid or not
      let validOrder = await order.findOne({
        _id: mongoose.Types.ObjectId(data.orderId),
      });
     
      if (!validOrder) return resolve({ statusCode: 200, status: false, message: 'Invalid Link' });

      let orgOrderStatus = validOrder.orderStatus;
      let orderStatus = ['pending', 'rejected', 'deliveryRejected', 'delivered', 'cancelled'];

      // check the order status
      if (orderStatus.includes(orgOrderStatus))
        return resolve({ statusCode: 200, status: false, message: 'Link expired', data: {} });

      let validDate = await sellerOrderHistory.findOne({
        orderId: mongoose.Types.ObjectId(data.orderId),
      });
      if (!validDate) return resolve({ statusCode: 200, status: false, message: 'Invalid Link' });

      // link will expire after 7 days
     
      if (validDate) {
        let confirmedDate = validDate.assignedToDriver.date;
        let today = moment(new Date()).format('DD-MM-YYYY');
        let checkDate = moment(confirmedDate, 'DD-MM-YYYY').add(7, 'days');
        let convertedDate = moment(checkDate).format('DD-MM-YYYY');
        if (today > convertedDate) {
          return resolve({ statusCode: 200, status: false, message: 'Link expired' });
        }
      }

      return resolve({ statusCode: 200, status: true, message: 'Valid link' });
    } catch (error) {
      reject(error);
    }
  });
};

const doGetSellerDriverStatuses = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateStatusList(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      let validOrder = await order.findOne({
        _id: mongoose.Types.ObjectId(data.orderId),
      });
      if (!validOrder) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid order id' });
      }

      let statusList = [];
      let orgOrderStatus = validOrder.orderStatus;
      if (orgOrderStatus == 'confirmed') {
        statusList.push('Assigned', 'Rejected');
      } else if (
        orgOrderStatus == 'deliveryAssigned' ||
        orgOrderStatus == 'startDelivery' ||
        orgOrderStatus == 'attemptDelivery' ||
        orgOrderStatus == 'failedToDelivery'
      ) {
        // statusList.push('Assigned', 'Start Pickup');
        statusList.push('Start Delivery', 'Attempt Delivery', 'Failed To Delivery', 'Delivered');
      }
      // else if (
      //   orgOrderStatus == 'startPickup' ||
      //   orgOrderStatus == 'attemptPickup' ||
      //   orgOrderStatus == 'failedToPickup'
      // ) {
      //   statusList.push('Start Pickup', 'Attempt Pickup', 'Failed To Pickup', 'Pickup Complete');
      // }
      // else if (orgOrderStatus == 'completePickup') {
      //   statusList.push('Start Delivery', 'Attempt Delivery', 'Failed To Delivery', 'Delivered');
      // }
      // else if (
      //   orgOrderStatus == 'startDelivery' ||
      //   orgOrderStatus == 'attemptDelivery' ||
      //   orgOrderStatus == 'failedToDelivery'
      // ) {
      //   statusList.push('Start Delivery', 'Attempt Delivery', 'Failed To Delivery', 'Delivered');
      // }
      else if (orgOrderStatus == 'delivered') {
        statusList.push('Delivered');
      }
      return resolve({
        statusCode: 200,
        status: true,
        message: 'Driver statuses',
        data: {
          statuses: statusList,
        },
      });
    } catch (error) {
      reject(error);
    }
  });
};

const doGetSellerDriverOrderDetails = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateOrderDetails(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      const id = data.orderId;

      // get the order details
      let orderDetails = await order.aggregate([
        {
          $match: {
            isActive: true,
            _id: mongoose.Types.ObjectId(id),
          },
        },
        //get the seller account details
        {
          $lookup: {
            from: 'accounts',
            localField: 'sellerId',
            foreignField: '_id',
            as: 'sellerAccountDetails',
          },
        },
        //get the user account details
        {
          $lookup: {
            from: 'accounts',
            localField: 'userId',
            foreignField: '_id',
            as: 'userAccountDetails',
          },
        },
        //get the seller default address id details from sellerdetails
        {
          $lookup: {
            from: 'sellerdetails',
            localField: 'sellerId',
            foreignField: 'sellerId',
            as: 'sellerAddressDetails',
          },
        },
        {
          $unwind: {
            path: '$sellerAccountDetails',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $unwind: {
            path: '$sellerAddressDetails',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $project: {
            _id: 1,
            orderId: 1,
            paymentType: 1,
            userId: 1,
            paymentType: 1,
            sellerCountryCode: 1,
            orderStatus: 1,
            //deliveryDate: 1,
            deliveryType: '$pickupType',
            deliveryType: {
              $cond: {
                if: { $eq: ['$pickupType', 'schedulePickup'] },
                then: 'scheduleDelivery',
                else: 'immediateDelivery',
              },
            },
            deliveryAssignedDate: '$expectedPickupDate',
            deliveryAssignedTime: {
              fromTime: '$expectedPickupTime.fromTime',
              toTime: '$expectedPickupTime.toTime',
            },
            boxSize: '$cartDetails.commonBoxSize',
            weight: '$cartDetails.totalWeight',
            fromDetails: {
              name: { $concat: ['$sellerAccountDetails.firstName', '  ', '$sellerAccountDetails.lastName'] },
              code: '$sellerAccountDetails.code',
              mobile: '$sellerAccountDetails.mobile',
              sellerAddress: {
                $first: {
                  $filter: {
                    input: '$sellerAccountDetails.address',
                    as: 'item',
                    cond: { $eq: ['$$item._id', '$sellerAddressDetails.addressId'] },
                  },
                },
              },
            },
            toDetails: {
              name: {
                $concat: [
                  { $first: '$userAccountDetails.firstName' },
                  '  ',
                  { $first: '$userAccountDetails.lastName' },
                ],
              },
              code: { $first: '$userAccountDetails.code' },
              mobile: { $first: '$userAccountDetails.mobile' },
              userAddress: '$userDetails',
              grandTotal: {
                $cond: { if: { $eq: ['$paymentType', 'cod'] }, then: '$cartDetails.grandTotal', else: '' },
              },
            },
          },
        },
      ]);

      if (!orderDetails.length)
        return resolve({ statusCode: 400, status: false, message: 'Invalid order id', data: {} });

      let orgOrderStatus = orderDetails[0].orderStatus;
      let orderStatus = ['pending', 'rejected', 'deliveryRejected', 'delivered'];

      if (orderStatus.includes(orgOrderStatus)) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid order id', data: {} });
      } else {
        return resolve({ statusCode: 200, status: true, message: 'order details are', data: orderDetails[0] });
      }
    } catch (error) {
      reject(error);
    }
  });
};

const doSellerDriverStatusUpdate = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateSellerDriverStatusUpdate(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      let validOrder = await order.findOne({
        _id: mongoose.Types.ObjectId(data.orderId),
      });
      if (!validOrder) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid orderId' });
      }

      if (
        validOrder.orderStatus == 'pending' ||
        validOrder.orderStatus == 'rejected' ||
        validOrder.orderStatus == 'accepted'
      ) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid orderId' });
      }

      let id = data.orderId;
      let orderStatus = data.status;
      let notificationStatus = '';

      let delivered = false;
      let deliveryDate = validOrder.expectedDeliveryDate;
      let expectedDeliveryDate = validOrder.expectedDeliveryDate;
      let expectedDeliveryTime = validOrder.expectedDeliveryTime;
      let pickupDate = validOrder.expectedPickupDate;
      let isDeliveryStarted = validOrder.isDeliveryStarted;
      let isPickupStarted = validOrder.isPickupStarted;
      let isDeliveryAssigned = validOrder.isDeliveryAssigned;
      let isPickupCompleted = validOrder.isPickupCompleted;

      if (data.status == 'driverAssigned') {
        isDeliveryAssigned = true;
        orderStatus = 'deliveryAssigned';
        notificationStatus = 'assigned';
        if (validOrder.pickupType == 'schedulePickup') {
          expectedDeliveryDate = validOrder.expectedPickupDate;
          expectedDeliveryTime = validOrder.expectedPickupTime;
          deliveryDate = validOrder.expectedPickupDate;
          pickupDate =  validOrder.expectedPickupDate
        } else {
          expectedDeliveryDate = moment().tz(validOrder.timeZone).format('YYYY-MM-DD');
          expectedDeliveryTime = validOrder.expectedDeliveryTime;
          pickupDate =  moment().tz(validOrder.timeZone).format('YYYY-MM-DD');
        }
      }
      if (data.status == 'driverRejected') {
        orderStatus = 'deliveryRejected';
        notificationStatus = 'rejected';
      }
      // if (data.status == 'startPickup') {
      //   isDeliveryAssigned = true;
      //   isPickupStarted = true;
      //   notificationStatus = 'start pickup';
      // }
      // if (data.status == 'attemptPickup') {
      //   notificationStatus = 'attempt pickup';
      // }
      // if (data.status == 'failedToPickup') {
      //   notificationStatus = 'failed to pickup';
      // }
      // if (data.status == 'completePickup') {
      //   pickupDate = Date.now();
      //   isPickupCompleted = true
      //   notificationStatus = 'complete pickup';
      // }
      if (data.status == 'startDelivery') {
        notificationStatus = 'start delivery';
        isPickupStarted = true;
        isPickupCompleted = true;
        expectedDeliveryDate = moment().tz(validOrder.timeZone).format('YYYY-MM-DD');
        expectedPickupDate = moment().tz(validOrder.timeZone).format('YYYY-MM-DD');
        pickupDate =  Date.now();
        if (validOrder.pickupType == 'schedulePickup') {
          expectedDeliveryTime = validOrder.expectedPickupTime;
        } else {
          expectedDeliveryTime = validOrder.expectedDeliveryTime;
        }
        isDeliveryStarted = true;
      }
      if (data.status == 'attemptDelivery') {
        notificationStatus = 'attempt delivery';
      }
      if (data.status == 'failedToDelivery') {
        notificationStatus = 'failed to delivery';
      }
      if (data.status == 'delivered') {
        notificationStatus = 'delivered';
        delivered = true;
        deliveryDate = Date.now();
      }
      order
        .updateOne(
          { _id: mongoose.Types.ObjectId(data.orderId) },
          {
            $set: {
              orderStatus: orderStatus,
              delivered,
              deliveryDate,
              isPickupStarted,
              isDeliveryStarted,
              isDeliveryAssigned,
              pickupDate,
              isPickupCompleted,
              expectedDeliveryDate,
              expectedDeliveryTime,
            },
          }
        )
        .then(async (response) => {
          // if driver reject the order then remove that orderId from driver orders
          if (data.status == 'driverRejected') {
            await sellerDriver.updateOne(
              { _id: mongoose.Types.ObjectId(validOrder.selectedDriverId) },
              { $pull: { orders: mongoose.Types.ObjectId(data.orderId) } }
            );
          }

          notificationStatus = `driver ${notificationStatus}`;

          let orderDetail = {
            id: id,
            orderId: validOrder.orderId,
            sellerId: validOrder.sellerId,
            status: data.status,
            notificationStatus: notificationStatus,
            userId: validOrder.userId,
          };
          // add confirmed order details to order history and notification
          const orderDetails = await addOrderHistoryDetails(orderDetail);
          return resolve({ statusCode: 200, status: true, message: 'Order status updated successfully' });
        });
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  doSellerDriverLogin,
  doCheckSellerDriverLink,
  doGetSellerDriverStatuses,
  doGetSellerDriverOrderDetails,
  doSellerDriverStatusUpdate,
};
