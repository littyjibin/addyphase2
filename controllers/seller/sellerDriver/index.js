const {
    doSellerDriverLogin,
    doCheckSellerDriverLink,
    doGetSellerDriverStatuses,
    doGetSellerDriverOrderDetails,
    doSellerDriverStatusUpdate
  } = require('./controller');


const sellerDriverLogin = (req, res, next) => {
    doSellerDriverLogin(req.body)
      .then(({ statusCode = 200, message, status, data }) => {
        res.status(statusCode).json({
          message,
          status,
          ...(data && { data }),
        });
      })
      .catch((error) => {
        next(error);
      });
  };

  const checkSellerDriverLink = (req, res, next) => {
    doCheckSellerDriverLink(req.body)
      .then(({ statusCode = 200, message, status, data }) => {
        res.status(statusCode).json({
          message,
          status,
          ...(data && { data }),
        });
      })
      .catch((error) => {
        next(error);
      });
  };

  const getSellerDriverStatuses = (req, res, next) => {
    doGetSellerDriverStatuses(req.params)
      .then(({ statusCode = 200, message, status, data }) => {
        res.status(statusCode).json({
          message,
          status,
          ...(data && { data }),
        });
      })
      .catch((error) => {
        next(error);
      });
  };

  const getSellerDriverOrderDetails = (req, res, next) => {
    doGetSellerDriverOrderDetails(req.body, req.user)
      .then(({ statusCode = 200, message, status, data }) => {
        res.status(statusCode).json({
          message,
          status,
          ...(data && { data }),
        });
      })
      .catch((error) => {
        next(error);
      });
  };

  const sellerDriverStatusUpdate = (req, res, next) => {
    doSellerDriverStatusUpdate(req.body)
      .then(({ statusCode = 200, message, status, data }) => {
        res.status(statusCode).json({
          message,
          status,
          ...(data && { data }),
        });
      })
      .catch((error) => {
        next(error);
      });
  };
  


  module.exports = {
    sellerDriverLogin,
    checkSellerDriverLink,
    getSellerDriverStatuses,
    getSellerDriverOrderDetails,
    sellerDriverStatusUpdate
  };