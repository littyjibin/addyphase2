const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const { promisify } = require('util');
const sendSms = require('../../helpers/sms/sendSms');
const Account = require('../../model/Account');
const send = require('../../model/user/send');

const mongoose = require("mongoose");
const { validateSignup, validateAddress,validateAddressweb, validateEditAddress,
  validateUserName, validateUserNameSpecialChar, validateProfileUpdate,
  validateSettingsUpdate } = require('../../validations/user/user');

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function validateEmail(emailAdress) {
  let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (emailAdress.match(regexEmail)) {
    return true;
  }
}

const doLogin = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateSignup(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      let currentDate = new Date();
      let futureDate = new Date(currentDate.getTime() + 1 * 60000);
      let params = data;
      const otp = _.random(10000, 99999);
      const userExist = await Account.findOne({
        mobile: params.mobile,
        code: params.code,
      });

      if (!userExist) {
        params.code = params.code.replace('+', '');
        let user = new Account({
          mobile: params.mobile,
          otp: otp,
          didRegistered: 0,
          code: params.code,
          expireOtp: futureDate,
          countryCode: params.countryCode
        });
        await user.save();
      } else {
        await Account.updateOne(
          {
            mobile: params.mobile,
            code: params.code,
          },
          {
            otp: otp,
            expireOtp: futureDate,
          }
        ).then((res1) => {
          console.log(res1);
        });
      }

      let payload = {
        origin: 'ADDYAE',
        destination: params.code + params.mobile,
        message: otp + ' is your otp from addy',
      };
      console.log(payload)
      await sendSms(payload)
        .then((_) => {
          return resolve({
            status: true,
            data: {},
            message: 'user Login SuccessFully',
          });
        })
        .catch((error) => {
          return resolve({
            status: false,
            data: {},
            message: error,
          });
        });
    } catch (error) {
      reject(error);
    }
  });
};

const doGetRecentReciepient = () => {
  return new Promise(async (resolve, reject) => {
    try {
      let recentReciepient = await Account.find({ didRegistered: 1 }, { mobile: 1, firstName: 1, lastName: 1, logo: 1 }).sort({ _id: -1 }).limit(10);
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { recentReciepient } });
    } catch (error) {
      reject(error);
    }
  });
};


const doVerifyOtp = (data) => {
  return new Promise(async (resolve, reject) => {
    const params = data;

    if (!params.lang) {
      return resolve({
        status: false,
        data: {},
        message: 'lang missing',
      });
    }

    if (params.lang != 'english' && params.lang != 'arabic') {
      return resolve({
        status: false,
        data: {},
        message: 'Invalid lang',
      });
    }

    if (!params.otp) {
      if (params.lang == 'english') {
        return resolve({
          status: false,
          data: {},
          message: 'Invalid otp',
        });
      } else {
        return resolve({
          status: false,
          data: {},
          message: 'غير صالح OTP',
        });
      }
    }
    if (!params.mobile) {
      if (params.lang == 'english') {
        return resolve({
          status: false,
          data: {},
          message: 'Invalid mobile',
        });
      } else {
        return resolve({
          status: false,
          data: {},
          message: 'موبايل غير صالح',
        });
      }
    }
    try {
      const output = await Account.findOne({
        mobile: params.mobile,
        otp: params.otp,
        code: params.code,
      }).lean();

      if (output) {
        if (output.expireOtp < new Date()) {
          return resolve({
            status: false,
            data: {},
            message: 'Otp Expired',
          });
        }
        let payload = {
          userId: output._id,
        };
        let token = jwt.sign(
          {
            data: payload,
            // exp: Math.floor(Date.now() / 1000) + JWT_EXPIRY_SECONDS
          },
          'JWT_KEY',
          {
            expiresIn: '30 days',
          }
        );

        let mode = '';
        let toScreen = '';
        if (output.didRegistered == 1) {
          let orders = await send.findOne({
            $or: [{ userId: output._id }, { assigneeId: output._id }],
          });

          if (orders) {
            mode = 1;
            toScreen = "toOrder";
          } else {
            mode = 1;
            toScreen = "home";
          }
        }
        console.log("test", output)
        if (!output.firstName || !output.lastName) {
          mode = 0;

          toScreen = 'know_more';
        } else if (!output.address || (output.address && output.address.length === 0)) {
          mode = 0;

          toScreen = 'address';
        } else if (!output.userName) {
          mode = 0;

          toScreen = 'userName';
        }

        if (params.lang == 'english') {
          return resolve({
            status: true,
            data: {
              api_key: token + '',
              mobile: params.mobile,
              mode: mode,
              toScreen: toScreen,
            },
            message: 'Mobile number verified',
          });
        } else {
          return resolve({
            status: true,
            data: {
              api_key: token + '',
              mobile: params.mobile,
              mode: mode,
              toScreen: toScreen,
            },
            message: 'تم التحقق من رقم الهاتف المحمول',
          });
        }
      } else {
        if (params.lang == 'english') {
          return resolve({
            status: false,
            data: {},
            message: 'Invalid otp',
          });
        } else {
          return res.send({
            status: false,
            data: {},
            message: 'otp غير صالح',
          });
        }
      }
    } catch (error) {
      reject(error);
    }
  });
};

const doResendOtp = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateSignup(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      let currentDate = new Date();
      let futureDate = new Date(currentDate.getTime() + 1 * 60000);
      let params = data;
      const otp = _.random(10000, 99999);
      const userExist = await Account.findOne({
        mobile: params.mobile,
        code: params.code,
      });

      if (!userExist) {
        return resolve({
          status: false,
          message: "User doesn't exist",
        });
      } else {
        await Account.updateOne(
          {
            mobile: params.mobile,
            code: params.code,
          },
          {
            otp: otp,
            expireOtp: futureDate,
          }
        ).then((res1) => {
          console.log(res1);
        });
      }

      let payload = {
        origin: 'ADDYAE',
        destination: params.code + params.mobile,
        message: otp + ' is your otp from addy',
      };
      await sendSms(payload)
        .then((_) => {
          return resolve({
            status: true,
            data: {},
            message: 'OTP sent to your mobile number',
          });
        })
        .catch((error) => {
          return resolve({
            status: false,
            data: {},
            message: error,
          });
        });
    } catch (error) {
      reject(error);
    }
  });
};

const doUpdateUserInfo = (data, user) => {
  return new Promise(async (resolve, reject) => {
    let userId = user._id;

    let params = data;

    if (!params.first_name) {
      return resolve({
        status: false,
        message: 'Please enter first name',
      });
    }
    if (!params.last_name) {
      return resolve({
        status: false,
        message: 'Please enter last name',
      });
    }
    try {
      params.first_name = params.first_name.toLowerCase();
      params.last_name = params.last_name.toLowerCase();
      params.first_name = capitalizeFirstLetter(params.first_name);
      params.last_name = capitalizeFirstLetter(params.last_name);

      let knowMore = {};

      if (params.email) {
        // email format validation
        let validMail = validateEmail(params.email);
        if (!validMail) {
          return resolve({
            status: false,
            message: 'Invalid email',
          });
        }

        // existing email validation
        let existingEmail = await Account.findOne({ email: params.email });
        if (existingEmail) {
          return resolve({
            status: false,
            message: 'Existing email',
          });
        }

        knowMore = {
          firstName: params.first_name,
          lastName: params.last_name,
          email: params.email,
        };
      } else {
        knowMore = {
          firstName: params.first_name,
          lastName: params.last_name,
        };
      }

      Account.updateOne(
        {
          _id: userId,
        },
        knowMore
      ).then((response) => {
        return resolve({
          status: true,
          message: 'Customer data updated',
        });
      });
    } catch (error) {
      reject(error);
    }
  });
};

const doAddNewAddress = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateAddress(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      var addressObj = [{
        address: data.address,
        houseNumber: data.houseNumber,
        area: data.area,
        street: data.street,
        buildingName: data.buildingName,
        deliveryInstruction: data.deliveryInstruction,
        pinCode: data.pinCode,
        city: data.city,
        location: {
          latitude: data.latitude,
          longitude: data.longitude,
        },
      }]
      let userDetails = await Account.findOne({ _id: user._id });
      let existingAddress = false;
      userDetails.address.forEach((e, i) => {
        if (
          e.address == data.address &&
          e.houseNumber == data.houseNumber &&
          e.street == data.street &&
          e.area == data.area &&
          e.city == data.city &&
          e.location.latitude == data.latitude &&
          e.location.longitude == data.longitude
        ) {
          existingAddress = true;
        }
      });

      if (existingAddress) {
        return resolve({ statusCode: 400, status: false, message: 'Existing Address.' });
      } else {
        Account.updateOne(
          { _id: mongoose.Types.ObjectId(user._id) },
          {
            "$push": {
              "address": {
                "$each": addressObj
              }
            }
          }
        ).then((response) => {
          return resolve({
            status: true,
            message: 'Address added successfully',
          });
        });
      }
    } catch (error) {
      reject(error);
    }
  });
}

const doUpdateUserAddress = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateEditAddress(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      let userDetails = await Account.findOne({ _id: user._id });
      let existingAddress = false;
      userDetails.address.forEach((e, i) => {
        if (
          e.address == data.address &&
          e.houseNumber == data.houseNumber &&
          e.street == data.street &&
          e.area == data.area &&
          e.city == data.city &&
          e.location.latitude == data.latitude &&
          e.location.longitude == data.longitude
        ) {
          existingAddress = true;
        }
      });
      if (existingAddress) {
        return resolve({ statusCode: 400, status: false, message: 'Existing Address.' });
      } else {
        let currentDate = new Date();
        Account.updateOne(
          { "address._id": mongoose.Types.ObjectId(data.id) },
          {
            $set: {
              "address.$.updatedAt": currentDate,
              "address.$.address": data.address,
              "address.$.houseNumber": data.houseNumber,
              "address.$.pinCode": data.pinCode,
              "address.$.deliveryInstruction": data.deliveryInstruction,
              "address.$.street": data.street,
              "address.$.area": data.area,
              "address.$.buildingName": data.buildingName,
              "address.$.city": data.city,
              "address.$.location": {
                latitude: data.latitude,
                longitude: data.longitude,
              },
            },
          }
        ).then((response) => {
          return resolve({
            status: true,
            message: 'Address updated successfully',
          });
        });
      }
    } catch (error) {
      reject(error);
    }
  });
};

const doGetUserAddress = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      //get from db
      let address = {};
      const result = await Account.aggregate([
        {
          $match: { _id: mongoose.Types.ObjectId(data) },
        },
        {
          $project: {
            addresses: {
              $filter: {
                input: "$address",
                as: "item",
                cond: {
                  $eq: ["$$item.isDeleted", false],
                },
              },
            },
          },
        },
      ]).catch((err) => {
        console.log(err);
      });

      if (result[0]) {
        address = result[0].addresses;
        return resolve({ statusCode: 200, status: true, message: 'Success', data: address });
      } else {
        return resolve({ statusCode: 400, status: false, message: 'Invalid id' });

      }


    } catch (error) {
      reject(error);
    }
  });
};
const doDeleteUserAddress = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      //get from db

      let categories = await Account.findOne({ "address._id": mongoose.Types.ObjectId(data) });
      if (categories) {
        await Account.updateOne(
          { "address._id": mongoose.Types.ObjectId(data) },
          {
            $set: { "address.$.isDeleted": true },
          }
        )
          .then(() => {
            return resolve({ statusCode: 200, status: true, message: 'Address deleted succesfully' });
          })
          .catch((error) => {
            return resolve({ statusCode: 400, status: false, message: error, data: {} });
          });
      } else {
        return resolve({ statusCode: 400, status: false, message: 'Invalid id' });
      }
    } catch (error) {
      reject(error);
    }
  });
};

const doUpdateUsername = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const dataValidation = await validateUserName(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      let existingUsername = await Account.findOne({ userName: (data.userName).toLowerCase() });
      if (existingUsername) {
        return resolve({ statusCode: 400, status: false, message: 'Username not available.' });
      } else {
        let characterFormat = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
        if (characterFormat.test(data.userName)) {
          return resolve({
            status: false,
            message: "Special characters are not allowed",
            statusCode: 400,
          });
        } else {
          const dataValidation = await validateUserNameSpecialChar(data);
          if (dataValidation.error) {
            const message = dataValidation.error.details[0].message.replace(/"/g, '');
            return resolve({
              status: false,
              message: message,
              statusCode: 400,
            });
          } else {
            function fullNumbers(uName) {
              return /^\d+$/.test(uName);
            }

            let numbers = fullNumbers(data.userName)
            if (numbers) {
              let userdetails = await Account.findOne({ _id: user._id })
              if (data.userName.length != 5 && data.userName != userdetails.mobile) {
                return resolve({ statusCode: 400, status: false, message: 'Numbers should be 5 digits' });
              }
            } else {
              if (data.userName.length < 6) {
                return resolve({ statusCode: 400, status: true, message: 'Username should have minimum 6 characters' });
              } else if (data.userName.length > 15) {
                return resolve({ statusCode: 400, status: false, message: 'Username should not exceed 15 characters' });
              }
            }
            await Account.updateOne(
              {
                _id: user._id,
              },
              {
                $set: {
                  userName: data.userName,
                  didRegistered: 1,
                  webUser: false,
                },
              });
            let result = await send.find({
              $or: [
                { userId: mongoose.Types.ObjectId(user._id) },
                { assigneeId: mongoose.Types.ObjectId(user._id) },
              ],
            });
            let toScreen = 0;

            if (result.length) {
              toScreen = 1;
            }
            return resolve({
              statusCode: 200, status: true, message: 'Username updated', data: {
                toScreen,
              }
            });
          }
        }
      }
    } catch (error) {
      reject(error);
    }
  });
};

const doVerifyUsername = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      console.log("username", data.userName)
      const dataValidation = await validateUserName(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      let existingUsername = await Account.findOne({ userName: (data.userName).toLowerCase() });
      if (existingUsername) {
        return resolve({ statusCode: 400, status: false, message: 'Username not available.' });
      } else {
        // validation for special characters
        let characterFormat = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
        if (characterFormat.test(data.userName)) {
          return resolve({
            status: false,
            message: "Special characters are not allowed",
            statusCode: 400,
          });
        } else {
          const dataValidation = await validateUserNameSpecialChar(data);
          if (dataValidation.error) {
            const message = dataValidation.error.details[0].message.replace(/"/g, '');
            return resolve({
              status: false,
              message: message,
              statusCode: 400,
            });
          } else {

            // validation for full numbers
            function fullNumbers(uName) {
              return /^\d+$/.test(uName);
            }

            let numbers = fullNumbers(data.userName)
            if (numbers) {
              let userdetails = await Account.findOne({ _id: user._id })
              if (data.userName.length != 5 && data.userName != userdetails.mobile) {
                return resolve({ statusCode: 400, status: false, message: 'Numbers should be 5 digits' });
              }
            } else {
              if (data.userName.length < 6) {
                return resolve({ statusCode: 400, status: false, message: 'Username should have minimum 6 characters' });
              } else if (data.userName.length > 15) {
                return resolve({ statusCode: 400, status: false, message: 'Username should not exceed 15 characters' });
              }
            }

            return resolve({ statusCode: 200, status: true, message: 'Username available' });
          }
        }
      }
    } catch (error) {
      reject(error);
    }
  });
};

const doAddNewAddressWeb = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateAddressweb(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      console.log("eeeee", user)
      var addressObj = [{
        address: data.address,
        houseNumber: data.houseNumber,
        area: data.area,
        street: data.street,
        buildingName: data.buildingName,
        city: data.city,
        location: {
          latitude: data.latitude,
          longitude: data.longitude,
        },
      }]
      let userDetails = await Account.findOne({ _id: data.userId });
      let existingAddress = false;
      userDetails.address.forEach((e, i) => {
        if (
          e.address == data.address &&
          e.houseNumber == data.houseNumber &&
          e.street == data.street &&
          e.area == data.area &&
          e.city == data.city &&
          e.location.latitude == data.latitude &&
          e.location.longitude == data.longitude
        ) {
          existingAddress = true;
        }
      });

      if (existingAddress) {
        return resolve({ statusCode: 200, status: false, message: 'Existing Address.' });
      } else {
        Account.updateOne(
          { _id: mongoose.Types.ObjectId(data.userId) },
          {
            "$push": {
              "address": {
                "$each": addressObj
              }
            }
          }
        ).then((response) => {
          return resolve({
            status: true,
            message: 'Address added successfully',
          });
        });
      }
    } catch (error) {
      reject(error);
    }
  });
}
const doGetUserAddressWeb = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      //get from db
      let address = {};
      const result = await Account.aggregate([
        {
          $match: { _id: mongoose.Types.ObjectId(data) },
        },
        {
          $project: {
            addresses: {
              $filter: {
                input: "$address",
                as: "item",
                cond: {
                  $eq: ["$$item.isDeleted", false],
                },
              },
            },
          },
        },
      ]).catch((err) => {
        console.log(err);
      });

      if (result) {
        address = result[0].addresses;
      }

      return resolve({ statusCode: 200, status: true, message: 'Success', data: address });
    } catch (error) {
      reject(error);
    }
  });
};
const doListAddressWeb = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      let address = {};

      if (!data) {
        return resolve({
          status: true,
          message: "orderId missing",
          statusCode: 200,
        });

      }

      let validOrder = await send.findOne({ _id: data });
      if (!validOrder) {
        return resolve({
          status: false,
          message: "Invalid orderId",
          statusCode: 200,
        });
      }

      let result = []

      result = await Account.aggregate([
        {
          $match: {
            code: validOrder.code + "",
            mobile: validOrder.toMobile + "",
          },
        },
        {
          $project: {
            addresses: {
              $filter: {
                input: "$address",
                as: "item",
                cond: {
                  $eq: ["$$item.isDeleted", false],
                },
              },
            },
          },
        },
      ]).catch((err) => {
        console.log(err);
      });

      if (!result.length) {
        result = await Account.aggregate([
          {
            $match: { _id: mongoose.Types.ObjectId(validOrder.assigneeId) },
          },
          {
            $project: {
              addresses: {
                $filter: {
                  input: "$address",
                  as: "item",
                  cond: {
                    $eq: ["$$item.isDeleted", false],
                  },
                },
              },
            },
          },
        ]).catch((err) => {
          console.log(err);
        });
      }

      if (result.length) {
        address = result[0].addresses;
      }
      return resolve({
        status: true,
        message: "List addresses",
        statusCode: 200,
        data: { addresses: address },
      });

    } catch (error) {
      return resolve({
        status: false,
        message: "catch_" + error,
        statusCode: 200,
        data: {},
      });

    }
  });
};

const doUpdateUserProfile = (data, user) => {
  return new Promise(async (resolve, reject) => {
    let userId = user._id;
    let params = data;
    try {
      if (data.firstName || data.lastName || data.log || data.email || data.mobile) {
        const dataValidation = await validateProfileUpdate(data);
        if (dataValidation.error) {
          const message = dataValidation.error.details[0].message.replace(/"/g, '');
          return resolve({
            status: false,
            message: message,
            statusCode: 400,
          });
        }
        if (params.firstName) {
          params.firstName = params.firstName.toLowerCase();
          params.firstName = capitalizeFirstLetter(params.firstName);
        }
        if (params.lastName) {
          params.lastName = params.lastName.toLowerCase();
          params.lastName = capitalizeFirstLetter(params.lastName);
        }
        let knowMore = {};

        if (params.email) {
          let validMail = validateEmail(params.email);
          if (!validMail) {
            return resolve({
              status: false,
              message: 'Invalid email',
            });
          }
        }
        if (params?.email)
          knowMore.email = params.email
        if (params?.logo)
          knowMore.logo = params.logo
        if (params?.firstName)
          knowMore.firstName = params.firstName
        if (params?.lastName)
          knowMore.lastName = params.lastName
        if (params?.mobile)
          knowMore.mobile = params.mobile

        Account.updateOne(
          {
            _id: userId,
          },
          knowMore
        ).then((response) => {
          return resolve({
            status: true,
            message: 'Customer data updated',
          });
        });
      } else {
        return resolve({
          status: false,
          message: 'Please add update params',
        });
      }
    } catch (error) {
      reject(error);
    }
  });
};

const doUpdateUserSettings = (data, user) => {
  return new Promise(async (resolve, reject) => {
    let userId = user._id;
    let params = data;
    try {
      const dataValidation = await validateSettingsUpdate(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      let settings = {
        calls: {
          "callForStartPickUp": data.callForStartPickUp,
          "callForAttemptPickUp": data.callForAttemptPickUp,
          "callForStartDelivery": data.callForStartDelivery,
          "callForAttemptDelivery": data.callForAttemptDelivery
        },
        pushNotification: {
          "notifyForStartPickUp": data.notifyForStartPickUp,
          "notifyForAttemptPickUp": data.notifyForAttemptPickUp,
          "notifyForStartDelivery": data.notifyForStartDelivery,
          "notifyForAttemptDelivery": data.notifyForAttemptDelivery
        },
        whatsapp: {
          "whatsappNotifyForStartPickUp": data.whatsappNotifyForStartPickUp,
          "whatsappNotifyForAttemptPickUp": data.whatsappNotifyForAttemptPickUp,
          "whatsappNotifyForStartDelivery": data.whatsappNotifyForStartDelivery,
          "whatsappNotifyForAttemptDelivery": data.whatsappNotifyForAttemptDelivery
        }
      }

      Account
      .updateOne(
        { _id: mongoose.Types.ObjectId(userId) },
        {
          $set: { settings: settings },
        }
      ).then((response) => {
        return resolve({
          status: true,
          message: 'Customer settings updated',
        });
      });
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  doLogin,
  doVerifyOtp,
  doResendOtp,
  doUpdateUserInfo,
  // doAddAddress,
  doGetUserAddress,
  doUpdateUserAddress,
  doDeleteUserAddress,
  doVerifyUsername,
  doUpdateUsername,
  doAddNewAddress,
  doAddNewAddressWeb,
  doGetUserAddressWeb,
  doListAddressWeb,
  doGetRecentReciepient,
  doUpdateUserProfile,
  doUpdateUserSettings
};
