const { doAddCollectDetails,doGetCollectUserByMobile,doGetCollectUserByUsername,doGetCollectOrderDetail } = require('./controller');

const getCollectUserByMobile = (req, res, next) => {
  doGetCollectUserByMobile(req.body,req.user)
  .then(({ statusCode = 200, message, status, data }) => {
    res.status(statusCode).json({
      message,
      status,
      ...(data && { data }),
    });
  })
  .catch((error) => {
    next(error);
  });
};

const getCollectUserByUsername = (req, res, next) => {
  doGetCollectUserByUsername(req.body,req.user)
.then(({ statusCode = 200, message, status, data }) => {
  res.status(statusCode).json({
    message,
    status,
    ...(data && { data }),
  });
})
.catch((error) => {
  next(error);
});
};

const AddCollectDetails = (req, res, next) => {
  doAddCollectDetails(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getCollectOrderDetail = (req, res, next) => {
  doGetCollectOrderDetail(req.body, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        data
      });
    })
    .catch((error) => {
      next(error);
    });
};



module.exports = {
  AddCollectDetails,
  getCollectUserByMobile,
  getCollectUserByUsername,
  getCollectOrderDetail
};
