const user = require('../../../../model/Account');
const collect = require('../../../../model/user/collect');
const Driver = require('../../../../model/seller/driver/driver')
const Notification = require("../../../../model/user/notification");
const packagesize = require('../../../../model/admin/master/packageSize');
const pickupTime = require('../../../../model/admin/master/pickupTime');
const OrderHistory = require("../../../../model/seller/driver/order_history");
const timeZoneValidation = require('../../../../helpers/firebase/timeZoneValidation');
const { validateMobile, validateSearchByUserName, validateCollectDetails,validatecollectOrderDetails } = require('../../../../validations/user/collectValidation');
const sendSms = require('../../../../helpers/sms/sendSms');
const numberValidation = require("../../../../helpers/number/numberValidation");
const mongoose = require("mongoose");
const moment = require('moment-timezone');
let nodemailer = require("nodemailer");
const sellerDetail = require('../../../../model/seller/registration/sellerDetails');
const apiKey = "7c979460e5312bfb763f7c731ee7bdfd";
const apiSecret = "1db38674e85ee5b2de8e0cf27e3e0e39";

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}


function capitalizeWords(string) {
  return string.replace(/(?:^|\s)\S/g, function (a) {
    return a.toUpperCase();
  });
}

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

const doGetCollectUserByMobile = (data, userDetails) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateMobile(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      let validNumber = numberValidation(data.code, data.mobile);
      let params = data;
      if (!validNumber) {
        return resolve({
          status: false,
          data: {},
          message: 'Destination number' + " " + params.mobile + ' is invalid',
        });
      }

      data.code = data.code.replace('+', '');

      //get from db
      let sellerDetails;
      let users = await user.findOne(
        {
          mobile: data.mobile,
          code: data.code,
          _id: { $ne: userDetails._id },
          firstName: { $exists: true },
          lastName: { $exists: true },
        },
        { firstName: 1, lastName: 1, isSeller: 1 }
      );

      if (users && users.isSeller) {
        sellerDetails = await sellerDetail.findOne(
          {
            sellerId: users._id,
          },
          { businessName: 1, storeUrl: 1, logo: 1 }
        );
      }

      if (!users) {
        let payload = {
          origin: 'ADDYAE',
          destination: data.code + data.mobile,
        };
        await sendSms(payload)
          .then((_) => {
            return resolve({ statusCode: 200, status: false, message: 'Success', data: {} });
          })
          .catch((error) => {
            return resolve({ statusCode: 400, status: false, message: error, data: {} });
          });
      }
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { users, sellerDetails } });


    } catch (error) {
      reject(error);
    }
  });
};

const doGetCollectUserByUsername = (data, userDetails) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateSearchByUserName(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      const username = data.userName;
      if (username && username.trim().length > 0) {
        function fullNumbers(username) {
          return /^\d+$/.test(username);
        }
        let sellerDetails;
        let numbers = fullNumbers(username)
        if (numbers) {
          return resolve({ statusCode: 400, status: false, message: 'User Name must be a string value ' });
        } else {
          var results = await user.findOne(
            {
              userName: username,
              _id: { $ne: userDetails._id },
              didRegistered: 1,
              $or: [{ webUser: { $exists: false } }, { webUser: false }],
            },
            { address: 0, settings: 0, webUser: 0, didRegistered: 0, createdAt: 0, updatedAt: 0, __v: 0, isActive: 0 });
          if (results && results.isSeller) {

            sellerDetails = await sellerDetail.findOne(
              {
                sellerId: results._id,
              },
              { businessName: 1, storeUrl: 1, logo: 1 }
            );
          }

          if (results) {
            return resolve({ statusCode: 200, status: true, message: 'Success', data: { results, sellerDetails } });
          } else {
            return resolve({ statusCode: 200, status: false, message: 'No user found' });
          }
        }
      }
    } catch (error) {
      reject(error);
    }
  });
};



const doAddCollectDetails = (data, userDetails) => {
  return new Promise(async (resolve, reject) => {
    try {

      //validate incoming data
      const dataValidation = await validateCollectDetails(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      let isPackageSizeExist = await packagesize.findOne({ _id: data.packageSizeId });
      if (!isPackageSizeExist) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid package Id.' });
      }

      let ispickupTimeExist = await pickupTime.findOne({ _id: data.timeId });
      if (!ispickupTimeExist) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid time ID.' });
      }
      if (data.paymentMethodForPackagePrice == "cash") {
        if (data.driverName || data.driverMobile) {
          return resolve({ statusCode: 400, status: false, message: "partner is not allowed for cash payment" });
        }
      }

      data.package_details = data.package_details.toLowerCase();
      data.package_details = capitalizeFirstLetter(data.package_details);
      let validAssignee;
      if (!data.assigneeId) {
        data.addyUser = false;
        if (!data.toMobile) {
          return resolve({ statusCode: 400, status: false, message: "assignee missing" });
        } else {
          if (!data.code) {
            return resolve({ statusCode: 400, status: false, message: "country code missing" });
          }
        }
      }
      if (data.assigneeId) {
        validAssignee = await user.findOne({
          $and: [
            { _id: mongoose.Types.ObjectId(data.assigneeId) },
            { _id: { $ne: mongoose.Types.ObjectId(userDetails._id) } },
          ],
        });
        if (!validAssignee) {
          return resolve({ statusCode: 400, status: false, message: "Invalid assigneeId" });
        }
      }

      if (data.collectForSomeoneElse) {
        if (!data.receiverMobile) {
          return resolve({ statusCode: 400, status: false, message: "Please add receiverMobile " });
        }
      }

      if (data.deliveryType != "addy" && data.deliveryType != "partner") {
        return resolve({ statusCode: 400, status: false, message: "invalid delivery type" });
      }
      if (data.deliveryType == "partner") {
        if (!data.driverMobile) {
          return resolve({ statusCode: 400, status: false, message: "Enter driver number" });
        }
        if (!data.driverName) {
          return resolve({ statusCode: 400, status: false, message: "Enter driver name" });
        }
        if (!data.driverMobileCode) {
          return resolve({ statusCode: 400, status: false, message: "Enter driver mobile code" });
        }
      }
      var last = await collect.find().sort({ $natural: -1 }).limit(1);
      let lastOrderId = 0;
      if (last.length) {
        lastOrderId = parseInt(last[0].order_id);
      }
      lastOrderId = lastOrderId + 1;
      let str = lastOrderId.toString().padStart(6, "0");
      if (data.assigneeId) {
        var userId = data.assigneeId;
        var notfication = new Notification({
          userId: userId,
          message: "A new package assigned to you",
          is_read: 0,
        });
        await notfication.save();
      }

      //save to db
      let package_data = {
        status: "waiting",
        isApproved: false,
        packageSize: data.packageSize,
        needVan: data.needVan,
        sendItem: data.sendItem,
        userId: userDetails._id,
        isRejected: false,
        details: data.package_details,
        deliveryType: data.deliveryType,
        selfAddress: data.self_address_id,
        photos: data.photos,
        priceOfPackage: data.priceOfPackage,
        order_id: str,
        toMobile: data.toMobile,
        needVan: data.needVan,
        packageSizeId: data.packageSizeId,
        pickupTime: data.pickupTime,
        day: data.day,
        timeId: data.timeId,
        addyUser: data.addyUser,
        paymentMethodForPackagePrice: data.paymentMethodForPackagePrice,
        isPackageAmountCollected: data.isPackageAmountCollected,
        deliveryFeePaidBy: data.deliveryFeePaidBy,
        paymentMethodForDeliveryFee: data.paymentMethodForDeliveryFee

      };
      if (data.assigneeId) {
        package_data.assigneeId = data.assigneeId;
        package_data.assigneeAddress = data.assigneeAddress;
      } else if (data.toMobile) {
        data.code = data.code.replace("+", "");
        package_data.toMobile = data.toMobile;
        package_data.code = data.code;
      }
      if (data.selectedCompanyId) {
        package_data.selectedCompanyId = data.selectedCompanyId;
      }
      if (data.deliveryType == "partner" && data.paymentMethodForPackagePrice !== "cash") {
        package_data.driverName = data.driverName;
        package_data.driverMobile = data.driverMobile;
        package_data.driverMobileCode = data.driverMobileCode;
      }
      package_data.collector = userDetails._id;
      if (data.assigneeId) {
        package_data.sender = data.assigneeId;
      }
      if (data.collectForSomeoneElse) {
        package_data.collectForSomeoneElse = data.collectForSomeoneElse;
        package_data.receiverMobile = data.receiverMobile;
      }


      let obj = new collect(package_data);
      var schemaObj = await obj.save();
      if (data.deliveryType == "addy") {
        let transporter = nodemailer.createTransport({
          host: "smtp.hostinger.com",
          port: 587,
          auth: {
            user: "no-rep@addy.ae",
            pass: "PassNoRep890",
          },
        });
        const mailOptions = {
          from: "no-rep@addy.ae", // sender address
          to: [
            "littycherian22@gmail.com",
          ], // list of receivers
          subject: "New Order created!", // Subject line
          html: `<p>Order id: ${schemaObj.order_id} <br> ${process.env.FRONTEND_URL}assign/${schemaObj._id} '</p>`, // plain text body
        };
        transporter.sendMail(mailOptions, async function (err, info) {
          if (err) {
            await user.deleteOne({
              _id: mongoose.Types.ObjectId(schemaObj._id),
            });
            return resolve({ statusCode: 200, status: true, message: 'Something went wrong: email error.' });
          } else {

          }
        });
      } else {
        let existingDriver = await Driver.findOne({ mobile: data.driverMobile });

        if (!existingDriver) {
          // generating driver id
          let last1 = await Driver.find().sort({ $natural: -1 }).limit(1);
          let lastDriverId = 0;
          if (last1.length) {
            lastDriverId = parseInt(last1[0].driverId);
          }
          lastDriverId = lastDriverId + 1;
          let driverId = lastDriverId.toString().padStart(6, "0");
          // saving new driver
          let driverDetails = {
            name: package_data.driverName,
            mobile: package_data.driverMobile,
            driverId: driverId,
            orders: [schemaObj._id],
          };

          let schemaObject = new Driver(driverDetails);

          schemaObject.save();
        } else {
          await Driver.updateOne(
            { mobile: package_data.driverMobile },
            {
              $push: { orders: schemaObj._id },
              $set: { name: package_data.driverName },
            }
          ).then((response) => {
            console.log(response);
          });
        }
      }
      if (data.toMobile || data.receiverMobile) {
        if (data.toMobile) {
          var smsglobal = require("smsglobal")(apiKey, apiSecret);
          var payload = {
            origin: "ADDYAE",
            destination: data.code + data.toMobile,
            message: process.env.FRONTEND_URL + 'user/' + schemaObj._id,
          };
        } else {
          var smsglobal = require("smsglobal")(apiKey, apiSecret);
          var payload = {
            origin: "ADDYAE",
            destination: data.code + data.receiverMobile,
            message: process.env.FRONTEND_URL + 'user/' + schemaObj._id,
          }
        }

        smsglobal.sms.send(payload, async function (error, response) {
          if (error) {
            collect.deleteOne({ _id: schemaObj._id }).then((respons) => {
              return res.send({
                error: true,
                message: error.data.errors.destination.errors[0],
              });
            });
          }

          if (response) {
            let historyData = {
              order_id: schemaObj._id,
              waiting: {
                value: true,
                date: new Date(),
              },
            };
            let historyObj = OrderHistory(historyData);
            await historyObj.save();
            return resolve({
              statusCode: 200, status: true, message: 'package added', data: {
                _id: schemaObj._id,
                orderId: schemaObj.order_id,
              },
            });
          }
        });
        // sms sending finished
      } else {
        if (validAssignee.fcmId) {
          let fcmIdToken = [];
          let payloadData = {
            id: schemaObj._id + "",
            orderId: schemaObj.order_id,
            tokens: fcmIdToken,
            title: "Order Placed",
            message: "A new package assigned to you",
          };
          fcmIdToken.push(validAssignee.fcmId);
          firebaseAdmin.sendMulticastNotification(payloadData);
        }
        var smsglobal = require("smsglobal")(apiKey, apiSecret);
        var payload = {
          origin: "ADDYAE",
          destination: validAssignee.code + validAssignee.mobile,
          message: `${process.env.FRONTEND_URL}user/${schemaObj._id}`,
        };

        smsglobal.sms.send(payload, async function (error, response) {
          if (error) {
            collect.deleteOne({ _id: schemaObj._id }).then((respons) => {
              return res.send({
                error: true,
                message: error.data.errors.destination.errors[0],
              });
            });
          }

          if (response) {
          }
        });
        let historyData = {
          order_id: schemaObj._id,
          waiting: {
            value: true,
            date: new Date(),
          },
        };

        let historyObj = OrderHistory(historyData);
        await historyObj.save();
        return res.collect({
          error: false,
          message: "package added",
          data: {
            _id: schemaObj._id,
            orderId: schemaObj.order_id,
          },
        });
      }


    } catch (error) {
      reject(error);
    }
  });
};


const doGetCollectOrderDetail = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let params = req;
      const dataValidation = await validatecollectOrderDetails(params);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      let validOrder = await collect
        .findOne(
          {
            $and: [
              { _id: mongoose.Types.ObjectId(params.orderId) },
              {
                $or: [{ userId: mongoose.Types.ObjectId(res._id) }, { assigneeId: mongoose.Types.ObjectId(res._id) }],
              },
            ],
          },
          {
            status: 1,
            photos: 1,
            sender: 1,
            collector: 1,
            deliveryType: 1,
            toMobile: 1,
            code: 1,
            details: 1,
            driverName: 1,
            driverMobile: 1,
            isDriverAssigned: 1,
          }
        )
        .populate({
          path: 'driverId',
          select: ['name', 'image', 'mobile'],
        })
        .lean();
      if (validOrder) {
        for (let i = 0; i < validOrder.photos.length; i++) {
          validOrder.photos[i] = {
            image: process.env.BASE_URL + validOrder.photos[i],
          };
        }
        let sender = await user.findOne({
          _id: mongoose.Types.ObjectId(validOrder.sender),
        });

        if (sender) {
          validOrder.sender_collector = {
            _id: sender._id,
            firstName: sender.firstName,
            lastName: sender.lastName,
          };

          if (sender.photos) {
            validOrder.sender_collector.image =
              sender.photos;
          } else {
            validOrder.sender_collector.image = "";
          }
        } else {
          let existingUser = await user.findOne({
            code: validOrder.code + '',
            mobile: validOrder.toMobile + '',
          });
          if (existingUser) {
            validOrder.sender_collector = {
              _id: existingUser._id,
              firstName: existingUser.firstName,
              lastName: existingUser.lastName,
            };
          } else {
            validOrder.sender_collector = {
              mobile: validOrder.toMobile,
            };
          }
          delete validOrder.sender;
          delete validOrder.collector;
          validOrder.sender = 1;
        }

        delete validOrder.sender;
        delete validOrder.collector;
        validOrder.sender = 1;

        if (validOrder.deliveryType == 'addy') {
          if (validOrder.driverId) {
            validOrder.driver = validOrder.driverId;
            if (validOrder.driver.name) {
              validOrder.driver.name = validOrder.driver.name.toLowerCase();
              validOrder.driver.name = capitalizeWords(validOrder.driver.name);
            }

            if (validOrder.driver.image) {
              validOrder.driver.image = validOrder.driver.image;
            } else {
              validOrder.driver.image = '';
            }
            delete validOrder.driverId;
            delete validOrder.deliveryType;
          } else {
            validOrder.driver = {};

            delete validOrder.driverId;
            delete validOrder.deliveryType;
          }
        } else {
          validOrder.driverName = validOrder.driverName.toLowerCase();
          validOrder.driverName = capitalizeWords(validOrder.driverName);
          validOrder.driver = {
            name: validOrder.driverName,
            mobile: validOrder.driverMobile,
          };

          delete validOrder.driverName;
          delete validOrder.driverMobile;
          delete validOrder.deliveryType;
        }

        let order_history = await OrderHistory.findOne(
          { order_id: params.orderId },
          {
            _id: 0,
            order_id: 0,
            __v: 0,
          }
        ).lean();

        let orderTracking = [];

        let orderTrackingResponseModel = [
          {
            status: '',
            date: '',
            active: false,
          },
          {
            status: '',
            date: '',
            active: true,
          },
          {
            status: '',
            date: '',
            active: false,
          },
          {
            status: '',
            date: '',
            active: false,
          },
        ];
        // assigning values to orderTracking
        if (order_history.waiting) {
          orderTracking.push({
            status: 'Order placed',
            date: order_history.waiting.date,
            active: false,
          });
        }

        if (order_history.Approved) {
          orderTracking.push({
            status: 'Approved',
            date: order_history.Approved.date,
            active: order_history.Approved.value,
          });
        } else {
          if (!order_history.Rejected) {
            orderTracking.push({
              status: 'Waiting for approval',
              date: '',
              active: true,
            });
          }
        }

        if (order_history.Rejected) {
          orderTracking.push({
            status: 'Rejected',
            date: order_history.Rejected.date,
            active: order_history.Rejected.value,
          });
        }
        if (order_history.assigned_to_driver) {
          orderTracking.push({
            status: 'Assigning to Driver',
            date: order_history.assigned_to_driver.date,
            active: order_history.assigned_to_driver.value,
          });
        }
        if (order_history.driver_approved) {
          orderTracking.push({
            status: 'Assigned to delivery driver',
            date: order_history.driver_approved.date,
            active: order_history.driver_approved.value,
          });
        } else {
          if (!order_history.driver_rejected) {
            orderTracking.push({
              status: 'Assigned to delivery driver',
              date: '',
              active: false,
            });
          }
        }

        if (order_history.driver_rejected && !order_history.driver_approved) {
          orderTracking.push({
            status: 'Rejected by driver',
            date: order_history.driver_rejected.date,
            active: order_history.driver_rejected.value,
          });
        }

        if (order_history.pickup_initiated) {
          orderTracking.push({
            status: 'Pickup initiated',
            date: order_history.pickup_initiated.date,
            active: order_history.pickup_initiated.value,
          });
        } else {
          orderTracking.push({
            status: 'Pickup',
            date: '',
            active: false,
          });
        }

        if (order_history.pickup_completed) {
          orderTracking.push({
            status: 'Pickup completed',
            date: order_history.pickup_completed.date,
            active: order_history.pickup_completed.value,
          });
        }

        if (order_history.pickup_failed) {
          orderTracking.push({
            status: 'Pickup failed',
            date: order_history.pickup_failed.date,
            active: order_history.pickup_failed.value,
          });
        }

        if (order_history.delivery_initiated) {
          orderTracking.push({
            status: 'Delivery initiated',
            date: order_history.delivery_initiated.date,
            active: order_history.delivery_initiated.value,
          });
        } else {
          orderTracking.push({
            status: 'Delivery',
            date: '',
            active: false,
          });
        }

        if (order_history.delivery_completed) {
          orderTracking.push({
            status: 'Delivery completed',
            date: order_history.delivery_completed.date,
            active: order_history.delivery_completed.value,
          });
        }

        if (order_history.delivery_failed) {
          orderTracking.push({
            status: 'Delivery failed',
            date: order_history.delivery_failed.date,
            active: order_history.delivery_failed.value,
          });
        }

        if (validOrder.status == 'approved' && validOrder.isDriverAssigned) {
          for (let it of orderTracking) {
            if (it.status == 'Approved') {
              it.active = false;
            }
          }
        }
        let activeInTracking = orderTracking.find((e) => e.active);
        orderTrackingResponseModel[1].status = activeInTracking.status;
        orderTrackingResponseModel[1].date = activeInTracking.date;

        let indexOfActiveInTracking = orderTracking.indexOf(activeInTracking);

        // assigning previous tracking history
        if (indexOfActiveInTracking != 0) {
          orderTrackingResponseModel[0].status = orderTracking[indexOfActiveInTracking - 1].status;
          orderTrackingResponseModel[0].date = orderTracking[indexOfActiveInTracking - 1].date;
        }

        if (activeInTracking.status != 'Rejected' && activeInTracking.status != 'Rejected by driver') {
          if (indexOfActiveInTracking < orderTracking.length - 2) {
            orderTrackingResponseModel[2].status = orderTracking[indexOfActiveInTracking + 1].status;

            orderTrackingResponseModel[3].status = orderTracking[indexOfActiveInTracking + 2].status;
          } else if (indexOfActiveInTracking == orderTracking.length - 2) {
            orderTrackingResponseModel[2].status = orderTracking[indexOfActiveInTracking + 1].status;
          }
        } else {
          orderTrackingResponseModel[2].status = '';
          orderTrackingResponseModel[3].status = '';
        }

        orderTrackingResponseModel.forEach((e, i) => {
          let timeZone = timeZoneValidation(res.code);
          if (req.lang == 'english') {
            if (e.date) {
              e.date = moment(e.date).tz(timeZone);

              e.date = moment(e.date).format('h.mm a DD-MM-YYYY');
            }
          } else {
            if (e.date) {
              e.date = moment(e.date).tz(timeZone);

              e.date = moment(e.date).format('DD-MM-YYYY hh.mm a');
            }
          }
        });
        return resolve({
          statusCode: 200,
          status: true,
          message: 'Order details are',
          data: {
            order_details: validOrder,
            order_history: orderTrackingResponseModel,
          },
        });
      } else {
        return resolve({ statusCode: 400, status: false, message: 'Invalid order id' });
      }
    } catch (error) {
      reject(error);
    }
  });
};


module.exports = {
  doAddCollectDetails,
  doGetCollectUserByUsername,
  doGetCollectUserByMobile,
  doGetCollectOrderDetail
};
