const user = require('../../../../model/Account');
const send = require('../../../../model/user/send');
const collect = require('../../../../model/user/collect');
const Driver = require('../../../../model/seller/driver/driver');
const Notification = require('../../../../model/user/notification');
const OrderHistory = require('../../../../model/seller/driver/order_history');
const {
  validateMobile,
  validateSendDetails,
  validateSearchByUserName,
  checkLink,
  validateApprovePackageWebOfExistingUser,
  validateApprovePackageWeb,
  validateApprovePackage,
  validateOrderDetails,
  validateAddComplaint,
  validateCancelOrder,
  validateregister
} = require('../../../../validations/user/sendValidation');
const sendSms = require('../../../../helpers/sms/sendSms');
const sellerDetail = require('../../../../model/seller/registration/sellerDetails');
const mongoose = require('mongoose');
const moment = require('moment-timezone');
let nodemailer = require('nodemailer');
const SendmailTransport = require('nodemailer/lib/sendmail-transport');
const timeZoneValidation = require('../../../../helpers/firebase/timeZoneValidation');
const firebaseAdmin = require('../../../../helpers/firebase/firebaseAdmin');
const numberValidation = require('../../../../helpers/number/numberValidation');
const Account = require('../../../../model/Account');
const apiKey = '7c979460e5312bfb763f7c731ee7bdfd';
const apiSecret = '1db38674e85ee5b2de8e0cf27e3e0e39';

const courierCompany = require('../../../../model/admin/courierCompany');

const { getOrderDetailsByUserId } = require('../../../../helpers/product/orderHelper');

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}


function capitalizeWords(string) {
  return string.replace(/(?:^|\s)\S/g, function (a) {
    return a.toUpperCase();
  });
}

// get user details by mobile number
const doGetUserByMobile = (data, userDetails) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateMobile(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      let validNumber = numberValidation(data.code, data.mobile);
      let params = data;
      if (!validNumber) {
        return resolve({
          statusCode: 400,
          status: false,
          data: {},
          message: 'Destination number' + ' ' + params.mobile + ' is invalid',
        });
      }

      data.code = data.code.replace('+', '');

      //get from db
      let users = await user.findOne(
        {
          mobile: data.mobile,
          code: data.code,
          _id: { $ne: userDetails._id },
          firstName: { $exists: true },
          lastName: { $exists: true },
        },
        { firstName: 1, lastName: 1, mobile: 1, code: 1 }
      );

      if (!users) {
        let payload = {
          origin: 'ADDYAE',
          destination: data.code + data.mobile,
        };
        await sendSms(payload)
          .then((_) => {
            return resolve({ statusCode: 400, status: false, message: 'No user found', data: {} });
          })
          .catch((error) => {
            return resolve({ statusCode: 400, status: false, message: error, data: {} });
          });
      }
      return resolve({ statusCode: 200, status: true, message: 'Success', data: { users } });
    } catch (error) {
      reject(error);
    }
  });
};

const doGetUserByUsername = (data, userDetails) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateSearchByUserName(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      const username = data.userName;
      if (username && username.trim().length > 0) {
        function fullNumbers(username) {
          return /^\d+$/.test(username);
        }
        let sellerDetails;
        let numbers = fullNumbers(username);
        if (numbers) {
          return resolve({ statusCode: 400, status: false, message: 'User Name must be a string value ' });
        } else {
          var results = await user.findOne(
            {
              userName: username,
              _id: { $ne: userDetails._id },
              didRegistered: 1,
              $or: [{ webUser: { $exists: false } }, { webUser: false }],
            },
            { mobile: 1, firstName: 1, lastName: 1, code: 1 }
          );

          if (results) {
            return resolve({ statusCode: 200, status: true, message: 'Success', data: { results } });
          } else {
            return resolve({ statusCode: 400, status: false, message: 'No user found', data: {} });
          }
        }
      }
    } catch (error) {
      reject(error);
    }
  });
};

const doAddSendDetails = (data, userDetails) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateSendDetails(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      data.package_details = data.package_details.toLowerCase();
      data.package_details = capitalizeFirstLetter(data.package_details);
      let validAssignee;
      if (!data.assigneeId) {
        data.addyUser = false;
        if (!data.toMobile) {
          return resolve({ statusCode: 400, status: false, message: 'assignee missing' });
        } else {
          if (!data.code) {
            return resolve({ statusCode: 400, status: false, message: 'country code missing' });
          }
        }
      }
      console.log("userDetails", userDetails)
      if (data.assigneeId) {
        validAssignee = await user.findOne({
          $and: [
            { _id: mongoose.Types.ObjectId(data.assigneeId) },
            { _id: { $ne: mongoose.Types.ObjectId(userDetails._id) } },
          ],
        });
        if (!validAssignee) {
          return resolve({ statusCode: 400, status: false, message: 'Invalid assigneeId' });
        }
      }
      if (data.isPacakgeAmountCollected) {
        if (data.paymentMethodForPackagePrice == 'cash') {
          if (data.driverName || data.driverMobile) {
            return resolve({ statusCode: 200, status: false, message: 'partner is not allowed for cash payment' });
          }
        }
      }
      if (data.deliveryType == 'addy') {
        if (!data.selectedCompanyId) {
          return resolve({ statusCode: 400, status: false, message: 'Please select courier partner' });
        }
      }
      if (data.deliveryType != 'addy' && data.deliveryType != 'partner') {
        return resolve({ statusCode: 400, status: false, message: 'invalid delivery type' });
      }
      if (data.paymentMethodForPackagePrice !== 'cash' && !data.selectedCompanyId && data.deliveryType !== 'addy' || data.deliveryType == 'partner') {
        if (!data.driverMobile) {
          return resolve({ statusCode: 400, status: false, message: 'Enter driver number' });
        }
        if (!data.driverName) {
          return resolve({ statusCode: 400, status: false, message: 'Enter driver name' });
        }
        if (!data.driverMobileCode) {
          return resolve({ statusCode: 400, status: false, message: 'Enter driver mobile code' });
        }
      }
    
      var last = await send.find().sort({ $natural: -1 }).limit(1);
      let lastOrderId = 0;
      if (last.length) {
        lastOrderId = parseInt(last[0].orderId);
      }
      lastOrderId = lastOrderId + 1;
      let str = lastOrderId.toString().padStart(6, '0');
      if (data.assigneeId) {
        var userId = data.assigneeId;
        var notfication = new Notification({
          userId: userId,
          message: 'A new package assigned to you',
          is_read: 0,
        });
        await notfication.save();
      }

      //save to db
      let package_data = {
        status: 'waiting',
        isApproved: false,
        packageSize: data.packageSize,
        needVan: data.needVan,
        userId: userDetails._id,
        isRejected: false,
        details: data.package_details,
        deliveryType: data.deliveryType,
        selfAddress: data.self_address_id,
        photos: data.photos,
        order_id: str,
        toMobile: data.toMobile,
        packageSizeId: data.packageSizeId,
        pickupTime: data.pickupTime,
        day: data.day,
        timeId: data.timeId,
        addyUser: data.addyUser,
        deliveryFeePaidBy: data.deliveryFeePaidBy,
        paymentMethodForDeliveryFee: data.paymentMethodForDeliveryFee,
      };

      if (data.isPacakgeAmountCollected) {
        package_data.isPacakgeAmountCollected = data.isPacakgeAmountCollected;
        package_data.paymentMethodForPackagePrice = data.paymentMethodForPackagePrice;
        package_data.priceOfPackage = data.priceOfPackage;
      } else {
        package_data.isPacakgeAmountCollected = false;
      }
      if (data.assigneeId) {
        package_data.assigneeId = data.assigneeId;
        package_data.assigneeAddress = data.assigneeAddress;
      } else if (data.toMobile) {
        data.code = data.code.replace('+', '');
        package_data.toMobile = data.toMobile;
        package_data.code = data.code;
      }
      if (data.selectedCompanyId) {
        package_data.selectedCompanyId = data.selectedCompanyId;
      }
      if (data.deliveryType && data.paymentMethodForPackagePrice !== 'cash') {
        package_data.driverName = data.driverName;
        package_data.driverMobile = data.driverMobile;
        package_data.driverMobileCode = data.driverMobileCode;
      }
      package_data.sender = userDetails._id;
      if (data.assigneeId) {
        package_data.collector = data.assigneeId;
        package_data.addyUser = true;
      } else {
        package_data.addyUser = false;
      }
      let obj = new send(package_data);
      var schemaObj = await obj.save();
      if (data.deliveryType == 'addy') {
        let transporter = nodemailer.createTransport({
          host: 'smtp.hostinger.com',
          port: 587,
          auth: {
            user: 'no-rep@addy.ae',
            pass: 'PassNoRep890',
          },
        });
        const mailOptions = {
          from: 'no-rep@addy.ae', // sender address
          to: ['littycherian22@gmail.com', 'amrutha@cybaze.com'], // list of receivers
          subject: 'New Order created!', // Subject line
          html: `<p>Order id: ${schemaObj.order_id} <br> ${process.env.FRONTEND_URL}user/${schemaObj._id} </p>`, // plain text body
        };
        transporter.sendMail(mailOptions, async function (err, info) {
          if (err) {
            await user.deleteOne({
              _id: mongoose.Types.ObjectId(schemaObj._id),
            });
            return resolve({ statusCode: 400, status: false, message: 'Something went wrong: email error.' });
          } else {
          }
        });
      } else {
        let existingDriver = await Driver.findOne({ mobile: data.driverMobile });

        if (!existingDriver) {
          // generating driver id
          let last1 = await Driver.find().sort({ $natural: -1 }).limit(1);
          let lastDriverId = 0;
          if (last1.length) {
            lastDriverId = parseInt(last1[0].driverId);
          }
          lastDriverId = lastDriverId + 1;
          let driverId = lastDriverId.toString().padStart(6, '0');
          // saving new driver
          let driverDetails = {
            name: package_data.driverName,
            mobile: package_data.driverMobile,
            code: package_data.driverMobileCode,
            driverId: driverId,
            orders: [schemaObj._id],
          };

          let schemaObject = new Driver(driverDetails);

          schemaObject.save();
          console.log("schemaObject", driverDetails)
        } else {
          await Driver.updateOne(
            { mobile: package_data.driverMobile },
            {
              $push: { orders: schemaObj._id },
              $set: { name: package_data.driverName },
            }
          ).then((response) => {
            console.log(response);
          });
        }
      }
      if (data.toMobile) {
        var smsglobal = require('smsglobal')(apiKey, apiSecret);
        var payload = {
          origin: 'ADDYAE',
          destination: data.code + data.toMobile,
          message: process.env.FRONTEND_URL + 'user/' + schemaObj._id,
        };

        smsglobal.sms.send(payload, async function (error, response) {
          if (error) {
            send.deleteOne({ _id: schemaObj._id }).then((respons) => {
              return res.send({
                error: true,
                message: error.data.errors.destination.errors[0],
              });
            });
          }

          if (response) {
            let historyData = {
              order_id: schemaObj._id,
              waiting: {
                value: true,
                date: new Date(),
              },
            };
            let historyObj = OrderHistory(historyData);
            await historyObj.save();
            return resolve({
              statusCode: 200,
              status: true,
              message: 'package added',
              data: {
                _id: schemaObj._id,
                orderId: schemaObj.order_id,
              },
            });
          }
        });
        // sms sending finished
      } else {
        if (validAssignee.fcmId) {
          let fcmIdToken = [];
          let payloadData = {
            id: schemaObj._id + '',
            orderId: schemaObj.order_id,
            tokens: fcmIdToken,
            title: 'Order Placed',
            message: 'A new package assigned to you',
          };
          fcmIdToken.push(validAssignee.fcmId);
          firebaseAdmin.sendMulticastNotification(payloadData);
        }
        var smsglobal = require('smsglobal')(apiKey, apiSecret);
        var payload = {
          origin: 'ADDYAE',
          destination: validAssignee.code + validAssignee.mobile,
          message: `${process.env.FRONTEND_URL}user/${schemaObj._id}`,
        };

        smsglobal.sms.send(payload, async function (error, response) {
          if (error) {
            send.deleteOne({ _id: schemaObj._id }).then((respons) => {
              return res.send({
                error: true,
                message: error.data.errors.destination.errors[0],
              });
            });
          }

          if (response) {
          }
        });
        let historyData = {
          order_id: schemaObj._id,
          waiting: {
            value: true,
            date: new Date(),
          },
        };

        let historyObj = OrderHistory(historyData);
        await historyObj.save();
        return res.send({
          error: false,
          message: 'package added',
          data: {
            _id: schemaObj._id,
            orderId: schemaObj.order_id,
          },
        });
      }
    } catch (error) {
      reject(error);
    }
  });
};

const doCheckLink = async (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      let params = data;

      let dataValidation = checkLink(params);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');

        return resolve({ statusCode: 400, status: false, message: message });
      }

      let validOrderId = await send.findOne({
        _id: mongoose.Types.ObjectId(params.orderId),
      });
      if (!validOrderId) {
        return resolve({ statusCode: 200, status: false, message: 'Invalid Link' });
      } else {
        if (validOrderId.isApproved || validOrderId.isRejected) {
          return resolve({ statusCode: 200, status: false, message: 'Link expired' });
        }
      }

      let responseData = {
        toMobile: validOrderId.toMobile,
      };

      let existingUser = await user.findOne({
        code: validOrderId.code + '',
        mobile: validOrderId.toMobile + '',
        firstName: { $exists: true },
        lastName: { $exists: true },
        address: { $exists: true },
      });
      if(existingUser){
      console.log("testttttttttt", existingUser.address.length)
      if (existingUser.address.length) {
        responseData.toScreen = 'address';
      } else if (!existingUser.address.length) {
        responseData.toScreen = 'addressAdd';
      }
      else {
        let appUser = await user.findOne({ _id: validOrderId.assigneeId });

        if (appUser) {
          responseData.toScreen = 'address';
        } else {
          responseData.toScreen = 'know_more';
        }
      }
    }else{
      responseData.toScreen = 'know_more';
    }
      return resolve({ statusCode: 200, status: true, message: 'Valid Link', data: { responseData } });
    } catch (err) {
      reject(err);
    }
  });
};

const doAcceptPackageWeb = (data, userDetails) => {
  return new Promise(async (resolve, reject) => {
    try {
      let params = data;
      let dataValidation = {};
      // params validation
      if (params.addressId && params.addressId != '') {
        dataValidation = validateApprovePackageWebOfExistingUser(params);
      } else {
        dataValidation = validateApprovePackageWeb(params);
      }
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({ statusCode: 400, error: true, message: message });
      }
      // checking whether order is valid

      let validOrder = await send.findOne({
        _id: mongoose.Types.ObjectId(params.orderId),
      });
      // checking whether order is valid
      if (!validOrder) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid order_id' });
      }

      let existingExternalUser = await user.findOne({
        mobile: validOrder.toMobile,
        code: validOrder.code,
      });

      if (!existingExternalUser) {
        existingExternalUser = await user.findOne({
          _id: validOrder.assigneeId,
        });
      }
      // saving outside user's data
      if (!existingExternalUser) {
        params.firstName = params.firstName.trim();
        params.lastName = params.lastName.trim();

        params.firstName = params.firstName.toLowerCase();
        params.lastName = params.lastName.toLowerCase();

        params.firstName = capitalizeFirstLetter(params.firstName);
        params.lastName = capitalizeFirstLetter(params.lastName);
        let userData = {
          mobile: params.mobile,
          firstName: params.firstName,
          lastName: params.lastName,
          email: params.email,
          code: validOrder.code,
          deliveryInstruction: params.deliveryInstruction,
          address: [
            {
              address: params.address.address,
              houseNumber: params.address.house_no,
              buildingName: params.address.building_name,
              street: params.address.street,
              area: params.address.area,
              city: params.address.city,
              location: {
                latitude: params.latitude,
                longitude: params.longitude,
              },
            },
          ],
          webUser: true,
          didRegistered: 0,
        };

        let schemaObj = user(userData);

        schemaObj.save().then(async (response) => {
          await send.updateOne(
            { _id: params.orderId },
            {
              $set: {
                isApproved: true,
                status: 'approved',
                assigneeId: response._id,
                assigneeAddress: response.address[0]._id,
              },
            }
          );
        });
      } else {
        if (params.addressId) {
          // adding existing addressId to order
          await send.updateOne(
            { _id: params.orderId },
            {
              $set: {
                isApproved: true,
                status: 'approved',
                assigneeId: existingExternalUser._id,
                assigneeAddress: params.addressId,
              },
            }
          );
        } else {
          return resolve({ statusCode: 400, status: false, message: 'addressId missing' });
        }
      }
      if (validOrder.deliveryType == 'partner') {
        let smsglobal = require('smsglobal')(apiKey, apiSecret);
        let payload = {
          origin: 'ADDYAE',
          destination: validOrder.driverMobileCode + validOrder.driverMobile,
          message: `Order id:${validOrder._id},
        ${process.env.FRONTEND_URL}driver/userdriver/${validOrder._id}`,
        };

        smsglobal.sms.send(payload, async function (error, response) {
          if (error) {
            return resolve({ statusCode: 400, status: false, message: 'Something went wrong: driver sms error' });
          }
          if (response) {
            return resolve({ statusCode: 200, status: true, message: 'Order confirmed' });
          }
        });
      } else {
        return resolve({ statusCode: 200, status: true, message: 'Order confirmed' });
      }
    } catch (error) {
      return resolve({ statusCode: 400, status: false, message: error + '' });
    }
  });
};

const doRegisterUserFromWeb = (data) => {
  return new Promise(async (resolve, reject) => {


    try {
      const dataValidation = await validateregister(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      let params = data;
      params.firstName = params.firstName.trim();
      params.lastName = params.lastName.trim();

      params.firstName = params.firstName.toLowerCase();
      params.lastName = params.lastName.toLowerCase();

      params.firstName = capitalizeFirstLetter(params.firstName);
      params.lastName = capitalizeFirstLetter(params.lastName);
      let userData = {
        mobile: params.mobile,
        firstName: params.firstName,
        lastName: params.lastName,
        email: params.email,
        code: params.code,
        deliveryInstruction: params.deliveryInstruction,
        address: [
          {
            address: params.address.address,
            houseNumber: params.address.house_no,
            buildingName: params.address.building_name,
            street: params.address.street,
            area: params.address.area,
            city: params.address.city,
            location: {
              latitude: params.latitude,
              longitude: params.longitude,
            },
          },
        ],
        webUser: true,
        didRegistered: 0,
      };

      let schemaObj = user(userData);

      schemaObj.save().then(async (response) => {
        if (response) {
          await send.updateOne(
            { _id: params.orderId },
            {
              $set: {
                assigneeId: response._id,
                assigneeAddress: response.address[0]._id,
              },
            }
          );
          return resolve({ statusCode: 200, status: true, message: 'User Details Added', data: response._id});
        }
      });

    } catch (error) {
      return resolve({ statusCode: 400, status: false, message: error + '' });
    }
  });
};

const doGetCustomerOrders = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let today = moment().format('MMM DD,YY');
      let tomorrow = moment().add(1, 'days').format('MMM DD,YY');

      let approveIncomingSend = await send
        .aggregate([
          {
            $match: {
              isApproved: false,
              isRejected: false,
              userId: mongoose.Types.ObjectId(res._id),
              status: 'waiting',
            },
          },
          {
            $lookup: {
              from: 'accounts',
              localField: 'userId',
              foreignField: '_id',
              as: 'assigner',
            },
          },
          {
            $project: {
              type: 'send',
              orderId: '$order_id',
              title: '$details',
              status: 1,
              expectedDeliveryDate: '$day',
              paymentMethod: '$paymentMethodForPackagePrice',
              logo: { $ifNull: [{ $first: '$assigner.logo' }, ""] },
              name: {
                $concat: [
                  { $arrayElemAt: ['$assigner.firstName', 0] },
                  ' ',
                  { $arrayElemAt: ['$assigner.lastName', 0] },
                ],
              },
            },
          },
        ])
        .catch((error) => {
          return resolve({ statusCode: 400, status: false, message: 'error' });
        });
      let timeZone = timeZoneValidation(res.code);
      approveIncomingSend.forEach((e, i) => {
        let newDate = moment(e.expectedDeliveryDate, 'DD-MM-YYYY').add(1, 'days');
        e.expectedDeliveryDate = moment(newDate).tz(timeZone);
        e.expectedDeliveryDate = moment(e.expectedDeliveryDate).format('MMM DD,YY');
        if (e.expectedDeliveryDate == today) {
          e.expectedDeliveryDate = "Today"
        }
        if (e.expectedDeliveryDate == tomorrow) {
          e.expectedDeliveryDate = "Tomorrow"
        }
      });

      let approveIncomingCollect = await collect
        .aggregate([
          {
            $match: {
              isApproved: false,
              isRejected: false,
              userId: mongoose.Types.ObjectId(res._id),
              status: 'waiting',
            },
          },
          {
            $lookup: {
              from: 'accounts',
              localField: 'userId',
              foreignField: '_id',
              as: 'assigner',
            },
          },
          {
            $project: {
              type: 'collect',
              orderId: '$order_id',
              title: '$details',
              status: 1,
              date_time: '$createdAt',
              paymentMethod: '$paymentMethodForPackagePrice',
              logo: { $ifNull: [{ $first: '$assigner.logo' }, ""] },
              name: {
                $concat: [
                  { $arrayElemAt: ['$assigner.firstName', 0] },
                  ' ',
                  { $arrayElemAt: ['$assigner.lastName', 0] },
                ],
              },
            },
          },
        ])
        .catch((error) => {
          return resolve({ statusCode: 400, status: false, message: 'error' });
        });

      approveIncomingCollect.forEach((e, i) => {
        e.expectedDeliveryDate = moment(e.expectedDeliveryDate).tz(timeZone);
        e.expectedDeliveryDate = moment(e.expectedDeliveryDate).format('MMM DD,YY');
        if (e.expectedDeliveryDate == today) {
          e.expectedDeliveryDate = "Today"
        }
        if (e.expectedDeliveryDate == tomorrow) {
          e.expectedDeliveryDate = "Tomorrow"
        }
      });

      let approveIncoming = [...approveIncomingSend, ...approveIncomingCollect];

      // Active orders from send package
      let activeOrdersSend = await send
        .aggregate([
          {
            $match: {
              status: {
                $nin: [
                  'waiting',
                  'rejected',
                  'driver_rejected',
                  'pickup_failed',
                  'delivery_completed',
                  'delivery_failed',
                ],
              },
              $or: [
                { userId: mongoose.Types.ObjectId(req.userId) },
                { assigneeId: mongoose.Types.ObjectId(req.userId) },
              ],
            },
          },
          {
            $lookup: {
              from: 'accounts',
              localField: 'userId',
              foreignField: '_id',
              as: 'assigner',
            },
          },
          {
            $lookup: {
              from: 'accounts',
              localField: 'assigneeId',
              foreignField: '_id',
              as: 'assignee',
            },
          },
          {
            $project: {
              type: 'send',
              orderId: '$order_id',
              title: '$details',
              status: 1,
              expectedDeliveryDate: '$createdAt',
              paymentMethod: '$paymentMethodForPackagePrice',
              toMobile: 1,
              code: 1,
              logo: { $ifNull: [{ $first: '$assigner.logo' }, ""] },
              assigner: { $arrayElemAt: ['$assigner', 0] },
              assignee: { $arrayElemAt: ['$assignee', 0] },
              isDriverAssigned: 1,
              deliveryType: 1,
              isApproved: 1,
            },
          },
        ])
        .catch((error) => {
          console.log(error);
        });
      let filteredActiveOrdersSend = [];
      for (let e of activeOrdersSend) {
        if (e.status == 'waiting' && e.assigner._id + '' != req.userId + '') {
        } else {
          if (e.toMobile && !e.assignee) {
            // external user
            let existingUser = await user.findOne({
              code: e.code + '',
              mobile: e.toMobile + '',
              firstName: { $exists: true },
              lastName: { $exists: true },
            });

            if (existingUser) {
              e.name = existingUser.firstName + ' ' + existingUser.lastName;
            } else {
              e.name = e.toMobile;
            }
          } else {
            // addy user
            if (e.assigner._id == req.userId) {
              if (e.assignee.firstName && e.assignee.lastName) {
                e.name = e.assignee.firstName + ' ' + e.assignee.lastName;
              } else {
                e.name = e.toMobile;
              }
            } else {
              if (e.assigner.firstName && e.assigner.lastName) {
                e.name = e.assigner.firstName + ' ' + e.assigner.lastName;
              } else {
                e.name = e.toMobile;
              }
            }
          }
          e.expectedDeliveryDate = moment(e.expectedDeliveryDate).tz(timeZone);
          e.expectedDeliveryDate = moment(e.expectedDeliveryDate).format('MMM DD,YY');
          if (e.expectedDeliveryDate == today) {
            e.expectedDeliveryDate = "Today"
          }
          if (e.expectedDeliveryDate == tomorrow) {
            e.expectedDeliveryDate = "Tomorrow"
          }
          delete e.assignee;
          delete e.assigner;
          delete e.toMobile;
          delete e.code;
          filteredActiveOrdersSend.push(e);
        }
      }

      // Active orders from collect package
      let activeOrdersCollect = await collect
        .aggregate([
          {
            $match: {
              status: {
                $nin: [
                  'waiting',
                  'rejected',
                  'driver_rejected',
                  'pickup_failed',
                  'delivery_completed',
                  'delivery_failed',
                ],
              },
              $or: [
                { userId: mongoose.Types.ObjectId(req.userId) },
                { assigneeId: mongoose.Types.ObjectId(req.userId) },
              ],
            },
          },
          {
            $lookup: {
              from: 'accounts',
              localField: 'userId',
              foreignField: '_id',
              as: 'assigner',
            },
          },
          {
            $lookup: {
              from: 'accounts',
              localField: 'assigneeId',
              foreignField: '_id',
              as: 'assignee',
            },
          },
          {
            $project: {
              type: 'collect',
              orderId: '$order_id',
              title: '$details',
              status: 1,
              expectedDeliveryDate: '$createdAt',
              paymentMethod: '$paymentMethodForPackagePrice',
              toMobile: 1,
              code: 1,
              logo: { $ifNull: [{ $first: '$assigner.logo' }, ""] },
              assigner: { $arrayElemAt: ['$assigner', 0] },
              assignee: { $arrayElemAt: ['$assignee', 0] },
              isDriverAssigned: 1,
              deliveryType: 1,
              isApproved: 1,
            },
          },
        ])
        .catch((error) => {
          console.log(error);
        });
      let filteredActiveOrdersCollect = [];
      for (let e of activeOrdersCollect) {
        if (e.status == 'waiting' && e.assigner._id + '' != req.userId + '') {
        } else {
          if (e.toMobile && !e.assignee) {
            // external user
            let existingUser = await user.findOne({
              code: e.code + '',
              mobile: e.toMobile + '',
              firstName: { $exists: true },
              lastName: { $exists: true },
            });

            if (existingUser) {
              e.name = existingUser.firstName + ' ' + existingUser.lastName;
            } else {
              e.name = e.toMobile;
            }
          } else {
            // addy user
            if (e.assigner._id == req.userId) {
              if (e.assignee.firstName && e.assignee.lastName) {
                e.name = e.assignee.firstName + ' ' + e.assignee.lastName;
              } else {
                e.name = e.toMobile;
              }
            } else {
              if (e.assigner.firstName && e.assigner.lastName) {
                e.name = e.assigner.firstName + ' ' + e.assigner.lastName;
              } else {
                e.name = e.toMobile;
              }
            }
          }
          e.expectedDeliveryDate = moment(e.expectedDeliveryDate).tz(timeZone);
          e.expectedDeliveryDate = moment(e.expectedDeliveryDate).format('MMM DD,YY');
          if (e.expectedDeliveryDate == today) {
            e.expectedDeliveryDate = "Today"
          }
          if (e.expectedDeliveryDate == tomorrow) {
            e.expectedDeliveryDate = "Tomorrow"
          }
          delete e.assignee;
          delete e.assigner;
          delete e.toMobile;
          delete e.code;
          filteredActiveOrdersCollect.push(e);
        }
      }

      // Get orders from ministore of user
      let ministoreOrders = [];
      ministoreOrders = await getOrderDetailsByUserId(res._id);
      let timeZoneMinistore = timeZoneValidation(res.code);
      ministoreOrders.forEach((e, i) => {
        
        e.expectedDeliveryDate = moment(e.expectedDeliveryDate).tz(timeZoneMinistore);
        e.expectedDeliveryDate = moment(e.expectedDeliveryDate).format('MMM DD,YYYY');
        if (e.expectedDeliveryDate == today) {
          e.expectedDeliveryDate = "Today"
        }
        if (e.expectedDeliveryDate == tomorrow) {
          e.expectedDeliveryDate = "Tomorrow"
        }
      });

      let orderStatus = ['cancelled', 'rejected', 'delivered'];
      let activeMinistoreOders = ministoreOrders.filter((item) => {
        if (!orderStatus.includes(item.status)) {
          return true;
        }
      });

      let activeOrders = [...activeOrdersSend, ...activeOrdersCollect, ...activeMinistoreOders];

      // get the inactive orders
      let inactiveMinistoreOders = ministoreOrders.filter((item) => {
        if (orderStatus.includes(item.status)) {
          return true;
        }
      });

      // get inactiveOrders in send
      let inactiveOrdersSend = await send
        .aggregate([
          {
            $match: {
              status: {
                $in: ['rejected', 'driver_rejected', 'pickup_failed', 'delivery_completed', 'delivery_failed'],
              },
              $or: [{ userId: mongoose.Types.ObjectId(res._id) }, { assigneeId: mongoose.Types.ObjectId(res._id) }],
            },
          },
          {
            $lookup: {
              from: 'accounts',
              localField: 'userId',
              foreignField: '_id',
              as: 'assigner',
            },
          },
          {
            $lookup: {
              from: 'accounts',
              localField: 'assigneeId',
              foreignField: '_id',
              as: 'assignee',
            },
          },
          {
            $project: {
              orderId: '$order_id',
              title: '$details',
              status: 1,
              expectedDeliveryDate: '$createdAt',
              paymentMethod: '$paymentMethodForPackagePrice',
              toMobile: 1,
              code: 1,
              assigner: { $arrayElemAt: ['$assigner', 0] },
              assignee: { $arrayElemAt: ['$assignee', 0] },
            },
          },
        ])
        .catch((error) => {
          console.log(error);
        });
      for (let e of inactiveOrdersSend) {
        if (e.toMobile) {
          // external user
          let existingUser = await user.findOne({
            code: e.code + '',
            mobile: e.toMobile + '',
          });
          if (existingUser) {
            e.name = existingUser.firstName + ' ' + existingUser.lastName;
          } else {
            e.name = e.toMobile;
          }
        } else {
          // addy user
          if (e.assigner._id == req.userId) {
            e.name = e.assignee.firstName + ' ' + e.assignee.lastName;
          } else {
            e.name = e.assigner.firstName + ' ' + e.assigner.lastName;
          }
        }
        e.expectedDeliveryDate = moment(e.expectedDeliveryDate).tz(timeZone);
        e.expectedDeliveryDate = moment(e.expectedDeliveryDate).format('MMM DD, h.mm a');
        delete e.assignee;
        delete e.assigner;
        delete e.code;
        delete e.toMobile;
      }

      // get inactiveOrders in collect
      let inactiveOrdersCollect = await collect
        .aggregate([
          {
            $match: {
              status: {
                $in: ['rejected', 'driver_rejected', 'pickup_failed', 'delivery_completed', 'delivery_failed'],
              },
              $or: [{ userId: mongoose.Types.ObjectId(res._id) }, { assigneeId: mongoose.Types.ObjectId(res._id) }],
            },
          },
          {
            $lookup: {
              from: 'accounts',
              localField: 'userId',
              foreignField: '_id',
              as: 'assigner',
            },
          },
          {
            $lookup: {
              from: 'accounts',
              localField: 'assigneeId',
              foreignField: '_id',
              as: 'assignee',
            },
          },
          {
            $project: {
              orderId: '$order_id',
              title: '$details',
              status: 1,
              expectedDeliveryDate: '$createdAt',
              paymentMethod: '$paymentMethodForPackagePrice',
              toMobile: 1,
              code: 1,
              assigner: { $arrayElemAt: ['$assigner', 0] },
              assignee: { $arrayElemAt: ['$assignee', 0] },
            },
          },
        ])
        .catch((error) => {
          console.log(error);
        });
      for (let e of inactiveOrdersCollect) {
        if (e.toMobile) {
          // external user
          let existingUser = await user.findOne({
            code: e.code + '',
            mobile: e.toMobile + '',
          });
          if (existingUser) {
            e.name = existingUser.firstName + ' ' + existingUser.lastName;
          } else {
            e.name = e.toMobile;
          }
        } else {
          // addy user
          if (e.assigner._id == req.userId) {
            e.name = e.assignee.firstName + ' ' + e.assignee.lastName;
          } else {
            e.name = e.assigner.firstName + ' ' + e.assigner.lastName;
          }
        }
        e.expectedDeliveryDate = moment(e.expectedDeliveryDate).tz(timeZone);
        e.expectedDeliveryDate = moment(e.expectedDeliveryDate).format('MMM DD, h.mm a');
        delete e.assignee;
        delete e.assigner;
        delete e.code;
        delete e.toMobile;
      }

      let inactiveOrders = [...inactiveOrdersSend, ...inactiveOrdersCollect, ...inactiveMinistoreOders];

      let approveIds = [];
      let newArray = [];

      for (let item of approveIncoming) {
        approveIds.push(item._id + '');
      }

      for (let item1 of activeOrders) {
        if (!approveIds.includes(item1._id + '')) {
          if (item1.status == 'approved' && item1.isDriverAssigned && item1.deliveryType == 'addy') {
            item1.status = 'driver_approved';
          }
          if (!item1.isApproved && item1.status == 'Assigned to driver') {
            item1.status = 'waiting';
          }
          newArray.push(item1);
        }
      }

      // Get orders from ministore of user
      // let ministoreOrders = [];
      // ministoreOrders = await getOrderDetailsByUserId(res._id);
      // let timeZoneMinistore = timeZoneValidation(res.code);
      // ministoreOrders.forEach((e, i) => {
      //   e.expectedDeliveryDate = moment(e.expectedDeliveryDate).tz(timeZoneMinistore);
      //   e.expectedDeliveryDate = moment(e.expectedDeliveryDate).format('MMM DD');
      // });

      // // get the pending orders
      // let activeMinistoreOders = ministoreOrders.filter((item) => {
      //   if (item.orderStatus == 'pending' || item.orderStatus == 'accepted') {
      //     return true;
      //   }
      // });
      // // get the pending orders
      // let inActiveMinistoreOders = ministoreOrders.filter((item) => {
      //   if (item.orderStatus == 'rejected' || item.orderStatus == 'cancelled' || item.orderStatus == 'delivered') {
      //     return true;
      //   }
      // });

      return resolve({
        statusCode: 200,
        status: true,
        message: 'Customer orders are',
        data: {
          approveIncoming: approveIncoming.reverse(),
          active_orders: newArray.reverse(),
          inactive_orders: inactiveOrders.reverse(),
        },
      });
    } catch (error) {
      return resolve({ statusCode: 500, status: false, message: error + '' });
    }
  });
};

const doGetOrderDetail = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let params = req;
      const dataValidation = await validateOrderDetails(params);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      let validOrder = await send
        .findOne(
          {
            $and: [
              { _id: mongoose.Types.ObjectId(params.orderId) },
              {
                $or: [{ userId: mongoose.Types.ObjectId(res._id) }, { assigneeId: mongoose.Types.ObjectId(res._id) }],
              },
            ],
          },
          {
            status: 1,
            photos: 1,
            sender: 1,
            collector: 1,
            deliveryType: 1,
            toMobile: 1,
            code: 1,
            details: 1,
            driverName: 1,
            driverMobile: 1,
            isDriverAssigned: 1,
          }
        )
        .populate({
          path: 'driverId',
          select: ['name', 'image', 'mobile'],
        })
        .lean();

      if (validOrder) {
        let collector = await user.findOne({
          _id: mongoose.Types.ObjectId(validOrder.collector),
        });

        if (collector) {
          validOrder.sender_collector = {
            _id: collector._id,
            firstName: collector.firstName,
            lastName: collector.lastName,
          };
          if (collector.photos) {
            validOrder.sender_collector.image =
            collector.photos;
          } else {
            validOrder.sender_collector.image = "";
          }
        } else {
          let existingUser = await user.findOne({
            code: validOrder.code + '',
            mobile: validOrder.toMobile + '',
          });
          if (existingUser) {
            validOrder.sender_collector = {
              _id: existingUser._id,
              firstName: existingUser.firstName,
              lastName: existingUser.lastName,
            };
          } else {
            validOrder.sender_collector = {
              mobile: validOrder.toMobile,
            };
          }

          delete validOrder.toMobile;
          delete validOrder.code;
        }

        delete validOrder.sender;
        delete validOrder.collector;
        validOrder.sender = 1;

        if (validOrder.deliveryType == 'addy') {
          if (validOrder.driverId) {
            validOrder.driver = validOrder.driverId;
            if (validOrder.driver.name) {
              validOrder.driver.name = validOrder.driver.name.toLowerCase();
              validOrder.driver.name = capitalizeWords(validOrder.driver.name);
            }

            if (validOrder.driver.image) {
              validOrder.driver.image = validOrder.driver.image;
            } else {
              validOrder.driver.image = '';
            }
            delete validOrder.driverId;
            delete validOrder.deliveryType;
          } else {
            validOrder.driver = {};

            delete validOrder.driverId;
            delete validOrder.deliveryType;
          }
        } else {
          validOrder.driverName = validOrder.driverName.toLowerCase();
          validOrder.driverName = capitalizeWords(validOrder.driverName);
          validOrder.driver = {
            name: validOrder.driverName,
            mobile: validOrder.driverMobile,
          };

          delete validOrder.driverName;
          delete validOrder.driverMobile;
          delete validOrder.deliveryType;
        }

        let order_history = await OrderHistory.findOne(
          { order_id: params.orderId },
          {
            _id: 0,
            order_id: 0,
            __v: 0,
          }
        ).lean();

        let orderTracking = [];

        let orderTrackingResponseModel = [
          {
            status: '',
            date: '',
            active: false,
          },
          {
            status: '',
            date: '',
            active: true,
          },
          {
            status: '',
            date: '',
            active: false,
          },
          {
            status: '',
            date: '',
            active: false,
          },
        ];
        // assigning values to orderTracking
        if (order_history.waiting) {
          orderTracking.push({
            status: 'Order placed',
            date: order_history.waiting.date,
            active: false,
          });
        }

        if (order_history.Approved) {
          orderTracking.push({
            status: 'Approved',
            date: order_history.Approved.date,
            active: order_history.Approved.value,
          });
        } else {
          if (!order_history.Rejected) {
            orderTracking.push({
              status: 'Waiting for approval',
              date: '',
              active: true,
            });
          }
        }

        if (order_history.Rejected) {
          orderTracking.push({
            status: 'Rejected',
            date: order_history.Rejected.date,
            active: order_history.Rejected.value,
          });
        }
        if (order_history.assigned_to_driver) {
          orderTracking.push({
            status: 'Assigning to Driver',
            date: order_history.assigned_to_driver.date,
            active: order_history.assigned_to_driver.value,
          });
        }
        if (order_history.driver_approved) {
          orderTracking.push({
            status: 'Assigned to delivery driver',
            date: order_history.driver_approved.date,
            active: order_history.driver_approved.value,
          });
        } else {
          if (!order_history.driver_rejected) {
            orderTracking.push({
              status: 'Assigned to delivery driver',
              date: '',
              active: false,
            });
          }
        }

        if (order_history.driver_rejected && !order_history.driver_approved) {
          orderTracking.push({
            status: 'Rejected by driver',
            date: order_history.driver_rejected.date,
            active: order_history.driver_rejected.value,
          });
        }

        if (order_history.pickup_initiated) {
          orderTracking.push({
            status: 'Pickup initiated',
            date: order_history.pickup_initiated.date,
            active: order_history.pickup_initiated.value,
          });
        } else {
          orderTracking.push({
            status: 'Pickup',
            date: '',
            active: false,
          });
        }

        if (order_history.pickup_completed) {
          orderTracking.push({
            status: 'Pickup completed',
            date: order_history.pickup_completed.date,
            active: order_history.pickup_completed.value,
          });
        }

        if (order_history.pickup_failed) {
          orderTracking.push({
            status: 'Pickup failed',
            date: order_history.pickup_failed.date,
            active: order_history.pickup_failed.value,
          });
        }

        if (order_history.delivery_initiated) {
          orderTracking.push({
            status: 'Delivery initiated',
            date: order_history.delivery_initiated.date,
            active: order_history.delivery_initiated.value,
          });
        } else {
          orderTracking.push({
            status: 'Delivery',
            date: '',
            active: false,
          });
        }

        if (order_history.delivery_completed) {
          orderTracking.push({
            status: 'Delivery completed',
            date: order_history.delivery_completed.date,
            active: order_history.delivery_completed.value,
          });
        }

        if (order_history.delivery_failed) {
          orderTracking.push({
            status: 'Delivery failed',
            date: order_history.delivery_failed.date,
            active: order_history.delivery_failed.value,
          });
        }

        if (validOrder.status == 'approved' && validOrder.isDriverAssigned) {
          for (let it of orderTracking) {
            if (it.status == 'Approved') {
              it.active = false;
            }
          }
        }
        let activeInTracking = orderTracking.find((e) => e.active);
        orderTrackingResponseModel[1].status = activeInTracking.status;
        orderTrackingResponseModel[1].date = activeInTracking.date;

        let indexOfActiveInTracking = orderTracking.indexOf(activeInTracking);

        // assigning previous tracking history
        if (indexOfActiveInTracking != 0) {
          orderTrackingResponseModel[0].status = orderTracking[indexOfActiveInTracking - 1].status;
          orderTrackingResponseModel[0].date = orderTracking[indexOfActiveInTracking - 1].date;
        }

        if (activeInTracking.status != 'Rejected' && activeInTracking.status != 'Rejected by driver') {
          if (indexOfActiveInTracking < orderTracking.length - 2) {
            orderTrackingResponseModel[2].status = orderTracking[indexOfActiveInTracking + 1].status;

            orderTrackingResponseModel[3].status = orderTracking[indexOfActiveInTracking + 2].status;
          } else if (indexOfActiveInTracking == orderTracking.length - 2) {
            orderTrackingResponseModel[2].status = orderTracking[indexOfActiveInTracking + 1].status;
          }
        } else {
          orderTrackingResponseModel[2].status = '';
          orderTrackingResponseModel[3].status = '';
        }

        orderTrackingResponseModel.forEach((e, i) => {
          let timeZone = timeZoneValidation(res.code);
          if (req.lang == 'english') {
            if (e.date) {
              e.date = moment(e.date).tz(timeZone);

              e.date = moment(e.date).format('h.mm a DD-MM-YYYY');
            }
          } else {
            if (e.date) {
              e.date = moment(e.date).tz(timeZone);

              e.date = moment(e.date).format('DD-MM-YYYY hh.mm a');
            }
          }
        });
        return resolve({
          statusCode: 200,
          status: true,
          message: 'Order details are',
          data: {
            order_details: validOrder,
            order_history: orderTrackingResponseModel,
          },
        });
      } else {
        return resolve({ statusCode: 400, status: false, message: 'Invalid order id' });
      }
    } catch (error) {
      reject(error);
    }
  });
};

const doAcceptPackage = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let params = req;
      let dataValidation = {};
      dataValidation = validateApprovePackage(params);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({ statusCode: 400, status: false, message: message });
      }
      validOrder = await send.findOne({ _id: mongoose.Types.ObjectId(params.order_id) });
      if (!validOrder) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid orderId' });
      }
      if (params.approved == 1) {
        await send.updateOne(
          { _id: params.order_id },
          {
            $set: {
              isApproved: true,
              status: 'approved',
              assigneeAddress: params.address_id,
            },
          }
        );

        let approvedObj = {
          value: true,
          date: new Date(),
        };
        await OrderHistory.updateOne(
          { order_id: params.order_id },
          {
            $set: { Approved: approvedObj, 'waiting.value': false },
          }
        );
        var assignerId = validOrder.userId;
        let obj = new Notification({
          userId: assignerId,
          message: 'Your package with order id ' + validOrder.order_id + ' is accepted',
          is_read: 0,
        });
        await obj.save();
        let notificationUser = await user.findOne({
          _id: mongoose.Types.ObjectId(assignerId),
        });
        if (notificationUser) {
          if (notificationUser.fcmId) {
            let fcmIdToken = [];
            let payloadData = {
              id: validOrder._id + '',
              orderId: validOrder.order_id + '',
              tokens: fcmIdToken,
              title: 'Order Accepted',
              message: 'Your package with order id ' + validOrder.order_id + ' is accepted',
            };
            fcmIdToken.push(notificationUser.fcmId);
            firebaseAdmin.sendMulticastNotification(payloadData);
          }
        }
        // logic for driver assignment

        if (validOrder.deliveryType == 'addy') {
          let driver = await Driver.findOne({ _id: validOrder.driverId });
          if (!validOrder.isDriverAssigned) {
            let smsglobal = require('smsglobal')(apiKey, apiSecret);

            let payload = {
              origin: 'ADDYAE',
              destination: validOrder.driverMobileCode + validOrder.driverMobile,
              message: `Order id:${validOrder.order_id},
              ${process.env.FRONTEND_URL}driver/${validOrder._id}`,
            };

            smsglobal.sms.send(payload, async function (error, response) {
              if (error) {
                return resolve({ statusCode: 400, status: false, message: 'Something went wrong: driver sms error' });
              }
              if (response) {
                return resolve({ statusCode: 200, status: true, message: 'Order confirmed' });
              }
            });
          } else {
            return resolve({ statusCode: 200, status: true, message: 'Order confirmed' });
          }
        } else if (validOrder.deliveryType == 'partner') {
          //Existing Partner

          let smsglobal = require('smsglobal')(apiKey, apiSecret);
          let payload = {
            origin: 'ADDYAE',
            destination: validOrder.driverMobileCode + validOrder.driverMobile,
            message: `Order id:${validOrder.order_id},
            ${process.env.FRONTEND_URL}driver/${validOrder._id}`,
          };

          smsglobal.sms.send(payload, async function (error, response) {
            if (error) {
              return resolve({ statusCode: 400, status: false, message: 'Something went wrong: driver sms error' });
            }

            if (response) {
              return resolve({ statusCode: 200, status: true, message: 'Order confirmed' });
            }
          });
        } else {
          return resolve({ statusCode: 200, status: true, message: 'Order confirmed' });
        }
      } else {
        await send.updateOne({ _id: params.order_id }, { $set: { isRejected: true, status: 'rejected' } });

        let rejectedObj = {
          value: true,
          date: new Date(),
        };

        await OrderHistory.updateOne(
          { order_id: params.order_id },
          {
            $set: { Rejected: rejectedObj, 'waiting.value': false },
          }
        );

        var userId = validOrder.userId;

        var notfication = new Notification({
          userId: userId,
          message: 'Your package with order id ' + validOrder.order_id + ' is rejected',
          is_read: 0,
        });

        await notfication.save();
        let notificationUser = await user.findOne({
          _id: mongoose.Types.ObjectId(userId),
        });
        if (notificationUser) {
          if (notificationUser.fcmId) {
            let fcmIdToken = [];
            let payloadData = {
              id: validOrder._id + '',
              orderId: validOrder.order_id + '',
              tokens: fcmIdToken,
              title: 'Order Rejected',
              message: 'Your package with order id ' + validOrder.order_id + ' is rejected',
            };
            fcmIdToken.push(notificationUser.fcmId);
            firebaseAdmin.sendMulticastNotification(payloadData);
          }
        }
        return resolve({ statusCode: 400, status: false, message: 'Order rejected' });
      }
    } catch (error) {
      return resolve({ statusCode: 200, status: false, message: error + '' });
    }
  });
};

const doAddComplaint = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let params = req;
      let dataValidation = {};
      dataValidation = validateAddComplaint(params);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({ statusCode: 400, status: false, message: message });
      }
      const id = params.orderId;
      let order = "";
      order = await send.findOne({
        _id: mongoose.Types.ObjectId(req.orderId),
      });
      if (!order) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid orderId' });
      }
      const data = res;
      const userId = data.userId;
      const obj = {
        speaker: userId,
        message: params.message,
        createdAt: Date.now(),
      };
      await send.updateOne({ _id: id }, { $push: { complaints: obj } })
        .then((_) => {
          let transporter = nodemailer.createTransport({
            host: "smtp.hostinger.com",
            port: 587,
            auth: {
              user: "no-rep@addy.ae",
              pass: "PassNoRep890",
            },
          });
          const mailOptions = {
            from: "no-rep@addy.ae", // sender address
            to: [
              "littycherian22@gmail.com"
            ], // list of receivers
            subject: "Order complaint", // Subject line
            html:
              "<p>Order id:" +
              order.order_id +
              "<br>Complaint: " +
              params.message +
              "</p>", // plain text body
          };
          transporter.sendMail(mailOptions, function (err, info) {
            if (err) {
              return resolve({ statusCode: 400, status: false, message: err + "" });
            } else {
              return resolve({ statusCode: 200, status: true, message: 'Complaint submitted successfully' });
            }
          });
        })
        .catch((err) => {
          return resolve({ statusCode: 400, status: false, message: 'Something went wrong' });
        });
    } catch (error) {
      return resolve({ statusCode: 400, status: false, message: error + '' });
    }
  });
};

const doCancelOrderBySender = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const cancelOrderStatus = 'cancelled';
      //validate incoming data
      const dataValidation = await validateCancelOrder(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      const { id } = data;
      const userId = user._id;

      let validOrder = await order.findOne({
        _id: mongoose.Types.ObjectId(id),
        userId: mongoose.Types.ObjectId(userId),
        isActive: true,
      });
      const sellerId = validOrder._id;

      if (!validOrder) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid id' });
      }

      let orgOrderStatus = validOrder.orderStatus;
      let orderStatus = ['pending', 'accepted', 'confirmed', 'deliveryAssigned', 'deliveryRejected'];

      // check the order status
      if (!orderStatus.includes(orgOrderStatus))
        return resolve({ statusCode: 400, status: false, message: `Sorry, We can't cancel your order` });

      order
        .updateOne(
          { _id: mongoose.Types.ObjectId(id) },
          {
            $set: { orderStatus: cancelOrderStatus },
          }
        )
        .then(async (response) => {
          let orderDetail = {
            id: id,
            orderId: validOrder.orderId,
            sellerId: sellerId,
            status: cancelOrderStatus,
            notificationStatus: cancelOrderStatus,
            userId: userId,
          };
          // add cancel order details to order history and notification
          const orderDetails = await addOrderHistoryDetails(orderDetail);

          // send message to driver about this order cancel
          if (validOrder?.selectedDriverId) {
            let isDriverExist = await sellerDriver.findById({ sellerId: sellerId, _id: validOrder.selectedDriverId });
            if (isDriverExist) {
              await sellerDriver.updateOne(
                { _id: mongoose.Types.ObjectId(validOrder.selectedDriverId) },
                { $pull: { orders: mongoose.Types.ObjectId(id) } }
              );
              let driverCode = isDriverExist.code;
              let driverMobile = isDriverExist.mobile;
              let payload = {
                origin: 'ADDYAE',
                destination: driverCode + driverMobile,
                message: `Assigned order with order id ${validOrder.orderId} is cancelled`,
              };
              console.log(payload);
              // await sendSms(payload)
              //   .then((_) => {
              //     return resolve({ statusCode: 200, status: true, message: 'Order confirmed successfully' });
              //   })
              //   .catch((error) => {
              //     return resolve({ statusCode: 400, status: false, message: error});
              //   });
            }
          }

          return resolve({ statusCode: 200, status: true, message: 'Order cancelled successfully' });
        });
    } catch (error) {
      reject(error);
    }
  });
};

const doGetUserDetail = async (req) => {
  return new Promise(async (resolve, reject) => {
    try {
      let params = req;
      const dataValidation = await validateOrderDetails(params);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      let validOrder = await send
        .findOne(
          {
            _id: mongoose.Types.ObjectId(params.orderId),

          },
          {
            status: 1,
            photos: 1,
            sender: 1,
            details: 1,
            assigneeId: 1,
            deliveryType: 1,
            addyUser:1,
            toMobile: 1,
            code: 1,
            isDriverAssigned: 1,
          }
        )
        .populate({
          path: 'sender',
          select: ['firstName', 'lastName', 'mobile', 'logo'],
        })
        .lean();

      if (validOrder) {
        return resolve({
          statusCode: 200,
          status: true,
          message: 'Order details are',
          data: {
            userDetails: validOrder
          },
        });
      } else {
        return resolve({ statusCode: 400, status: false, message: 'Invalid order id' });
      }
    } catch (error) {
      reject(error);
    }
  });
};


// get active courier company by name or phone number
const doSearchCourierCompaniesByUser = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      let { searchBy } = data;

      let searchQuery;
      searchQuery = [
        { mobile: { $regex: `${searchBy}`, $options: 'i' } },
        { companyName: { $regex: `${searchBy}`, $options: 'i' } },
      ];

      //get user details
      const userDetails = await Account.findOne({
        _id: user._id, didRegistered: 1
      });
      if (!userDetails) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid user' });
      }
      if (userDetails) countryCode = userDetails.countryCode;

      let courierCompanies = await courierCompany
        .aggregate([
          {
            $match: {
              isActive: true,
              countryCode: countryCode,
              ...(searchBy && { $or: searchQuery }),
            },
          },
          {
            $project: {
              __v: 0,
              createdAt: 0,
              updatedAt: 0,
              boxRate: 0
            },
          },
        ])
        .sort({ _id: -1 });

      return resolve({ statusCode: 200, status: true, message: 'Success', data: { courierCompanies } });
    } catch (error) {
      reject(error);
    }
  });
};


// get active courier company by country
const doListCourierCompaniesByUser = (user) => {
  return new Promise(async (resolve, reject) => {
    try {

      let countryCode = '';
      //get user details
      const userDetails = await Account.findOne({
        _id: user._id, didRegistered: 1
      });
      if (!userDetails) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid user' });
      }
      if (userDetails) countryCode = userDetails.countryCode;

      let courierCompanies = await courierCompany
        .aggregate([
          {
            $match: {
              isActive: true,
              countryCode: countryCode
            },
          },
          {
            $project: {
              __v: 0,
              createdAt: 0,
              updatedAt: 0,
              boxRate: 0
            },
          },
        ])
        .sort({ _id: -1 });

      return resolve({ statusCode: 200, status: true, message: 'Success', data: { courierCompanies } });
    } catch (error) {
      reject(error);
    }
  });
};
module.exports = {
  doGetUserByMobile,
  doAddSendDetails,
  doGetUserByUsername,
  doCheckLink,
  doAcceptPackageWeb,
  doGetCustomerOrders,
  doAcceptPackage,
  doGetOrderDetail,
  doAddComplaint,
  doCancelOrderBySender,
  doGetUserDetail,
  doSearchCourierCompaniesByUser,
  doListCourierCompaniesByUser,
  doRegisterUserFromWeb
};
