const { doGetUserByMobile,doAddSendDetails,
  doGetUserByUsername,doCheckLink,
  doAcceptPackageWeb,doGetCustomerOrders,doAcceptPackage,
  doGetOrderDetail,doAddComplaint,doGetUserDetail,doSearchCourierCompaniesByUser ,
  doListCourierCompaniesByUser,doRegisterUserFromWeb} = require('./controller');


const getUserByMobile = (req, res, next) => {
  doGetUserByMobile(req.body, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getUserByUsername = (req, res, next) => {
  doGetUserByUsername(req.body, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const addSendDetails = (req, res, next) => {
  doAddSendDetails(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const checkLink = (req, res, next) => {
  doCheckLink(req.body)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const acceptPackageWeb = (req, res, next) => {
  doAcceptPackageWeb(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getCustomerOrders = (req, res, next) => {
  doGetCustomerOrders(req.body, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const acceptPackage = (req, res, next) => {
  doAcceptPackage(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};
const getOrderDetail = (req, res, next) => {
  doGetOrderDetail(req.body, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        data,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getUserDetail = (req, res, next) => {
  doGetUserDetail(req.body, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        data
      });
    })
    .catch((error) => {
      next(error);
    });
};


const addComplaint = (req, res, next) => {
  doAddComplaint(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const searchCourierCompaniesByUser = (req, res, next) => {
  doSearchCourierCompaniesByUser(req?.body, req?.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const listCourierCompaniesByUser = (req, res, next) => {
  doListCourierCompaniesByUser( req?.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const registerUserFromWeb = (req, res, next) => {
  doRegisterUserFromWeb( req.body)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};




module.exports = {
  getUserByMobile,
  addSendDetails,
  getUserByUsername,
  checkLink,
  acceptPackageWeb,
  getCustomerOrders,
  acceptPackage,
  getOrderDetail,
  addComplaint,
  getUserDetail,  
  searchCourierCompaniesByUser,
  listCourierCompaniesByUser,
  registerUserFromWeb
};
