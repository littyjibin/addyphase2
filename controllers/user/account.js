const { doLogin, doVerifyOtp, doResendOtp, doUpdateUserInfo, 
  doGetUserAddress, doUpdateUserAddress, doDeleteUserAddress, 
  doVerifyUsername,doUpdateUsername,doAddNewAddress,doAddNewAddressWeb,
  doGetUserAddressWeb,doListAddressWeb,doGetRecentReciepient,doUpdateUserProfile,
  doUpdateUserSettings } = require('./controller');

const login = (req, res, next) => {
  doLogin(req.body)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
const verifyOtp = (req, res, next) => {
  doVerifyOtp(req.body)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getRecentReciepient = (req, res, next) => {
  doGetRecentReciepient()
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const resendOtp = (req, res, next) => {
  doResendOtp(req.body)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const updateUserInfo = (req, res, next) => {
  doUpdateUserInfo(req.body, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const addNewAddress = (req, res, next) => {
  doAddNewAddress(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getUserAddress = (req, res, next) => {
  doGetUserAddress(req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const updateUserAddress = (req, res, next) => {
  doUpdateUserAddress(req.body, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const deleteUserAddress = (req, res, next) => {
  doDeleteUserAddress(req.body.id)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const verifyUserName = (req, res, next) => {
  doVerifyUsername(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const updateUserName = (req, res, next) => {
  doUpdateUsername(req.body, req.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        data
      });
    })
    .catch((error) => {
      next(error);
    });
};
const addNewAddressWeb = (req, res, next) => {
  doAddNewAddressWeb(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getUserAddressWeb = (req, res, next) => {
  doGetUserAddressWeb(req.params.id)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
const listAddressWeb = (req, res, next) => {
  doListAddressWeb(req.body.orderId)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
const updateUserProfile = (req, res, next) => {
  doUpdateUserProfile(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

const updateUserSettings = (req, res, next) => {
  doUpdateUserSettings(req.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};


module.exports = {
  login,
  verifyOtp,
  resendOtp,
  updateUserInfo,
  getUserAddress,
  updateUserAddress,
  deleteUserAddress,
  verifyUserName,
  updateUserName,
  addNewAddress,
  addNewAddressWeb,
  getUserAddressWeb,
  listAddressWeb,
  getRecentReciepient,
  updateUserProfile,
  updateUserSettings
};
