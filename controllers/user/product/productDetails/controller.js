const mongoose = require('mongoose');
const product = require('../../../../model/seller/product/product');
const account = require('../../../../model/Account');
const sellerDetail = require('../../../../model/seller/registration/sellerDetails');
const productCategory = require('../../../../model/seller/productCategory/productCategory');
const storeView = require('../../../../model/user/product/storeViews');
const productView = require('../../../../model/user/product/productView');
const cart = require('../../../../model/user/product/cart');

// get all active products by seller id for logged in users -web
const doUserGetAllActiveProductsBySellerId = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!data.sellerId) return resolve({ statusCode: 400, status: false, message: 'Please add sellerId', data: {} });

      //  checking seller exist or not
      let isSellerExist = await account.findOne({
        _id: data.sellerId,
        isSeller: true,
        isActive: true,
      });
      if (!isSellerExist)
        return resolve({ statusCode: 400, status: false, message: 'The seller id you entered was not found.' });

      let sellerDetails = await sellerDetail.aggregate([
        {
          $match: {
            sellerId: mongoose.Types.ObjectId(data.sellerId),
            isActive: true,
          },
        },
        {
          $lookup: {
            from: 'accounts',
            localField: 'addressId',
            foreignField: 'address._id',
            as: 'userDetails',
          },
        },
        {
          $unwind: {
            path: '$userDetails',
          },
        },
        {
          $project: {
            sellerId: 1,
            firstName: '$userDetails.firstName',
            lastName: '$userDetails.lastName',
            countryCode: '$userDetails.countryCode',
            code: '$userDetails.code',
            mobile: '$userDetails.mobile',
            address: {
              $filter: {
                input: '$userDetails.address',
                as: 'userAddress',
                cond: { $eq: ['$$userAddress._id', '$addressId'] },
              },
            },
            _id: 1,
            businessName: 1,
            logo: 1,
            colorName: 1,
            colorCode: 1,
          },
        },
        {
          $project: {
            _id: 1,
            sellerId: 1,
            businessName: 1,
            logo: 1,
            colorName: 1,
            colorCode: 1,
            firstName: 1,
            lastName: 1,
            code: 1,
            mobile: 1,
            countryCode: 1,
            address: { $first: '$address.address' },
            houseNumber: { $first: '$address.houseNumber' },
            street: { $first: '$address.street' },
            area: { $first: '$address.area' },
            latitude: { $first: '$address.location.latitude' },
            longitude: { $first: '$address.location.longitude' },
          },
        },
      ]);

      if (!sellerDetails)
        return resolve({ statusCode: 400, status: false, message: 'The seller id you entered was not found.' });

      // Add the view count of store
      let isStoreExist = await storeView.findOne({
        sellerId: data.sellerId,
      });
      if (!isStoreExist) {
        let schemaObj = new storeView({ sellerId: data.sellerId, userId: user._id, viewCount: 1 }).save();
      } else {
        let isStoreUserExist = await storeView.findOne({
          sellerId: data.sellerId,
          userId: user._id,
        });
        if (!isStoreUserExist) {
          await storeView.findOneAndUpdate(
            { sellerId: mongoose.Types.ObjectId(data.sellerId) },
            {
              $push: { userId: user._id },
              $inc: { viewCount: 1 },
            }
          );
        }
      }

      let userDetails = {};

      if (user._id) {
        //  checking user exist
        let isUserExist = await account.findOne({
          _id: user._id,
        });
        if (isUserExist) userDetails.countryCode = isUserExist.countryCode;
        let cartCount = 0
          //  checking user cart
          isCartExist = await cart.findOne({
            userId: user._id,
          }).countDocuments()
          if (isCartExist) cartCount = isCartExist;
          userDetails.cart = cartCount
      }

      //get from db
      let products = await product
        .aggregate([
          {
            $match: {
              isActiveProduct: true,
              sellerId: mongoose.Types.ObjectId(data.sellerId),
            },
          },
          {
            $unwind: {
              path: '$variants',
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $project: {
              productName: 1,
              basicSellingPrice: 1,
              isVariant: 1,
              countryCode: 1,
              images: {
                $filter: {
                  input: '$images',
                  as: 'item',
                  cond: { $eq: ['$$item.isDefault', true] },
                },
              },
              sellingPrice: '$variants.sellingPrice',
              variantImages: {
                $filter: {
                  input: '$variants.images',
                  as: 'items',
                  cond: { $eq: ['$$items.isDefault', true] },
                },
              },
            },
          },
          {
            $group: {
              _id: '$_id',
              image: { $first: '$images.imageUrl' },
              isVariant: { $first: '$isVariant' },
              countryCode: { $first: '$countryCode' },
              productName: { $first: '$productName' },
              price: { $first: '$basicSellingPrice' },
              variantImages: { $push: { variantImage: '$variantImages.imageUrl', price: '$sellingPrice' } },
            },
          },
        ])
        .sort({ _id: -1 });
      for (item of products) {
        if (item.isVariant) {
          for (image of item.variantImages) {
            if (image.variantImage.length) {
              item.image = image.variantImage[0];
              item.price = image.price;
              delete item.variantImages;
              break;
            }
          }
        } else {
          delete item.variantImages;
          item.image = item.image[0];
        }
      }

      // pagination
      let limit = 30;
      let page = 0;
      if (data.limit) {
        limit = parseInt(data.limit);
      }

      if (data.page) {
        page = parseInt(data.page) - 1;
      }

      let nextPage = false;
      let start = page * limit;
      let end = page * limit + limit;
      let productDetails = products.slice(start, end);
      if (products.length > end) {
        nextPage = true;
      } else {
        nextPage = false;
      }
      if (products.length == 0) {
        nextPage = false;
      }
      let totalPage = Math.ceil(products.length / limit);
      return resolve({
        statusCode: 200,
        status: true,
        message: 'Success',
        data: {
          sellerDetails: sellerDetails[0],
          userDetails: userDetails,
          products: {
            totalPage,
            nextPage,
            productDetails,
          },
        },
      });

      // return resolve({ statusCode: 200, status: true, message: 'Success', data: { products } });
    } catch (error) {
      reject(error);
    }
  });
};

// get active product by id - web
const doUserGetActiveProductById = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      let products = await product.aggregate([
        {
          $match: {
            sellerId: mongoose.Types.ObjectId(data.sellerId),
            isActiveProduct: true,
            _id: mongoose.Types.ObjectId(data.id),
          },
        },
        {
          $lookup: {
            from: 'uoms',
            localField: 'uom',
            foreignField: '_id',
            as: 'uomDetails',
          },
        },
        {
          $project: {
            variants: {
              $filter: {
                input: '$variants',
                as: 'variant',
                cond: { $eq: ['$$variant.isActiveVariant', true] },
              },
            },
            _id: 1,
            productName: 1,
            countryCode: 1,
            description: 1,
            taggedCategories: 1,
            uom: { $first: '$uomDetails.uom' },
            piece: 1,
            basicCostPrice: 1,
            basicSellingPrice: 1,
            images: 1,
            isActiveProduct: 1,
            isVariant: 1,
            isDeliveryVerification: 1,
          },
        },
      ]);

      await productCategory.populate(products, { path: 'taggedCategories', select: ['_id', 'productCategory'] });
      if (!products.length)
        return resolve({ statusCode: 400, status: false, message: 'Invalid seller id or id', data: {} });

      // Add the view count of product
      let isProductExist = await productView.findOne({
        sellerId: data.sellerId,
        productId: data.id,
      });
      if (!isProductExist) {
        let schemaObj = new productView({
          sellerId: data.sellerId,
          productId: data.id,
          userId: user._id,
          viewCount: 1,
        }).save();
      } else {
        let isProductUserExist = await productView.findOne({
          sellerId: data.sellerId,
          productId: data.id,
          userId: user._id,
        });

        if (!isProductUserExist) {
          await productView.findOneAndUpdate(
            { sellerId: mongoose.Types.ObjectId(data.sellerId), productId: data.id },
            {
              $push: { userId: user._id },
              $inc: { viewCount: 1 },
            }
          );
        }
      }

      return resolve({ statusCode: 200, status: true, message: 'Success', data: { products } });
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  doUserGetAllActiveProductsBySellerId,
  doUserGetActiveProductById,
};
