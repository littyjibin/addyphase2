const {
    doUserGetAllActiveProductsBySellerId,
    doUserGetActiveProductById
  } = require('./controller');


const userGetAllActiveProductsBySellerId = (req, res, next) => {
    doUserGetAllActiveProductsBySellerId(req.body,req.user)
      .then(({ statusCode = 200, message, status, data }) => {
        res.status(statusCode).json({
          message,
          status,
          ...(data && { data }),
        });
      })
      .catch((error) => {
        next(error);
      });
  };


  const userGetActiveProductById = (req, res, next) => {
    doUserGetActiveProductById(req.body,req.user)
      .then(({ statusCode = 200, message, status, data }) => {
        res.status(statusCode).json({
          message,
          status,
          ...(data && { data }),
        });
      })
      .catch((error) => {
        next(error);
      });
  };

  module.exports = {
    userGetAllActiveProductsBySellerId,
    userGetActiveProductById
  };