const {
    doGetOrderReview,
    doPlaceOrder,
    doGetOrderDetailsByOrderId,
    doRescheduleDeliveryDateByUser,
    doCancelOrderByUser,
    doRaiseComplaintsAboutOrderByUser,
    doReturnOrderByUser
} = require('./controller');

const placeOrder = (req, res, next) => {
  doPlaceOrder(req?.body, req?.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};
const getOrderReview = (req, res, next) => {
    doGetOrderReview(req?.body,req?.user)
      .then(({ statusCode = 200, message, status, data }) => {
        res.status(statusCode).json({
          message,
          status,
          ...(data && { data }),
        });
      })
      .catch((error) => {
        next(error);
      });
  };

  const getOrderDetailsByOrderId = (req, res, next) => {
    doGetOrderDetailsByOrderId(req?.params,req?.user)
      .then(({ statusCode = 200, message, status, data }) => {
        res.status(statusCode).json({
          message,
          status,
          ...(data && { data }),
        });
      })
      .catch((error) => {
        next(error);
      });
  };
  const rescheduleDeliveryDateByUser = (req, res, next) => {
    doRescheduleDeliveryDateByUser(req?.body, req?.user)
      .then(({ statusCode = 200, message, status }) => {
        res.status(statusCode).json({
          message,
          status
        });
      })
      .catch((error) => {
        next(error);
      });
  };
  const cancelOrderByUser = (req, res, next) => {
    doCancelOrderByUser(req?.params, req?.user)
      .then(({ statusCode = 200, message, status }) => {
        res.status(statusCode).json({
          message,
          status
        });
      })
      .catch((error) => {
        next(error);
      });
  };

  const raiseComplaintsAboutOrderByUser = (req, res, next) => {
    doRaiseComplaintsAboutOrderByUser(req?.body, req?.user)
      .then(({ statusCode = 200, message, status }) => {
        res.status(statusCode).json({
          message,
          status
        });
      })
      .catch((error) => {
        next(error);
      });
  };

  const returnOrderByUser = (req, res, next) => {
    doReturnOrderByUser(req?.body, req?.user)
      .then(({ statusCode = 200, message, status,data }) => {
        res.status(statusCode).json({
          message,
          status,
          ...(data && { data })
        });
      })
      .catch((error) => {
        next(error);
      });
  };

module.exports = {
  getOrderReview,
  placeOrder,
  getOrderDetailsByOrderId,
  rescheduleDeliveryDateByUser,
  cancelOrderByUser,
  raiseComplaintsAboutOrderByUser,
  returnOrderByUser
};
