const mongoose = require('mongoose');
const _ = require('lodash');
const moment = require('moment-timezone');

const cart = require('../../../../model/user/product/cart');
//const product = require('../../../../model/seller/product/product');
const account = require('../../../../model/Account');
const sellerDetail = require('../../../../model/seller/registration/sellerDetails');
const sellerCourierCompany = require('../../../../model/seller/registration/sellerCourierCompanies');
const sellerDriver = require('../../../../model/seller/registration/sellerDriver');
//const packagesize = require('../../../../model/admin/master/packageSize');
const sellerSetting = require('../../../../model/seller/settings/settings');
const order = require('../../../../model/user/product/order');
const orderReturn = require('../../../../model/user/product/orderReturn');
const pickupTime = require('../../../../model/admin/master/pickupTime');
const sendSms = require('../../../../helpers/sms/sendSms');
const timeZoneValidation = require('../../../../helpers/firebase/timeZoneValidation');

const { sendMail } = require('../../../../helpers/email/emailHelper');

const {
  addOrderHistoryDetails,
  getOrderHistoryByOrderId,
  isOrderValidByUser,
} = require('../../../../helpers/product/orderHelper');

const {
  validateOrderReview,
  validatePlaceOrder,
  validateGetUserOrderById,
  validateRescheduleDeliveryDate,
  validateCancelOrder,
  validateRaiseComplaints,
  validateReturnOrder,
} = require('../../../../validations/user/product/orderValidation');
const {
  getCartItems,
  getCourierCompanyFeeDetails,
  getBoxSizeDetailsbyWeight,
} = require('../../../../helpers/product/cartHelper');
const { addPlacedOrderDetails } = require('../../../../helpers/product/orderHelper');

const orderStatus = 'pending';

// get cart product by user id
const doGetOrderReview = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateOrderReview(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      const userId = user._id;
      const sellerId = data.sellerId;

      //   get the vat details of seller
      let isSeller = await account.findOne(
        {
          _id: sellerId,
          isActive: true,
          isSeller: true,
        },
        {}
      );

      if (!isSeller) return resolve({ status: false, message: 'Invalid seller id.', statusCode: 400 });

      //   get the vat details of seller
      let sellerDetails = await sellerDetail
        .findOne(
          {
            sellerId: sellerId,
            isActive: true,
          },
          { storeUrl: 1, vat: 1 }
        )
        .lean();

      if (!sellerDetails) return resolve({ status: false, message: 'Invalid seller id.', statusCode: 400 });

      // check user address exist or not
      let isUserAddress = await account.aggregate([
        {
          $match: {
            _id: mongoose.Types.ObjectId(userId),
            isActive: true,
            address: { $elemMatch: { _id: mongoose.Types.ObjectId(data.addressId), isDeleted: false } },
          },
        },
      ]);

      if (!isUserAddress.length) return resolve({ status: false, message: 'Invalid address id.', statusCode: 400 });

      // get seller settings
      let sellerSettings = await sellerSetting.findOne(
        {
          sellerId: sellerId,
        },
        {
          payment: 1,
        }
      );
      sellerDetails.cashOnDelivery = true;
      sellerDetails.onlinePayment = true;
      sellerDetails.enableVat = false;
      sellerDetails.priceIncludeVat = false;
      sellerDetails.priceIncludeDeliveryFee = false;
      if (sellerSettings) {
        if (sellerSettings?.payment.length) {
          sellerDetails.cashOnDelivery = sellerSettings.payment[0].cashOnDelivery;
          sellerDetails.onlinePayment = sellerSettings.payment[0].onlinePayment;
          sellerDetails.enableVat = sellerSettings.payment[0].enableVat;
          sellerDetails.priceIncludeVat = sellerSettings.payment[0].priceIncludeVat;
          sellerDetails.priceIncludeDeliveryFee = sellerSettings.payment[0].priceIncludeDeliveryFee;
        }
      }

      let vat = 0;
      // checking vat included or not
      if (!sellerDetails?.priceIncludeVat) {
        if (sellerDetails?.enableVat) {
          vat = sellerDetails.vat != null ? sellerDetails.vat : 0;
        }
      }

      // Get the cart items
      let productDetails = await getCartItems(userId, sellerId);

      // remove the out of stock items from cart
      let products = productDetails.filter((item) => {
        if (item.outOfStock == false) {
          return true;
        }
      });

      if (!products.length) return resolve({ status: false, message: 'Cart is empty.', statusCode: 400 });

      // initialize the cart details
      cartDetails = {
        distance: 0,
        totalWeight: 0,
        itemTotal: 0,
        deliveryFee: 0,
        vat: 0,
        grandTotal: 0,
      };

      // Get the distance between seller and user
      let distance = parseInt(data.distance);
      let totalWeight = 0;

      // Get the total amount of cart products
      products.map((product) => {
        cartDetails.itemTotal += product.total;
        totalWeight += product.totalWeight;
      });

      // initialize variables
      let companyId = '';
      let driverId = '';
      let totalAmountItems = 0; //total delivery fee
      let mainPieceAmount = 0;
      let remainingPieceAmount = 0;

      // checking price included delivery fee
      if (sellerDetails?.priceIncludeDeliveryFee) {
        totalAmountItems = 0;
        //   get the company details of seller
        let sellerDefaultCourierCompany = await sellerCourierCompany.findOne({
          sellerId: sellerId,
          isDefault: true,
          isActive: true,
        });
        if (sellerDefaultCourierCompany) {
          companyId = sellerDefaultCourierCompany._id;
        } else {
          let sellerDefaultDriver = await sellerDriver.findOne({
            sellerId: sellerId,
            isDefault: true,
            isActive: true,
          });
          driverId = sellerDefaultDriver._id;
        }
      } else {
        // Get the delivery fee of default courier company
        let sellerCourierDetails = await getCourierCompanyFeeDetails(sellerId, totalWeight, distance);

        if (sellerCourierDetails && sellerCourierDetails.length) {
          // calculate the number of pieces
          let pieces = totalWeight / sellerCourierDetails[0].packageSizeWeight;

          // get the piece of total weight
          pieceCount = Math.trunc(pieces);

          floatPart = Number((pieces - pieceCount).toFixed(2));
          if (pieceCount == 0 || floatPart == 0) {
            totalAmountItems = sellerCourierDetails[0].total;
          } else {
            // calculate the amount of main pieces
            mainPieceAmount = pieceCount * sellerCourierDetails[0].total;
            floatPart = Number((pieces - pieceCount).toFixed(2));

            let remainingWeight = floatPart * sellerCourierDetails[0].packageSizeWeight;

            // get the remaining package size in courier company in which package size is greater than remaining weight
            let remainingPackageSizes = sellerCourierDetails[0].packageSize.filter((x) => x.weight >= remainingWeight);

            // get the minimum package size
            var min = Math.min.apply(
              null,
              remainingPackageSizes.map((item) => item.weight)
            );
            //  Filter the array with minimum weight
            let result = remainingPackageSizes.filter((item) => item.weight === min);

            let boxSizeArray = sellerCourierDetails[0].courierCompanies.boxRate;
            let resultDistance = boxSizeArray.filter(
              (item) => item.boxSize == result[0]._id + '' && item.upto >= distance
            );
            // if the distance is in db(combany box rate) then get the minimum distance rate
            if (resultDistance.length) {
              let minDistances = Math.min.apply(
                null,
                resultDistance.map((item) => item.upto)
              );
              let minDistance = resultDistance.filter((item) => item.upto === minDistances);
              remainingPieceAmount = minDistance[0].total;
            } else {
              // if the distance is not in db (company box rate)  the  get the maximum distance
              let maxDistances = Math.max.apply(
                null,
                boxSizeArray.map((item) => item.upto)
              );
              let maxDistance = boxSizeArray.filter(
                (item) => item.upto === maxDistances && item.boxSize == result[0]._id + ''
              );
              remainingPieceAmount = maxDistance[0].total;
              //   console.log(maxDistance);
            }

            let totalPieceRate = mainPieceAmount + remainingPieceAmount;

            totalAmountItems = totalPieceRate;
          }
          companyId = sellerCourierDetails[0].companyId;
          cartDetails.boxSizeId = sellerCourierDetails[0].packageSizeId;
          cartDetails.boxSizeName = sellerCourierDetails[0].packageSizeName;
        }

        let sellerDriverDetails = [];

        // if no courier company is set as default then check the another delivery method
        if (totalAmountItems == 0) {
          // get the default driver delivery fee details
          sellerDriverDetails = await sellerDriver.aggregate([
            {
              $match: {
                sellerId: mongoose.Types.ObjectId(sellerId),
                isDefault: true,
              },
            },
            // get the delivery fee details based on distance
            {
              $project: {
                mobile: 1,
                courier: {
                  $filter: {
                    input: '$deliveryFee',
                    as: 'deliveryFeeDetails',
                    cond: {
                      $gte: ['$$deliveryFeeDetails.upto', distance],
                    },
                  },
                },
                courierMax: { $max: '$deliveryFee' },
              },
            },
            // if distance is there then get the minimum distance
            {
              $set: {
                courierMin: { $min: '$courier' },
              },
            },
            {
              $project: {
                delivery: { $ifNull: ['$courierMin', '$courierMax'] },
              },
            },
          ]);
          if (sellerDriverDetails.length) {
            driverId = sellerDriverDetails[0]._id;
            totalAmountItems = sellerDriverDetails[0].delivery.rate;
          }
        }

        cartDetails.deliveryFee = totalAmountItems;
        cartDetails.companyId = companyId;
        cartDetails.driverId = driverId;
        cartDetails.totalWeight = totalWeight;
        cartDetails.distance = parseInt(data.distance);
      }

      // Get the user address
      let userDetails = await account.aggregate([
        //get all user based on user  id
        {
          $match: {
            _id: mongoose.Types.ObjectId(userId),
            isActive: true,
            address: { $elemMatch: { isDeleted: false } },
          },
        },
        {
          $project: {
            firstName: 1,
            countryCode: 1,
            mobile: 1,
            address: 1,
            address: {
              $filter: {
                input: '$address',
                as: 'addressDetails',
                cond: { $eq: ['$$addressDetails._id', mongoose.Types.ObjectId(data.addressId)] },
              },
            },
          },
        },
        {
          $project: {
            _id: 1,
            firstName: 1,
            mobile: 1,
            countryCode: 1,
            address: { $first: '$address.address' },
            houseNumber: { $first: '$address.houseNumber' },
            street: { $first: '$address.street' },
            area: { $first: '$address.area' },
            latitude: { $first: '$address.location.latitude' },
            longitude: { $first: '$address.location.longitude' },
          },
        },
      ]);

      // Get the grand total amount of cart products
      let vatTotal = (cartDetails.itemTotal * vat) / 100;
      cartDetails.vat = vatTotal;
      cartDetails.grandTotal = cartDetails.itemTotal + vatTotal + cartDetails.deliveryFee;
      return resolve({
        statusCode: 200,
        status: true,
        message: 'Success',
        data: { products, cartDetails, userDetails, sellerDetails },
      });
    } catch (error) {
      reject(error);
    }
  });
};

const doPlaceOrder = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validatePlaceOrder(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      const userId = user._id;
      const sellerId = data.sellerId;

      //   get the  details of seller
      let isSeller = await account.findOne(
        {
          _id: sellerId,
          isActive: true,
          isSeller: true,
        },
        {}
      );
      if (!isSeller) return resolve({ status: false, message: 'Invalid seller id.', statusCode: 400 });

      // check user address exist or not
      let isUserAddress = await account.aggregate([
        {
          $match: {
            _id: mongoose.Types.ObjectId(userId),
            isActive: true,
            address: { $elemMatch: { _id: mongoose.Types.ObjectId(data.addressId), isDeleted: false } },
          },
        },
      ]);

      if (!isUserAddress.length) return resolve({ status: false, message: 'Invalid address id.', statusCode: 400 });

      //if(isSeller.countryCode!=isUserAddress) return resolve({ status: false, message: 'Sorry, delivery is not available to your country', statusCode: 400 });

      // check the delivery date after one day
      let expectedDeliveryDate = moment(data.expectedDeliveryDate).tz(data.timeZone).format('YYYY-MM-DD');
      let tomorrow = moment().add(2, 'days').tz(data.timeZone).format('YYYY-MM-DD');

      if (expectedDeliveryDate < tomorrow) {
        return resolve({ status: false, message: 'Please add delivery date after 2 days', statusCode: 400 });
      }

      //  check the pick up time exist or not
      let isPickupTime = await pickupTime.findOne({
        _id: data.expectedDeliveryTime.pickupTimeId,
      });
      if (!isPickupTime) return resolve({ status: false, message: 'Invalid pick up time.', statusCode: 400 });

      let cashOnDelivery = true;
      let onlinePayment = true;
    
      // checking payment method allowed or not
      let sellerSettings = await sellerSetting.findOne({ sellerId: sellerId });
      if (sellerSettings?.payment.length) {       
        cashOnDelivery = sellerSettings?.payment[0]?.cashOnDelivery;
        onlinePayment = sellerSettings?.payment[0]?.onlinePayment;
        
      }
    
      if (!cashOnDelivery && data.paymentType == 'cod')
        return resolve({
          status: false,
          message: 'Cash on delivery is not available please select another payment method',
          statusCode: 400,
        });
      if (!onlinePayment && data.paymentType == 'online')
        return resolve({
          status: false,
          message: 'Online payment is not available please select another payment method',
          statusCode: 400,
        });

      // Get the cart items
      let productDetails = await getCartItems(userId, sellerId);

      // remove the out of stock items from cart
      let products = productDetails.filter((item) => {
        if (item.outOfStock == false) {
          return true;
        }
      });

      if (!products.length) return resolve({ status: false, message: 'Cart is empty.', statusCode: 400 });

      // Get the user address
      let userDetails = await account.aggregate([
        //get all user based on user  id
        {
          $match: {
            _id: mongoose.Types.ObjectId(userId),
            isActive: true,
            address: { $elemMatch: { isDeleted: false } },
          },
        },
        {
          $project: {
            firstName: 1,
            address: 1,
            address: {
              $filter: {
                input: '$address',
                as: 'addressDetails',
                cond: { $eq: ['$$addressDetails._id', mongoose.Types.ObjectId(data.addressId)] },
              },
            },
          },
        },
        {
          $project: {
            _id: 1,
            addressId: { $first: '$address._id' },
            address: { $first: '$address.address' },
            houseNumber: { $first: '$address.houseNumber' },
            buildingName: { $first: '$address.buildingName' },
            street: { $first: '$address.street' },
            area: { $first: '$address.area' },
            city: { $first: '$address.city' },
            pinCode: { $first: '$address.pinCode' },
            latitude: { $first: '$address.location.latitude' },
            longitude: { $first: '$address.location.longitude' },
            deliveryInstruction: { $first: '$address.deliveryInstruction' },
          },
        },
      ]);

      if (!data.cartDetails.driverId) {
        delete data.cartDetails.driverId;
      }
      if (!data.cartDetails.companyId) {
        delete data.cartDetails.companyId;
      }

      if (data.cartDetails) {
        // calculating common box size based on weight
        let boxSizeDetails = await getBoxSizeDetailsbyWeight(data.cartDetails.totalWeight);
        if (boxSizeDetails) {
          data.cartDetails.commonBoxSizeId = boxSizeDetails._id;
          data.cartDetails.commonBoxSize = boxSizeDetails.size;
        }
      }

      let orderId = '';
      let isOrderId = await order.findOne({}).sort({ _id: -1 });
      if (!isOrderId) {
        // orderId = 'ADDY-1234';
        orderId = '1001';
      } else {
        let lastInsertedOrderId = isOrderId.orderId;
        // get everything after last dash
        orderId = parseInt(lastInsertedOrderId.split('-').pop());
        // orderId = 'ADDY-' + (orderId + 1);
        orderId = orderId + 1;
      }

      let vatPercentage = 0;
      if (isSeller?.vat) {
        vatPercentage = isSeller.vat;
      }

      let obj = {
        userId: userId,
        orderId: orderId,
        sellerId: sellerId,
        sellerCountryCode: isSeller.countryCode,
        vatPercentage: vatPercentage,
        cartDetails: data.cartDetails,
        products: products,
        userDetails: userDetails[0],
        paymentType: data.paymentType,
        isPhoto: data.isPhoto,
        expectedDeliveryDate: expectedDeliveryDate,
        expectedDeliveryTime: data.expectedDeliveryTime,
        deliveryDate: expectedDeliveryDate,
        noOfItems: products.length,
        orderStatus: orderStatus,
        timeZone: data.timeZone,
      };

      if (data.paymentType == 'cod') {
        new order(obj)
          .save()
          .then(async (response) => {
            // add placed order details to order history and notification
            const orderDetails = await addPlacedOrderDetails(response);

            return resolve({
              statusCode: 200,
              status: true,
              message: 'Order placed successfully.',
              data: {
                orderId: response.orderId,
                deliveryDate: response.expectedDeliveryDate,
                deliveryFromTime: response.expectedDeliveryTime.fromTime,
                deliveryTime: response.expectedDeliveryTime.toTime,
              },
            });
          })
          .catch(async (error) => {
            return resolve({ statusCode: 400, status: false, message: error.message });
          });
      }
      if (data.paymentType == 'online') {
        return resolve({ statusCode: 400, status: false, message: 'pending' });
      }
    } catch (error) {
      reject(error);
    }
  });
};

// get orders by order id
const doGetOrderDetailsByOrderId = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      let today = moment().format('MMM DD,YY');
      let tomorrow = moment().add(1, 'days').format('MMM DD,YY');

      //validate incoming data
      const dataValidation = await validateGetUserOrderById(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      const userId = user._id;
      const { id } = data;

      //get from db
      let orderDetailsArray = await order.aggregate([
        {
          $match: {
            userId: mongoose.Types.ObjectId(userId),
            isActive: true,
            _id: mongoose.Types.ObjectId(id),
          },
        },
        {
          $lookup: {
            from: 'accounts',
            localField: 'sellerId',
            foreignField: '_id',
            as: 'sellerAccountDetails',
          },
        },
        {
          $lookup: {
            from: 'sellerdetails',
            localField: 'sellerId',
            foreignField: 'sellerId',
            as: 'sellerDetails',
          },
        },
        {
          $lookup: {
            from: 'sellerdrivers',
            localField: 'selectedDriverId',
            foreignField: '_id',
            as: 'driverDetails',
          },
        },
        {
          $lookup: {
            from: 'couriercompanies',
            localField: 'selectedCompanyId',
            foreignField: '_id',
            as: 'companyDetails',
          },
        },
        {
          $project: {
            _id: 1,
            orderId: 1,
            noOfItems: 1,
            orderStatus: 1,
            paymentType: 1,
            userId: 1,
            paymentType: 1,
            expectedDeliveryDate: 1,
            expectedDeliveryTime: '$expectedDeliveryTime.fromTime',
            deliveryDate: 1,
            delivered: 1,
            // expectedDeliveryTime: { $concat: ['$expectedDeliveryTime.fromTime', '-', '$expectedDeliveryTime.toTime'] },
            sellerCountryCode: 1,
            packedProductImages: 1,
            products: {
              $map: {
                input: '$products',
                as: 'product',
                in: {
                  productId: '$$product.productId',
                  variantId: '$$product.variantId',
                  productName: '$$product.productName',
                  image: '$$product.image',
                  instruction: '$$product.instruction',
                  uom: '$$product.uom',
                  uomValue: '$$product.uomValue',
                  color: '$$product.color',
                  colorCode: '$$product.colorCode',
                  quantity: '$$product.quantity',
                  price: '$$product.price',
                  total: '$$product.total',
                },
              },
            },
            itemTotal: { $ifNull: ['$cartDetails.itemTotal', 0] },
            deliveryFee: { $ifNull: ['$cartDetails.deliveryFee', 0] },
            vat: { $ifNull: ['$cartDetails.vat', 0] },
            grandTotal: { $ifNull: ['$cartDetails.grandTotal', 0] },
            sellerDetail: {
              sellerLogo: { $ifNull: [{ $first: '$sellerAccountDetails.logo' }, ''] },
              businessName: { $first: '$sellerDetails.businessName' },
            },
            deliveryDetails: {
              Code: { $ifNull: [{ $first: '$driverDetails.code' }, { $first: '$companyDetails.code' }] },
              mobile: { $ifNull: [{ $first: '$driverDetails.mobile' }, { $first: '$companyDetails.mobile' }] },
              image: { $ifNull: [{ $first: '$driverDetails.image' }, { $first: '$companyDetails.logo' }] },
            },
          },
        },
      ]);

      let orderDetails = orderDetailsArray[0];
      if (!orderDetails) return resolve({ status: false, message: 'Invalid order id.', statusCode: 400 });

      orderDetails.isReschedule = true;
      orderDetails.isCancel = false;
      if (orderDetails) {
        if (_.isEmpty(orderDetails.deliveryDetails)) {
          let obj = {
            code: '',
            mobile: '',
            image: '',
          };
          orderDetails.deliveryDetails = obj;
        }
        let statusReschedule = ['startDelivery', 'attemptDelivery', 'failedToDelivery', 'delivered', 'cancelled'];
        let statusCancel = ['pending', 'accepted', 'confirmed', 'deliveryRejected'];

        // after start delivery customer cannot reschedule the delivery date
        if (statusReschedule.includes(orderDetails.orderStatus)) {
          orderDetails.isReschedule = false;
        }

        // after driver assigned customer cannot cancel the order
        if (statusCancel.includes(orderDetails.orderStatus)) orderDetails.isCancel = true;

        orderDetails.expectedDeliveryDate = moment(orderDetails.expectedDeliveryDate).format('MMM DD,YY');
        orderDetails.deliveryDate = moment(orderDetails.deliveryDate).format('MMM DD,YY hh mm a');

        if (orderDetails.expectedDeliveryDate == today) {
          orderDetails.expectedDeliveryDate = 'Today';
        }
        if (orderDetails.expectedDeliveryDate == tomorrow) {
          orderDetails.expectedDeliveryDate = 'Tomorrow';
        }
        orderDetails.expectedDeliveryDate =
          orderDetails.expectedDeliveryDate + ' - ' + orderDetails.expectedDeliveryTime;
        delete orderDetails.expectedDeliveryTime;
      }

      // Get order history by order id
      let orderHistory = await getOrderHistoryByOrderId(id);

      return resolve({
        statusCode: 200,
        status: true,
        message: 'Success',
        data: { orderDetails, orderHistory },
      });
    } catch (error) {
      reject(error);
    }
  });
};

// Reschedule delivery date by user
const doRescheduleDeliveryDateByUser = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateRescheduleDeliveryDate(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      const { id } = data;
      const userId = user._id;

      // check if order is valid or not
      let validOrder = await isOrderValidByUser(id, userId);
      if (!validOrder) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid id' });
      }

      //  check the pick up time exist or not
      let isPickupTime = await pickupTime.findOne({
        _id: data.expectedDeliveryTime.pickupTimeId,
      });
      if (!isPickupTime) return resolve({ status: false, message: 'Invalid pick up time.', statusCode: 400 });

      // if (validOrder.isDeliveryStarted) {
      //   return resolve({
      //     statusCode: 400,
      //     status: false,
      //     message: 'You cannot change the delivery date. Order already start delivery.',
      //   });
      // }

      let orgOrderStatus = validOrder.orderStatus;
      let orderStatus = ['startDelivery', 'attemptDelivery', 'failedToDelivery', 'delivered'];
      // check the order status
      if (orderStatus.includes(orgOrderStatus))
        return resolve({
          statusCode: 400,
          status: false,
          message: 'You cannot change the delivery date. Order already start delivery.',
          data: {},
        });

      let timeZone = timeZoneValidation(user.code);

      // check the delivery date
      // let expectedDeliveryDate = moment(data.expectedDeliveryDate).tz(data.timeZone).format('YYYY-MM-DD');
      let expectedDeliveryDate = moment(data.expectedDeliveryDate).tz(timeZone).format('YYYY-MM-DD');
      let today = moment().tz(timeZone).format('YYYY-MM-DD');
      if (expectedDeliveryDate < today) {
        return resolve({ status: false, message: 'Invalid delivery date', statusCode: 400 });
      }

      createdDate = moment(validOrder.createdAt).tz(timeZone).format('YYYY-MM-DD');

      if (createdDate == today) {
        // check the delivery date after two day
        let dayAfterTomorrow = moment().add(2, 'days').tz(timeZone).format('YYYY-MM-DD');
        if (expectedDeliveryDate < dayAfterTomorrow) {
          return resolve({ status: false, message: 'Please add delivery date after 2 days', statusCode: 400 });
        }
      }

      order
        .updateOne(
          { _id: mongoose.Types.ObjectId(id) },
          {
            $set: {
              expectedDeliveryDate: expectedDeliveryDate,
              deliveryDate: expectedDeliveryDate,
              expectedDeliveryTime: data.expectedDeliveryTime,
            },
          }
        )
        .then(async (response) => {
          return resolve({ statusCode: 200, status: true, message: 'Delivery date updated successfully' });
        });
    } catch (error) {
      reject(error);
    }
  });
};

// cancel order by user
const doCancelOrderByUser = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const cancelOrderStatus = 'cancelled';
      //validate incoming data
      const dataValidation = await validateCancelOrder(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }
      const { id } = data;
      const userId = user._id;

      // check if order is valid or not
      let validOrder = await isOrderValidByUser(id, userId);
      if (!validOrder) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid id' });
      }
      const sellerId = validOrder.sellerId;

      if (validOrder.isDeliveryAssigned) {
        return resolve({ statusCode: 400, status: false, message: `Sorry, We can't cancel your order` });
      }

      // let orgOrderStatus = validOrder.orderStatus;
      // let orderStatus = ['pending', 'accepted', 'confirmed', 'deliveryAssigned', 'deliveryRejected'];

      // // check the order status
      // if (!orderStatus.includes(orgOrderStatus))
      //   return resolve({ statusCode: 400, status: false, message: `Sorry, We can't cancel your order` });

      order
        .updateOne(
          { _id: mongoose.Types.ObjectId(id) },
          {
            $set: { orderStatus: cancelOrderStatus },
          }
        )
        .then(async (response) => {
          let orderDetail = {
            id: id,
            orderId: validOrder.orderId,
            sellerId: sellerId,
            status: cancelOrderStatus,
            notificationStatus: cancelOrderStatus,
            userId: userId,
          };
          // add cancel order details to order history and notification
          const orderDetails = await addOrderHistoryDetails(orderDetail);

          // send message to driver about this order cancel
          if (validOrder?.selectedDriverId) {
            let isDriverExist = await sellerDriver.findById({ sellerId: sellerId, _id: validOrder.selectedDriverId });
            if (isDriverExist) {
              await sellerDriver.updateOne(
                { _id: mongoose.Types.ObjectId(validOrder.selectedDriverId) },
                { $pull: { orders: mongoose.Types.ObjectId(id) } }
              );
              let driverCode = isDriverExist.code;
              let driverMobile = isDriverExist.mobile;
              let payload = {
                origin: 'ADDYAE',
                destination: driverCode + driverMobile,
                message: `Assigned order with order id ${validOrder.orderId} is cancelled`,
              };
              console.log(payload);
              // await sendSms(payload)
              //   .then((_) => {
              //     return resolve({ statusCode: 200, status: true, message: 'Order confirmed successfully' });
              //   })
              //   .catch((error) => {
              //     return resolve({ statusCode: 400, status: false, message: error});
              //   });
            }
          }

          return resolve({ statusCode: 200, status: true, message: 'Order cancelled successfully' });
        });
    } catch (error) {
      reject(error);
    }
  });
};

const doRaiseComplaintsAboutOrderByUser = async (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const userId = user._id;
      let dataValidation = {};
      dataValidation = validateRaiseComplaints(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({ statusCode: 400, status: false, message: message });
      }
      const id = data.id;
      console.log(userId);
      let isOrder = await order.findOne({
        _id: mongoose.Types.ObjectId(id),
        userId: userId,
      });
      if (!isOrder) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid id' });
      }

      const obj = {
        userId: userId,
        message: data.message,
        createdAt: Date.now(),
      };

      await order
        .updateOne({ _id: mongoose.Types.ObjectId(id) }, { $push: { complaints: obj } })
        .then(async (_) => {
          let toMail = [];
          if (user.email) {
            toMail.push(user.email);
          }
          if (toMail.length) {
            const mailDetails = {
              to: toMail, // list of receivers
              subject: 'Order complaint', // Subject line
              html: '<p>Order id:' + isOrder.orderId + '<br>Complaint: ' + data.message + '</p>', // plain text body
            };
            let isMailSend = await sendMail(mailDetails);
          }
          return resolve({ statusCode: 200, status: true, message: 'Complaint submitted successfully' });
        })
        .catch((err) => {
          return resolve({ statusCode: 400, status: false, message: 'Something went wrong' + err });
        });
    } catch (error) {
      return resolve({ statusCode: 400, status: false, message: error + '' });
    }
  });
};

const doReturnOrderByUser = async (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const userId = user._id;
      const returnOrderStatus = "returnRequested"

      let dataValidation = validateReturnOrder(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({ statusCode: 400, status: false, message: message });
      }
      const id = data.id;
      let orderDetails = await order.findOne({
        _id: mongoose.Types.ObjectId(id),
        userId: userId,
        delivered :true
      });
      if (!orderDetails) {
        return resolve({ statusCode: 400, status: false, message: 'Invalid id' });
      }
      
      const sellerId = orderDetails.sellerId;

      // checking order already returned or not
      let returnedOrder = await orderReturn.findOne({
        orderObjectId: data.id,
      });
      if (returnedOrder) {
        return resolve({ statusCode: 400, status: false, message: 'Order already returned' });
      }
     
      // get the return days of seller
      let sellerDetails = await sellerDetail.findOne({
        sellerId: mongoose.Types.ObjectId(sellerId),
      });

      let returnDays = 0;
      if (sellerDetails?.returnPolicyDays) returnDays = sellerDetails.returnPolicyDays;
    
      let timeZone = timeZoneValidation(user.code);
      let today = moment().tz(timeZone).format('YYYY-MM-DD');

      // return eligibility
      let returnOrderDate = new Date(moment(orderDetails.deliveryDate).add(returnDays, 'days'));
      returnOrderDate = moment(returnOrderDate).tz(timeZone).format('YYYY-MM-DD');
      if (returnOrderDate < today) {
        return resolve({ status: false, message: 'Not eligible for return', statusCode: 400 });
      }
    
     
      let requestedProducts = data.products;

      let orderedProducts = orderDetails.products;
      requestedProducts = requestedProducts.map((product) =>
        _.find(orderedProducts, {
          variantId: mongoose.Types.ObjectId(product.variantId),
        })
      );
      let returnItems = requestedProducts.length;
     
      let refundableAmount = 0;
      for (let item of requestedProducts) {
        refundableAmount = refundableAmount + item.total;
      }

      await order.updateOne(
        {
          _id: data.id,
          userId: user._id,
        },
        {
          orderStatus: returnOrderStatus,
          isReturned: true,
          'cartDetails.returnRefundAmount' : refundableAmount
        }
      );

      let replacedOrderId = 'R-' + orderDetails.orderId;

      let newObj = {
        returnId: replacedOrderId,
        userId: userId,
        sellerId: sellerId,
        orderObjectId: data.id,
        orderId: orderDetails.orderId,
        paymentType: orderDetails.paymentType,
        noOfItems: orderDetails.noOfItems,
        returnItems: returnItems,
        returnReason: data.returnReason,
        userDetails: orderDetails.userDetails,
        products: requestedProducts,
        cartDetails: orderDetails.cartDetails,
        status: returnOrderStatus,
        refundableAmount: refundableAmount,
      };

      let schemaObj = new orderReturn(newObj);
      await schemaObj
        .save()
        .then(async (response) => {
          console.log(response)
           let orderDetail = {
            id: data.id,
            orderId: orderDetails.orderId,
            sellerId: sellerId,
            status: returnOrderStatus,
            notificationStatus: returnOrderStatus,
            userId: userId,
          };

           // add cancel order details to order history and notification
          const orderDetailAddHistory= await addOrderHistoryDetails(orderDetail);


          // let products = newObj.products.map((product) => ({
          //   name: product.productName,
          //   image: product.image,
          //   quantity: product.quantity,
          //   amount: product.specialPrice,
          //   realPrice: product.price,
          //   brandName: product.brandName,
          //   uomValue: product.uomValue,
          // }))
         
          // let toMail = [];
          // if (user.email) {
          //   toMail.push(user.email);
          // }
          // if (toMail.length) {
          //   const mailDetails = {
          //     to: toMail, // list of receivers
          //     subject: 'Order Returned Successfully', // Subject line
          //     html: '<p>Order id:' + orderDetails.orderId + '<br>Returned Products: ' + products + '</p>', // plain text body
          //   };
          //   let isMailSend = await sendMail(mailDetails);
          // }
          return resolve({
            statusCode: 200,
            status: true,
            message: 'Order returned successfully',
            data: {
              returnId: response.returnId,
            },
          });
        })
        .catch((err) => {
          return resolve({ statusCode: 400, status: false, message: 'Something went wrong' + err });
        });
    } catch (error) {
      return resolve({ statusCode: 400, status: false, message: error + '' });
    }
  });
};

module.exports = {
  doGetOrderReview,
  doPlaceOrder,
  doGetOrderDetailsByOrderId,
  doRescheduleDeliveryDateByUser,
  doCancelOrderByUser,
  doRaiseComplaintsAboutOrderByUser,
  doReturnOrderByUser,
};
