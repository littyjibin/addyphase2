const {
  doAddProductToCart,
  doGetCartProducts,
  doDeleteCartItem,
  doUpdateCartItem,
  doUpdateCartInstruction,
} = require('./controller');

const addProductToCart = (req, res, next) => {
  doAddProductToCart(req?.body, req?.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const getCartProducts = (req, res, next) => {
  doGetCartProducts(req?.params, req?.user)
    .then(({ statusCode = 200, message, status, data }) => {
      res.status(statusCode).json({
        message,
        status,
        ...(data && { data }),
      });
    })
    .catch((error) => {
      next(error);
    });
};

const deleteCartItem = (req, res, next) => {
  doDeleteCartItem(req?.params?.id, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};
const updateCartItem = (req, res, next) => {
  doUpdateCartItem(req?.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};
const updateCartInstruction = (req, res, next) => {
  doUpdateCartInstruction(req?.body, req.user)
    .then(({ statusCode = 200, message, status }) => {
      res.status(statusCode).json({
        message,
        status,
      });
    })
    .catch((error) => {
      next(error);
    });
};

module.exports = {
  addProductToCart,
  getCartProducts,
  deleteCartItem,
  updateCartItem,
  updateCartInstruction,
};
