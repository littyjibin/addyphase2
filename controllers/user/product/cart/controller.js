const mongoose = require('mongoose');
const _ = require('lodash');

const cart = require('../../../../model/user/product/cart');
const product = require('../../../../model/seller/product/product');
const account = require('../../../../model/Account');
const sellerDetail = require('../../../../model/seller/registration/sellerDetails');

const { getCartItems } = require('../../../../helpers/product/cartHelper');

const {
  validateCart,
  validateDeleteCartItem,
  validateUpdateCartItem,
  validateGetCart,
  validateUpdateCartInstruction,
} = require('../../../../validations/user/product/cartValidation');

const doAddProductToCart = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateCart(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      let { _id: userId } = user;
      let { productId, variantId, sellerId } = data;
      let instruction = data.instruction;
      let quantity = parseInt(data.quantity);

      //   check product is valid or not
      let validProduct = await product.findOne({
        _id: productId,
        sellerId: sellerId,
        isActiveProduct: true,
      });
      if (validProduct) {
        if (validProduct.isVariant) {
          //   check product variant is valid or not
          validProduct = await product.findOne({
            _id: productId,
            isActiveProduct: true,
            variants: { $elemMatch: { _id: variantId, isActiveVariant: true } },
          });
        } else {
          variantId = productId;
        }
      }
      if (!validProduct) {
        return resolve({
          statusCode: 400,
          status: false,
          message: 'Invalid product.',
          data: {},
        });
      }

      //check if product and variant already added to the cart
      const existingCart = await cart.findOne({
        sellerId,
        userId,
        productId,
        variantId,
      });

      //if already exist in the cart increment quantity
      if (existingCart) {
        quantity += parseInt(existingCart.quantity);

        // check the purchase limit of product
        if (quantity > validProduct.piece) {
          return resolve({
            statusCode: 400,
            status: false,
            message: `The purchase limit of ${validProduct.productName} is ${validProduct.piece}.Current quantity in cart is ${existingCart.quantity}`,
            data: {},
          });
        }

        //update product quantity
        await cart.updateOne(
          {
            userId,
            productId,
            variantId,
          },
          {
            instruction,
            quantity,
          }
        );
        return resolve({
          statusCode: 200,
          status: true,
          message: 'Product successfully added to your cart.',
          data: { cartId: existingCart._id, productId, variantId, quantity },
        });
      }

      // check the purchase limit of product
      if (quantity > validProduct.piece) {
        return resolve({
          statusCode: 400,
          status: false,
          message: `The purchase limit of ${validProduct.productName} is ${validProduct.piece}`,
          data: {},
        });
      }

      // get the variant details of product
      const variantDetails = _.find(validProduct.variants, {
        _id: mongoose.Types.ObjectId(variantId),
      });
      new cart({
        userId,
        sellerId,
        productId,
        variantId,
        quantity,
        instruction,
        variantDetails,
      })
        .save()
        .then((response) => {
          return resolve({
            statusCode: 200,
            status: true,
            message: 'Product Successfully added to your cart.',
            data: { cartId: response._id, productId, variantId, quantity },
          });
        })
        .catch(async (error) => {
          return resolve({ statusCode: 400, status: false, message: error });
        });
    } catch (error) {
      reject(error);
    }
  });
};

// get cart product by user id
const doGetCartProducts = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateGetCart(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      const userId = user._id;
      const sellerId = data.sellerId;

      //   get the vat details of seller - pending add vat details and payment mode
      let sellerDetails = await sellerDetail.findOne(
        {
          sellerId: sellerId,
          isActive: true,
        },
        { storeUrl: 1 }
      );

      if (!sellerDetails) return resolve({ status: false, message: 'Invalid seller id.', statusCode: 400 });

      // Get cart item of user
      let products = await getCartItems(userId, sellerId);

      if (!products.length) return resolve({ status: true, message: 'Cart is empty.', statusCode: 200 });

      cartDetails = {
        itemTotal: 0,
      };

      // remove the out of stock items from cart
      let stockProducts = products.filter((item) => {
        if (item.outOfStock == false) {
          return true;
        }
      });

      // Get the total amount of available products in cart
      stockProducts.map((product) => {
        cartDetails.itemTotal += product.total;
      });
      return resolve({
        statusCode: 200,
        status: true,
        message: 'Success',
        data: { products, cartDetails, sellerDetails },
      });
    } catch (error) {
      reject(error);
    }
  });
};

// delete  cart product by cart id
const doDeleteCartItem = (cartId, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      let userId = user._id;
      //validate incoming data
      const dataValidation = await validateDeleteCartItem({ cartId });
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      //check if cart id is valid and delete
      const cartItemDelete = await cart.findOneAndDelete({
        _id: cartId,
        userId,
      });

      if (cartItemDelete) {
        return resolve({
          status: true,
          message: 'Cart item deleted successfully.',
          statusCode: 200,
        });
      } else {
        return resolve({
          status: false,
          message: 'Invalid cart item.',
          statusCode: 400,
        });
      }
    } catch (error) {
      reject(error);
    }
  });
};

// update cart product by cart id
const doUpdateCartItem = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateUpdateCartItem(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      const { cartId, quantity } = data;
      const { _id: userId } = user || {};

      const {
        variantId,
        productId,
        quantity: cartQuantity,
      } = (await cart.findOne({
        userId,
        _id: cartId,
      })) || {};

      if (!variantId && productId) {
        return resolve({
          status: false,
          message: 'Invalid cart item',
          statusCode: 400,
        });
      }
      const { productName, piece } = (await product.findOne({ _id: productId })) || {};

      // check the purchase limit of product
      if (quantity > piece) {
        return resolve({
          statusCode: 400,
          status: false,
          message: `The purchase limit of ${productName} is ${piece}`,
        });
      }

      // update cart items
      const updatedCartItem = await cart.findOneAndUpdate({ userId, _id: cartId }, { quantity }, { new: true });

      if (updatedCartItem) {
        return resolve({
          statusCode: 200,
          status: false,
          message: 'Cart updated successfully',
        });
      } else {
        return resolve({
          statusCode: 400,
          status: false,
          message: 'Invalid cart item',
        });
      }
    } catch (error) {
      reject(error);
    }
  });
};

// update cart instruction  by cart id
const doUpdateCartInstruction = (data, user) => {
  return new Promise(async (resolve, reject) => {
    try {
      //validate incoming data
      const dataValidation = await validateUpdateCartInstruction(data);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, '');
        return resolve({
          status: false,
          message: message,
          statusCode: 400,
        });
      }

      const { cartId, instruction } = data;
      const { _id: userId } = user || {};

      const isCart = await cart.findOne({ userId, _id: cartId });
      if (!isCart) {
        return resolve({
          status: false,
          message: 'Invalid cart item',
          statusCode: 400,
        });
      }

      // update cart items
      const updatedCartItem = await cart.findOneAndUpdate({ userId, _id: cartId }, { instruction }, { new: true });
      console.log(updatedCartItem);
      if (updatedCartItem) {
        return resolve({
          statusCode: 200,
          status: true,
          message: 'Instruction updated successfully',
        });
      } else {
        return resolve({
          statusCode: 400,
          status: false,
          message: 'Invalid cart item',
        });
      }
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  doAddProductToCart,
  doGetCartProducts,
  doDeleteCartItem,
  doUpdateCartItem,
  doUpdateCartInstruction,
};
