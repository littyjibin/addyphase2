const jwt = require('jsonwebtoken');
const Account = require('../../../model/Account');

const auth = async (req, res, next) => {
  try {
    const token = req.header('Authorization').replace('Bearer ', '');
    const userDetails = jwt.verify(token, 'JWT_KEY');
    const data = userDetails.data;
    const userId = data.userId;
    const user = await Account.findOne({
      _id: userId,
    });
    if (!user) {
      throw new Error();
    }
    req.user = user;
    req.identity = userDetails;
    req.token = token;
    next();
  } catch (error) {
    res.send({
      status: false,
      message: 'Unauthorized',
    });
  }
};

module.exports = auth;
