const s3 = require('../constants/digitalOceanSpace');

const uploadImage = (file, fileName) => {
  return new Promise(async (resolve, reject) => {
    try {
      await s3.upload(
        {
          Bucket: process.env.SPACE_BUCKET_NAME,
          Key: fileName,
          Body: file,
          ACL: 'public-read',
        },
        (err, data) => {
          if (err){console.log(err); return reject(err)};
          return resolve({ url: data.Location });
        }
      );
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = { uploadImage };
