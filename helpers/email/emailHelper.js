let nodemailer = require('nodemailer');

const sendMail = (mailDetails) => {
  return new Promise(async (resolve, reject) => {
    try {
      let transporter = nodemailer.createTransport({
        host: 'smtp.hostinger.com',
        port: 587,
        auth: {
          user: 'no-rep@addy.ae',
          pass: 'PassNoRep890',
        },
      });

      const mailOptions = {
        from: 'no-rep@addy.ae', // sender address
        to: mailDetails.to, // list of receivers
        subject:mailDetails.subject, // Subject line
        html: mailDetails.html, // plain text body
      };
      console.log(mailOptions)
      transporter.sendMail(mailOptions, function (err, info) {
        if (err) {
          console.log(err)
          return resolve({
            status: false,
            message: err + '',
          });
        } else {
          console.log("success")
          return resolve({
            status: true,
            message: 'Email send successfully',
          });
        }
      });
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  sendMail,
};
