const libphonenumber = require("libphonenumber-js");

const numberValidation = (code, mobile) => {
  let country = "";

  if (code == "91") {
    country = "IN";
  } else if (code == "971") {
    country = "UA";
  } else if (code == "973") {
    country = "BH";
  } else if (code == "974") {
    country = "QA";
  } else if (code == "966") {
    country = "SA";
  } else if (code == "968") {
    country = "OM";
  } else if (code == "965") {
    country = "KW";
  }

  let regionCode = libphonenumber.getCountries();
  regionCode.forEach((e, i) => {
    // console.log(e);
  });

  let validNumber = libphonenumber.isValidNumber("+" + code + mobile, country);

  if (validNumber) {
    return true;
  }
};

module.exports = numberValidation;
