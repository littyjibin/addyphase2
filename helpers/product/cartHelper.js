const mongoose = require('mongoose');
const _ = require('lodash');

const cart = require('../../model/user/product/cart');
const sellerCourierCompany = require('../../model/seller/registration/sellerCourierCompanies');
const boxSize = require('../../model/admin/master/packageSize');

// Get all cat items
const getCartItems = (userId, sellerId) => {
  return new Promise(async (resolve, reject) => {
    try {
      // Get the cart product details
      let products = await cart.aggregate([
        //get all cart based on user  id
        {
          $match: {
            userId: mongoose.Types.ObjectId(userId),
            sellerId: mongoose.Types.ObjectId(sellerId),
          },
        },
        //get all product added to cart from products collection based on product id saved
        {
          $lookup: {
            from: 'products',
            localField: 'productId',
            foreignField: '_id',
            as: 'product',
          },
        },
        {
          $unwind: '$product',
        },
        {
          $project: {
            'product.variants': {
              //only get the variant that variant id and variant id saved in cart are equal
              $filter: {
                input: '$product.variants',
                as: 'variants',
                cond: { $eq: ['$$variants._id', '$variantId'] },
              },
            },
            variantId: 1,
            productId: 1,
            quantity: 1,
            instruction: 1,
            product: {
              productName: 1,
              uom: 1,
              piece: 1,
              countryCode: 1,
              weight: 1,
              images: 1,
              basicSellingPrice: 1,
              basicCostPrice: 1,
              isActiveProduct: 1,
              isDeliveryVerification: 1,
              boxSize: 1,
            },
          },
        },
        {
          $unwind: {
            path: '$product.variants',
            preserveNullAndEmptyArrays: true,
          },
        },

        //uom value
        {
          $lookup: {
            from: 'uoms',
            localField: 'product.uom',
            foreignField: '_id',
            as: 'uomDetails',
          },
        },

        {
          $unwind: {
            path: '$uomDetails',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $set: {
            price: { $ifNull: ['$product.variants.sellingPrice', '$product.basicSellingPrice'] },
            costPrice: { $ifNull: ['$product.variants.costPrice', '$product.basicCostPrice'] },
            isActiveVariant: { $ifNull: ['$product.variants.isActiveVariant', true] },
            quantity: {
              $cond: { if: { $gte: ['$quantity', '$product.piece'] }, then: '$product.piece', else: '$quantity' },
            },
            varImage: { $first: '$product.variants.images.imageUrl' },
            images: { $first: '$product.images.imageUrl' },
          },
        },
        {
          $project: {
            _id: 0,
            cartId: '$_id',
            variantId: '$variantId',
            productId: '$productId',
            countryCode: '$product.countryCode',
            productName: '$product.productName',
            instruction: { $ifNull: ['$instruction', ''] },
            image: { $ifNull: ['$varImage', '$images'] },
            uom: '$uomDetails.uom',
            uomId: '$uomDetails._id',
            uomValue: '$product.variants.uomValue',
            color: '$product.variants.colorName',
            colorCode: '$product.variants.colorCode',
            piece: '$product.piece',
            price: '$price',
            weight: '$product.weight',
            quantity: '$quantity',
            total: {
              $multiply: ['$price', '$quantity'],
            },
            totalWeight: {
              $multiply: ['$product.weight', '$quantity'],
            },
            //  boxSize: '$product.boxSize',
            //   isActiveProduct: '$product.isActiveProduct',
            isDeliveryVerification: '$product.isDeliveryVerification',
            //    isActiveVariant: '$isActiveVariant',
            outOfStock: {
              $cond: {
                if: {
                  $and: [
                    { $eq: ['$product.isActiveProduct', true] },
                    //{ $eq: ['$product.variants.isActiveVariant', true] },
                    { $eq: ['$isActiveVariant', true] },
                  ],
                },
                then: false,
                else: true,
              },
            },
          },
        },
      ]);
      return resolve(products);
    } catch (error) {
      reject(error);
    }
  });
};

// Get all default courier company details of seller
const getCourierCompanyFeeDetails = (sellerId, totalWeight, distance) => {
  return new Promise(async (resolve, reject) => {
    try {
      let sellerCourierDetails = await sellerCourierCompany.aggregate([
        {
          $match: {
            sellerId: mongoose.Types.ObjectId(sellerId),
            isActive: true,
            isDefault: true,
          },
        },
        {
          $lookup: {
            from: 'couriercompanies',
            localField: 'courierCompany',
            foreignField: '_id',
            as: 'courierCompany',
          },
        },
        {
          $unwind: {
            path: '$courierCompany',
            //preserveNullAndEmptyArrays: true,
          },
        },
        {
          $lookup: {
            from: 'packagesizes',
            localField: 'courierCompany.boxRate.boxSize',
            foreignField: '_id',
            as: 'packageSize',
          },
        },
        // get the package sizes greater than the total weight. also take the maximum packag size
        {
          $project: {
            courierCompanies: '$courierCompany',
            companyId: '$courierCompany._id',
            packageSize: '$packageSize',
            courier: {
              $filter: {
                input: '$packageSize',
                as: 'packagesizes',
                cond: {
                  $gte: ['$$packagesizes.weight', totalWeight],
                },
              },
            },
            courierMax: { $max: '$packageSize' },
          },
        },
        // if more than one package size ar there the  take the minimum package size
        {
          $set: {
            courierMin: { $min: '$courier' },
          },
        },
        // if no package size is available in db then take the maximum package size
        {
          $project: {
            packageSize: '$packageSize',
            courierCompanies: '$courierCompanies',
            companyId: '$companyId',
            packageSizeDetails: { $ifNull: ['$courierMin', '$courierMax'] },
            courierMin: '$courierMin',
          },
        },
        // get the distance that is greater than the given distance
        {
          $project: {
            packageSize: '$packageSize',
            courierCompanies: '$courierCompanies',
            companyId: '$companyId',
            courierMin: '$courierMin',
            packageSizeDetails: '$packageSizeDetails',
            distance: {
              $filter: {
                input: '$courierCompanies.boxRate',
                as: 'courierCompanyDetails',
                cond: {
                  $and: [
                    { $eq: ['$$courierCompanyDetails.boxSize', '$packageSizeDetails._id'] },
                    { $gte: ['$$courierCompanyDetails.upto', distance] },
                  ],
                },
              },
            },
            distanceMax: {
              $filter: {
                input: '$courierCompanies.boxRate',
                as: 'courierCompanyDetails',
                cond: {
                  $eq: ['$$courierCompanyDetails.boxSize', '$packageSizeDetails._id'],
                },
              },
            },
            distanceMax1: { $max: '$courierCompanies.boxRate' },
          },
        },
        {
          $set: {
            distanceMin: { $min: '$distance' },
            distanceMax: { $max: '$distanceMax' },
          },
        },
        // if minimum distance is not availble then get the maximum distance in db
        {
          $project: {
            packageSize: '$packageSize',
            courierMin: '$courierMin',
            packageSizeDetails: '$packageSizeDetails',
            courierCompanies: '$courierCompanies',
            companyId: '$companyId',
            distanceDetails: { $ifNull: ['$distanceMin', '$distanceMax'] },
          },
        },
        {
          $project: {
            packageSize: '$packageSize',
            courierCompanies: '$courierCompanies',
            companyId: '$companyId',
            courierMin: '$courierMin',
            packageSizeId: '$packageSizeDetails._id',
            packageSizeName: '$packageSizeDetails.size',
            packageSizeWeight: '$packageSizeDetails.weight',
            total: '$distanceDetails.total',
          },
        },
      ]);

      return resolve(sellerCourierDetails);
    } catch (error) {
      reject(error);
    }
  });
};

// Get box size details based on weight
const getBoxSizeDetailsbyWeight = (totalWeight) => {
 
  return new Promise(async (resolve, reject) => {
    try {
      let boxSizeDetails = await boxSize
        .findOne({ isActive: true, weight: { $gte: totalWeight } })
        .sort({ weight: 1 })
        .limit(1); //get the min weight

        
      // if package size is not available then get the maximum size
      if (!boxSizeDetails) {
        boxSizeDetails = await boxSize.findOne({ isActive: true }).sort({ weight: -1 }).limit(1); 
      }

      return resolve(boxSizeDetails);
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  getCartItems,
  getCourierCompanyFeeDetails,
  getBoxSizeDetailsbyWeight,
};
