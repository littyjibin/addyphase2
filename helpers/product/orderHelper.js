const mongoose = require('mongoose');
const _ = require('lodash');
const moment = require('moment-timezone');

const cart = require('../../model/user/product/cart');
//const sellerOrderHistory = require('../../model/seller/registration/sellerOrderHistoryOld');
const sellerOrderHistory = require('../../model/seller/product/sellerOrderHistory');
const notification = require('../../model/user/notification');
const account = require('../../model/Account');
const order = require('../../model/user/product/order');
const orderHistory = require('../../model/seller/product/sellerOrderHistory');

const sendSms = require('../../helpers/sms/sendSms');

const notificationType = 'onlinestore';

// add placed order details to order history
const addPlacedOrderDetails = (response) => {
  return new Promise(async (resolve, reject) => {
    try {
      const userId = response.userId;

      let dateFormat = moment().format('MMM DD,YYYY hh:mm A');
      // save order detils to order history
      let historyData = {
        orderId: response._id,
        pending: {
          value: true,
          date: new Date(),
          dateFormat: dateFormat,
        },
      };
      let historyObj = sellerOrderHistory(historyData);
      await historyObj.save();

      // notification added to db
      let notfication = new notification({
        userId: userId,
        message: 'Order placed successfully',
        is_read: 0,
        type: notificationType,
      });
      await notfication.save();

      //clear user all cart items
      await cart.deleteMany({ userId });

      // get user details
      const { code, mobile } = await account.findOne({ _id: userId });

      // sending sms to user
      let payload = {
        origin: 'ADDYAE',
        destination: code + mobile,
        message: `Thank you for shopping with us.Your order with id ${response.orderId} with ${response.noOfItems} items placed successfully.`,
      };

      //   await sendSms(payload)
      //     .then((_) => {
      //       return resolve({ statusCode: 200, status: true, message: 'Order Placed successfully' });
      //     })
      //     .catch((error) => {
      //       return resolve({ statusCode: 400, status: false, message: error, data: {} });
      //     });
      return resolve();
    } catch (error) {
      reject(error);
    }
  });
};

// add order history details during status change
const addOrderHistoryDetails = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      const { id, orderId, userId, status, notificationStatus, sellerId } = data;

      // update order history
      let orderHistory = await sellerOrderHistory
        .findOne({
          orderId: mongoose.Types.ObjectId(id),
        })
        .lean();
      let dateFormat = moment().format('MMM DD, YYYY hh:mm a');

      for (const property in orderHistory) {
        orderHistory[property].value = false;
      }
     
      orderHistory[status] = {
        value: true,
        date: new Date(),
        dateFormat: dateFormat,
      };

      await sellerOrderHistory.updateOne({ orderId: id }, orderHistory);

      if (userId) {
        // notification added to db
        let notfication = new notification({
          userId: userId,
          message: `Your order with order id ${orderId}  ${notificationStatus}`,
          is_read: 0,
          type: notificationType,
        });
        await notfication.save();
      }

      if (sellerId) {
        // notification added to db
        let notfication = new notification({
          userId: sellerId,
          message: `Your ministore order with order id ${orderId}  ${notificationStatus}`,
          is_read: 0,
          type: notificationType,
        });
        await notfication.save();
      }

      // get user details
      // const { code, mobile } = await account.findOne({ _id: userId });

      // sending sms to user
      // let payload = {
      //   origin: 'ADDYAE',
      //   destination:code + mobile,
      //   message: `Thank you for shopping with us.Your order with id ${response.orderId} with ${response.noOfItems} items placed successfully.`,
      // };

      //   await sendSms(payload)
      //     .then((_) => {
      //       return resolve({ statusCode: 200, status: true, message: 'Order Placed successfully' });
      //     })
      //     .catch((error) => {
      //       return resolve({ statusCode: 400, status: false, message: error, data: {} });
      //     });
      return resolve();
    } catch (error) {
      reject(error);
    }
  });
};

// get order details by user id - web order listing
const getOrderDetailsByUserId = (userId) => {
  return new Promise(async (resolve, reject) => {
    try {
      let orderDetails = [];
      //get from db
      orderDetails = await order.aggregate([
        {
          $match: {
            userId: mongoose.Types.ObjectId(userId),
            isActive: true,
          },
        },
        {
          $lookup: {
            from: 'accounts',
            localField: 'sellerId',
            foreignField: '_id',
            as: 'sellerDetails',
          },
        },
        {
          $lookup: {
            from: 'sellerdrivers',
            localField: 'selectedDriverId',
            foreignField: '_id',
            as: 'driverDetails',
          },
        },
        {
          $lookup: {
            from: 'couriercompanies',
            localField: 'selectedCompanyId',
            foreignField: '_id',
            as: 'companyDetails',
          },
        },
        {
          $project: {
            _id: 1,
            orderId: 1,
            type: 'ministore',
            title: { $toString: "$noOfItems" },
            status: '$orderStatus',
            paymentType: 1,
            paymentType: 1,
            expectedDeliveryDate: 1,
            sellerCountryCode: 1,
            grandTotal: '$cartDetails.grandTotal',
            logo: { $ifNull: [{ $first: '$sellerDetails.logo' }, ""] },
            isSellerChangedPaymentType: { $ifNull: ['$isSellerChangedPaymentType', false] },
            name: { $concat: [{ $first: '$sellerDetails.firstName' }, ' ', { $first: '$sellerDetails.lastName' }] },
          },
        },
      ]);

      return resolve(orderDetails);
    } catch (error) {
      reject(error);
    }
  });
};

// get order details by id
const getOrderDetailsById = (id) => {
  return new Promise(async (resolve, reject) => {
    try {
      let orderDetails = [];

      //get from db
      orderDetails = await order.aggregate([
        {
          $match: {
            _id: mongoose.Types.ObjectId(id),
            isActive: true,
          },
        },
        {
          $lookup: {
            from: 'accounts',
            localField: 'userId',
            foreignField: '_id',
            as: 'userDetails',
          },
        },
        {
          $lookup: {
            from: 'sellerdrivers',
            localField: 'selectedDriverId',
            foreignField: '_id',
            as: 'driverDetails',
          },
        },
        {
          $lookup: {
            from: 'couriercompanies',
            localField: 'selectedCompanyId',
            foreignField: '_id',
            as: 'companyDetails',
          },
        },
        {
          $project: {
            _id: 1,
            orderId: 1,
            noOfItems: 1,
            paymentType: 1,
            userId: 1,
            sellerCountryCode: 1,
            deliveryDate: 1,
            expectedDeliveryDate: 1,
            orderStatus: 1,
            delivered:1,
            expectedDeliveryTime: { $concat: ['$expectedDeliveryTime.fromTime', '-', '$expectedDeliveryTime.toTime'] },
            expectedDeliveryFromTime: '$expectedDeliveryTime.fromTime',
            products: 1,
            // itemsSold: { $push:  { productName: "$products.productName", quantity: "$products.quantity" } },
            itemTotal: '$cartDetails.itemTotal',
            deliveryFee: '$cartDetails.deliveryFee',
            vat: '$cartDetails.vat',
            grandTotal: '$cartDetails.grandTotal',
            customerDetails: {
              name: { $concat: [{ $first: '$userDetails.firstName' }, ' ', { $first: '$userDetails.lastName' }] },
              code: { $first: '$userDetails.code' },
              mobile: { $first: '$userDetails.mobile' },
              image: { $ifNull: [{ $first: '$userDetails.logo' }, ''] }
            },
            deliveryDetails: {
              Code: { $ifNull: [{ $first: '$driverDetails.code' }, { $first: '$companyDetails.code' }] },
              mobile: { $ifNull: [{ $first: '$driverDetails.mobile' }, { $first: '$companyDetails.mobile' }] },
              image: { $ifNull: [{ $first: '$driverDetails.image' }, { $first: '$companyDetails.logo' }] },
            },
          },
        },
      ]);

      return resolve(orderDetails[0]);
    } catch (error) {
      reject(error);
    }
  });
};

// get order history details by id
const getOrderHistoryByOrderId = (orderId) => {
  return new Promise(async (resolve, reject) => {
    try {
      let orderHistories = [];

      //get from db
      orderHistories = await orderHistory.findOne(
        { orderId: orderId },
        { __v: 0, createdAt: 0, updatedAt: 0, _id: 0, orderId: 0 }
      );
  
      let orderTracking = [];
      


      // assigning values to orderTracking
      if (orderHistories.driverAssigned!="{}") {
        orderTracking.push({
          status: 'Assigned',
          date: orderHistories.driverAssigned.date,
          dateFormat: moment(orderHistories.driverAssigned.date).format('MMM DD,YYYY hh:mm a'),
          active: orderHistories.driverAssigned.value,
        });
      }
      if (orderHistories.companyAssigned!="{}") {
        orderTracking.push({
          status: 'Assigned',
          date: orderHistories.companyAssigned.date,
          dateFormat: moment(orderHistories.companyAssigned.date).format('MMM DD,YYYY hh:mm a'),
          active: orderHistories.companyAssigned.value,
        });
      }
      if(!orderTracking.length){
        return resolve(orderTracking);
      }
      if (orderHistories.startPickup!="{}") {
        orderTracking.push({
          status: 'Start Pickup',
          date: orderHistories.startPickup.date,
          dateFormat: moment(orderHistories.startPickup.date).format('MMM DD,YYYY hh:mm a'),
          active: orderHistories.startPickup.value,
        });
      }else{
        orderTracking.push({
          status: 'Start Pickup',
          date: '',
          dateFormat: '',
          active: false,
        });
      }
      if (orderHistories.attemptPickup!="{}") {
        orderTracking.push({
          status: 'Attempt Pickup',
          date: orderHistories.attemptPickup.date,
          dateFormat: moment(orderHistories.attemptPickup.date).format('MMM DD,YYYY hh:mm a'),
          active: orderHistories.attemptPickup.value,
        });
      }else{
        orderTracking.push({
          status: 'Attempt Pickup',
          date: '',
          dateFormat: '',
          active: false,
        });
      }
      if (orderHistories.completePickup!="{}") {
        orderTracking.push({
          status: 'Complete Pickup',
          date: orderHistories.completePickup.date,
          dateFormat: moment(orderHistories.completePickup.date).format('MMM DD,YYYY hh:mm a'),
          active: orderHistories.completePickup.value,
        });
      }else{
        orderTracking.push({
          status: 'Complete Pickup',
          date: '',
          dateFormat: '',
          active: false,
        });
      }

      if (orderHistories.startDelivery!="{}") {
        orderTracking.push({
          status: 'Start Delivery',
          date: orderHistories.startDelivery.date,
          dateFormat: moment(orderHistories.startDelivery.date).format('MMM DD,YYYY hh:mm a'),
          active: orderHistories.startDelivery.value,
        });
      }else{
        orderTracking.push({
          status: 'Start Delivery',
          date: '',
          dateFormat: '',
          active: false,
        });
      }
      if (orderHistories.attemptDelivery!="{}") {
        orderTracking.push({
          status: 'Attempt Delivery',
          date: orderHistories.attemptDelivery.date,
          dateFormat: moment(orderHistories.attemptDelivery.date).format('MMM DD, YYYY hh:mm a'),
          active: orderHistories.attemptDelivery.value,
        });
      }else{
        orderTracking.push({
          status: 'Attempt Delivery',
          date: '',
          dateFormat: '',
          active: false,
        });
      }
      if (orderHistories.failedToDelivery!="{}") {
        orderTracking.push({
          status: 'Failed Delivery',
          date: orderHistories.failedToDelivery.date,
          dateFormat: moment(orderHistories.failedToDelivery.date).format('MMM DD, YYYY hh:mm a'),
          active: orderHistories.failedToDelivery.value,
        });
      }else{
        orderTracking.push({
          status: 'Failed Delivery',
          date: '',
          dateFormat: '',
          active: false,
        });
      }
      if (orderHistories.delivered!="{}") {
        orderTracking.push({
          status: 'Delivered',
          date: orderHistories.delivered.date,
          dateFormat: moment(orderHistories.delivered.date).format('MMM DD, YYYY'),
          active: orderHistories.delivered.value,
        });
      }else{
        orderTracking.push({
          status: 'Delivered',
          date: '',
          dateFormat: '',
          active: false,
        });
      }
     
      //console.log(orderTracking);
      return resolve(orderTracking);
    } catch (error) {
      reject(error);
    }
  });
};

// check order is valid or not by id and userId
const isOrderValidByUser = (id, userId) => {
  return new Promise(async (resolve, reject) => {
    try {
      let validOrder = await order.findOne({
        _id: mongoose.Types.ObjectId(id),
        userId: mongoose.Types.ObjectId(userId),
        isActive: true,
      });
      return resolve(validOrder);
    } catch (error) {
      reject(error);
    }
  });
};

// check order is valid or not by id and sellerId
const isOrderValidBySeller = (id, sellerId) => {
  return new Promise(async (resolve, reject) => {
    try {
      let validOrder = await order.findOne({
        _id: mongoose.Types.ObjectId(id),
        sellerId: mongoose.Types.ObjectId(sellerId),
        isActive: true,
      });
      return resolve(validOrder);
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  addPlacedOrderDetails,
  addOrderHistoryDetails,
  getOrderDetailsByUserId,
  getOrderDetailsById,
  getOrderHistoryByOrderId,
  isOrderValidByUser,
  isOrderValidBySeller
};
