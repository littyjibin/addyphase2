const apiKey = process.env.smsApiKey;
const apiSecret = process.env.smsApiSecret;
const smsGlobal = require('smsglobal')(apiKey, apiSecret);

const sendSms = async (payload) => {
  return new Promise(async (resolve, reject) => {
    smsGlobal.sms
      .send(payload)
      .then((res) => {
        return resolve();
      })
      .catch((error) => {
        return reject(error.data.errors.destination.errors[0]);
      });
  });
};

module.exports = sendSms;
