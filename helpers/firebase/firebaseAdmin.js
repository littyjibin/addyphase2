const admin = require("firebase-admin");
const serviceAccount = require("./fire-base.json");
// add your firebase db url here
// const FIREBASE_DATABASE_URL;
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});
const firebaseAdmin = {};
firebaseAdmin.sendMulticastNotification = function (payload) {
  console.log(payload);
  const message = {
    notification: {
      title: payload.title,
      body: payload.message,
    },
    tokens: payload.tokens,
    data: {
      id: payload.id,
      orderId: payload.orderId,
    },
  };
  return admin
    .messaging()
    .sendMulticast(message)
    .then((res) => {
      console.log(res);
    })
    .catch((er) => {
      console.log(er);
    });
};
module.exports = firebaseAdmin;
