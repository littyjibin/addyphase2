const timeZoneValidation = (code) => {
  let country = "";

  if (code == "91") {
    country = "Asia/Kolkata";
  } else if (code == "971") {
    country = "Asia/Dubai";
  } else if (code == "973") {
    country = "Asia/Bahrain";
  } else if (code == "974") {
    country = "Asia/Qatar";
  } else if (code == "966") {
    country = "Asia/Riyadh";
  } else if (code == "968") {
    country = "Asia/Muscat";
  } else if (code == "965") {
    country = "Asia/Kuwait";
  }

  return country;
};

module.exports = timeZoneValidation;
