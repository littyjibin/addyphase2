const mongoose = require("mongoose");

const sellerPackageSchema = mongoose.Schema(
    {
      
        packageName: {
            type: String,
            required: true
        },
        packageFor: {
            type: String,
            required: true
        },
        country: {
            type: String,
            required: true
        },
        countryCode: {
            type: String,
            required: true
        },
        ordersCount: {
            type: Number,
            required: true
        },
        price: {
            type: Number,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        packageDuration: {
            type: String,
            required: true
        },
        startDate: {
            type: Date,
            required: true
        },
        endDate: {
            type: Date,
            required: true
        },
        features: {
            type: Object,
            required: true
        },
        termsAndConditions: {
            type: String,
            required: true
        },
        isActive: {
            type: Boolean,
            default: true
        },
        createdBy: {
            type: mongoose.Types.ObjectId
        },
        updatedBy: {
            type: mongoose.Types.ObjectId
        },
    },
    {
        timestamps: true,
    }
);

const sellerPackage = mongoose.model("sellerpackages", sellerPackageSchema);
module.exports = sellerPackage;
