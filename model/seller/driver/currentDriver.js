const mongoose = require('mongoose')

const driverSchema = new mongoose.Schema({

    driverId: {
        type: String,
        required: [true, "driverId missing"],
    },
    position:{
        type: number,
        
    },
    
    createdAt: { 
        type: Date,
        required: false
    },
    updatedAt: { 
        type: Date,
        required: false
    }
})



module.exports = mongoose.model('driver',driverSchema)
