const mongoose = require("mongoose");

const orderHistorySchema = new mongoose.Schema({
  order_id: {
    type: mongoose.Types.ObjectId,
    required: [true, "order_id missing"],
  },
  waiting: {
    value: {
      type: Boolean,
    },
    date: {
      type: Date,
    },
  },
  Approved: {
    value: {
      type: Boolean,
    },
    date: {
      type: Date,
    },
  },
  Rejected: {
    value: {
      type: Boolean,
    },
    date: {
      type: Date,
    },
  },
  assigned_to_driver: {
    value: {
      type: Boolean,
    },
    date: {
      type: Date,
    },
  },
  driver_approved: {
    value: {
      type: Boolean,
    },
    date: {
      type: Date,
    },
  },
  driver_rejected: {
    value: {
      type: Boolean,
    },
    date: {
      type: Date,
    },
  },
  pickup_initiated: {
    value: {
      type: Boolean,
    },
    date: {
      type: Date,
    },
  },
  pickup_completed: {
    value: {
      type: Boolean,
    },
    date: {
      type: Date,
    },
  },
  pickup_failed: {
    value: {
      type: Boolean,
    },
    date: {
      type: Date,
    },
  },
  delivery_initiated: {
    value: {
      type: Boolean,
    },
    date: {
      type: Date,
    },
  },
  delivery_completed: {
    value: {
      type: Boolean,
    },
    date: {
      type: Date,
    },
  },
  delivery_failed: {
    value: {
      type: Boolean,
    },
    date: {
      type: Date,
    },
  },
});

module.exports = mongoose.model("orderHistory", orderHistorySchema);
