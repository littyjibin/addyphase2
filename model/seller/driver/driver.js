const mongoose = require('mongoose');

const driverSchema = new mongoose.Schema(
  {
    mobile: {
      type: String,
      required: [true, 'mobile number missing'],
      max: 50,
    },
    otp: {
      type: Number,
      required: false,
    },
    expireOtp: {
      type: Date,
    },
    code: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: [true, 'drive name missing'],
      max: 50,
      lowercase: true,
    },
    image: {
      type: String,
    },
    password: {
      type: String,
      max: 50,
    },
    isActive: {
      type: Boolean,
      default: true,
    },
    lastOccupied: {
      type: Boolean,
      default: false,
    },
    orders: [
      {
        type: mongoose.Schema.Types.ObjectId,
      },
    ],
    createdAt: {
      type: Date,
      required: false,
    },
    updatedAt: {
      type: Date,
      required: false,
    },
  
  },
  {
    timestamps: true,
  }
);

driverSchema.pre('save', function (next) {
  let now = new Date();
  if (!this.createdAt) {
    this.createdAt = now;
  }
  this.updatedAt = now.getTime();
  next();
});

module.exports = mongoose.model('drivers', driverSchema);
