const mongoose = require("mongoose");

const contactSchema = mongoose.Schema(
    {
        sellerId: {
            type: mongoose.Types.ObjectId,
            required: [true, 'sellerId missing'],
          },
        userName: {
            type: String,
            trim:true,
            required: [true, "User name is missing"],
        },
        logo: {
            type: String,
            default: "",
        },
        logoFlag: {
            type: Boolean
        },
        phone: [
            {
                code: {
                    type: Number
                  },
                number: {
                    type: String,
                    default: false,
                },
                addyUser: {
                    type: Boolean
                  }
            }
        ],
        email: [
            {
                email: {
                    type: String,
                    default: false,
                }
            }
        ],
        isActive: {
            type: Boolean,
            default: true,
        }
    },
    {
        timestamps: true,
    }
);

const color = mongoose.model("contacts", contactSchema);
module.exports = color;
