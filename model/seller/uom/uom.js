const mongoose = require("mongoose");

const uomSchema = mongoose.Schema(
    {
        uom: {
            type: String,
            trim:true,
            required: [true, "Uom is missing"],
        },
        isActive: {
            type: Boolean,
            default: true,
        },
        createdBy: {
            type: mongoose.Types.ObjectId       
        },
        updatedBy: {
            type: mongoose.Types.ObjectId               
        },
    },
    {
        timestamps: true,
    }
);

const uom = mongoose.model("uoms", uomSchema);
module.exports = uom;
