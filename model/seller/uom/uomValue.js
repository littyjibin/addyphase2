const mongoose = require("mongoose");

const uomValueSchema = mongoose.Schema(
    {
        uomId: {
            type: mongoose.Types.ObjectId ,
            required: [true, "Uom id is missing"],
        },
        uomValue: {
            type: String,
            trim:true,
            required: [true, "Uom value is missing"],
        },
        isActive: {
            type: Boolean,
            default: true,
        },
        createdBy: {
            type: mongoose.Types.ObjectId       
        },
        updatedBy: {
            type: mongoose.Types.ObjectId               
        },
    },
    {
        timestamps: true,
    }
);

const uomValue = mongoose.model("uomvalues", uomValueSchema);
module.exports = uomValue;
