const mongoose = require("mongoose");

const sellerColorSchema = mongoose.Schema(
    {
        sellerId: {
            type: mongoose.Types.ObjectId,
            required: [true, "Seller id is missing"],
            ref:"Account"
        },
        colorName: {
            type: String,
            trim:true,
            required: [true, "Color name is missing"],
        },
        colorCode: {
            type: String,
            trim:true,
            required: [true, "Color code is missing"],
        },
        isActive: {
            type: Boolean,
            default: true,
        },
        createdBy: {
            type: mongoose.Types.ObjectId       
        },
        updatedBy: {
            type: mongoose.Types.ObjectId               
        },
    },
    {
        timestamps: true,
    }
);

const sellerColor = mongoose.model("sellercolors", sellerColorSchema);
module.exports = sellerColor;
