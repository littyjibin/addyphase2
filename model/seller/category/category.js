const mongoose = require("mongoose");

const sellerCategorySchema = mongoose.Schema(
    {
        categoryTitle: {
            type: String,
            required: [true, "Title is missing"],
        },
        description: {
            type: String,
            required: [true, "Description is missing"],
        },
        isActive: {
            type: Boolean,
            default: true,
        },
        createdBy: {
            type: mongoose.Types.ObjectId       
        },
        updatedBy: {
            type: mongoose.Types.ObjectId               
        },
    },
    {
        timestamps: true,
    }
);

const sellerCategory = mongoose.model("sellercategories", sellerCategorySchema);
module.exports = sellerCategory;
