const mongoose = require("mongoose");

const sellerCourierCompanySchema = mongoose.Schema(
    {
        sellerId: {
            type: mongoose.Types.ObjectId,
            trim:true,
            required: [true, "seller id is missing"],
        },
        courierCompany: {
            type: mongoose.Types.ObjectId,
            trim:true,
            required: [true, "courier company is missing"],
            ref:"couriercompanies"
        },
        isDefault: {
            type: Boolean,
            default: false,
        },
        isActive: {
            type: Boolean,
            default: true,
        }
    },
    {
        timestamps: true,
    }
);

const sellerCourierCompany = mongoose.model("sellerCourierCompanies", sellerCourierCompanySchema);
module.exports = sellerCourierCompany;
