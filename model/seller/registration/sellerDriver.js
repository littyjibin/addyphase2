const mongoose = require('mongoose');

const driverSchema = new mongoose.Schema(
  {
    sellerId: {
      type: mongoose.Types.ObjectId,
      required: [true, 'sellerId missing'],
    },
    mobile: {
      type: String,
      required: [true, 'mobile number missing'],
      max: 50,
    },
    code: {
      type: Number,
    },
    name: {
      type: String,
      required: [true, 'drive name missing'],
      max: 50,
      lowercase: true,
    },
    image: {
      type: String,
    },
    isActive: {
      type: Boolean,
      default: true,
    },
    deliveryFee: [
      {
        from: { type: Number },
        upto: { type: Number },
        rate: { type: Number }
      },
    ],
    isDefault: {
      type: Boolean,
      default: false,
    },
    orders: [
      {
        type: mongoose.Schema.Types.ObjectId,
      },
    ],
  },
  {
    timestamps: true,
  }
);


const sellerDriver = mongoose.model("sellerdrivers", driverSchema);
module.exports = sellerDriver;


