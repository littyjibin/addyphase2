const mongoose = require("mongoose");

const sellerDetailsSchema = mongoose.Schema(
    {
        sellerId: {
            type: mongoose.Types.ObjectId,
            required: [true, "Seller id is missing"],
        },
        categoryId: {
            type:  mongoose.Types.ObjectId,
            required: [true, "Category id is missing"],
        },
        addressId: {
            type:  mongoose.Types.ObjectId,
            required: [true, "Address id is missing"],
            ref:"accounts"
        },
        // logo: {
        //     type: String,
        //     required: [true, "Logo id is missing"],
        // },
        businessName: {
            type: String,
            required: [true, "Business name is missing"],
        },
        storeUrl: {
            type: String,
            required: [true, "Sore url is missing"],
        },
        colorName: {
            type: String,
        //    required: [true, "Color name is missing"],
        },
        colorCode: {
            type:  String,
            required: [true, "Color code is missing"],
        },
        packageId: {
            type:  mongoose.Types.ObjectId,
            required: [true, "Package id is missing"],
        },
        // courierCompany: {
        //     type:  [mongoose.Types.ObjectId],
        //     ref:"couriercompanies"
        // },      
        commercialRegNum: {
            type: String,
            required: false
        },
        taxNumber: {
            type: String,
            required: false
        },
        vat: {
            type: Number,
            required: false
        },
        returnPolicyDays: {
            type: Number,
            required: false,
        },
        bankDetails: [{
            bankName: {
                type: String,
                required: false
            },
            accountNumber: {
                type: String,
                required: false
            },
            IBAN: {
                type: String,
                required: false
            },
            bankAddress: {
                type: String,
                required: false
            },
        }],
        isActive: {
            type: Boolean,
            default: true,
        },
      
    },
    {
        timestamps: true,
    }
);

const sellerDetails = mongoose.model("sellerdetails", sellerDetailsSchema);
module.exports = sellerDetails;
