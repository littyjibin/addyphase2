const mongoose = require('mongoose');

const sellerOrderHistorySchema = new mongoose.Schema(
  {
    orderId: {
      type: mongoose.Types.ObjectId,
      required: [true, 'orderId is missing'],
    },
    pending: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    },
    cancelled: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    },
    accepted: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    },    
    rejected: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    },    
    paymentModeChangedBySeller: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    }, 
    paymentModeChangedByUser: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    },  
    assignedToDriver: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    },
    assignedToCompany: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    },
    driverAssigned: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    },
    driverRejected: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    },
    companyAssigned: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    },
    companyRejected: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    },
    startPickup: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    },
    attemptPickup: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    },
    failedToPickup: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    },
    completePickup: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    },
    startDelivery: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    },
    attemptDelivery: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    },
    failedToDelivery: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    },
    delivered: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    },
    returnRequested: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    },
    returnRejected: {
      value: {
        type: Boolean,
      },
      date: {
        type: Date,
      },
      dateFormat: {
        type: String,
      }
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model('sellerorderHistories', sellerOrderHistorySchema);
