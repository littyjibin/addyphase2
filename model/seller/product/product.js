const mongoose = require("mongoose");

const productSchema = mongoose.Schema(
    {
        productId: {
            type: Number,
            required: [true, "Product id is missing"],
        },
        sellerId: {
            type: mongoose.Types.ObjectId,
            required: [true, "Seller id is missing"],
        },
        productName: {
            type: String,
            trim:true,
            required: [true, "Product name is missing"],
        },
        countryCode: {
            type: String,
            trim:true,
            required: [true, "Country code is missing"],
        },
        description: {
            type: String,
            required: [true, "Product description is missing"],
        },
        taggedCategories: {
            type: [mongoose.Types.ObjectId],
            required: [true, "Tagged catgories is missing"],
            ref:"productcategories"
        },
        uom: {
            type: mongoose.Types.ObjectId,
            trim:true,
            required: [true, "uom is missing"],
            ref:"uoms"
        },
        defaultImage: {
            type: String,
            required: [true, "Default image is missing"],
        },
        // boxSize: {
        //     type: mongoose.Types.ObjectId,
        //     trim:true,
        //     required: [true, "boxSize is missing"],
        //     ref:"packagesizes"
        // },
        height: {
            type: Number,
            trim:true,
            required: [true, "Height is missing"],
        },
        width: {
            type: Number,
            trim:true,
            required: [true, "Width is missing"],
        }, 
        length: {
            type: Number,
            trim:true,
            required: [true, "Length is missing"],
        }, 
        weight: {
            type: Number,
            trim:true,
            required: [true, "Weight is missing"],
        },
        piece: {
            type:Number,
            trim:true,
            required: [true, "Piece is missing"]
        },
        basicCostPrice: {
            type: Number,
            trim:true,
            required: [true, "Basic cost price is missing"],
        },
        basicSellingPrice: {
            type: Number,
            trim:true,
            required: [true, "Basic selling price is missing"],
        },
        images: { type:  [{
            imageUrl: { type: String },
            isDefault :{type: Boolean,default: false}  
          }] 
        },
        variants: [{
            // uomValue: { type: mongoose.Types.ObjectId,ref:"uomvalues" },
            uomValue:{ type: String },
            costPrice: { type: Number },
            sellingPrice: { type: Number },
            images: { type:  [{
                imageUrl: { type: String },
                isDefault :{type: Boolean,default: false}  
              }] 
            },
            colorName: {
                type: String
            },
            colorCode: {
                type: String
            },
            //colorId: { type: mongoose.Types.ObjectId,ref:"colors" },
            // color: { type:  {
            //     colorName: { type: String },
            //     colorCode :{type: String}  
            //   } 
            // },
            isActiveVariant: {  type: Boolean,default: true},
            isDefaultVariant: {  type: Boolean,default: false},
        }],
        isActiveProduct: {
            type: Boolean,
            default: true,
        }, 
        isVariant: {            
            type: Boolean,
            default: false,
        },
        isDeliveryVerification: {            
            type: Boolean,
            default: true,
        },  
        type: {            
            type: String,
            default: "manual",
        },    
    },
    {
        timestamps: true,
    }
);

const products = mongoose.model("products", productSchema);
module.exports = products;
