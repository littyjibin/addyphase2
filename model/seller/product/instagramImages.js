const mongoose = require('mongoose');

const instagramImageSchema = mongoose.Schema(
  {
    sellerId: {
      type: mongoose.Types.ObjectId,
    },
    mediaType: {
      type: String,
      required: [true, 'Media type is missing'],
    },
    mediaCaption: {
      type:String
    },
    imageDetails: [
      {
        url: { type: String, required: [true, 'url is missing'] },
        mediaId: { type: String, required: [true, 'mediaId is missing'] },
      },
    ],
  },
  {
    timestamps: true,
  }
);

const instagramImages = mongoose.model('instagramImages', instagramImageSchema);
module.exports = instagramImages;
