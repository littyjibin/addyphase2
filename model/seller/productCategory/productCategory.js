const mongoose = require("mongoose");

const sellerProductCategorySchema = mongoose.Schema(
    {
        sellerId: {
            type: mongoose.Types.ObjectId,
            required: [true, "Seller id is missing"],
        },
        productCategory: {
            type: String,
            required: [true, "productCategory is missing"],
        },
        isActive: {
            type: Boolean,
            default: true,
        },
        createdBy: {
            type: mongoose.Types.ObjectId       
        },
        updatedBy: {
            type: mongoose.Types.ObjectId               
        },
    },
    {
        timestamps: true,
    }
);

const sellerProductCategory = mongoose.model("productcategories", sellerProductCategorySchema);
module.exports = sellerProductCategory;
