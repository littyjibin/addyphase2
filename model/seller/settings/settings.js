const { boolean, string } = require("joi");
const mongoose = require("mongoose");

const settingsSchema = mongoose.Schema(
    {
        sellerId: {
            type: mongoose.Types.ObjectId,
            required: [true, "Seller id is missing"],
        },
        notifications: [
            {
                callForeOutForDelivery: {
                    type: Boolean,
                    default: false,
                },
                callForMarkForDelivery: {
                    type: Boolean,
                    default: true,
                },
                pushNotifyForeOutForDelivery: {
                    type: Boolean,
                    default: false,
                },
                pushNotifyForMarkForDelivery: {
                    type: Boolean,
                    default: true,
                },
                whatsappNotifyForeOutForDelivery: {
                    type: Boolean,
                    default: false,
                },
                whatsappNotifyForMarkForDelivery: {
                    type: Boolean,
                    default: false,
                }
            },
            {
                timestamps: true,
            }
        ],
        payment: [
            {
                cashOnDelivery: {
                    type: Boolean,
                    default: false,
                },
                onlinePayment: {
                    type: Boolean,
                    default: false,
                },
                enableVat: {
                    type: Boolean,
                    default: false,
                },
                priceIncludeVat: {
                    type: Boolean,
                    default: false,
                },
                priceIncludeDeliveryFee: {
                    type: Boolean,
                    default: false,
                }
            },
            {
                timestamps: true,
            }
        ],
        privacy: [
            {
                displayMobileNumber: {
                    type: Boolean,
                    default: false,
                },
                displayWhatsappNumber: {
                    type: Boolean,
                    default: false,
                }
            },
            {
                timestamps: true,
            }
        ],
        businessDetails: [
            {
                displayComRegNumber: {
                    type: Boolean,
                    default: false,
                },
                displayTaxNumber: {
                    type: Boolean,
                    default: false,
                }

            },
            {
                timestamps: true,
            }
        ]

    },
    {
        timestamps: true,
    }
);

const settings = mongoose.model("sellersettings", settingsSchema);
module.exports = settings;
