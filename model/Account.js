const mongoose = require('mongoose');

const accountSchema = new mongoose.Schema({
  mobile: {
    type: String,
    required: true,
  },
  otp: {
    type: Number,
    required: false,
  },
  firstName: {
    type: String,
    required: false,
  },
  lastName: {
    type: String,
    required: false,
  },
  logo: {
    type: String,
    required: false,
  },
  email: {
    type: String,
    required: false,
  },
  location: {
    latitude: {
      type: String,
      required: false,
    },
    longitude: {
      type: String,
      required: false,
    },
  },
  address: [
    {
      address: {
        type: String,
        required: false,
      },
      houseNumber: {
        type: String,
        required: false,
      },
      buildingName: {
        type: String,
        required: false,
      },
      street: {
        type: String,
        required: false,
      },
      area: {
        type: String,
        required: false,
      },
      city: {
        type: String,
        required: false,
      },
      pinCode: {
        type: String,
        required: false,
      },
      location: {
        latitude: {
          type: String,
        },
        longitude: {
          type: String,
        },
      },
      deliveryInstruction: {
        type: String,
        required: false,
      },
      isDeleted: {
        type: Boolean,
        default: false,
      },
    },
    {
      timestamps: true,
    }
  ],
  userName: {
    type: String,
    required: false,
    lowercase: true,
  },
  //isSeller: false,
  didRegistered: Number,
  webUser: {
    type: Boolean,
  },
  code: {
    type: String,
  },
  countryCode: {
    type: String
  },
  isSeller: {
    type: Boolean,
    default: false
  },
  isActive: {
    type: Boolean,
    default: true
  },
  fcmId: {
    type: String,
  },
  expireOtp: {
    type: Date,
  },
  settings: {
    calls: {
      callForStartPickUp: {
        type: Boolean,
        default: true
      },
      callForAttemptPickUp: {
        type: Boolean,
        default: true
      },
      callForStartDelivery: {
        type: Boolean,
        default: true
      },
      callForAttemptDelivery: {
        type: Boolean,
        default: true
      },
    },
    pushNotification: {
      notifyForStartPickUp: {
        type: Boolean,
        default: true
      },
      notifyForAttemptPickUp: {
        type: Boolean,
        default: true
      },
      notifyForStartDelivery: {
        type: Boolean,
        default: true
      },
      notifyForAttemptDelivery: {
        type: Boolean,
        default: true
      },
    },
    whatsapp: {
      whatsappNotifyForStartPickUp: {
        type: Boolean,
        default: true
      },
      whatsappNotifyForAttemptPickUp: {
        type: Boolean,
        default: true
      },
      whatsappNotifyForStartDelivery: {
        type: Boolean,
        default: true
      },
      whatsappNotifyForAttemptDelivery: {
        type: Boolean,
        default: true
      },
    },

  },
},
  {
    timestamps: true,
  });

module.exports = mongoose.model('Account', accountSchema);
