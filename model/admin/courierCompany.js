const mongoose = require('mongoose');

const courierCompanySchema = mongoose.Schema(
  {
    companyId: {
      type: String,
      required: true,
    },
    companyName: {
      type: String,
      required: true,
    },
    logo: {
      type: String,
      required: true,
    },
    code: {
      type: String,
      required: true,
    },
    mobile: {
      type: String,
      required: true,
    },
    countryCode: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },    
    webUrl: {
      type: String,
      required: true,
    },
    address: {
      type: String,
      required: true,
    }, 
    street: {
      type: String,
      required: true,
    },
    area: {
      type: String,
      required: true,
    },
    city: {
      type: String,
      required: true,
    },
    pinCode: {
      type: String,
      required: true,
    },
    country: {
      type: String,
      required: true,
    },
    isActive: {
      type: Boolean,
      default: true,
    },
    boxRate: [
        {
          boxSize:{type:mongoose.Types.ObjectId},
          from: { type: Number },
          upto: { type: Number },
          rate: { type: Number },
          driverCommission: { type: Number },
          total: { type: Number }
        }
    ],
    
    createdBy: {
      type: mongoose.Types.ObjectId,
    },
    updatedBy: {
      type: mongoose.Types.ObjectId,
    },
  },
  {
    timestamps: true,
  }
);

const couriercompany = mongoose.model('couriercompanies', courierCompanySchema);
module.exports = couriercompany;
