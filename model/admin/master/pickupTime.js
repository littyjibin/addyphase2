const mongoose = require("mongoose");

const pickupTimeSchema = mongoose.Schema(
    {
        fromTime: {
            type: String
        },
        toTime: {
            type: String
        },
        isActive: {
            type: Boolean,
            default: true,
        },
        createdBy: {
            type: mongoose.Types.ObjectId       
        },
        updatedBy: {
            type: mongoose.Types.ObjectId               
        },
    },
    {
        timestamps: true,
    }
);

const pickupTime = mongoose.model("pickuptimes", pickupTimeSchema);
module.exports = pickupTime;
