const mongoose = require("mongoose");

const packageSizeSchema = mongoose.Schema(
    {
        size: {
            type: String,
            trim:true,
            required: [true, "Size is missing"],
        },
        // dimension: {
        //     type: String,
        //     trim:true,
        //     required: [true, "Dimension is missing"],
        // },
        height: {
            type: Number,
            trim:true,
            required: [true, "Height is missing"],
        },
        width: {
            type: Number,
            trim:true,
            required: [true, "Width is missing"],
        }, 
        length: {
            type: Number,
            trim:true,
            required: [true, "Length is missing"],
        }, 
        weight: {
            type: Number,
            trim:true,
            required: [true, "Weight is missing"],
        },       
        isActive: {
            type: Boolean,
            default: true,
        },
        createdBy: {
            type: mongoose.Types.ObjectId       
        },
        updatedBy: {
            type: mongoose.Types.ObjectId               
        },
    },
    {
        timestamps: true,
    }
);

const packagesizes = mongoose.model("packagesizes", packageSizeSchema);
module.exports = packagesizes;
