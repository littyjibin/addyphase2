const mongoose = require('mongoose');

const colorSchema = mongoose.Schema(
  {
    colorName: {
      type: String,
      trim: true,
      required: [true, 'Color name is missing'],
    },
    colorCode: {
      type: String,
      trim: true,
      required: [true, 'Color code is missing'],
    },
    isActive: {
      type: Boolean,
      default: true,
    },
    createdBy: {
      type: mongoose.Types.ObjectId,
    },
    updatedBy: {
      type: mongoose.Types.ObjectId,
    },
  },
  {
    timestamps: true,
  }
);

const color = mongoose.model('colors', colorSchema);
module.exports = color;
