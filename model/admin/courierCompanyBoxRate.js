const mongoose = require('mongoose');

const courierCompanyBoxRteSchema = mongoose.Schema(
  {
    companyId: {
      type: mongoose.Types.ObjectId,
      required: true,
    }, 
    boxSize: {
        type:  mongoose.Types.ObjectId,
        required: true,
    }, 
    from: {
        type: Number,
        required: true,
    },
    upto: {
        type: Number,
        required: true,
    },  
    rate: {
        type: Number,
        required: true,
    },
    driverCommission: {
        type: Number,
        required: true,
    },
    total: {
        type: Number,
        required: true,
    },
    // boxSize: 
    //   {
    //     boxSizeId:mongoose.Types.ObjectId,
    //     boxSizeDetails: [
    //       {
    //         from: { type: Number},
    //         upTo: { type: Number},
    //         rate: { type: Number},
    //         driverCommission: { type: Number},
    //         total:{type:Number}
    //       }
    //     ]
    //   },
    
  },
  {
    timestamps: true,
  }
);

const courierCompanyBoxRate = mongoose.model('couriercompanyboxrates', courierCompanyBoxRteSchema);
module.exports = courierCompanyBoxRate;
