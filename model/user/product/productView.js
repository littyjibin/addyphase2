const mongoose = require('mongoose');

const productViewSchema = mongoose.Schema(
  {
    sellerId: {
      type: mongoose.Types.ObjectId,
      required: [true, 'Seller id is missing'],
    },
    productId: {
        type: mongoose.Types.ObjectId,
        required: [true, 'Product id is missing'],
    },
    userId: [
      mongoose.Types.ObjectId
    ],
    viewCount: {
        type: Number,
        required: [true, 'View count is missing'],
      },
    isActive: {
      type: Boolean,
      default: true,
    },
  },
  {
    timestamps: true,
  }
);

const productView = mongoose.model('productviews', productViewSchema);
module.exports = productView;
