const mongoose = require('mongoose');
const aggregatePaginate = require('mongoose-aggregate-paginate-v2');

const Schema = mongoose.Schema(
  {
    userId: {
      type: mongoose.Types.ObjectId,
      ref: 'accounts',
    },
    sellerId: {
      type: mongoose.Types.ObjectId,
      ref: 'accounts',
    },
    returnId: String,
    orderObjectId: mongoose.Types.ObjectId,
    orderId: String,
    returnReason:String,
    selectedDiverId: mongoose.Types.ObjectId,
    selectedCompanyId: mongoose.Types.ObjectId,
    paymentType: String,
    deliveryDate: Date,
    noOfItems: Number,
    returnItems: Number,
    products: Array,
    cartDetails: Object,
    userDetails: Object,
    status: {
      type: String,
      default: 'returnrequested',
      enum: [
        'returnRequested',
        'returnAccepted',
        'returnRejected',
        'returnCollected'
      ],
    },
    deliveredDate: Date,
    collectedDate: Date,
    submittedDate: Date,
    pickedUpDate: Date,
    canceledDate: Date,
    returnReason: {
      type: String,
      required: [true, 'Return reason is missing'],
    },
    refundableAmount: Number,
    deliveryAssignedDate: Date,
    refundedAmount: Number,
    isActive: {
      type: Boolean,
      default: true,
    },
  },
  {
    timestamps: true,
  }
);

Schema.plugin(aggregatePaginate);

const returnOrders = mongoose.model('orderReturns', Schema);

module.exports = returnOrders;
