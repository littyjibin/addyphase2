const mongoose = require('mongoose');

const cartSchema = mongoose.Schema(
  {
    sellerId: {
      type: mongoose.Types.ObjectId,
      required: [true, 'Seller id is missing'],
    },
    variantId: {
      type: mongoose.Types.ObjectId,
      required: [true, 'variant id is missing'],
    },
    productId: {
      type: mongoose.Types.ObjectId,
      required: [true, 'product id is missing'],
    },
    userId: {
      type: mongoose.Types.ObjectId,
      required: [true, 'user id is missing'],
    },
    instruction: {
        type: String
      },
    quantity: {
      type: Number,
    },
    variantDetails: {
      type: Object,
    },
    isActive: {
      type: Boolean,
      default: true,
    },
  },

  {
    timestamps: true,
  }
);

const cart = mongoose.model('cart', cartSchema);
module.exports = cart;
