const mongoose = require('mongoose');

const storeViewSchema = mongoose.Schema(
  {
    sellerId: {
      type: mongoose.Types.ObjectId,
      required: [true, 'Seller id is missing'],
    },
    userId: [
      mongoose.Types.ObjectId
    ],
    viewCount: {
        type: Number,
        required: [true, 'View count is missing'],
      },
    isActive: {
      type: Boolean,
      default: true,
    },
  },
  {
    timestamps: true,
  }
);

const storeView = mongoose.model('storeviews', storeViewSchema);
module.exports = storeView;
