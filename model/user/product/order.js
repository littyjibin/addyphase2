const mongoose = require('mongoose');

const orderSchema = mongoose.Schema(
  {
    userId: {
      type: mongoose.Types.ObjectId,
      required: [true, 'User id is missing'],
    },
    sellerId: {
      type: mongoose.Types.ObjectId,
      required: [true, 'Seller id is missing'],
    },
    sellerCountryCode: {
      type: String,
      required: [true, 'Seller country code is missing'],
    },
    vatPercentage: {
      type: String,
      required: [true, 'Vat percentage is missing'],
    },
    orderId: {
      type: String,
      required: [true, 'Order id is missing'],
    },
    paymentType: {
      type: String,
      required: [true, 'Payment type is missing'],
    },
    timeZone: {
      type: String,
      required: [true, 'Time zone is missing'],
    },
    expectedDeliveryDate: {
      type: Date,
      required: [true, 'Delivery date is missing'],
    },
    expectedDeliveryTime: {
      type: {
        fromTime: { type: String, required: [true, 'fromTime is missing'] },
        toTime: { type: String, required: [true, 'toTime missing'] },
        pickupTimeId: { type: mongoose.Types.ObjectId, required: [true, 'id is missing'] },
      },
      required: [true, 'expected delivery time details are missing'],
    },
    userDetails: {
      type: Object,
      required: [true, 'User Details is missing'],
    },
    orderStatus: {
      type: String,
      required: [true, 'User Details is missing'],
      enum: [
        'pending',
        'accepted',
        'rejected',
        'cancelled',
        'confirmed',
        'driverAssigned',
        'driverRejected',
        'companyAssigned',
        'companyRejected',
        'startPickup',
        'attemptPickup',
        'failedToPickup',
        'completePickup',
        'startDelivery',
        'attemptDelivery',
        'failedToDelivery',
        'delivered',
        'returnRequested',
      ],
    },
    delivered: {
      type: Boolean,
      default: false,
    },
    deliveryDate: {
      type: Date,
      required: [true, 'Delivery date is missing'],
    },
    products: {
      type: Array,
      required: [true, 'Products are missing'],
    },
    products: [
      {
        cartId: { type: mongoose.Types.ObjectId, required: [true, 'cartId is missing'] },
        variantId: { type: mongoose.Types.ObjectId, required: [true, 'variantId is missing'] },
        countryCode: { type: String, required: [true, 'countryCode is missing'] },
        productId: { type: mongoose.Types.ObjectId, required: [true, 'productId is missing'] },
        productName: { type: String, required: [true, 'productName is missing'] },
        image: { type: String, required: [true, 'image is missing'] },
        instruction: { type: String, default: '' },
        uom: { type: String, required: [true, 'uom is missing'] },
        uomId: { type: mongoose.Types.ObjectId, required: [true, 'uomId is missing'] },
        uomValue: { type: String, default: '' },
        color: { type: String, default: '' },
        colorCode: { type: String, default: '' },
        piece: { type: Number, required: [true, 'piece is missing'] }, //allowed purchase limit of product
        price: { type: Number, required: [true, 'price is missing'] },
        quantity: { type: Number, required: [true, 'quantity is missing'] }, //number of items added by user
        total: { type: Number, required: [true, 'total is missing'] },
        weight: { type: Number, required: [true, 'weight is missing'] },
        totalWeight: { type: Number, required: [true, 'totalWeight is missing'] },
        //  isActiveProduct: { type: Boolean, required: [true, 'isActiveProduct is missing'] },
        //  isActiveVariant: { type: Boolean, required: [true, 'isActiveVariant is missing'] },
        outOfStock: { type: Boolean, required: [true, 'outOfStock is missing'] },
        isDeliveryVerification: { type: Boolean, required: [true, 'isDeliveryVerification is missing'] },
      },
    ],
    cartDetails: {
      type: {
        distance: { type: Number, required: [true, 'distance is missing'] },
        totalWeight: { type: Number, required: [true, 'totalWeight is missing'] },
        itemTotal: { type: Number, required: [true, 'itemTotal is missing'] },
        deliveryFee: { type: Number, required: [true, 'deliveryFee is missing'] },
        vat: { type: Number, required: [true, 'Vat is missing'] },
        grandTotal: { type: Number, required: [true, 'grandTotal is missing'] },
        companyId: { type: mongoose.Types.ObjectId },
        driverId: { type: mongoose.Types.ObjectId },
        boxSizeId: { type: mongoose.Types.ObjectId },
        boxSizeName: String,
        commonBoxSizeId: { type: mongoose.Types.ObjectId },
        commonBoxSize: String,
        returnRefundAmount: { type: Number},
      },
      required: [true, 'Cart detils are missing'],
    },
    noOfItems: {
      type: Number,
      required: [true, 'Number of item is missing'],
    },
    isReturned: {
      type: Boolean,
      default: false,
    },
    isPhoto: {
      type: Boolean,
      default: false,
    },
    isActive: {
      type: Boolean,
      default: true,
    },
    pickupType: {
      type: String,
    },
    expectedPickupDate: {
      type: Date,
    },
    expectedPickupTime: {
      type: {
        fromTime: { type: String, required: [true, 'fromTime is missing'] },
        toTime: { type: String, required: [true, 'toTime missing'] },
        pickupTimeId: { type: mongoose.Types.ObjectId, required: [true, 'pick up time id is missing'] },
      },
    },
    pickupDate: {
      type: Date,
    },
    selectedCompanyId: {
      type: mongoose.Types.ObjectId,
    },
    selectedDriverId: {
      type: mongoose.Types.ObjectId,
    },
    packedProductImages: {
      type: Array,
    },
    paymentStatus: {
      type: String,
      enum: ['pending', 'completed', 'failed'],
    },
    isSellerChangedPaymentType: {
      type: Boolean,
      default: false,
    },
    isUserChangedPaymentType: {
      type: Boolean,
      default: false,
    },
    isDeliveryAssigned: {
      type: Boolean,
      default: false,
    },
    isDeliveryStarted: {
      type: Boolean,
      default: false,
    },
    isPickupStarted: {
      type: Boolean,
      default: false,
    },
    isPickupCompleted: {
      type: Boolean,
      default: false,
    },
    complaints: [
      {
        message: String,
        userId: { type: mongoose.Schema.Types.ObjectId, ref: 'Account' },
        createdAt: {
          type: Date,
          required: false,
        },
      },
    ],
  },
  {
    timestamps: true,
  }
);

const orders = mongoose.model('orders', orderSchema);
module.exports = orders;
