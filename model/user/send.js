const { string } = require("joi");
const mongoose = require("mongoose");

const sendSchema = mongoose.Schema(
    {
        approved: String,
        status: String,
        isApproved: Boolean,
        isRejected: Boolean,
        userId: { type: mongoose.Schema.Types.ObjectId, ref: "Account" },
        assigneeId: { type: mongoose.Schema.Types.ObjectId, ref: "Account" },
        sender: { type: mongoose.Schema.Types.ObjectId, ref: "Account" },
        collector: { type: mongoose.Schema.Types.ObjectId, ref: "Account" },
        details: {
            type: String,
            required: true,
        },
        photos: [String],
        deliveryType: String,
        driverId: { type: mongoose.Schema.Types.ObjectId, ref: "driver" },
        driverName: String,
        driverMobile: String,
        driverMobileCode: String,
        code: String,
        assigneeAddress: { type: mongoose.Schema.Types.ObjectId, ref: "Address" },
        selfAddress: { type: mongoose.Schema.Types.ObjectId, ref: "Address" },
        order_id: String,
        assignerName: String,
        toMobile: String,
        isDriverAssigned: {
            type: Boolean,
            default: false,
        },
        complaints: [
            {
                message: String,
                speaker: { type: mongoose.Schema.Types.ObjectId, ref: "Account" },
                createdAt: {
                    type: Date,
                    required: false,
                },
            },
        ],
        packageSizeId: { type: mongoose.Schema.Types.ObjectId, ref: "packagesizes" },
        pickupTime: {
            type: String,
            required: [true, "pickupTime is missing"],
        },
        day:{
            type: Date,
            required: true,
        },
        timeId:{
            type: mongoose.Schema.Types.ObjectId
        },
        needVan: {
            type: Boolean,
            default: true,
        },
        isActive: {
            type: Boolean,
            default: true,
        },
        selectedCompanyId: {
            type:  mongoose.Types.ObjectId,
          },
        deliveryFeePaidBy:String,
        deliveryFeePaidBy:String,
        isPacakgeAmountCollected:Boolean,
        priceOfPackage:Number,
        paymentMethodForDeliveryFee:String,
        paymentMethodForPackagePrice:String,
        estimatedDeliveryFee:Number,
        addyUser: {
            type: Boolean,
            default: true,   
        },
        paymentStatus: {
            type: String,
            enum: [
              'pending',
              'completed',
              'failed',
            ]
          },
          isSellerChangedDeliveryFeeType: {
            type: Boolean,
            default: false
          },
          isSellerChangedPackageAmountType: {
            type: Boolean,
            default: false
          },
          isUserChangedDeliveryFeeType: {
            type: Boolean,
            default: false
          },
          isUserChangedPackageAmountType: {
            type: Boolean,
            default: false
          },
        },
    
    {
        timestamps: true,
    }
);

const senders = mongoose.model("sendpackage", sendSchema);
module.exports = senders;
