const mongoose = require("mongoose");

const notificationsSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Types.ObjectId,
    required: [true, "notification userId missing"],
  },
  message: {
    type: String,
    required: [true, "notification message missing"],
    max: 50,
  },
  is_read: {
    type: Number,
    default: 0,
  },
  is_view: {
    type: Number,
    default: 0,
  },
  type: {
    type: String
  },
  createdAt: {
    type: Date,
    required: false,
  },
  updatedAt: {
    type: Date
  },
});

notificationsSchema.pre("save", function (next) {
  let now = new Date();
  if (!this.createdAt) {
    this.createdAt = now;
  }
  this.updatedAt = now.getTime();
  next();
});

module.exports = mongoose.model("notification", notificationsSchema);
