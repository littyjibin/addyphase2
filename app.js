const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const rateLimit = require('express-rate-limit');
const helmet = require('helmet');
const mongoSanitize = require('express-mongo-sanitize');
const xss = require('xss-clean');
const hpp = require('hpp');
const mongoose = require('mongoose');
const fileUpload = require('express-fileupload');
require('dotenv').config();

const globalErrorHandler = require('./errors/globalErrorHandler');
const sellerRoutes = require('./routes/seller/routes');
const adminRoutes = require('./routes/admin/routes');
const userRoutes = require('./routes/user/routes');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ limit: '50mb', extended: true }));


//file upload
app.use(fileUpload());

// Set security HTTP headers
app.use(helmet());

// Data sanitization against No SQL query injection
app.use(mongoSanitize());

// Data sanitization against XSS(clean user input from malicious HTML code)
app.use(xss());

// Prevent parameter pollution
app.use(hpp());

//cors
app.use(cors());

//morgan
app.use(morgan('dev'));

// limit each IP to 500 requests per 10 min
const limiter = rateLimit({
  windowMs: 10 * 60 * 1000,
  max: 5000,
});

app.use(limiter);

app.use('/seller', sellerRoutes);
app.use('/admin', adminRoutes);
app.use('/user', userRoutes);

//undefined routes
app.use('*', (req, res) => {
  res.status(500).json({
    status: false,
    message: 'Undefined route.',
  });
});

//global error handler
app.use(globalErrorHandler);

const PORT = process.env.PORT || 5000;

mongoose
  .connect(process.env.DB_CONNECTION_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then((_) => {
    app.listen(PORT, () => console.log(`server started at http://localhost:${PORT}`));
  })
  .catch((error) => {
    console.log(error);
  });

//un handled rejections
process.on('unhandledRejection', (error) => {
  console.log('UNHANDLED REJECTION!!!  shutting down ...');
  console.log(error.name, error.message);
  // server.close(() => {
  //   process.exit(1);
  // });
});
