const aws = require('aws-sdk');

const spacesEndpoint = new aws.Endpoint(process.env.SPACE_END_POINT);
const s3 = new aws.S3({
  endpoint: spacesEndpoint,
  accessKeyId: process.env.SPACE_ACCESS_KEY,
  secretAccessKey: process.env.SPACE_SECRET_ACCESS_KEY,
});

module.exports = s3;
