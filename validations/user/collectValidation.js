const Joi = require('joi');

const validateMobile = (data) => {
  const Schema = Joi.object({
    code: Joi.string().required().label('code'),
    mobile: Joi.string().required().label('mobile'),
  });
  return Schema.validate(data);
};

const validateSearchByUserName = (data) => {
  const Schema = Joi.object({
    userName: Joi.string().min(3).max(16).required().label('userName')
  });
  return Schema.validate(data);
};

const validateCollectDetails = (data) => {
  const Schema = Joi.object({
    toMobile: Joi.string().required().label('toMobile'),
    code: Joi.string().required().label('code'),
    package_details: Joi.string().required().label('package_details'),
    packageSizeId: Joi.string().label('packageSizeId'),
    needVan: Joi.boolean().label('needVan'),
    deliveryFeePaidBy: Joi.string().required().label('deliveryFeePaidBy'),
    paymentMethodForDeliveryFee: Joi.string().required().label('paymentMethodForDeliveryFee'),
    isPackageAmountCollected: Joi.boolean().label('isPackageAmountCollected'),
    priceOfPackage: Joi.number().label('priceOfPackage'),
    paymentMethodForPackagePrice: Joi.string().label('paymentMethodForPackagePrice'),
    pickupTime: Joi.string().required().label('pickupTime'),
    day: Joi.date().required().label('day'),
    timeId: Joi.string().required().label('timeId'),
    self_address_id:Joi.string().required().label('self_address_id'),
    collectForSomeoneElse: Joi.boolean().label('collectForSomeoneElse'),
    deliveryType: Joi.string().required().label('deliveryType'),
    sendItem:Joi.string().required().label('sendItem'), 
    collectForSomeoneElse: Joi.boolean().label('collectForSomeoneElse'),
    receiverMobile: Joi.number().label('receiverMobile'), 
    assigneeId: Joi.string().label('assigneeId'),
    driverName: Joi.string().label('driverName'),
    driverMobile: Joi.string().label('driverMobile'),
    driverMobileCode: Joi.string().label('driverMobileCode'),
    assigneeAddress:Joi.string().label('assigneeAddress'),
    selectedCompanyId: Joi.objectId().label('selectedCompanyId'),
    photos: Joi.array().label('photos'),
  });
  return Schema.validate(data);
};

const validatecollectOrderDetails = (data) => {
  const Schema = Joi.object({
    orderId: Joi.objectId()
      .required()
      .label("orderId")
      .messages({ "string.pattern.name": "Invalid orderId." }),
        lang: Joi.string().label('lang'),
  });

  return Schema.validate(data);
};

module.exports = {  validateMobile, validateSearchByUserName,validateCollectDetails,validatecollectOrderDetails };
