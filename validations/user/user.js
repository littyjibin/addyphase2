const Joi = require('joi');

const validateSignup = (data) => {
  const Schema = Joi.object({
    code: Joi.string().required().label('code'),
    countryCode: Joi.string().required().label('countryCode'),
    mobile: Joi.string().required().label('number'),
  });
  return Schema.validate(data);
};

const validateAddress = (data) => {
  const Schema = Joi.object({
    address:Joi.string().required().label('address'),
    houseNumber:Joi.string().required().label('houseNumber'),
    street:Joi.string().required().label('street'),
    area:Joi.string().required().label('area'),
    buildingName: Joi.string().label('buildingName'),
    city:Joi.string().required().label('city'),
    pinCode:Joi.number().required().label('pinCode'),
    latitude:Joi.number().required().label('latitude'),
    longitude:Joi.number().required().label('longitude')
  });
  return Schema.validate(data);
};

const validateAddressweb = (data) => {
  const Schema = Joi.object({
    address:Joi.string().required().label('address'),
    houseNumber:Joi.string().required().label('houseNumber'),
    street:Joi.string().required().label('street'),
    area:Joi.string().required().label('area'),
    buildingName: Joi.string().label('buildingName'),
    city:Joi.string().required().label('city'),
    pinCode:Joi.number().required().label('pinCode'),
    latitude:Joi.number().required().label('latitude'),
    longitude:Joi.number().required().label('longitude'),
    userId:Joi.string().required().label('userId'),
  });
  return Schema.validate(data);
};



const validateEditAddress = (data) => {
  const Schema = Joi.object({
    address: Joi.string().required().label('address'),
    houseNumber: Joi.string().required().label('houseNumber'),
    street: Joi.string().required().label('street'),
    area: Joi.string().required().label('area'),
    city: Joi.string().required().label('city'),
    buildingName: Joi.string().label('buildingName'),
    pinCode: Joi.number().required().label('pinCode'),
    latitude: Joi.number().required().label('latitude'),
    longitude: Joi.number().required().label('longitude'),
    id: Joi.string().required().label('id'),
    
  });
  return Schema.validate(data);
};

const validateUserName = (data) => {
  const Schema = Joi.object({
    userName: Joi.string().required().label('userName') 
  });
  return Schema.validate(data);
};


const validateUserNameSpecialChar = (data) => {
  const Schema = Joi.object({
    
    userName: Joi.string().min(3).max(16).required().label('userName') 
  });
  return Schema.validate(data);
};

const validateProfileUpdate = (data) => {
  const Schema = Joi.object({
    logo:Joi.string().label('logo'),
    firstName:Joi.string().label('firstName'),
    lastName:Joi.string().label('lastName'),
    email:Joi.string().label('email'),
    code: Joi.string().label('code'),
    mobile: Joi.string().label('mobile'),
    
  });
  return Schema.validate(data);
};

const validateSettingsUpdate = (data) => {
  const Schema = Joi.object({
    callForStartPickUp:Joi.string().required().label('callForStartPickUp'),
    callForAttemptPickUp:Joi.string().required().label('callForAttemptPickUp'),
    callForStartDelivery:Joi.string().required().label('callForStartDelivery'),
    callForAttemptDelivery:Joi.string().required().label('callForAttemptDelivery'),
    notifyForStartPickUp: Joi.string().required().label('notifyForStartPickUp'),
    notifyForAttemptPickUp: Joi.string().required().label('notifyForAttemptPickUp'),
    notifyForStartDelivery:Joi.string().required().label('notifyForStartDelivery'),
    notifyForAttemptDelivery:Joi.string().required().label('notifyForAttemptDelivery'),
    whatsappNotifyForStartPickUp:Joi.string().required().label('whatsappNotifyForStartPickUp'),
    whatsappNotifyForAttemptPickUp:Joi.string().required().label('whatsappNotifyForAttemptPickUp'),
    whatsappNotifyForStartDelivery: Joi.string().required().label('whatsappNotifyForStartDelivery'),
    whatsappNotifyForAttemptDelivery: Joi.string().required().label('whatsappNotifyForAttemptDelivery'),
    
  });
  return Schema.validate(data);
};

module.exports = { validateSettingsUpdate,validateProfileUpdate,validateSignup,
  validateAddressweb,validateAddress, validateEditAddress, validateUserName,validateUserNameSpecialChar };
