const Joi = require('joi');
Joi.objectId = require("joi-objectid")(Joi);

const validateCart = (data) => {
    const Schema = Joi.object({
        sellerId: Joi.objectId()
        .required()
        .label("Seller")
        .messages({ "string.pattern.name": "Invalid seller ID." }),
      productId: Joi.objectId()
        .required()
        .label("Product")
        .messages({ "string.pattern.name": "Invalid product ID." }),
      variantId: Joi.objectId().allow(null, '')
        .label("Variant")
        .messages({ "string.pattern.name": "Invalid variant ID." }),
      quantity: Joi.number().required().label("Quantity"),
      instruction: Joi.string().allow(null, '').label("instruction")
    });
  
    return Schema.validate(data);
  };
  const validateDeleteCartItem = (data) => {
    const Schema = Joi.object({
      cartId: Joi.objectId()
        .required()
        .label("Cart ID")
        .messages({ "string.pattern.name": "Invalid Cart ID." }),
    });
    return Schema.validate(data);
  };

  const validateUpdateCartItem = (data) => {
    const Schema = Joi.object({
      cartId: Joi.objectId()
        .required()
        .label("Cart ID")
        .messages({ "string.pattern.name": "Invalid Cart ID." }),
      quantity: Joi.number().required().label("Quantity"),
    });
    return Schema.validate(data);
  };
  const validateUpdateCartInstruction = (data) => {
    const Schema = Joi.object({
      cartId: Joi.objectId()
        .required()
        .label("Cart ID")
        .messages({ "string.pattern.name": "Invalid Cart ID." }),
      instruction: Joi.string().required().label("instruction"),
    });
    return Schema.validate(data);
  };
  const validateGetCart = (data) => {
    const Schema = Joi.object({
      sellerId: Joi.objectId()
        .required()
        .label("Seller Id")
        .messages({ "string.pattern.name": "Invalid Seller Id." }),
    });
    return Schema.validate(data);
  };




module.exports = {
    validateCart,
    validateDeleteCartItem,
    validateUpdateCartItem,
    validateGetCart,
    validateUpdateCartInstruction
};
