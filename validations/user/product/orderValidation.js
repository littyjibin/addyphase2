const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

const validateOrderReview = (data) => {
  const Schema = Joi.object({
    sellerId: Joi.objectId().required().label('Seller Id').messages({ 'string.pattern.name': 'Invalid Seller Id.' }),
    addressId: Joi.objectId().required().label('Address Id').messages({ 'string.pattern.name': 'Invalid Address Id.' }),
    distance: Joi.number().required().label('distance'),
  });
  return Schema.validate(data);
};

const validatePlaceOrder = (data) => {
  const Schema = Joi.object({
    sellerId: Joi.objectId().required().label('Seller').messages({ 'string.pattern.name': 'Invalid seller ID.' }),
    addressId: Joi.objectId().required().label('Address').messages({ 'string.pattern.name': 'Invalid address ID.' }),
    cartDetails: Joi.object()
      .keys({
        distance: Joi.number().required().label('distance'),
        totalWeight: Joi.number().required().label('totalWeight'),
        itemTotal: Joi.number().required().label('itemTotal'),
        deliveryFee: Joi.number().required().label('deliveryFee'),
        vat: Joi.number().required().label('vat'),
        grandTotal: Joi.number().required().label('grandTotal'),
        companyId: Joi.objectId().allow(null, '').label('companyId'),
        driverId: Joi.objectId().allow(null, '').label('driverId'),
        boxSizeId: Joi.objectId().label('boxSizeId').messages({ 'string.pattern.name': 'Invalid box size id.' }),
        boxSizeName: Joi.string().label('boxSizeName'),
      })
      .required()
      .label('Cart Detail'),
    paymentType: Joi.string().required().valid('cod', 'online').label('paymentType'),
    isPhoto: Joi.boolean().required().label('isPhoto'),
    expectedDeliveryDate: Joi.string().required().label('expectedDeliveryDate'),
    expectedDeliveryTime: Joi.object()
      .keys({
        pickupTimeId: Joi.objectId().required().label('pickupTimeId'),
        fromTime: Joi.string().required().label('fromTime'),
        toTime: Joi.string().required().label('toTime'),
      })
      .required()
      .label('expectedDeliveryTime'),
      timeZone: Joi.string().required().label('timeZone'),
  });

  return Schema.validate(data);
};

const validateGetUserOrderById = (data) => {
  const Schema = Joi.object({
    id: Joi.objectId().required().label('Id').messages({ 'string.pattern.name': 'Invalid Id.' })
  });
  return Schema.validate(data);
};

const validateRescheduleDeliveryDate = (data) => {
  const Schema = Joi.object({
    id: Joi.objectId().required().label('id').messages({ 'string.pattern.name': 'Invalid id.' }),
    expectedDeliveryDate: Joi.string().required().label('expectedDeliveryDate'),
    expectedDeliveryTime: Joi.object()
    .keys({
      pickupTimeId: Joi.objectId().required().label('pickupTimeId'),
      fromTime: Joi.string().required().label('fromTime'),
      toTime: Joi.string().required().label('toTime'),
    })
    .required()
    .label('expectedDeliveryTime1')
    // timeZone: Joi.string().required().label('timezone'),
  });
  return Schema.validate(data);
};


const validateCancelOrder = (data) => {
  const Schema = Joi.object({
    id: Joi.objectId().required().label('id').messages({ 'string.pattern.name': 'Invalid id.' })
  });
  return Schema.validate(data);
};


const validateRaiseComplaints = (data) => {
  const Schema = Joi.object({
    id: Joi.objectId().required().label('id').messages({ 'string.pattern.name': 'Invalid id.' }),
    message: Joi.string().required().label('message'),
  });
  return Schema.validate(data);
};

const validateReturnOrder = (data) => {
  const Schema = Joi.object({
    id: Joi.objectId().required().label('id').messages({ 'string.pattern.name': 'Invalid id.' }),
    returnReason: Joi.string().required().label('returnReason'),
    products: Joi.array().items(
      Joi.object().keys({
        productId: Joi.objectId().required().label('productId').messages({ 'string.pattern.name': 'Invalid product id.' }),
        variantId: Joi.objectId().required().label('variantId').messages({ 'string.pattern.name': 'Invalid variant id.' }),
      })
    ).required()
  });
  return Schema.validate(data);
};


module.exports = {
  validateOrderReview,
  validatePlaceOrder,
  validateGetUserOrderById,
  validateRescheduleDeliveryDate,
  validateCancelOrder,
  validateRaiseComplaints,
  validateReturnOrder
};
