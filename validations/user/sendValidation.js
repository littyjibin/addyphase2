const Joi = require('joi');
Joi.objectId = require("joi-objectid")(Joi);

const validateMobile = (data) => {
  const Schema = Joi.object({
    code: Joi.string().required().label('code'),
    mobile: Joi.string().required().label('mobile'),
  });
  return Schema.validate(data);
};
const validateSendDetails = (data) => {
  const Schema = Joi.object({
    toMobile: Joi.string().required().label('toMobile'),
    code: Joi.string().required().label('code'),
    package_details: Joi.string().required().label('package_details'),
    packageSizeId: Joi.string().label('packageSizeId'),
    needVan: Joi.boolean().label('needVan'),
    deliveryFeePaidBy: Joi.string().required().label('deliveryFeePaidBy'),
    paymentMethodForDeliveryFee: Joi.string().required().label('paymentMethodForDeliveryFee'),
    isPackageAmountCollected: Joi.boolean().label('isPackageAmountCollected'),
    priceOfPackage: Joi.number().label('priceOfPackage'),
    paymentMethodForPackagePrice: Joi.string().label('paymentMethodForPackagePrice'),
    pickupTime: Joi.string().required().label('pickupTime'),
    day: Joi.date().required().label('day'),
    timeId: Joi.string().required().label('timeId'),
    self_address_id: Joi.string().required().label('self_address_id'),
    deliveryType: Joi.string().required().label('deliveryType'),
    assigneeId: Joi.string().label('assigneeId'),
    driverName: Joi.string().label('driverName'),
    driverMobile: Joi.string().label('driverMobile'),
    driverMobileCode: Joi.string().label('driverMobileCode'),
    assigneeAddress: Joi.string().label('assigneeAddress'),
    selectedCompanyId: Joi.objectId().label('selectedCompanyId'),
    photos: Joi.array().label('photos'),
  });
  return Schema.validate(data);
};

const validateSearchByUserName = (data) => {
  const Schema = Joi.object({
    userName: Joi.string().min(3).max(16).required().label('userName')
  });
  return Schema.validate(data);
};

const checkLink = (data) => {
  const Schema = Joi.object({
    orderId: Joi.objectId()
      .required()
      .label("orderId")
      .messages({ "string.pattern.name": "Invalid orderId." }),
  });

  return Schema.validate(data);
};

const validateOrderDetails = (data) => {
  const Schema = Joi.object({
    orderId: Joi.objectId()
      .required()
      .label("orderId")
      .messages({ "string.pattern.name": "Invalid orderId." }),
        lang: Joi.string().label('lang'),
  });

  return Schema.validate(data);
};



const validateAddComplaint = (data) => {
  const Schema = Joi.object({
    orderId: Joi.objectId()
      .required()
      .label("orderId")
      .messages({ "string.pattern.name": "Invalid orderId." }),
    message: Joi.string().required().label('message'),
  });
  return Schema.validate(data);
};


const validateApprovePackageWebOfExistingUser = (data) => {
  const Schema = Joi.object({
    orderId: Joi.objectId()
      .required()
      .label("orderId")
      .messages({ "string.pattern.name": "Invalid orderId." }),
    addressId: Joi.objectId(),
  });

  return Schema.validate(data);
};
const validateApprovePackageWeb = (data) => {
  const Schema = Joi.object({
    orderId: Joi.objectId()
      .required()
      .label("orderId")
      .messages({ "string.pattern.name": "Invalid orderId." }),
    addressId: Joi.string()
      .allow("", null)
  });

  return Schema.validate(data);
};
var addressSchema = Joi.object()
  .keys({
    address: Joi.string()
      .required()
      .label("address")
      .messages({ "string.pattern.name": "Invalid address" }),
    house_no: Joi.string()
      .required()
      .label("house_no")
      .messages({ "string.pattern.name": "Invalid house_no" }),
      building_name: Joi.string()
      .required()
      .label("building_name")
      .messages({ "string.pattern.name": "Invalid building_name" }),
    street: Joi.string()
      .label("street")
      .messages({ "string.pattern.name": "Invalid street" }),
    area: Joi.string()
      .label("area")
      .messages({ "string.pattern.name": "Invalid area" }),
    city: Joi.string()
      .required()
      .label("city")
      .messages({ "string.pattern.name": "Invalid city" }),
  })
  .required();

// var addressSchema = Joi.object()
//   .keys({
//     address: Joi.string()
//       .required()
//       .label("address")
//       .messages({ "string.pattern.name": "Invalid address" }),
//     buildingName: Joi.string()
//       .required()
//       .label("buildingName")
//       .messages({ "string.pattern.name": "buildingName required" }),
//     house_no: Joi.string()
//       .required()
//       .label("house_no")
//       .messages({ "string.pattern.name": "Invalid house_no" }),
//     street: Joi.string()
//       .required()
//       .label("street")
//       .messages({ "string.pattern.name": "Invalid street" }),
//     area: Joi.string()
//       .required()
//       .label("area")
//       .messages({ "string.pattern.name": "Invalid area" }),
//     city: Joi.string()
//       .required()
//       .label("city")
//       .messages({ "string.pattern.name": "Invalid city" }),
//   })
//   .required();




  const validateApprovePackage = (data) => {
    const Schema = Joi.object({
      order_id: Joi.objectId()
        .required()
        .label("orderId")
        .messages({ "string.pattern.name": "Invalid order_id." }),
      firstName: Joi.string()
        .required()
        .label("firstName")
        .messages({ "string.pattern.name": "Invalid first name." }),
      lastName: Joi.string()
        .required()
        .label("lastName")
        .messages({ "string.pattern.name": "Invalid last name." }),
      email: Joi.string().allow("", null),
      latitude: Joi.string()
        .required()
        .label("latitude")
        .messages({ "string.pattern.name": "Invalid latitude" }),
      longitude: Joi.string()
        .required()
        .label("longitude")
        .messages({ "string.pattern.name": "Invalid longitude" }),
      mobile: Joi.string()
        .required()
        .label("mobile")
        .messages({ "string.pattern.name": "Invalid mobile." }),
        approved: Joi.string().required()
        .label("approved")
        .messages({ "string.pattern.name": "Invalid approved value." }),
      deliveryInstruction: Joi.string().allow("", null),
      addressId: Joi.string()
        .allow("", null)
    });
  
    return Schema.validate(data);
  };

  const validateCancelOrder = (data) => {
    const Schema = Joi.object({
      id: Joi.objectId().required().label('id').messages({ 'string.pattern.name': 'Invalid id.' })
    });
    return Schema.validate(data);
  };
  

  const validateregister = (data) => {
    const Schema = Joi.object({
      orderId: Joi.objectId()
      .required()
      .label("orderId")
      .messages({ "string.pattern.name": "Invalid order_id." }),
      code: Joi.string().required().label('code'),
      firstName: Joi.string()
        .required()
        .label("firstName")
        .messages({ "string.pattern.name": "Invalid first name." }),
      lastName: Joi.string()
        .required()
        .label("lastName")
        .messages({ "string.pattern.name": "Invalid last name." }),
      email: Joi.string().allow("", null),
      latitude: Joi.string()
        .required()
        .label("latitude")
        .messages({ "string.pattern.name": "Invalid latitude" }),
      longitude: Joi.string()
        .required()
        .label("longitude")
        .messages({ "string.pattern.name": "Invalid longitude" }),
      mobile: Joi.string()
        .required()
        .label("mobile")
        .messages({ "string.pattern.name": "Invalid mobile." }),
      deliveryInstruction: Joi.string().allow("", null),
      address:addressSchema
    });
  
    return Schema.validate(data);
  };

module.exports = { validateMobile, validateSendDetails, 
  validateSearchByUserName, checkLink, 
  validateApprovePackageWebOfExistingUser, 
  validateApprovePackageWeb,
  validateApprovePackage,
  validateOrderDetails,validateAddComplaint,validateCancelOrder,validateregister };
