const Joi = require('joi');

const validateCourierCompany = (data) => {
  const Schema = Joi.object({
    companyName: Joi.string().required().label('companyName'),
    countryCode: Joi.string().required().label('countryCode'),
    description: Joi.string().required().label('description'),
    code: Joi.string().required().label('code'),
    mobile: Joi.string().required().label('mobile'),
    webUrl: Joi.string().required().label('webUrl'),
    address: Joi.string().required().label('address'),
    street: Joi.string().required().label('street'),
    area: Joi.string().required().label('area'),
    city: Joi.string().required().label('city'),
    pinCode: Joi.string().required().label('pinCode'),
    country: Joi.string().required().label('country'),
    logo: Joi.string().required().label('logo'),
    boxRate: Joi.array().items(
      Joi.object().keys({
        boxSize: Joi.string().required(),
        from: Joi.number().required(),
        upto: Joi.number().required(),
        rate: Joi.number().required(),
        driverCommission: Joi.number().required()
      })
    ),
  });
  return Schema.validate(data);
};

const validateCourierCompanyBoxRate = (data) => {
  const Schema = Joi.object({
    companyId: Joi.objectId()
    .required()
    .label("Company id")
    .messages({ "string.pattern.name": "Invalid Company id." }),
    boxSize: Joi.objectId()
    .required()
    .label("Box Size")
    .messages({ "string.pattern.name": "Invalid boxSize." }),
    from: Joi.number().integer().required().label('from'),
    upto: Joi.number().integer().positive().required().label('upto'),
    rate: Joi.number().integer().positive().required().label('rate'), 
    driverCommission: Joi.number().integer().positive().required().label('driverCommission'), 
    // boxSizeId: Joi.array().items(
    //   Joi.object().keys({
    //     from: Joi.number().integer().positive().required().label('from'),
    //     upto: Joi.number().integer().positive().required().label('upto'),
    //     rate: Joi.number().integer().positive().required().label('rate'), 
    //     driverCommission: Joi.number().integer().positive().required().label('driverCommission'), 
    //   })
    // ), 
  
  });
  return Schema.validate(data);
};




module.exports = { validateCourierCompany,validateCourierCompanyBoxRate};
