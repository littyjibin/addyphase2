const Joi = require('joi');

const validatePackageSize = (data) => {
  const Schema = Joi.object({
    size: Joi.string().required().label('size'),
  //  dimension: Joi.string().required().label('dimension'),
    height: Joi.string().required().label('height'),
    width: Joi.string().required().label('width'),
    length: Joi.string().required().label('length'),
    weight: Joi.string().required().label('weight'),
  });
  return Schema.validate(data);
};

const validatePickupTime = (data) => {
  const Schema = Joi.object({
    fromTime: Joi.string().required().label('fromTime'),
    toTime: Joi.string().required().label('toTime')
  });
  return Schema.validate(data);
};


module.exports = { validatePackageSize ,validatePickupTime};
