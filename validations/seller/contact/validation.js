const Joi = require('joi');

const validateSellerContact = (data) => {
  const Schema = Joi.object({
    userName: Joi.string().required().label('userName'),
    phone: Joi.array().required().label('phone'),
    logo: Joi.string().allow('').allow(null).label('logo'),
  });
  return Schema.validate(data);
};

module.exports = { validateSellerContact };
