const Joi = require('joi');

const validateDriver = (data) => {
  const Schema = Joi.object({
    name: Joi.string().required().label('name'),
    code: Joi.string().required().label('code'),
    mobile: Joi.string().required().label('mobile'),
    profilePic: Joi.string()
  });
  return Schema.validate(data);
};

const validateDriverByNumber = (data) => {
  const Schema = Joi.object({
    name: Joi.string().label('name'),
    code: Joi.string().required().label('code'),
    mobile: Joi.string().required().label('mobile'),
    profilePic: Joi.string()
  });
  return Schema.validate(data);
};

const validateDriverLogin = (data) => {
  const Schema = Joi.object({
    code: Joi.string().required().label('code'),
    mobile: Joi.string().required().label('mobile'),
    profilePic: Joi.string()
  });
  return Schema.validate(data);
};

validateOrderDetails = (data) => {
  const Schema = Joi.object({
    orderId: Joi.objectId()
      .required()
      .label("orderId")
      .messages({ "string.pattern.name": "Invalid orderId." }),
  });
  return Schema.validate(data);
}

validateStatusUpdate = (data) => {
  const Schema = Joi.object({
    orderId: Joi.objectId().required().label("orderId").messages({ "string.pattern.name": "Invalid orderId." }),
    status: Joi.string().required().label('status'),
  });
  return Schema.validate(data);
}

validateStatusList = (data) => {
  const Schema = Joi.object({
    orderId: Joi.string().required().label('orderId'),
  });
  return Schema.validate(data);
}

validateEstimatedDeliveryFee = (data) => {
  const Schema = Joi.object({
    orderId: Joi.objectId().required().label("orderId").messages({ "string.pattern.name": "Invalid orderId." }),
    estimatedDeliveryFee: Joi.number().required().label('estimatedDeliveryFee'),
  });
  return Schema.validate(data);
}
module.exports = { validateDriver, validateDriverByNumber, validateDriverLogin, validateOrderDetails, validateStatusUpdate,validateStatusList,validateEstimatedDeliveryFee };
