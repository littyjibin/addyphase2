const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

const validateSellerProduct = (data) => {
  const Schema = Joi.object({
    id: Joi.string().label('id'),
    isActiveProduct: Joi.boolean().label('isActiveProduct'),
    productName: Joi.string().required().label('productName'),
    description: Joi.string().required().label('description'),
    countryCode: Joi.string().required().label('countryCode'),
    taggedCategories: Joi.array().required().label('taggedCategories'),
    images: Joi.array().items(
      Joi.object().keys({
        imageUrl: Joi.string().required(),
        isDefault: Joi.boolean().required(),
      })
    ),
    uom: Joi.string().required().label('uom'),
    piece: Joi.number().required().label('piece'),
    height: Joi.number().required().label('height'),
    width: Joi.number().required().label('width'),
    length: Joi.number().required().label('length'),
    weight: Joi.number().required().label('weight'),
    basicSellingPrice: Joi.number().integer().positive().required().label('basicSellingPrice'),
    basicCostPrice: Joi.number()
      .integer()
      .positive()
      .min(Joi.ref('basicSellingPrice'))
      .required()
      .label('basicCostPrice'),
    isDeliveryVerification: Joi.boolean().required().label('isDeliveryVerification'),
    variants: Joi.array().items(
      Joi.object().keys({
        uomValue: Joi.string().required(),
        sellingPrice: Joi.number().integer().positive().required().label('sellingPrice'),
        costPrice: Joi.number().integer().positive().min(Joi.ref('sellingPrice')).required().label('costPrice'),
        colorName: Joi.string().label('colorName'),
        colorCode: Joi.string().label('colorCode'),
        images: Joi.array().items(
          Joi.object().keys({
            imageUrl: Joi.string().required(),
            isDefault: Joi.boolean().required(),
          })
        ),
      })
    ),
  });
  return Schema.validate(data);
};
const validateUpdateSellerProduct = (data) => {
  const Schema = Joi.object({
    id: Joi.objectId().required().label('id1').messages({ 'string.pattern.name': 'Invalid id.' }),
    isActiveProduct: Joi.boolean().label('isActiveProduct'),
    productName: Joi.string().required().label('productName'),
    type: Joi.string().required().valid('manual', 'instagram').label('type'),
    defaultImage: Joi.string().required().label('defaultImage'),
    description: Joi.string().required().label('description'),
    countryCode: Joi.string().required().label('countryCode'),
    taggedCategories: Joi.array().required().label('taggedCategories'),
    images: Joi.array().items(
      Joi.object().keys({
        // _id: Joi.string().allow('').allow(null).label('image id'),
        // _id: Joi.objectId().allow('').allow(null).label('id').messages({ 'string.pattern.name': 'Invalid id.' }),
        imageUrl: Joi.string().required(),
        isDefault: Joi.boolean().required(),
      })
    ),
    uom: Joi.string().required().label('uom'),
    piece: Joi.number().label('piece'),
    height: Joi.number().label('height'),
    width: Joi.number().label('width'),
    length: Joi.number().label('length'),
    weight: Joi.number().label('weight'),
    basicSellingPrice: Joi.number().integer().positive().label('basicSellingPrice'),
    basicCostPrice: Joi.number().integer().positive().min(Joi.ref('basicSellingPrice')).label('basicCostPrice'),
    isDeliveryVerification: Joi.boolean().required().label('isDeliveryVerification'),
    variants: Joi.array().items(
      Joi.object().keys({
        uomValue: Joi.string().required(),
        sellingPrice: Joi.number().integer().positive().required().label('sellingPrice'),
        costPrice: Joi.number().integer().positive().min(Joi.ref('sellingPrice')).required().label('costPrice'),
        colorName: Joi.string().label('colorName'),
        colorCode: Joi.string().label('colorCode'),
        images: Joi.array().items(
          Joi.object().keys({
            // _id: Joi.objectId().label('id').messages({ 'string.pattern.name': 'Invalid id.' }),
            imageUrl: Joi.string().required(),
            isDefault: Joi.boolean().required(),
          })
        ),
      })
    ),
  });
  return Schema.validate(data);
};

const validateProductId = (data) => {
  const Schema = Joi.object({
    id: Joi.objectId().required().label('id').messages({ 'string.pattern.name': 'Invalid id.' }),
  });
  return Schema.validate(data);
};

const validateSellerMultipleProduct = (data) => {
  const Schema = Joi.array().items(
    Joi.object().keys({
      type: Joi.string().required().valid('manual', 'instagram').label('type'),
      isActiveProduct: Joi.boolean().label('isActiveProduct'),
      productName: Joi.string().required().label('productName'),
      defaultImage: Joi.string().required().label('defaultImage'),
      description: Joi.string().required().label('description'),
      countryCode: Joi.string().required().label('countryCode'),
      taggedCategories: Joi.array().required().label('taggedCategories'),
      images: Joi.array().items(
        Joi.object().keys({
          imageUrl: Joi.string().required(),
          isDefault: Joi.boolean().required(),
        })
      ),
      uom: Joi.string().required().label('uom'),
      piece: Joi.number().required().label('piece'),
      height: Joi.number().required().label('height'),
      width: Joi.number().required().label('width'),
      length: Joi.number().required().label('length'),
      weight: Joi.number().required().label('weight'),
      basicSellingPrice: Joi.number().integer().positive().required().label('basicSellingPrice'),
      basicCostPrice: Joi.number().integer().positive().required().label('basicCostPrice'),
      // basicCostPrice: Joi.number()
      //   .integer()
      //   .positive()
      //   .min(Joi.ref('basicSellingPrice'))
      //   .required()
      //   .label('basicCostPrice'),
      isDeliveryVerification: Joi.boolean().required().label('isDeliveryVerification'),
      variants: Joi.array().items(
        Joi.object().keys({
          uomValue: Joi.string().required(),
          sellingPrice: Joi.number().integer().positive().required().label('sellingPrice'),
          costPrice: Joi.number().integer().positive().required().label('costPrice'),
          // costPrice: Joi.number().integer().positive().min(Joi.ref('sellingPrice')).required().label('costPrice'),
          colorName: Joi.string().label('colorName'),
          colorCode: Joi.string().label('colorCode'),
          images: Joi.array().items(
            Joi.object().keys({
              imageUrl: Joi.string().required(),
              isDefault: Joi.boolean().required(),
            })
          ),
        })
      ),
    })
  );

  return Schema.validate(data);
};

const validateSellerInstagramImageDetails = (data) => {
  const Schema = Joi.array().items(
    Joi.object().keys({
      mediaType: Joi.string().required().label('mediaType'),
      mediaCaption: Joi.string().label('mediaCaption'),
      imageDetails: Joi.array().items(
        Joi.object().keys({
          url: Joi.string().required(),
          mediaId: Joi.string().required(),
        })
      ),
    })
  );
  return Schema.validate(data);
};

module.exports = {
  validateSellerProduct,
  validateSellerMultipleProduct,
  validateUpdateSellerProduct,
  validateProductId,
  validateSellerInstagramImageDetails,
};
