
const checkProductImageValidation = (req, res, next) => {
    if (req?.files?.image) {
      if (
        req.files.image.mimetype == 'image/png' ||
        req.files.image.mimetype == 'image/jpeg' ||
        req.files.image.mimetype == 'image/svg+xml' ||
        req.files.image.mimetype == 'image/jpg' ||
        req.files.image.mimetype == 'image/gif'
      ) {
     
      } else {
        return res.status(400).json({
          status: false,
          message: ' Image file type is not supported check with another file type. ',
        });
      }
    }
    next();
  };

  module.exports={checkProductImageValidation}