const Joi = require('joi');

const validateSellerDriverLogin = (data) => {
  const Schema = Joi.object({
    code: Joi.string().required().label('code'),
    mobile: Joi.string().required().label('mobile'),
    orderId: Joi.objectId().required().label('orderId').messages({ 'string.pattern.name': 'Invalid order id.' }),
  });
  return Schema.validate(data);
};

validateDriverLink = (data) => {
  const Schema = Joi.object({
    orderId: Joi.objectId().required().label('order id').messages({ 'string.pattern.name': 'Invalid order id.' }),
  });
  return Schema.validate(data);
};

validateStatusList = (data) => {
  const Schema = Joi.object({
    orderId: Joi.objectId().required().label('orderId').messages({ 'string.pattern.name': 'Invalid orderId.' }),
  });
  return Schema.validate(data);
};

validateOrderDetails = (data) => {
  const Schema = Joi.object({
    orderId: Joi.objectId().required().label('orderId').messages({ 'string.pattern.name': 'Invalid orderId.' }),
  });
  return Schema.validate(data);
};

validateSellerDriverStatusUpdate = (data) => {
  const Schema = Joi.object({
    orderId: Joi.objectId().required().label('orderId').messages({ 'string.pattern.name': 'Invalid orderId.' }),
    status: Joi.string().required().label('status'),
    status: Joi.string()
      .required()
      .valid(
        'driverAssigned',
        'driverRejected',
        'startPickup',
        'attemptPickup',
        'failedToPickup',
        'completePickup',
        'startDelivery',
        'attemptDelivery',
        'failedToDelivery',
        'delivered'
      )
      .label('status'),
  });
  return Schema.validate(data);
};

module.exports = {
  validateSellerDriverLogin,
  validateDriverLink,
  validateStatusList,
  validateOrderDetails,
  validateSellerDriverStatusUpdate,
};
