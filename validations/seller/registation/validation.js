const Joi = require('joi');

const validateSellerSellingType = (data) => {
  const Schema = Joi.object({
    categoryId: Joi.string().required().label('categoryId'),
  });
  return Schema.validate(data);
};

const validateSellerBusinessDetails = (data) => {
  const Schema = Joi.object({
    logo: Joi.string().required().label('logo'),
    businessName: Joi.string().required().label('businessName'),
    storeUrl: Joi.string().required().label('storeUrl'),
    // colorId: Joi.string().required().label('colorId')
    colorName: Joi.string().required().label('colorName'),
    colorCode: Joi.string().required().label('colorCode'),
  });
  return Schema.validate(data);
};

const validateSellerPackageDetails = (data) => {
  const Schema = Joi.object({
    packageId: Joi.string().required().label('packageId'),
    packageDuration: Joi.string().required().label('packageDuration'),
  });
  return Schema.validate(data);
};

const validateSellerSignup = (data) => {
  const Schema = Joi.object({
    categoryId: Joi.string().required().label('categoryId'),
    businessName: Joi.string().required().label('businessName'),
    storeUrl: Joi.string().required().label('storeUrl'),
   //colorName: Joi.string().required().label('colorName'),
    colorCode: Joi.string().required().label('colorCode'),
    packageId: Joi.string().required().label('packageId'),
    addressId: Joi.string().required().label('addressId'),
  });
  return Schema.validate(data);
};

const validateSellerCourierComapny = (data) => {
  const Schema = Joi.object({
    courierCompanyId: Joi.array().label('courierCompanyId'),
    courierPartnerCode: Joi.string().label('courierPartnerCode'),
    courierPartnerMobile: Joi.string().label('courierPartnerMobile'),
    driver: Joi.object().keys({
      image: Joi.string().required(),
      name: Joi.string().required(),
      code: Joi.string().required(),
      mobile: Joi.string().required(),
    }),
  });
  return Schema.validate(data);
};

const validateSellerDriver = (data) => {
  const Schema = Joi.object({
    id: Joi.string().label('id'),
    name: Joi.string().required().label('name'),
    code: Joi.string().required().label('code'),
    mobile: Joi.string().required().label('mobile'),
    image: Joi.string(),
    deliveryFee: Joi.array().items(
      Joi.object().keys({
        from: Joi.number().required(),
        upto: Joi.number().integer().positive().required().label('upto'),
        rate: Joi.number().required().label('rate'),
      })
    ), 
  });
  return Schema.validate(data);
};

const validateEditSellerDriver = (data) => {
  const Schema = Joi.object({
    id: Joi.string().required().label('id'),
    name: Joi.string().required().label('name'),
    code: Joi.string().required().label('code'),
    mobile: Joi.string().required().label('mobile'),
    image: Joi.string(),
    deliveryFee: Joi.array().items(
      Joi.object().keys({
        from: Joi.number().required(),
        upto: Joi.number().integer().positive().required().label('upto'),
        rate: Joi.number().required().label('rate'),
        _id:Joi.string().required().label('id'),
      })
    ), 
  });
  return Schema.validate(data);
};


const validateSetDefaultDeliveryMethod = (data) => {
  const Schema = Joi.object({
    id: Joi.string().required().label('id'),
    id: Joi.objectId()
    .required()
    .label("id")
    .messages({ "string.pattern.name": "Invalid id." }),
  });
  return Schema.validate(data);
};

const validateSellerProfileUpdate = (data) => {
  const Schema = Joi.object({
    logo:Joi.string().label('logo'),
    firstName:Joi.string().label('firstName'),
    lastName:Joi.string().label('lastName'),
    email:Joi.string().label('email'),
    code: Joi.string().label('code'),
    mobile: Joi.string().label('mobile'),
    businessName: Joi.string().label('businessName'),
    storeUrl: Joi.string().label('storeUrl'),
    
  });
  return Schema.validate(data);
};
module.exports = {
  validateSellerSignup,
  validateSellerCourierComapny,
  validateSellerSellingType,
  validateSellerBusinessDetails,
  validateSellerPackageDetails,
  validateSellerDriver,
  validateEditSellerDriver,
  validateSetDefaultDeliveryMethod,
  validateSellerProfileUpdate
};
