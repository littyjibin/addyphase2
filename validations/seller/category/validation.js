const Joi = require('joi');

const validateSellerCategory = (data) => {
  const Schema = Joi.object({
    categoryTitle: Joi.string().required().label('categoryTitle'),
    description: Joi.string().required().label('description'),
  });
  return Schema.validate(data);
};

module.exports = { validateSellerCategory };
