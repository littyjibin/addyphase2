const Joi = require('joi');

const validateSellerProductCategory = (data) => {
  const Schema = Joi.object({
    productCategory: Joi.string().required().label('productCategory')
  });
  return Schema.validate(data);
};

const validateSellerProductCategoryId = (data) => {
  const Schema = Joi.object({
    id: Joi.objectId().required().label('id').messages({ 'string.pattern.name': 'Invalid id.' }),
  });
  return Schema.validate(data);
};

module.exports = { validateSellerProductCategory,validateSellerProductCategoryId };
