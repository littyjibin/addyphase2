const Joi = require('joi');

const validateSellerColor = (data) => {
  const Schema = Joi.object({
    colorCode: Joi.string().required().label('colorCode'),
    colorName: Joi.string().required().label('colorName'),
  });
  return Schema.validate(data);
};
const validateSellerColorById = (data) => {
  const Schema = Joi.object({
    id: Joi.objectId().required().label('id').messages({ 'string.pattern.name': 'Invalid id.' }),
  });
  return Schema.validate(data);
};
const validateUpdateSellerColorById = (data) => {
  const Schema = Joi.object({
    id: Joi.objectId().required().label('id').messages({ 'string.pattern.name': 'Invalid id.' }),
    colorCode: Joi.string().label('colorCode'),
    colorName: Joi.string().label('colorName'),    
  });
  return Schema.validate(data);
};

module.exports = {
  validateSellerColor,
  validateSellerColorById,
  validateUpdateSellerColorById,
};
