const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

const validateGetOrder = (data) => {
  const Schema = Joi.object({
    status: Joi.string().required().valid('pending', 'accepted', 'shipped','rejected').label('status'),
  });
  return Schema.validate(data);
};

const validateGetOrderById = (data) => {
  const Schema = Joi.object({
    id: Joi.objectId().required().label('id').messages({ 'string.pattern.name': 'Invalid id.' }),
  });
  return Schema.validate(data);
};

const validateOrderStatusUpdate = (data) => {
  const Schema = Joi.object({
    id: Joi.objectId().required().label('id').messages({ 'string.pattern.name': 'Invalid id.' }),
    status: Joi.string().required().valid('accepted', 'rejected', 'assigned', 'shipped', 'startPickup').label('status'),
    // isOnline: Joi.boolean().required().label('isOnline'),
  });
  return Schema.validate(data);
};

const validateConfirmOrder = (data) => {
  const Schema = Joi.object({
    id: Joi.objectId().required().label('id').messages({ 'string.pattern.name': 'Invalid id.' }),
    isSellerChangedPaymentType: Joi.boolean().label('isSellerChangedPaymentType'),
    deliveryMethodId: Joi.objectId()
      .required()
      .label('deliveryMethodId')
      .messages({ 'string.pattern.name': 'Invalid delivery method id.' }),
    packedProductImages: Joi.array().label('packedProductImages'),
    pickupType: Joi.string().required().valid('immediatePickup', 'schedulePickup').label('pickupType'),
    expectedPickupDate: Joi.string().label('expectedPickupDate'),
    expectedPickupTime: Joi.object()
        .keys({
          pickupTimeId: Joi.objectId().required().label('pickupTimeId'),
          fromTime: Joi.string().required().label('fromTime'),
          toTime: Joi.string().required().label('toTime'),
        })
        .label('pickupTime'),
  });
  return Schema.validate(data);
};
const validateReschedulePickupDate = (data) => {
  const Schema = Joi.object({
    id: Joi.objectId().required().label('id').messages({ 'string.pattern.name': 'Invalid id.' }),
    expectedPickupDate: Joi.string().required().label('expectedPickupDate'),
    pickupType: Joi.string().required().label('pickupType'),
    expectedPickupTime: Joi.object()
        .keys({
          pickupTimeId: Joi.objectId().required().label('pickupTimeId'),
          fromTime: Joi.string().required().label('fromTime'),
          toTime: Joi.string().required().label('toTime'),
        })
        .label('pickupTime')
  });
  return Schema.validate(data);
};


const validateReturnOrderStatusUpdate = (data) => {
  const Schema = Joi.object({
    id: Joi.objectId().required().label('id').messages({ 'string.pattern.name': 'Invalid id.' }),
    status: Joi.string().required().valid('returnRejected').label('status')
  });
  return Schema.validate(data);
};

module.exports = {
  validateGetOrder,
  validateGetOrderById,
  validateOrderStatusUpdate,
  validateConfirmOrder,
  validateReschedulePickupDate,
  validateReturnOrderStatusUpdate
};
