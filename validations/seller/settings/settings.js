const Joi = require('joi');

const validateSellerNotificationSettings = (data) => {
    const Schema = Joi.object({
        callForeOutForDelivery: Joi.boolean().required().label('callForeOutForDelivery'),
        callForMarkForDelivery: Joi.boolean().required().label('callForMarkForDelivery'),
        pushNotifyForeOutForDelivery: Joi.boolean().required().label('pushNotifyForeOutForDelivery'),
        pushNotifyForMarkForDelivery: Joi.boolean().required().label('pushNotifyForMarkForDelivery'),
        whatsappNotifyForeOutForDelivery: Joi.boolean().required().label('whatsappNotifyForeOutForDelivery'),
        whatsappNotifyForMarkForDelivery: Joi.boolean().required().label('whatsappNotifyForMarkForDelivery'),
        notificationId: Joi.string().label('notificationId'),
        sellerId: Joi.string().label('sellerId')
    });
    return Schema.validate(data);
};

const validateSettings = (data) => {
    const Schema = Joi.object({
        sellerId: Joi.string().label('sellerId')
    });
    return Schema.validate(data);
};

const validateSellerPrivacySettings = (data) => {
    const Schema = Joi.object({
        displayMobileNumber: Joi.boolean().required().label('displayMobileNumber'),
        displayWhatsappNumber: Joi.boolean().required().label('displayWhatsappNumber'),
        privacyId: Joi.string().label('privacyId'),
        sellerId: Joi.string().label('sellerId')
    });
    return Schema.validate(data);
};

const validateSellerBusinessDetailsSettings = (data) => {
    const Schema = Joi.object({
        displayComRegNumber: Joi.boolean().required().label('displayComRegNumber'),
        displayTaxNumber: Joi.boolean().required().label('displayTaxNumber'),
        businessId: Joi.string().label('businessId'),
        sellerId: Joi.string().label('sellerId')
    });
    return Schema.validate(data);
};

const validateSellerTaxNumber = (data) => {
    const Schema = Joi.object({
        taxNumber: Joi.number().required().label('taxNumber')
    });
    return Schema.validate(data);
};


const validateSellerRegNumber = (data) => {
    const Schema = Joi.object({
        commercialRegNum: Joi.number().required().label('commercialRegNum')
    });
    return Schema.validate(data);
};

const validateSellerReturnDays = (data) => {
    const Schema = Joi.object({
        returnPolicyDays: Joi.number().required().label('returnPolicyDays')
    });
    return Schema.validate(data);
};

const validateSellerBankDetails = (data) => {
    const Schema = Joi.object({
        bankName: Joi.string().required().label('bankName'),
        accountNumber: Joi.string().required().label('accountNumber'),
        IBAN: Joi.string().required().label('IBAN'),
        bankAddress: Joi.string().required().label('bankAddress'),
    });
    return Schema.validate(data);
};
const validateSellerVat = (data) => {
    const Schema = Joi.object({
        vat: Joi.number().required().label('vat')
    });
    return Schema.validate(data);
};


const validateEditBankDetails = (data) => {
    const Schema = Joi.object({
        bankName: Joi.string().label('bankName'),
        accountNumber: Joi.string().label('accountNumber'),
        IBAN: Joi.string().label('IBAN'),
        bankAddress: Joi.string().label('bankAddress'),
        id: Joi.string().required().label('id'),
    });
    return Schema.validate(data);
};

const validateSellerPaymentSettings = (data) => {
    const Schema = Joi.object({
        cashOnDelivery: Joi.boolean().required().label('cashOnDelivery'),
        onlinePayment: Joi.boolean().required().label('onlinePayment'),
        priceIncludeVat: Joi.boolean().required().label('priceIncludeVat'),
        enableVat: Joi.boolean().required().label('enableVat'),
        priceIncludeDeliveryFee: Joi.boolean().required().label('priceIncludeDeliveryFee'),
        paymentId: Joi.string().label('paymentId')
    });
    return Schema.validate(data);
};

module.exports = { 
    validateSellerNotificationSettings, 
    validateSettings, 
    validateSellerPrivacySettings,
    validateSellerBusinessDetailsSettings,
    validateSellerTaxNumber,
    validateSellerRegNumber,
    validateSellerReturnDays,
    validateSellerBankDetails,
    validateSellerVat,
    validateEditBankDetails,
    validateSellerPaymentSettings };
