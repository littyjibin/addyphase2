const Joi = require('joi');

const validateSellerPackage = (data) => {
    const Schema = Joi.object({
        packageName: Joi.string().required().label('packageName'),
        packageFor: Joi.string().required().label('packageFor'),
        country: Joi.string().required().label('country'),
        countryCode: Joi.string().required().label('countryCode'),
        ordersCount: Joi.number().required().label('ordersCount'),
        price: Joi.number().required().label('price'),
        description: Joi.string().required().label('description'),
        features: Joi.object().required().label('features'),
       packageDuration: Joi.string().required().label('packageDuration'),
        startDate: Joi.string().required().label('startDate'),
        endDate: Joi.string().required().label('endDate'),
        termsAndConditions: Joi.string().required().label('termsAndConditions'),
    });
    return Schema.validate(data);
};

module.exports = { validateSellerPackage };
